<?php

$databases['default']['default'] = array(
  'driver' => 'mysql',
  'host' => 'db',
  'database' => 'stevens_edu',
  'username' => 'stevens_edu',
  'password' => 'password',
);

$conf['memcache_servers'] = array(
  'memcache:11211' => 'default',
);

$conf['file_temporary_path'] = '/tmp';
$conf['file_private_path'] = '/var/www/html/docroot/sites/stevens_edu/files/private';
$conf['file_public_path'] = 'sites/stevens_edu/files';

