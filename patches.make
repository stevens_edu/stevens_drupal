; Specify the version of Drupal being used.
core = 7.x

; Specify the api version of Drush Make.
api = 2

; N/A
; Patching README.txt file.
projects[term_permissions][patch][] = 
"https://www.drupal.org/files/issues/term_permissions-user-maxlength-2561491-0.patch"
