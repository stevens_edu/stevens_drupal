<?php

/**
 * @file
 * stevens_ann_mailer.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function stevens_ann_mailer_default_rules_configuration() {
  $items = array();
  $items['rules_send_flagged_node'] = entity_import('rules_config', '{ "rules_send_flagged_node" : {
      "LABEL" : "Send Flagged Node",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "flag" ],
      "ON" : {
        "flag_flagged_mail_sent" : [],
        "flag_flagged_memo_with_workflow_sent" : []
      },
      "DO" : [
        { "component_rules_send_node_mail" : {
            "send_node_mail_node" : [ "flagged-node" ],
            "send_node_mail_to" : "[flagged-node:field_mailing_list]",
            "send_node_mail_cc" : "[flagged-node:field-cc]",
            "send_node_mail_bcc" : "[flagged-node:field-bcc]",
            "send_node_mail_subject" : "[flagged-node:title]",
            "send_node_mail_header_from" : "Stevens Announcements",
            "send_node_mail_header_from_email" : "DoNotReply@stevens.edu"
          }
        }
      ]
    }
  }');
  $items['rules_send_node_mail'] = entity_import('rules_config', '{ "rules_send_node_mail" : {
      "LABEL" : "Send Node Mail",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "mimemail" ],
      "USES VARIABLES" : {
        "send_node_mail_node" : { "label" : "Node", "type" : "node" },
        "send_node_mail_to" : { "label" : "To", "type" : "text" },
        "send_node_mail_cc" : { "label" : "CC Recipient", "type" : "text" },
        "send_node_mail_bcc" : { "label" : "BCC Recipient", "type" : "text" },
        "send_node_mail_subject" : { "label" : "Subject", "type" : "text" },
        "send_node_mail_header_from" : { "label" : "From Header", "type" : "text" },
        "send_node_mail_header_from_email" : { "label" : "From Email Header", "type" : "text" }
      },
      "DO" : [
        { "mimemail" : {
            "USING" : {
              "key" : "renderednode",
              "to" : [ "send-node-mail-to" ],
              "cc" : [ "send-node-mail-cc" ],
              "bcc" : [ "send-node-mail-bcc" ],
              "from_name" : [ "send-node-mail-header-from" ],
              "from_mail" : [ "send-node-mail-header-from-email" ],
              "subject" : [ "send-node-mail-subject" ],
              "body" : [ "send-node-mail-node:nid" ],
              "language" : [ "" ]
            },
            "PROVIDE" : { "send_status" : { "send_status" : "Send status" } }
          }
        }
      ]
    }
  }');
  return $items;
}
