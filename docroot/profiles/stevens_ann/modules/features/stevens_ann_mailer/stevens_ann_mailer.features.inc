<?php

/**
 * @file
 * stevens_ann_mailer.features.inc
 */

/**
 * Implements hook_default_Workflow().
 */
function stevens_ann_mailer_default_Workflow() {
  $workflows = array();

  // Exported workflow: 'memo_workflow'
  $workflows['memo_workflow'] = entity_import('Workflow', '{
    "name" : "memo_workflow",
    "tab_roles" : [],
    "options" : [],
    "states" : {
      "(creation)" : {"weight":"-50","sysid":"1","state":"(creation)","status":"1","name":"(creation)"},
      "draft" : {"weight":"-19","sysid":"0","state":"Draft","status":"1","name":"draft"},
      "queued_for_approval" : {"weight":"-18","sysid":"0","state":"Queued for approval","status":"1","name":"queued_for_approval"},
      "sent" : {"weight":"-17","sysid":"0","state":"Sent","status":"1","name":"sent"}
    },
    "transitions" : {
      "_creation_to_draft" : {"roles":{"-1":-1},"name":"_creation_to_draft","label":"","rdf_mapping":[],"start_state":"(creation)","end_state":"draft"},
      "draft_to_draft" : {"roles":{"-1":-1,"21":21,"26":26},"name":"draft_to_draft","label":"","rdf_mapping":[],"start_state":"draft","end_state":"draft"},
      "draft_to_queued_for_approval" : {"roles":{"-1":-1},"name":"draft_to_queued_for_approval","label":"","rdf_mapping":[],"start_state":"draft","end_state":"queued_for_approval"},
      "queued_for_approval_to_draft" : {"roles":{"26":26},"name":"queued_for_approval_to_draft","label":"","rdf_mapping":[],"start_state":"queued_for_approval","end_state":"draft"},
      "queued_for_approval_to_queued_for_approval" : {"roles":{"-1":-1,"21":21,"26":26},"name":"queued_for_approval_to_queued_for_approval","label":"","rdf_mapping":[],"start_state":"queued_for_approval","end_state":"queued_for_approval"},
      "sent_to_sent" : {"roles":{"-1":-1,"21":21,"26":26},"name":"sent_to_sent","label":"","rdf_mapping":[],"start_state":"sent","end_state":"sent"}
    },
    "label" : "Memo workflow",
    "typeMap" : [],
    "rdf_mapping" : [],
    "system_roles" : { "-1" : "(author)", "21" : "mww author", "26" : "mww sender" }
  }');

  return $workflows;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function stevens_ann_mailer_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function stevens_ann_mailer_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function stevens_ann_mailer_flag_default_flags() {
  $flags = array();
  // Exported flag: "Mail Sent".
  $flags['mail_sent'] = array(
    'entity_type' => 'node',
    'title' => 'Mail Sent',
    'global' => 1,
    'types' => array(
      0 => 'announcement',
    ),
    'flag_short' => 'Send now',
    'flag_long' => '',
    'flag_message' => 'The announcement will be sent.',
    'unflag_short' => 'Unsend',
    'unflag_long' => '',
    'unflag_message' => 'Unsending is not possible, but we\'ll pretend.',
    'unflag_denied_text' => 'Sent',
    'link_type' => 'confirm',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 0,
      'rss' => 0,
      'search_index' => 0,
      'search_result' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'flag_confirmation' => 'Are you sure you want to send this announcement? Sending cannot be undone.',
    'unflag_confirmation' => 'Sorry, unsending the announcement is not possible. Are you sure you want to pretend it was not sent?',
    'module' => 'stevens_ann_mailer',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  // Exported flag: "Memo with Workflow Sent".
  $flags['memo_with_workflow_sent'] = array(
    'entity_type' => 'node',
    'title' => 'Memo with Workflow Sent',
    'global' => 1,
    'types' => array(
      0 => 'memo_with_workflow',
    ),
    'flag_short' => 'Send now',
    'flag_long' => '',
    'flag_message' => 'The announcement will be sent.',
    'unflag_short' => 'Unsend',
    'unflag_long' => '',
    'unflag_message' => 'Unsending is not possible, but we\'ll pretend.',
    'unflag_denied_text' => 'Sent',
    'link_type' => 'confirm',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 0,
      'rss' => 0,
      'search_index' => 0,
      'search_result' => 0,
      'diff_standard' => 0,
      'token' => 0,
      'workflow_tab' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'flag_confirmation' => 'Are you sure you want to send this announcement? Sending cannot be undone.',
    'unflag_confirmation' => 'Sorry, unsending the announcement is not possible. Are you sure you want to pretend it was not sent?',
    'module' => 'stevens_ann_mailer',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  return $flags;

}

/**
 * Implements hook_node_info().
 */
function stevens_ann_mailer_node_info() {
  $items = array(
    'announcement' => array(
      'name' => t('Memo'),
      'base' => 'node_content',
      'description' => t('Create a memo-style announcement.'),
      'has_title' => '1',
      'title_label' => t('Subject'),
      'help' => t('To send a message to the Stevens community, complete the fields below. Once finished, click "Save & Preview" to see a preview of your message. All drafts are saved once you have previewed your announcement. You can further edit or send the announcement immediately, or choose to log in at a later date to complete the process.'),
    ),
    'memo_with_workflow' => array(
      'name' => t('Memo with workflow'),
      'base' => 'node_content',
      'description' => t('Create a memo-style announcement. Sending requires approval.'),
      'has_title' => '1',
      'title_label' => t('Subject'),
      'help' => t('To send a message to the Stevens community, complete the fields below. Once finished, click "Save & Preview" to see a preview of your message. All drafts are saved once you have previewed your announcement. You can further edit or send the announcement immediately, or choose to log in at a later date to complete the process.'),
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
