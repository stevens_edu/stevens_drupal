<?php

/**
 * @file
 * stevens_ann_mailer.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function stevens_ann_mailer_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_additional_fields|node|announcement|default';
  $field_group->group_name = 'group_additional_fields';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'announcement';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Additional Fields',
    'weight' => '4',
    'children' => array(
      0 => 'field_bcc',
      1 => 'field_cc',
    ),
    'format_type' => 'hidden',
    'format_settings' => array(
      'label' => 'Additional Fields',
      'instance_settings' => array(
        'classes' => 'group-additional-fields field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_additional_fields|node|announcement|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_additional_fields|node|announcement|form';
  $field_group->group_name = 'group_additional_fields';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'announcement';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_recipients';
  $field_group->data = array(
    'label' => 'CC and BCC (optional)',
    'weight' => '4',
    'children' => array(
      0 => 'field_bcc',
      1 => 'field_cc',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'CC and BCC (optional)',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => 'group-additional-fields field-group-fieldset',
        'description' => 'Individual Stevens email addresses only.',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_additional_fields|node|announcement|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_additional_fields|node|memo_with_workflow|form';
  $field_group->group_name = 'group_additional_fields';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'memo_with_workflow';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_recipients';
  $field_group->data = array(
    'label' => 'CC and BCC (optional)',
    'weight' => '4',
    'children' => array(
      0 => 'field_bcc',
      1 => 'field_cc',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'CC and BCC (optional)',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => 'group-additional-fields field-group-fieldset',
        'description' => 'Individual Stevens email addresses only.',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_additional_fields|node|memo_with_workflow|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_inline_headers|node|announcement|default';
  $field_group->group_name = 'group_inline_headers';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'announcement';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Inline Headers',
    'weight' => '0',
    'children' => array(
      0 => 'field_inline_to',
      1 => 'field_inline_from',
    ),
    'format_type' => 'html-element',
    'format_settings' => array(
      'label' => 'Inline Headers',
      'instance_settings' => array(
        'classes' => 'testing-stuff',
        'element' => 'table',
        'show_label' => '0',
        'label_element' => 'div',
        'attributes' => '',
      ),
    ),
  );
  $field_groups['group_inline_headers|node|announcement|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_message|node|announcement|form';
  $field_group->group_name = 'group_message';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'announcement';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Message',
    'weight' => '2',
    'children' => array(
      0 => 'body',
      1 => 'field_inline_from',
      2 => 'field_inline_to',
      3 => 'title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Message',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => '',
        'description' => 'Your entry in the fields below will show in the contents of the message you send. The subject field will populate both the email subject line and the subject field in the memo header of your message.',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_message|node|announcement|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_message|node|memo_with_workflow|form';
  $field_group->group_name = 'group_message';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'memo_with_workflow';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Message',
    'weight' => '2',
    'children' => array(
      0 => 'body',
      1 => 'field_inline_from',
      2 => 'field_inline_to',
      3 => 'title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Message',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => '',
        'description' => 'Your entry in the fields below will show in the contents of the message you send. The subject field will populate both the email subject line and the subject field in the memo header of your message.',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_message|node|memo_with_workflow|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_recipients|node|announcement|form';
  $field_group->group_name = 'group_recipients';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'announcement';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Envelope',
    'weight' => '0',
    'children' => array(
      0 => 'field_mailing_list',
      1 => 'field_header_from',
      2 => 'field_header_from_email',
      3 => 'group_additional_fields',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Envelope',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => '',
        'description' => 'Select the distribution list(s) that reflect the audiences that should receive your announcement. If desired, expand and complete the CC and BCC fields to send to specific email addresses.',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_recipients|node|announcement|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_recipients|node|memo_with_workflow|form';
  $field_group->group_name = 'group_recipients';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'memo_with_workflow';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Envelope',
    'weight' => '0',
    'children' => array(
      0 => 'field_mailing_list',
      1 => 'field_header_from_email',
      2 => 'field_header_from',
      3 => 'group_additional_fields',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Envelope',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => '',
        'description' => 'Select the distribution list(s) that reflect the audiences that should receive your announcement. If desired, expand and complete the CC and BCC fields to send to specific email addresses.',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_recipients|node|memo_with_workflow|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Additional Fields');
  t('CC and BCC (optional)');
  t('Envelope');
  t('Inline Headers');
  t('Message');

  return $field_groups;
}
