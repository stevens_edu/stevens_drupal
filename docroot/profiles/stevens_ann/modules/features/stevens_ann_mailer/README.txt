Code for the Announcements Mailer feature.

Responsible for the composition, previewing, testing and sending of mass mail
messages to campus listservs.

--------------------------------

WARNING:

This feature uses role ID (rid) numbers that are specific to the Ann instance
at https://ann.stevens.edu. Any other use of this feature will require taking a
careful look at the site's rid values and modifying this feature accordingly.

Living list of areas within this feature that contain rid values:
- stevens_ann_mailer.views_default.inc (views: All Sent Mail Page)

--------------------------------
