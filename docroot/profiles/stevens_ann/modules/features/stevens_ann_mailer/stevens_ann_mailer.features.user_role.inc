<?php

/**
 * @file
 * stevens_ann_mailer.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function stevens_ann_mailer_user_default_roles() {
  $roles = array();

  // Exported role: delegator.
  $roles['delegator'] = array(
    'name' => 'delegator',
    'weight' => 6,
  );

  // Exported role: sender.
  $roles['sender'] = array(
    'name' => 'sender',
    'weight' => 5,
  );

  return $roles;
}
