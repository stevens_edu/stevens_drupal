<?php

/**
 * @file
 * stevens_ann_mailer.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function stevens_ann_mailer_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_create-announcement:node/add.
  $menu_links['main-menu_create-announcement:node/add'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/add',
    'router_path' => 'node/add',
    'link_title' => 'Create Announcement',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_create-announcement:node/add',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: main-menu_log-out:user/logout.
  $menu_links['main-menu_log-out:user/logout'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'user/logout',
    'router_path' => 'user/logout',
    'link_title' => 'Log Out',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_log-out:user/logout',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 40,
    'customized' => 1,
  );
  // Exported menu link: main-menu_manage-lists:admin/structure/taxonomy/mailing_lists.
  $menu_links['main-menu_manage-lists:admin/structure/taxonomy/mailing_lists'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'admin/structure/taxonomy/mailing_lists',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Manage Lists',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_manage-lists:admin/structure/taxonomy/mailing_lists',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -10,
    'customized' => 1,
  );
  // Exported menu link: main-menu_manage-users:admin/people.
  $menu_links['main-menu_manage-users:admin/people'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'admin/people',
    'router_path' => 'admin/people',
    'link_title' => 'Manage Users',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_manage-users:admin/people',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Create Announcement');
  t('Log Out');
  t('Manage Lists');
  t('Manage Users');

  return $menu_links;
}
