<?php

/**
 * @file
 * stevens_ann_mailer.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function stevens_ann_mailer_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access administration pages'.
  $permissions['access administration pages'] = array(
    'name' => 'access administration pages',
    'roles' => array(),
    'module' => 'system',
  );

  // Exported permission: 'access content'.
  $permissions['access content'] = array(
    'name' => 'access content',
    'roles' => array(
      'mww author' => 'mww author',
      'mww sender' => 'mww sender',
      'sender' => 'sender',
    ),
    'module' => 'node',
  );

  // Exported permission: 'access content overview'.
  $permissions['access content overview'] = array(
    'name' => 'access content overview',
    'roles' => array(
      'sender' => 'sender',
    ),
    'module' => 'node',
  );

  // Exported permission: 'access navbar'.
  $permissions['access navbar'] = array(
    'name' => 'access navbar',
    'roles' => array(),
    'module' => 'navbar',
  );

  // Exported permission: 'access user profiles'.
  $permissions['access user profiles'] = array(
    'name' => 'access user profiles',
    'roles' => array(
      'delegator' => 'delegator',
    ),
    'module' => 'user',
  );

  // Exported permission: 'access workbench'.
  $permissions['access workbench'] = array(
    'name' => 'access workbench',
    'roles' => array(
      'mww author' => 'mww author',
      'mww sender' => 'mww sender',
      'sender' => 'sender',
    ),
    'module' => 'workbench',
  );

  // Exported permission: 'administer mailing_lists vocabulary terms'.
  $permissions['administer mailing_lists vocabulary terms'] = array(
    'name' => 'administer mailing_lists vocabulary terms',
    'roles' => array(
      'delegator' => 'delegator',
    ),
    'module' => 'vppr',
  );

  // Exported permission: 'administer users'.
  $permissions['administer users'] = array(
    'name' => 'administer users',
    'roles' => array(
      'delegator' => 'delegator',
    ),
    'module' => 'user',
  );

  // Exported permission: 'create announcement content'.
  $permissions['create announcement content'] = array(
    'name' => 'create announcement content',
    'roles' => array(
      'sender' => 'sender',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any announcement content'.
  $permissions['delete any announcement content'] = array(
    'name' => 'delete any announcement content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own announcement content'.
  $permissions['delete own announcement content'] = array(
    'name' => 'delete own announcement content',
    'roles' => array(
      'sender' => 'sender',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any announcement content'.
  $permissions['edit any announcement content'] = array(
    'name' => 'edit any announcement content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own announcement content'.
  $permissions['edit own announcement content'] = array(
    'name' => 'edit own announcement content',
    'roles' => array(
      'sender' => 'sender',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in mailing_lists'.
  $permissions['edit terms in mailing_lists'] = array(
    'name' => 'edit terms in mailing_lists',
    'roles' => array(
      'delegator' => 'delegator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'flag mail_sent'.
  $permissions['flag mail_sent'] = array(
    'name' => 'flag mail_sent',
    'roles' => array(
      'sender' => 'sender',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'search content'.
  $permissions['search content'] = array(
    'name' => 'search content',
    'roles' => array(
      'sender' => 'sender',
    ),
    'module' => 'search',
  );

  // Exported permission: 'use advanced search'.
  $permissions['use advanced search'] = array(
    'name' => 'use advanced search',
    'roles' => array(
      'sender' => 'sender',
    ),
    'module' => 'search',
  );

  // Exported permission: 'view own unpublished content'.
  $permissions['view own unpublished content'] = array(
    'name' => 'view own unpublished content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
      'sender' => 'sender',
    ),
    'module' => 'node',
  );

  // Exported permission: 'view revisions'.
  $permissions['view revisions'] = array(
    'name' => 'view revisions',
    'roles' => array(
      'mww author' => 'mww author',
      'sender' => 'sender',
    ),
    'module' => 'node',
  );

  // Exported permission: 'view the administration theme'.
  $permissions['view the administration theme'] = array(
    'name' => 'view the administration theme',
    'roles' => array(),
    'module' => 'system',
  );

  return $permissions;
}
