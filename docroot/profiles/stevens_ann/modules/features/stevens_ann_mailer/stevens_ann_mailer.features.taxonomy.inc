<?php

/**
 * @file
 * stevens_ann_mailer.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function stevens_ann_mailer_taxonomy_default_vocabularies() {
  return array(
    'mailing_lists' => array(
      'name' => 'Distribution Lists',
      'machine_name' => 'mailing_lists',
      'description' => 'The set of listserv distribution lists available at Stevens.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
