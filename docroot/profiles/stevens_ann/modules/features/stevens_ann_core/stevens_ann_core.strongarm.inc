<?php
/**
 * @file
 * stevens_ann_core.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function stevens_ann_core_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_user__user';
  $strongarm->value = array(
    'view_modes' => array(
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'diff_standard' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(),
      'display' => array(
        'mimemail' => array(
          'default' => array(
            'weight' => '0',
            'visible' => FALSE,
          ),
        ),
        'summary' => array(
          'default' => array(
            'weight' => '5',
            'visible' => TRUE,
          ),
        ),
      ),
    ),
  );
  $export['field_bundle_settings_user__user'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'nodeaccess-grants';
  $strongarm->value = array(
    'view' => 0,
    'edit' => 0,
    'delete' => 0,
  );
  $export['nodeaccess-grants'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'nodeaccess-priority';
  $strongarm->value = 0;
  $export['nodeaccess-priority'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'nodeaccess-roles';
  $strongarm->value = array(
    1 => 0,
    2 => 0,
    11 => 0,
    16 => 0,
  );
  $export['nodeaccess-roles'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'nodeaccess_announcement';
  $strongarm->value = array(
    0 => array(
      'gid' => 1,
      'realm' => 'nodeaccess_rid',
      'grant_view' => 0,
      'grant_update' => 0,
      'grant_delete' => 0,
    ),
    1 => array(
      'gid' => 2,
      'realm' => 'nodeaccess_rid',
      'grant_view' => 0,
      'grant_update' => 0,
      'grant_delete' => 0,
    ),
    2 => array(
      'gid' => 11,
      'realm' => 'nodeaccess_rid',
      'grant_view' => 0,
      'grant_update' => 0,
      'grant_delete' => 0,
    ),
    3 => array(
      'gid' => 16,
      'realm' => 'nodeaccess_rid',
      'grant_view' => 1,
      'grant_update' => 0,
      'grant_delete' => 0,
    ),
  );
  $export['nodeaccess_announcement'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_frontpage';
  $strongarm->value = 'node';
  $export['site_frontpage'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_mail';
  $strongarm->value = 'itadmin@stevens.edu';
  $export['site_mail'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_name';
  $strongarm->value = 'Stevens Announcements';
  $export['site_name'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_slogan';
  $strongarm->value = 'Send announcements to the campus community';
  $export['site_slogan'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'theme_shet_settings';
  $strongarm->value = array(
    'toggle_logo' => 1,
    'toggle_name' => 1,
    'toggle_slogan' => 1,
    'toggle_node_user_picture' => 1,
    'toggle_comment_user_picture' => 1,
    'toggle_comment_user_verification' => 1,
    'toggle_favicon' => 1,
    'toggle_main_menu' => 1,
    'toggle_secondary_menu' => 1,
    'default_logo' => 1,
    'logo_path' => '',
    'logo_upload' => '',
    'default_favicon' => 1,
    'favicon_path' => '',
    'favicon_upload' => '',
    'breadcrumb_display' => 0,
    'breadcrumb_separator' => '/',
    'breadcrumb_home' => 0,
    'slideshow_display' => 0,
    'slideshow_js' => 0,
    'slideshow_effect' => 'scrollHorz',
    'slideshow_effect_time' => '10',
    'slideshow_randomize' => 0,
    'slideshow_wrap' => 0,
    'slideshow_pause' => 0,
    'responsive_meta' => 1,
    'responsive_respond' => 1,
    'credits_display' => 0,
    'scheme' => 'default',
    'palette' => array(
      'base' => '#7a7a7a',
      'link' => '#1487d4',
      'headingshadow' => '#eaf1ea',
      'slogan' => '#d5d5d5',
      'headertop' => '#2f2f2f',
      'headerbottom' => '#1b1a1a',
      'headermenu' => '#222222',
      'headermenulink' => '#ffffff',
      'headermenuborder' => '#353535',
      'headermenubghover' => '#1b1b1b',
      'bannertop' => '#efeeee',
      'bannerbottom' => '#dadada',
      'bannerborder' => '#fcfcfc',
      'contenttop' => '#e8e8e8',
      'contentbottom' => '#efefef',
      'blockbg' => '#fdfdfd',
      'buttontop' => '#0093c5',
      'buttonbottom' => '#0079a2',
      'buttontext' => '#fefefe',
      'buttontextshadow' => '#003547',
      'buttonboxshadow' => '#939da2',
      'buttonbghover' => '#008ab8',
      'footer' => '#181818',
      'footerlink' => '#e4e4e4',
      'footerbottomtop' => '#262626',
      'footerbottombottom' => '#1a1a1a',
    ),
    'theme' => 'shet',
    'info' => array(
      'fields' => array(
        'base' => 'Text color',
        'link' => 'Headings/Link',
        'headingshadow' => 'Headings shadow',
        'slogan' => 'Slogan',
        'headertop' => 'Header top',
        'headerbottom' => 'Header bottom',
        'headermenu' => 'Main menu strip',
        'headermenulink' => 'Main menu link',
        'headermenuborder' => 'Main menu/Footer menu border',
        'headermenubghover' => 'Main menu background hover (>=2 level)',
        'bannertop' => 'Banner top',
        'bannerbottom' => 'Banner bottom',
        'bannerborder' => 'Banner border',
        'contenttop' => 'Content top',
        'contentbottom' => 'Content bottom',
        'blockbg' => 'Block background',
        'blockshadow' => 'Block shadow',
        'buttontop' => 'Button top',
        'buttonbottom' => 'Button bottom',
        'buttontext' => 'Button text',
        'buttontextshadow' => 'Button text shadow',
        'buttonboxshadow' => 'Button box shadow',
        'buttonbghover' => 'Button background hover',
        'footer' => 'Footer 1',
        'footerlink' => 'Footer link',
        'footerbottomtop' => 'Footer 2 top',
        'footerbottombottom' => 'Footer 2 bottom',
      ),
      'schemes' => array(
        'default' => array(
          'title' => 'Default',
          'colors' => array(
            'base' => '#7a7a7a',
            'link' => '#1487d4',
            'headingshadow' => '#eaf1ea',
            'slogan' => '#d5d5d5',
            'headertop' => '#2f2f2f',
            'headerbottom' => '#1b1a1a',
            'headermenu' => '#222222',
            'headermenulink' => '#ffffff',
            'headermenuborder' => '#353535',
            'headermenubghover' => '#1b1b1b',
            'bannertop' => '#efeeee',
            'bannerbottom' => '#dadada',
            'bannerborder' => '#fcfcfc',
            'contenttop' => '#e8e8e8',
            'contentbottom' => '#efefef',
            'blockbg' => '#fdfdfd',
            'buttontop' => '#0093c5',
            'buttonbottom' => '#0079a2',
            'buttontext' => '#fefefe',
            'buttontextshadow' => '#003547',
            'buttonboxshadow' => '#939da2',
            'buttonbghover' => '#008ab8',
            'footer' => '#181818',
            'footerlink' => '#e4e4e4',
            'footerbottomtop' => '#262626',
            'footerbottombottom' => '#1a1a1a',
          ),
        ),
        'alignment' => array(
          'title' => 'Alignment',
          'colors' => array(
            'base' => '#7a7a7a',
            'link' => '#de6a00',
            'headingshadow' => '#ffffff',
            'slogan' => '#d5d5d5',
            'headertop' => '#222222',
            'headerbottom' => '#203e42',
            'headermenu' => '#222222',
            'headermenulink' => '#d4fffd',
            'headermenuborder' => '#222222',
            'headermenubghover' => '#203e42',
            'bannertop' => '#efeeee',
            'bannerbottom' => '#dadada',
            'bannerborder' => '#fcfcfc',
            'contenttop' => '#e8e8e8',
            'contentbottom' => '#efefef',
            'blockbg' => '#fdfdfd',
            'buttontop' => '#db9655',
            'buttonbottom' => '#de6a00',
            'buttontext' => '#fefefe',
            'buttontextshadow' => '#222222',
            'buttonboxshadow' => '#999999',
            'buttonbghover' => '#de6a00',
            'footer' => '#203e42',
            'footerlink' => '#d4fffd',
            'footerbottomtop' => '#203e42',
            'footerbottombottom' => '#222222',
          ),
        ),
        '' => array(
          'title' => 'Custom',
          'colors' => array(),
        ),
      ),
      'css' => array(
        0 => 'color/colors.css',
      ),
      'copy' => array(
        0 => 'logo.png',
      ),
      'gradients' => array(
        0 => array(
          'dimension' => array(
            0 => 0,
            1 => 0,
            2 => 0,
            3 => 0,
          ),
          'direction' => 'vertical',
          'colors' => array(
            0 => 'top',
            1 => 'bottom',
          ),
        ),
      ),
      'fill' => array(),
      'slices' => array(),
      'blend_target' => '#ffffff',
      'preview_css' => 'color/preview.css',
      'preview_js' => 'color/preview.js',
      'preview_html' => 'color/preview.html',
      'base_image' => 'color/base.png',
    ),
  );
  $export['theme_shet_settings'] = $strongarm;

  return $export;
}
