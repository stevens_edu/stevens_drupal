<?php
/**
 * @file
 * stevens_ann_core.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function stevens_ann_core_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'assign sender role'.
  $permissions['assign sender role'] = array(
    'name' => 'assign sender role',
    'roles' => array(
      'delegator' => 'delegator',
    ),
    'module' => 'role_delegation',
  );

  return $permissions;
}
