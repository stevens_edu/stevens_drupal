/**
 *  @file
 *  A simple calendar.
 */

/**
 * This will set our initial behavior, by starting up each individual calendar.
 */
 (function($) {
    // Store our function as a property of Drupal.behaviors.
    Drupal.behaviors.stevensEventCalendar = {
        attach: function(context, settings) {
            var classes = settings.stevensCalendar.classes;
            var presets = settings.stevensCalendar.opts;
            $.each(classes,
            function(key, value) {
                if ($('#' + value)[0]) {
                    var settings = presets[value].settings;
                    // console.log(settings);
                    $('#' + value).fullCalendar({
                        events: settings.calendar_source_url,
                    });
                }
            });

            // console.log(settings);
            // alert('oki');
            // $('#calendar').fullCalendar({
                // editable: true,
                // events: "/json-events.php",
                // events: "/news/event-calendar",
                // eventDrop: function(event, delta) {
                // alert(event.title + ' was moved ' + delta + ' days\n' +
                // '(should probably update your database)');
                // },
                // loading: function(bool) {
                // if (bool) $('#loading').show();
                // else $('#loading').hide();
                // }
                //                 events: function(start, end, callback) {
                //                     $.ajax({
                //                         url: 'http://stevens-dc.dev.news/event-calendar/',
                //                         dataType: 'json',
                //                         success: function(doc) {
                //                             // console.log(doc);
                //                             // var obj = $.parseJSON(doc);
                //                             // console.log(obj);							
                //                             var events = [];
                //                             $(doc).each(
                //                             function(index, value) {
                //                                 // console.log(value.title);
                //                                 events.push({
                //                                     title: value.title,
                //                                     start: value.start,
                // end: value.end,
                // url: value.url,
                // id:value.id
                //                                     // will be parsed
                //                                 });
                //                             });
                //                             callback(events);
                //                         }
                //                     });
                //                 }
            // });
        }
    }
} (jQuery));