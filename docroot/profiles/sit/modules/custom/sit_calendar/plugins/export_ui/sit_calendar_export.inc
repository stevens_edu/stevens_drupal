<?php

/**
 * Define this Export UI plugin.
 */
$plugin = array(
  'schema' => 'calendar_settings_presets',
  'access' => 'administer site configuration',
  'menu' => array(
    'menu prefix' => 'admin/config/media',
    'menu item' => 'calendar',
    'menu title' => 'Calendar Settings',
    'menu description' => 'Calendar settings management.',
  ),
  'title singular' => t('calendar'),
  'title plural' => t('calendars'),
  'title singular proper' => t('Calendar settings preset'),
  'title plural proper' => t('Calendar settings presets'),
  'use wizard' => TRUE,
  'form info' => array(
    'add order' => array(
      'basic' => t('Basic settings'),
    ),
    'edit order' => array(
      'basic' => t('Basic preset settings'),
    ),
    'clone order' => array(
      'basic' => t('Basic preset settings'),
    ),
    'forms' => array(
      'basic' => array(
        'form id' => 'sit_calendar_settings_basic',
      ),
      'edit' => array(
        'form id' => 'sit_calendar_settings_edit',
      ),
    ),
  ),
);

/**
 * Define the preset add/edit form. This is basic preset information
 * form callback. It configures machine name, etc, ...
 */
function sit_calendar_settings_basic($form, &$form_state) {
  $new = $form_state['form type'] == 'add';
  $form['#attributes']['class'][] = $new ? 'add-calendar' : 'edit-calendar';

  if (!$new) {
    $form['ctools_trail']['#markup'] = str_replace('»', '', $form['ctools_trail']['#markup']);
    $form['wrapper']['#type'] = 'fieldset';
  }

  $form['wrapper']['buttons'] = $form['buttons'];
  unset($form['buttons']);

  $form['wrapper']['machine_name'] = array(
    '#type' => 'machine_name',
    '#title' => t('Machine name'),
    '#required' => TRUE,
    '#description' => t("Enter calendar's machine name."),
    '#machine_name' => array(
      'exists' => '_calendar_machine_name_check',
    ),
    '#default_value' => isset($form_state['item']->machine_name) ? $form_state['item']->machine_name : '',
    '#disabled' => !$new && $new = $form_state['form type'] != 'clone',
  );

  $form['wrapper']['basic'] = array(
    '#type' => 'fieldset',
    '#title' => t('Basic options'),
    '#collapsible' => TRUE,
  );

  $form['wrapper']['basic']['calendar_source_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Event Calendar Source URL'),
    '#description' => t('Source URL for the JSON source.'),
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => isset($form_state['item']->settings['calendar_source_url']) ? $form_state['item']->settings['calendar_source_url'] : 'http://',
  );
  return $form;
}

function sit_calendar_settings_basic_submit($form, &$form_state) {
  $form_state['item']->machine_name = $form_state['values']['machine_name'];
  $form_state['item']->settings['calendar_source_url'] = $form_state['values']['calendar_source_url'];
  // @todo Clear cached presets.
  cache_clear_all('calendar_settings:export_ui:settings', 'cache');
}

/**
 * Check for existing presetn machine names
 */
function _calendar_machine_name_check($name) {
  $presets = sit_calendar_get_settings_presets();
  return isset($presets[$name]);
}

