<?php

/**
 * @file
 * Implement an drush commands for SIT web scraper.
 */

/**
 * Implementation of hook_drush_command().
 * Create the drush command of importing scraper data
 * Command to execute is 'sit-scraper'
 */
function sit_connector_drush_command() {
  $items = array();
  $items['sit-connector-clear-cache'] = array(
    'description' => 'Clear the all cache of client responses',
    'callback' => 'sit_connector_clear_cache_process',
    'drupal dependencies' => array('sit_connector'),
    'options' => array(
      '--limit' => 'Number of URL depth to scrape',
    ),
  );
  return $items;
}

function sit_connector_clear_cache_process(){
	ctools_include('object-cache');
	$endpoints     = variable_get('sit_connector_client_endpoints', array());
	foreach($endpoints as $endpoint){
		$endpoint_name = $endpoint['endpoint_name'];
      ctools_object_cache_clear_all('sit_json', $endpoint_name);
	}
}