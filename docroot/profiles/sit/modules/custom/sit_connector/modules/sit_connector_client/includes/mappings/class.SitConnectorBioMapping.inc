<?php
class SitConnectorBioMapping extends SitConnectorClientMapping implements SitConnectorClientAggregatorMappingInterface {
  public function __sleep() {
    return parent::__sleep();
  }

  public static function info() {
    return $info = array(
      'name' => t('Bio mapping'),
      'description' => t('Maps bios to nodes'),
      );
  }

  public function __construct($config) {
    parent::__construct($config);
  }

  public function getMappings() {
  	$mappings['title'] = array(
      'destinationField' => 'title',
      'sourceField' => 'cpipe_id',
    );
    $mappings['body'] = array(
      'destinationField' => 'body',
      'sourceField' => 'general_info_Background',
      'arguments' => array('format' => 'full_html'),
    );
    
    $mappings['field_bio_faculty_id'] = array(
      'destinationField' => 'field_bio_faculty_id',
      'sourceField' => 'faculty_id',
    );
    $mappings['field_bio_cpipe_id'] = array(
      'destinationField' => 'field_bio_cpipe_id',
      'sourceField' => 'cpipe_id',
    );
    $mappings['field_bio_initials'] = array(
      'destinationField' => 'field_bio_initials',
      'sourceField' => 'initials',
    );
    $mappings['field_bio_firstname'] = array(
      'destinationField' => 'field_bio_firstname',
      'sourceField' => 'first_name',
    );
    $mappings['field_bio_middlename'] = array(
      'destinationField' => 'field_bio_middlename',
      'sourceField' => 'middle_name',
    );
    $mappings['field_bio_lastname'] = array(
      'destinationField' => 'field_bio_lastname',
      'sourceField' => 'last_name',
    );
    $mappings['field_bio_research_blurb'] = array(
      'destinationField' => 'field_bio_research_blurb',
      'sourceField' => 'research_blurb',
      'arguments' => array('format' => 'full_html'),
    );
    $mappings['field_bio_intro'] = array(
      'destinationField' => 'field_bio_intro',
      'sourceField' => 'intro',
      'arguments' => array('format' => 'full_html'),
    );
    $mappings['field_bio_office_hours'] = array(
      'destinationField' => 'field_bio_office_hours',
      'sourceField' => 'office_hours',
      'arguments' => array('format' => 'full_html'),
    );
    $mappings['field_bio_salutation'] = array(
      'destinationField' => 'field_bio_salutation',
      'sourceField' => 'salutation',
    );
    $mappings['field_bio_title'] = array(
      'destinationField' => 'field_bio_title',
      'sourceField' => 'title',
    );
    
    // Honorary title goes here ("honorary_title")
    
    $mappings['field_bio_email'] = array(
      'destinationField' => 'field_bio_email',
      'sourceField' => 'email',
    );
    $mappings['field_bio_phone'] = array(
      'destinationField' => 'field_bio_phone',
      'sourceField' => 'phone',
    );
    $mappings['field_bio_fax'] = array(
      'destinationField' => 'field_bio_fax',
      'sourceField' => 'fax',
    );
    $mappings['field_bio_building'] = array(
      'destinationField' => 'field_bio_building',
      'sourceField' => 'building',
    );
    $mappings['field_bio_room'] = array(
      'destinationField' => 'field_bio_room',
      'sourceField' => 'room',
    );
    $mappings['field_bio_resume'] = array(
      'destinationField' => 'field_bio_resume',
      'sourceField' => 'resume_link',
    );
    $mappings['field_bio_website'] = array(
      'destinationField' => 'field_bio_website',
      'sourceField' => 'personal_site',
    );
    $mappings['field_bio_experience'] = array(
      'destinationField' => 'field_bio_experience',
      'sourceField' => 'experience',
    );
    $mappings['field_bio_consulting'] = array(
      'destinationField' => 'field_bio_consulting',
      'sourceField' => 'consulting',
    );

    
    $mappings['field_bio_acad_actserv'] = array(
      'destinationField' => 'field_bio_acad_actserv',
      'sourceField' => 'academic_activities_services',
    );
    $mappings['field_bio_pro_actserv'] = array(
      'destinationField' => 'field_bio_pro_actserv',
      'sourceField' => 'professional_activities_services_history',
    );
    $mappings['field_bio_technogenesis'] = array(
      'destinationField' => 'field_bio_technogenesis',
      'sourceField' => 'Technogenesis',
    );
    $mappings['field_bio_honor_degree'] = array(
      'destinationField' => 'field_bio_honor_degree',
      'sourceField' => 'honorary_degree',
    );
    $mappings['field_bio_education'] = array(
      'destinationField' => 'field_bio_education',
      'sourceField' => 'education',
      'arguments' => array('format' => 'full_html'),
    );
    $mappings['field_bio_awards'] = array(
      'destinationField' => 'field_bio_awards',
      'sourceField' => 'awards_honors',
      'arguments' => array('format' => 'full_html'),
    );
    $mappings['field_bio_funding'] = array(
      'destinationField' => 'field_bio_funding',
      'sourceField' => 'grants_contracts_funds',
      'arguments' => array('format' => 'full_html'),
    );
    $mappings['field_bio_patents'] = array(
      'destinationField' => 'field_bio_patents',
      'sourceField' => 'patents_inventions',
      'arguments' => array('format' => 'full_html'),
    );
    $mappings['field_bio_pro_orgs'] = array(
      'destinationField' => 'field_bio_pro_orgs',
      'sourceField' => 'professional_orgs_societies',
      'arguments' => array('format' => 'full_html'),
    );
    $mappings['field_bio_appt_thesis_suprv'] = array(
      'destinationField' => 'field_bio_appt_thesis_suprv',
      'sourceField' => 'appointments_thesis_supervisor',
      'arguments' => array('format' => 'full_html'),
    );
    $mappings['field_bio_research_interests'] = array(
      'destinationField' => 'field_bio_research_interests',
      'sourceField' => 'research_projects_interests',
      'arguments' => array('format' => 'full_html'),
    );
    $mappings['field_bio_mail_id'] = array(
      'destinationField' => 'field_bio_mail_id',
      'sourceField' => 'mail_id',
    );
    $mappings['field_shared_adminunits'] = array(
      'destinationField' => 'field_shared_adminunits',
      'sourceField' => array('adminUnits' => array('id', 'type')),
      'sourceMigration' => TRUE,
      'arguments' => array('source_type' => 'tid'),
    );
    $mappings['field_deleted'] = array(
      'destinationField' => 'field_deleted',
      'sourceField' => 'deleted',
    );
    $mappings['field_bio_status'] = array(
      'destinationField' => 'field_bio_status',
      'sourceField' => 'status',
      'callbacks' => array('sit_connector_client_cb_bool'),
    );
    
    //timestamp
    
    //browser_title
    
    //keywords
        
    $mappings['field_bio_image'] = array(
      'destinationField' => 'field_bio_image',
      'sourceField' => 'image_path',
      'arguments' => array('file_replace' => TRUE, 'file_function' => 'file_copy'),
    );
    $mappings['field_shared_role'] = array(
      'destinationField' => 'field_shared_role',
      'sourceField' => 'friendlyFlag',
      'arguments' => array('source_type' => 'name', 'create_term' => TRUE),
    );
    $mappings['field_courses_ref'] = array(
      'destinationField' => 'field_courses_ref',
      'sourceField' => 'course_ids',
      'sourceMigration' => TRUE,
      'separator' => '|',
    );
    $mappings['field_patents_ref'] = array(
      'destinationField' => 'field_patents_ref',
      'sourceField' => 'patent_ids',
      'sourceMigration' => TRUE,
      'separator' => '|',
    );
    
    return $mappings;
  }
}
