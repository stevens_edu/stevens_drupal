<?php
class SitConnectorCurriculumMapping extends SitConnectorClientMapping implements SitConnectorClientAggregatorMappingInterface {
  public function __sleep() {
    return parent::__sleep();
  }

  public static function info() {
    return $info = array(
      'name' => t('Curriculum mapping'),
      'description' => t('Maps curricula to nodes'),
    );
  }

  public function getMappings() {
    $mappings['title'] = array(
      'destinationField' => 'title',
      'sourceField' => 'curriculum_name',
      'defaultValue' => t('No name'),
    );
    $mappings['field_curriculum_id'] = array(
      'destinationField' => 'field_curriculum_id',
      'sourceField' => 'curriculum_id',
    );
    // Deprecated, since SIT now ensures we always have an admin unit
    //$mappings['field_program_id'] = array(
    //  'destinationField' => 'field_program_id',
    //  'sourceField' => 'program_id',
    //);
    
    // Course references
    $mappings['field_course_ref'] = array(
      'destinationField' => 'field_course_ref',
      'sourceField' => 'course_id',
      'sourceMigration' => TRUE,
    );
    $mappings['field_course_alternate'] = array(
      'destinationField' => 'field_course_alternate',
      'sourceField' => 'alternate_courses',
      'separator' => '|',
      'sourceMigration' => TRUE,
    );
    
    // Instructions
    $mappings['field_prereq_instructions'] = array(
      'destinationField' => 'field_prereq_instructions',
      'sourceField' => 'prerequisite_instructions',
      'arguments' => array('format' => 'full_html'),
    ); 
    $mappings['field_coreq_instructions'] = array(
      'destinationField' => 'field_coreq_instructions',
      'sourceField' => 'corequisite_instructions',
      'arguments' => array('format' => 'full_html'),
    );   
    
    $mappings['field_course_term'] = array(
      'destinationField' => 'field_course_term',
      'sourceField' => 'term',
      'arguments' => array('source_type' => 'name', 'create_term' => TRUE),
    );
    $mappings['field_shared_adminunits'] = array(
      'destinationField' => 'field_shared_adminunits',
      'sourceField' => array('adminUnits' => array('id', 'type')),
      'sourceMigration' => TRUE,
      'arguments' => array('source_type' => 'tid'),
    );
    
    // Misc flags and other mappings
    $mappings['field_is_default'] = array(
      'destinationField' => 'field_is_default',
      'sourceField' => 'isDefault',
    );
    
    $mappings['field_start_date'] = array(
      'destinationField' => 'field_start_date',
      'sourceField' => 'start',
    );
    $mappings['field_end_date'] = array(
      'destinationField' => 'field_end_date',
      'sourceField' => 'end',
    );

    return $mappings;
  }

  public function __construct($config) {
    parent::__construct($config);
  }
}
