<?php
class SitConnectorNewsMapping extends SitConnectorClientMapping implements SitConnectorClientAggregatorMappingInterface {
  public function __sleep() {
    return parent::__sleep();
  }

  public static function info() {
    return $info = array(
      'name' => t('SIT API: News mapping'),
      'description' => t('Maps API news to nodes'),
      );
  }

  public function __construct($config) {
    parent::__construct($config);
  }
  
  public function getMappings() {
    /** 
     * Unmapped destination fields 
     * field_contact
	   * field_phone
	   * field_link
	   * field_source
	   * field_dept
	   * field_media_relations term
	   * field_shared_adminunits term
	   */
    $mappings['field_news_events term'] = array(
      'destinationField' => 'field_news_events term',
      'sourceField' => 'main_category',
    );
    $mappings[''] = array(
      'destinationField' => '',
      'sourceField' => 'SIT_offices',
    );
    //$mappings[''] = array(
    //  'destinationField' => '',
    //  'sourceField' => 'article_name',
    //);
    $mappings['field_date'] = array(
      'destinationField' => 'field_date',
      'sourceField' => 'event_news_start_date',
      'callbacks' => array('strtotime'),
    );
    //$mappings[''] = array(
    //  'destinationField' => '',
    //  'sourceField' => 'event_news_end_date',
    //);
    $mappings['field_time'] = array(
      'destinationField' => 'field_time',
      'sourceField' => 'event_time',
    );
    $mappings['field_location'] = array(
      'destinationField' => 'field_location',
      'sourceField' => 'event_venue',
    );
    $mappings['title'] = array(
      'destinationField' => 'title',
      'sourceField' => 'headline',
    );
    $mappings['field_carousel_title'] = array(
      'destinationField' => 'field_carousel_title',
      'sourceField' => 'research_title',
    );
    //$mappings[''] = array(
    //  'destinationField' => '',
    //  'sourceField' => 'research_subtitle',
    //);
    $mappings['field_subtitle'] = array(
      'destinationField' => 'field_subtitle',
      'sourceField' => 'article_subtitle',
    );
    $mappings['field_author'] = array(
      'destinationField' => 'field_author',
      'sourceField' => 'author',
    );
    $mappings['body'] = array(
      'destinationField' => 'body',
      'sourceField' => 'news_event_text',
    );
    $mappings['created'] = array(
      'destinationField' => 'created',
      'sourceField' => 'timestamp',
    );
    $mappings['field_news_feed term'] = array(
      'destinationField' => 'field_news_feed term',
      'sourceField' => 'category_name',
    );
    $mappings['field_image'] = array(
      'destinationField' => 'field_image',
      'sourceField' => 'image_path',
    );
    
    return $mappings;
  }
}