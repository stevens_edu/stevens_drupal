<?php


/**
 * Job scheduler callback
 */
function sit_connector_client_cache_scheduler_process($job) {
  ctools_include('object-cache');
  ctools_object_cache_clear($job['data']['object'], $job['data']['name']);
}

/**
 * form submit function for clear all json cache
 */
function _sit_connector_client_clear_json_cache($form, &$form_state) {
	ctools_include('object-cache');
	$endpoints = variable_get('sit_connector_client_endpoints', array());
	foreach($endpoints as $endpoint){
		$endpoint_name = $endpoint['endpoint_name'];
    ctools_object_cache_clear_all('sit_json', $endpoint_name);
	}
}

