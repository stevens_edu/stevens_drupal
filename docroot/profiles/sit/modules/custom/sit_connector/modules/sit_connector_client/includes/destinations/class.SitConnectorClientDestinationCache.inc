<?php
class SitConnectorClientDestinationCache extends MigrateDestination implements SitConnectorClientAggregatorDestinationInterface {
	public static function info() {
		return array(
		  'name' => t('Cache'),
		  );
	}
	
	public function __construct(Array $config = array(), $options = array()) {
	}
	
	public function fields() {
	}
	
	public function import(stdClass $object, stdClass $row) {
	}
	
	public function configForm($form, &$form_state, $entity, $op = 'edit') {
	}
	
	public function configFormValidate($form, &$form_state) {
	}
}