<?php
class SitConnectorCourseMapping extends SitConnectorClientMapping implements SitConnectorClientAggregatorMappingInterface {
  public function __sleep() {
    return parent::__sleep();
  }

  public static function info() {
    return $info = array(
      'name' => t('Course mapping'),
      'description' => t('Maps courses to nodes'),
      );
  }

  public function getMappings() {
    $mappings['title'] = array(
      'destinationField' => 'title',
      'sourceField' => 'course_name',
    );
    $mappings['body'] = array(
      'destinationField' => 'body',
      'sourceField' => 'course_description',
      'arguments' => array('format' => 'full_html'),
    );
    
    // Basic information
    $mappings['field_course_id'] = array(
      'destinationField' => 'field_course_id',
      'sourceField' => 'course_id',
    );
    $mappings['field_course_no'] = array(
      'destinationField' => 'field_course_no',
      'sourceField' => 'course_no',
    );
    $mappings['field_course_no_prefix'] = array(
      'destinationField' => 'field_course_no_prefix',
      'sourceField' => 'course_code',
    );
    $mappings['field_course_section'] = array(
      'destinationField' => 'field_course_section',
      'sourceField' => 'course_section',
    );
    $mappings['field_course_major'] = array(
      'destinationField' => 'field_course_major',
      'sourceField' => 'friendlyMajor',
      'arguments' => array('source_type' => 'name', 'create_term' => TRUE),
    );
    $mappings['field_shared_url'] = array(
      'destinationField' => 'field_shared_url',
      'sourceField' => 'URL',
    );
    $mappings['field_course_special_instr'] = array(
      'destinationField' => 'field_course_special_instr',
      'sourceField' => 'special_instructions',
      'arguments' => array('format' => 'full_html'),
    );
    $mappings['field_course_area'] = array(
      'destinationField' => 'field_course_area',
      'sourceField' => 'area',
    );
    
    // Credits and hours
    $mappings['field_course_credit_hours'] = array(
      'destinationField' => 'field_course_credit_hours',
      'sourceField' => 'credit_hours',
    );
    $mappings['field_course_study_hours'] = array(
      'destinationField' => 'field_course_study_hours',
      'sourceField' => 'study_hours',
    );
    $mappings['field_course_lect_hours'] = array(
      'destinationField' => 'field_course_lect_hours',
      'sourceField' => 'lecture_hours',
    );
    $mappings['field_course_lab_hours'] = array(
      'destinationField' => 'field_course_lab_hours',
      'sourceField' => 'lab_hours',
    );
    
    // Other courses
    $mappings['field_shared_course_refs'] = array(
      'destinationField' => 'field_shared_course_refs',
      'sourceField' => 'crossListedCourseIds',
      'sourceMigration' => TRUE,
      'separator' => '|',
    );
    $mappings['field_course_alternate'] = array(
      'destinationField' => 'field_course_alternate',
      'sourceField' => 'alternate',
      'sourceMigration' => TRUE,
      'separator' => '|',
    );
    $mappings['field_course_prereq'] = array(
      'destinationField' => 'field_course_prereq',
      'sourceField' => 'prereqs',
      'sourceMigration' => TRUE,
      'separator' => '|',
    );
    $mappings['field_course_coreq'] = array(
      'destinationField' => 'field_course_coreq',
      'sourceField' => 'coreqs',
      'sourceMigration' => TRUE,
      'separator' => '|',
    );
    
    // Additional information
    $mappings['field_course_active'] = array(
      'destinationField' => 'field_course_active',
      'sourceField' => 'active',
      'callbacks' => array('sit_connector_client_cb_bool'),
    );
    $mappings['field_course_ceu'] = array(
      'destinationField' => 'field_course_ceu',
      'sourceField' => 'CEU',
    );
    $mappings['field_course_oncampus'] = array(
      'destinationField' => 'field_course_oncampus',
      'sourceField' => 'offered_oncampus',
      'callbacks' => array('sit_connector_client_cb_bool'),
    );
    $mappings['field_course_online'] = array(
      'destinationField' => 'field_course_online',
      'sourceField' => 'offered_webcampus',
      'callbacks' => array('sit_connector_client_cb_bool'),
    );
    $mappings['field_course_catalog'] = array(
      'destinationField' => 'field_course_catalog',
      'sourceField' => 'no_catalog',
      'callbacks' => array('sit_connector_client_cb_bool'),
    );
    
    $mappings['field_shared_adminunits'] = array(
      'destinationField' => 'field_shared_adminunits',
      'sourceField' => array('adminUnits' => array('id', 'type')),
      'sourceMigration' => TRUE,
      'arguments' => array('source_type' => 'tid'),
      'defaultValue' => array(),
    );
    
    $mappings['field_course_proglevel'] = array(
      'destinationField' => 'field_course_proglevel',
      'sourceField' => 'prog_level',
      'arguments' => array('source_type' => 'name', 'create_term' => TRUE),
    );
    
    return $mappings;
  }

  public function __construct($config) {
    parent::__construct($config);
  }
}
