<?php
class SitConnectorAdminUnitsMapping extends SitConnectorClientMapping implements SitConnectorClientAggregatorMappingInterface {
  public function __sleep() {
  	return parent::__sleep();
  }

	public static function info() {
		return $info = array(
		  'name' => t('API Admin units to Drupal Admin units'),
		  'description' => t('Drupal\'s Admin units are terms with additional fields for storing incoming, API sourced keys. This mapping handles those keys.'),
		  );
  }

  public function getMappings() {
  	$mappings['name'] = array(
  	  'destinationField' => 'name',
  	  'sourceField' => 'name',
  	  );
  	$mappings['parent'] = array(
  	  'destinationField' => 'parent',
  	  'sourceField' => array('pid', 'pid_type',),
  	  'sourceMigration' => TRUE,
  	  'arguments' => array('create_stubs' => TRUE),
  	  );
  	$mappings['uuid'] = array(
  	  'destinationField' => 'uuid',
  	  'sourceField' => 'uuid',
  	  );
  	$mappings['field_unit_id'] = array(
  	  'destinationField' => 'field_unit_id',
  	  'sourceField' => 'id',
  	  );
  	$mappings['field_unit_type'] = array(
  	  'destinationField' => 'field_unit_type',
  	  'sourceField' => 'type',
  	  );
  	$mappings['field_unit_parent_id'] = array(
  	  'destinationField' => 'field_unit_parent_id',
  	  'sourceField' => 'pid',
  	  );
  	$mappings['field_unit_parent_type'] = array(
  	  'destinationField' => 'field_unit_parent_type',
  	  'sourceField' => 'pid_type',
  	  );

  	return $mappings;
  }

	public function __construct($config) {
		parent::__construct($config);
  }
}

class SitConnectorNewsHubAdminUnitsMapping extends SitConnectorClientMapping implements SitConnectorClientAggregatorMappingInterface {
  public function __sleep() {
  	return parent::__sleep();
  }

	public static function info() {
		return $info = array(
		  'name' => t('Drupal Admin units to Drupal Admin units'),
		  'description' => t('Maps Drupal Admin Units to Drupal Admin units. Use this when aggregating Admin units from the news hub.'),
		  );
  }

  public function getMappings() {
  	$mappings['name'] = array(
  	  'destinationField' => 'name',
  	  'sourceField' => 'name',
  	  );
  	$mappings['parent'] = array(
  	  'destinationField' => 'parent',
  	  'sourceField' => array('field_parent_unit_id', 'field_parent_unit_type',),
  	  'sourceMigration' => TRUE,
  	  'arguments' => array('create_stubs' => TRUE),
  	  );
  	$mappings['uuid'] = array(
  	  'destinationField' => 'uuid',
  	  'sourceField' => 'uuid',
  	  );
  	$mappings['field_unit_id'] = array(
  	  'destinationField' => 'field_unit_id',
  	  'sourceField' => 'field_unit_id',
  	  );
  	$mappings['field_unit_type'] = array(
  	  'destinationField' => 'field_unit_type',
  	  'sourceField' => 'field_unit_type',
  	  );
  	$mappings['field_parent_unit_id'] = array(
  	  'destinationField' => 'field_parent_unit_id',
  	  'sourceField' => 'field_parent_unit_id',
  	  );
  	$mappings['field_parent_unit_type'] = array(
  	  'destinationField' => 'field_parent_unit_type',
  	  'sourceField' => 'field_parent_unit_type',
  	  );

  	return $mappings;
  }

	public function __construct($config) {
		parent::__construct($config);
  }
}
