<?php
/**
 * @file
 * Administrative forms and pages
 */
function aggregator_config_form($form, &$form_state, $entity, $op = 'edit') {
  // Build options from our interfaces
  $interfaces = array(
    'source' => 'SitConnectorClientAggregatorSourceInterface',
    'destination' => 'SitConnectorClientAggregatorDestinationInterface',
    'mapping' => 'SitConnectorClientAggregatorMappingInterface',
    );

  $result = db_select('registry', 'r')
            ->fields('r', array('name', 'filename', 'module'))
            ->condition('type', 'class')
            ->condition('module', 'sit_connector_client', '=')
            ->condition('filename', '%.test', 'NOT LIKE')
            ->execute();

  // Collate our classes by the components they implement
  foreach ($result as $record) {
  	$class = $record->name;

  	try { $reflector = new ReflectionClass($class); } catch (Exception $e) { continue; }

    foreach ($interfaces as $component => $interface) {
    	// Build the options
      if ($reflector && $reflector->implementsInterface($interface)) {
      	$info = call_user_func(array($class, 'info'));
      	$options[$component . '_options'][$class] = $info['name'];
      }
    }

    $interface = 'SitConnectorClientAggregatorSourceInterface';
  }

  extract($options);

	if ($op == 'clone') {
    $entity->name .= ' (cloned)';
    $entity->machine_name .= '_clone';
  }

  $form['name'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $entity->name,
  );

  // Machine-readable type name.
  $form['machine_name'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($entity->machine_name) ? $entity->machine_name : '',
    '#disabled' => $entity->isLocked(),
    '#machine_name' => array(
      'exists' => 'aggregator_config_load_multiple',
      'source' => array('name'),
      //'replace_pattern' => '[^a-z0-9-]+',
      // @todo: Punt: changed from dash to underscore and now validation errors. WTF.
      //'replace' => '',
    ),
    '#description' => t('A unique machine-readable name for this aggregator. It must only contain lowercase letters, numbers, and underscores.'),
  );

  foreach (array('source', 'destination', 'mapping') as $section) {

	  // Pull in the section configuration form
	  $form[$section] = array(
	    '#type' => 'fieldset',
	    '#title' => t(ucwords($section)),
	    '#collapsible' => TRUE,
	    '#collapsed' => FALSE,
	    '#prefix' => '<div id="' . $section . '">',
	    '#suffix' => '</div>',
	    );

	  $form[$section][$section . '_class'] = array(
	    '#type' => 'select',
	    '#title' => t(ucwords($section) . ' handler'),
	    '#default_value' => isset($entity->$section) ? get_class($entity->$section) : '',
	    '#options' => isset(${$section . '_options'}) ? ${$section . '_options'} : array(),
	    '#empty_option' => '--' . t('Select one') . '--',
	    '#empty_value' => '',
	    '#ajax' => array(
	      'callback' => '_sit_connector_client_endpoint_form_element_' . $section,
	      'wrapper' => $section,
	      'method' => 'replace',
	      'effect' => 'fade',
	      ),
	    );

	  if (is_object($form_state[$form_state['entity_type']]->$section)) {
	  	$form[$section][$section . '_options'] = $form_state[$form_state['entity_type']]->$section->configForm($form, $form_state, $form_state[$form_state['entity_type']]->$section, $op);
	  }

  } // end foreach

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 40,
  );

  return $form;
}

function aggregator_config_form_validate($form, &$form_state) {
  foreach (array('source', 'destination', 'mapping') as $section) {
  	if (!is_object($form_state[$form_state['entity_type']]->$section) && !empty($form_state['values'][$section . '_class'])) {
  		$class = $form_state['values'][$section . '_class'];
  		$form_state[$form_state['entity_type']]->$section = new $class();
  	}
  	if (is_callable(array($form_state[$form_state['entity_type']]->$section, 'configFormValidate'))) {
  	  $form_state[$form_state['entity_type']]->$section->configFormValidate($form, $form_state);
  	}
  }
}

function aggregator_config_form_submit($form, &$form_state) {
	$aggregator = entity_ui_form_submit_build_entity($form, $form_state);
	$aggregator->save();
}

function _sit_connector_client_endpoint_form_element_source($form, &$form_state) {
	return $form['source'];
}

function _sit_connector_client_endpoint_form_element_destination($form, &$form_state) {
	return $form['destination'];
}

function _sit_connector_client_endpoint_form_element_mapping($form, &$form_state) {
	return $form['mapping'];
}

/***************************************************
 *                      Dashboard
 ***************************************************/

function sit_connector_client_aggregator_dashboard($form, &$form_state, $machine_name) {


  $form = array();
  module_load_include('inc', 'migrate_ui', 'migrate_ui.pages');
  $form['operation'] = array(
    '#type' => 'select',
    '#title' => t('Operations'),
    '#description' => t('Warning: rolling back removes all mapped entities. There is no way to roll back an update.'),
    '#options' => array(
      'update' => t('Update'),
      'import' => t('Import'),
      'rollback' => t('Roll back'),
      'stop' => t('Stop'),
      'reset' => t('Reset'),
      ),
    '#empty_option' => '--' . t('Select one') . '--',
    '#empty_value' => '',
    );

  $form['limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Limit'),
    );

  $form['dashboard'] = array(
    '#type' => 'value',
    '#value' => array($machine_name),
    '#multiple_values' => TRUE,
    );

	$form['actions']['submit'] = array(
	  '#type' => 'submit',
	  '#value' => t('Submit'),
	  );

	return $form;
}

function sit_connector_client_aggregator_dashboard_validate($form, &$form_state) {

}

function sit_connector_client_aggregator_dashboard_submit($form, &$form_state) {
  $operation = $form_state['values']['operation'];
  $limit = $form_state['values']['limit'] ? $form_state['values']['limit'] : 0;
  $machine_name = (string)array_shift($form_state['values']['dashboard']);

  $operations = array();
  $operations[] = array('sit_connector_client_batch', array($operation, $machine_name, $limit));

  $batch = array(
    'operations' => $operations,
    'title' => t('Aggregator @op processing', array('@op' => $operation)),
    'file' => drupal_get_path('module', 'sit_connector_client') . '/sit_connector_client.admin.aggregator.inc',
    'init_message' => t('Starting aggregation process'),
    'progress_message' => t(''),
    'error_message' => t('An error occurred. Some or all of the aggregation processing has failed.'),
    'finished' => 'sit_connector_client_batch_finish',
  );

  batch_set($batch);
}

function sit_connector_client_batch($operation, $machine_name, $limit, &$context) {
  // If we got a stop message, skip everything else
  if (isset($context['results']['stopped'])) {
    $context['finished'] = 1;
    return;
  }

  $migration = Migration::getInstance($machine_name);

  // Messages generated by migration processes will be captured in this global
  global $_migrate_messages;
  $_migrate_messages = array();
  $migration->setOutputFunction('sit_connector_client_capture_message');

  // Perform the requested operation
  switch ($operation) {
  	case 'update':
  	  watchdog('client', t('doing update'));
  	  $migration->prepareUpdate();
  	  $result = $migration->processImport();
  	  break;
    case 'import':
      $result = $migration->processImport(array('limit' => $limit));
      break;
    case 'rollback':
      $result = $migration->processRollback(array('limit' => $limit));
      break;
    case 'stop':
      $migration->stopProcess();
      $result = Migration::RESULT_COMPLETED;
      break;
    case 'reset':
      $migration->resetStatus();
      $result = Migration::RESULT_COMPLETED;
      break;
  }

  switch ($result) {
    case Migration::RESULT_INCOMPLETE:
      // Default to half-done, in case we can't get a more precise fix
      $context['finished'] = .5;
      if (method_exists($migration, 'sourceCount')) {
        $total = $migration->sourceCount();
        if ($total && method_exists($migration, 'importedCount')) {
          $imported = $migration->importedCount();
          switch ($operation) {
            case 'import':
              $context['finished'] = $imported/$total;
              break;
            case 'rollback':
              $context['finished'] = ($total-$imported)/$total;
              break;
          }
        }
      }
      break;
    case MigrationBase::RESULT_SKIPPED:
      $_migrate_messages[] = t('Skipped !name due to unfulfilled dependencies',
        array('!name' => $machine_name));
      $context['finished'] = 1;
      break;
    case MigrationBase::RESULT_STOPPED:
      $context['finished'] = 1;
      // Skip any further operations
      $context['results']['stopped'] = TRUE;
      break;
    default:
      $context['finished'] = 1;
      break;
  }

  // Add any messages generated in this batch to the cumulative list
  foreach ($_migrate_messages as $message) {
    $context['results'][] = $message;
  }

  // While in progress, show the cumulative list of messages
  $full_message = '';
  foreach ($context['results'] as $message) {
    $full_message .= $message . '<br />';
  }
  $context['message'] = $full_message;
}

/**
 * Batch API finished callback - report results
 *
 * @param $success
 *  Ignored
 * @param $results
 *  List of results from batch processing
 * @param $operations
 *  Ignored
 */
function sit_connector_client_batch_finish($success, $results, $operations) {
  unset($results['stopped']);
  foreach ($results as $result) {
    drupal_set_message($result);
  }
}

function sit_connector_client_capture_message($message, $level) {
  if ($level != 'debug') {
    global $_migrate_messages;
    $_migrate_messages[] = $message;
  }
}
