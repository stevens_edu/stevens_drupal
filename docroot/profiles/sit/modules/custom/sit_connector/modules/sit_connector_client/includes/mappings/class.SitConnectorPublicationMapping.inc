<?php
class SitConnectorPublicationMapping extends SitConnectorClientMapping implements SitConnectorClientAggregatorMappingInterface {
  public function __sleep() {
    return parent::__sleep();
  }

  public static function info() {
    return $info = array(
      'name' => t('Publication mapping'),
      'description' => t('Maps publications to nodes'),
      );
  }

  public function getMappings() {
    $mappings['field_bio_reference'] = array(
      'destinationField' => 'field_bio_reference',
      'sourceField' => 'faculty_id',
      'sourceMigration' => TRUE,
    );  
  
    // Boolean and text mappings
    $mappings['title'] = array(
      'destinationField' => 'title',
      'sourceField' => 'title',
      'defaultValue' => 'None supplied',
      'callbacks' => array('sit_connector_client_truncate'),
    );
    $mappings['field_publication_title'] = array(
      'destinationField' => 'field_publication_title',
      'sourceField' => 'title',
      'defaultValue' => 'None supplied',
      'arguments' => array('format' => 'full_html'),
    );
    $mapping['body'] = array(
      'destinationField' => 'body',
      'sourceField' => 'publication_abstract',
      'arguments' => array('format' => 'full_html'),
    );
    $mappings['field_book_title'] = array(
      'destinationField' => 'field_book_title',
      'sourceField' => 'book_title',
    );
    $mappings['field_publisher'] = array(
      'destinationField' => 'field_publisher',
      'sourceField' => 'publisher',
    );
    $mappings['field_editors'] = array(
      'destinationField' => 'field_editors',
      'sourceField' => 'editors',
    );
    $mappings['field_publication'] = array(
      'destinationField' => 'field_publication',
      'sourceField' => 'publication',
    );
    $mappings['field_publication_type'] = array(
      'destinationField' => 'field_publication_type',
      'sourceField' => 'publication_type',
    );
    $mappings['field_conference'] = array(
      'destinationField' => 'field_conference',
      'sourceField' => 'conference',
    );
    $mappings['field_venue'] = array(
      'destinationField' => 'field_venue',
      'sourceField' => 'venue',
    );
    $mappings['field_month'] = array(
      'destinationField' => 'field_month',
      'sourceField' => 'month',
    );
    $mappings['field_year'] = array(
      'destinationField' => 'field_year',
      'sourceField' => 'year',
    );
    $mappings['field_page_number'] = array(
      'destinationField' => 'field_page_number',
      'sourceField' => 'page_number',
    );
    $mappings['field_issue'] = array(
      'destinationField' => 'field_issue',
      'sourceField' => 'issue',
    );
    $mappings['field_volume'] = array(
      'destinationField' => 'field_volume',
      'sourceField' => 'volume',
    );
    $mappings['field_authors'] = array(
      'destinationField' => 'field_authors',
      'sourceField' => 'Authors',
    );
    $mapping['field_publication_url'] = array(
      'destinationField' => 'field_publication_url',
      'sourceField' => 'publication_url',
    );
    $mapping['field_publication_url_size'] = array(
      'destinationField' => 'field_publication_url_size',
      'sourceField' => 'sizeof_publication_pdf',
    );
    $mapping['field_publication_date'] = array(
      'destinationField' => 'field_publication_date',
      'sourceField' => 'publication_date',
    );
    $mapping['field_issue'] = array(
      'destinationField' => 'field_issue',
      'sourceField' => 'issue',
    );
    return $mappings;
  }

  public function __construct($config) {
    parent::__construct($config);
  }
}
