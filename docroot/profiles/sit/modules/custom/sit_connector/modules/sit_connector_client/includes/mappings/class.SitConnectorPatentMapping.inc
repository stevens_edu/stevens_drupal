<?php
class SitConnectorPatentMapping extends SitConnectorClientMapping implements SitConnectorClientAggregatorMappingInterface {
  public function __sleep() {
    return parent::__sleep();
  }

  public static function info() {
    return $info = array(
      'name' => t('Patent mapping'),
      'description' => t('Maps patents to nodes'),
      );
  }

  public function getMappings() {
  	$mappings['field_patent_id'] = array(
  	  'destinationField' => 'field_patent_id',
  	  'sourceField' => 'patent_id',
  	);
  	$mappings['field_patent_number'] = array(
  	  'destinationField' => 'field_patent_number',
  	  'sourceField' => 'patent_number',
  	);
  	$mappings['title'] = array(
  	  'destinationField' => 'title',
  	  'sourceField' => 'title',
  	);
  	$mappings['field_abstract'] = array(
  	  'destinationField' => 'body',
  	  'sourceField' => 'abstract',
  	  'arguments' => array('format' => 'full_html'),
  	);
  	$mappings['field_patent_inventors'] = array(
  	  'destinationField' => 'field_patent_inventors',
  	  'sourceField' => 'inventors',
      'sourceMigration' => TRUE,
      'separator' => '|',
  	);
  	$mappings['field_shared_faculty_ids'] = array(
  	  'destinationField' => 'field_shared_faculty_ids',
  	  'sourceField' => 'faculty_id',
      'sourceMigration' => TRUE,
      'separator' => '|',
  	);
  	$mappings['field_shared_url'] = array(
  	  'destinationField' => 'field_shared_url',
  	  'sourceField' => 'link',
  	);

/******************************* DISABLED *************************************
  	$mappings['published'] = array(
  	  'destinationField' => 'published',
  	  'sourceField' => 'published',
  	);
  	$mappings['field_deleted'] = array(
  	  'destinationField' => 'field_deleted',
  	  'sourceField' => 'deleted',
  	);
  	$mappings['field_assignee_name'] = array(
  	  'destinationField' => 'field_assignee_name',
  	  'sourceField' => 'assignee_name',
  	);
  	$mappings['field_filing_date'] = array(
  	  'destinationField' => 'field_filing_date',
  	  'sourceField' => 'filing_date',
  	);
  	$mappings['field_granting_date'] = array(
  	  'destinationField' => 'field_granting_date',
  	  'sourceField' => 'granting_date',
  	);
  	$mappings['field_application_number'] = array(
  	  'destinationField' => 'field_application_number',
  	  'sourceField' => 'applicaton_number',
  	);
******************************** DISABLED ************************************/
    return $mappings;
  }

  public function __construct($config) {
    parent::__construct($config);
  }
}
