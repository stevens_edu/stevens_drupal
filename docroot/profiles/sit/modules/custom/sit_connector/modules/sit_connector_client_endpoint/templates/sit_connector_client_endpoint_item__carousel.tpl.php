<div class="sit-carousel-column">
  <?php if ($title): ?>
    <h3 class="carousel-field field-carousel-title-link"><?php print $title; ?></h3>
  <?php endif; ?>
  <?php if ($content): ?>
    <?php print $content; ?>
  <?php endif; ?>
</div>