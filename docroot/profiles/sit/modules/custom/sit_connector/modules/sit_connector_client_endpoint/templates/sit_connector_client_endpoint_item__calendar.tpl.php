<li>
  <?php if($title): ?>
    <h3 class="title"><?php print $url ? l($title, $url) : $title; ?></h3>
  <?php endif; ?>
  <p>
  <span class="start-date"><?php print $start; ?></span>
  <span class="end-date"><?php print $end; ?></span>
  </p>
</li>
