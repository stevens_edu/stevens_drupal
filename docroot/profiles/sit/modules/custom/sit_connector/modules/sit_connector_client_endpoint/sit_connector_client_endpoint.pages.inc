<?php
function sit_connector_client_endpoint_form($form, &$form_state, $entity, $op = 'edit') {

  $form = array();
  if ($op == 'clone') {
    $entity->label .= ' (cloned)';
    $entity->machine_name .= '-clone';
  }

  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Endpoint label'),
    '#default_value' => isset($entity->label) ? $entity->label : '',
    '#required' => TRUE,
    );
  
  $form['machine_name'] = array(
    '#type' => 'machine_name',
    '#title' => t('Endpoint machine name'),
    '#machine_name' => array(
      'exists' => 'endpoint_edit_endpoint_name_exists',
      'source' => array('label'),
      'label' => t('Machine name'),
      ),
    '#default_value' => isset($entity->machine_name) ? $entity->machine_name : '',
    '#disabled' => isset($entity->machine_name) ? TRUE : FALSE,
    '#required' => TRUE,
    );
    
  $form += _sit_connector_client_endpoint_form_element($form, $form_state, $entity);
  
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
    );
  
  if ($op == 'clone') {
  	return $form;
  }

  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => 'Delete',
    );
  return $form;
}

/**
 * Validation handler for sit_connector_client_endpoint_form()
 */
function sit_connector_client_endpoint_form_validate($form, &$form_state) {
  $endpoint = $form_state['values'];
  $form_state[$form_state['entity_type']]->type = $form_state['values']['type'];
  if (!valid_url($endpoint['url'], TRUE) && !empty($endpoint['machine_name'])) {
    //form_set_error('sit_connector_client', t('You must enter an valid url for each endpoint, including the scheme (http://)'));
  }
  if (empty($endpoint['name']) && !empty($endpoint['name'])) {
    form_set_error($i, t('Please supply a label for each endpoint.'));
  }  
}

/**
 * Submit handler for sit_connector_client_endpoint_form
 */
function sit_connector_client_endpoint_form_submit($form, &$form_state) {
	$endpoint = entity_ui_form_submit_build_entity($form, $form_state);
  $endpoint->save();
  $form_state['redirect'] = 'admin/config/services/endpoints';
  drupal_set_message(t('Your settings have been saved'));
}

function endpoint_edit_endpoint_name_exists($machine_name) {
	$result = db_select('sit_connector_client_endpoints', 'e')
    ->fields('e')
    ->condition('machine_name', $machine_name, '=')
    ->execute();
  if ($result->rowCount()) {
  	return TRUE;
  }
  return FALSE;
}

/**
 * Helper function for building an connector endpoint configuration form element
 */
function _sit_connector_client_endpoint_form_element($form, &$form_state, $endpoint = NULL) {

  if (!is_object($endpoint)) {
    $endpoint = (object)$endpoint;
  }

  $element = array();

  $element['type'] = array(
    '#type' => 'textfield',
    '#title' => t('Type'),
    '#default_value' => isset($endpoint->type) ? $endpoint->type : '',
    '#required' => TRUE,
    );

  $element['url_info'] = array(
    '#type' => 'fieldset',
    );
  if(0) {  
  $element['url_info']['use_auth'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use session based authentication.'),
    '#default_value' => !empty($endpoint->use_auth) ? $endpoint->use_auth : FALSE,
    );
  
  $element['url_info']['username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => !empty($endpoint->username) ? $endpoint->username : '',
    '#states' => array(
      'visible' => array(
        'input[name="use_auth"]' => array('checked' => TRUE),
        ),
      ),
    );
  
  $element['url_info']['password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#disabled' => !empty($endpoint->password) ? TRUE : FALSE,
    '#states' => array(
      'visible' => array(
        'input[name="use_auth"]' => array('checked' => TRUE),
        ),
      ),
    );
  }//if0
  $element['url_info']['method'] = array(
    '#type' => 'select',
    '#title' => t('Request method'),
    '#options' => array(
      'GET' => 'GET',
      'POST' => 'POST',
      ),
    '#default_value' => !empty($endpoint->method) ? $endpoint->method : 'GET',
    );
  
  $element['url_info']['url'] = array(
    '#type' => 'textfield',
    '#title' => t('Endpoint url'),
    '#default_value' => isset($endpoint->url) ? $endpoint->url : '',
    );
    
  $element['url_info']['params'] = array(
    '#type' => 'textarea',
    '#title' => t('Request params'),
    '#default_value' => !empty($endpoint->params) ? implode("\n", $endpoint->params) : '',
    );
  
  $element['cache_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable cache for this endpoint'),
    '#default_value' => isset($endpoint->cache_enabled) ? $endpoint->cache_enabled : FALSE,
  );
  
  $element['cache_lifetime'] = array(
    '#type' => 'select',
    '#title' => t('Cache lifetime(Sec)'),
    '#options' => array(
      60 => '1 min',
      180 => '3 min',
      300 => '5 min',
      600 => '10 min',
      900 => '15 min',
      1800 => '30min',
      2700 => '45 min',
      3600 => '1 hour',
      10800 => '3 hours',
      21600 => '6 hours',
      32400 => '9 hours',
      43200 => '12 hours',
      86400 => '1 day',
    ),
    '#default_value' => isset($endpoint->cache_lifetime) ? $endpoint->cache_lifetime : 3600,
    '#states' => array(
      'visible' => array(
        'input[name="cache_enabled"]' => array('checked' => TRUE),
      ),
    ),
  );

  return $element;
}

































function sit_connector_client_endpoint_type_form($form, &$form_state, $entity, $op = 'edit') {
  $form = array();
  if ($op == 'clone') {
    $entity->label .= ' (cloned)';
    $entity->machine_name .= '-clone';
  }

  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Endpoint label'),
    '#default_value' => isset($entity->label) ? $entity->label : '',
    );
  
  $form['machine_name'] = array(
    '#type' => 'machine_name',
    '#title' => t('Endpoint machine name'),
    '#machine_name' => array(
      'exists' => 'endpoint_edit_endpoint_name_exists',
      'source' => array('label'),
      'label' => t('Machine name'),
      ),
    '#default_value' => isset($entity->machine_name) ? $entity->machine_name : '',
    '#disabled' => isset($entity->machine_name) ? TRUE : FALSE,
    );
  
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
    );
  
  if ($op == 'clone') {
  	return $form;
  }

  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => 'Delete',
    );
    
  return $form;
}

/**
 * Validation handler for sit_connector_client_endpoint_form()
 */
function sit_connector_client_endpoint_type_form_validate($form, &$form_state) {
}

/**
 * Submit handler for sit_connector_client_endpoint_form
 */
function sit_connector_client_endpoint_type_form_submit($form, &$form_state) {
	$type = entity_ui_form_submit_build_entity($form, $form_state);
  $type->save();
  $form_state['redirect'] = 'admin/config/services/endpoint-types';
  drupal_set_message(t('Your settings have been saved'));
}

function endpoint_edit_endpoint_tyep_name_exists($machine_name) {
	$result = db_select('sit_connector_client_endpoint_types', 'e')
    ->fields('e')
    ->condition('machine_name', $machine_name, '=')
    ->execute();
  if ($result->rowCount()) {
  	return TRUE;
  }
  return FALSE;
}

class SitConnectorClientEndpointUIController extends EntityDefaultUIController {
	public function __construct($entity_type, $entity_info) {
		parent::__construct($entity_type, $entity_info);
	}
	
	public function overviewTable($conditions = array()) {
    $entities = entity_load($this->entityType, FALSE, $conditions);

    ksort($entities);


    $rows = array();
    foreach ($entities as $entity) {
      $rows[] = $this->overviewTableRow($conditions, entity_id($this->entityType, $entity), $entity);
    }
    // Assemble the right table header.
    $header = array(t('Label'));
    if (!empty($this->entityInfo['exportable'])) {
      $header[] = t('Status');
    }
    // Add operations with the right colspan.
    $field_ui = !empty($this->entityInfo['bundle of']) && module_exists('field_ui');
    $exportable = !empty($this->entityInfo['exportable']);
    $colspan = 3;
    $colspan = $field_ui ? $colspan + 2 : $colspan;
    $colspan = $exportable ? $colspan + 1 : $colspan;
    $header[] = array('data' => t('Operations'), 'colspan' => $colspan);

    $render = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('None.'),
    );

    return $render;
  }
}