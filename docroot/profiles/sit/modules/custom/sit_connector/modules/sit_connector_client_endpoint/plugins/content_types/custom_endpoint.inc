<?php

/**
 * @file
 * "No context" sample content type. It operates with no context at all. It would
 * be basically the same as a 'custom content' block, but it's not even that
 * sophisticated.
 *
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('New custom endpoint'),
  'description' => t('No context content type - requires and uses no context.'),

  // 'single' => TRUE means has no subtypes.
  'single' => TRUE,
  // Constructor.
  'content_types' => array('custom_endpoint_content_type'),
  // Name of a function which will render the block.
  'render callback' => 'custom_endpoint_content_type_render',
  // The default context.
  'defaults' => array(),

  // This explicitly declares the config form. Without this line, the func would be
  // ctools_plugin_example_custom_endpoint_content_type_edit_form.
  'edit form' => 'custom_endpoint_content_type_edit_form',

  // Icon goes in the directory with the content type.
  'icon' => 'icon_example.png',
  'category' => array(t('Miscellaneous'), -9),
  'top level' => TRUE,
  'all contexts' => TRUE,

  // this example does not provide 'admin info', which would populate the
  // panels builder page preview.
);

/**
 * Run-time rendering of the body of the block.
 *
 * @param $subtype
 * @param $conf
 *   Configuration as done at admin time.
 * @param $args
 * @param $context
 *   Context - in this case we don't have any.
 *
 * @return
 *   An object with at least title and content members.
 */
function custom_endpoint_content_type_render($subtype, $conf, $args, $contexts) {
  $block = new stdClass();

  $type = $conf['type'];
  $url = $conf['url'];
  $params = $conf['params'];  
  $method = $conf['method'];
  
  // Add keyword substitutions if we were configured to do so.
  if (!empty($contexts) && !empty($conf['substitute'])) {
  	// Do replace on the URL
    $url = ctools_context_keyword_substitute($url, array(), $contexts);
    
    // Do replace on the params
    if (is_array($params)) {
      foreach($params as $i => $param) {
        $params[$i] = ctools_context_keyword_substitute($param, array(), $contexts);
      }
    }
  }
  
  $values = array(
    'type' => $type,
    'url' => $url,
    'method' => $method,
    'params' => $params,
    );
    
  $endpoint = entity_create('sit_connector_client_endpoint', $values); 
  $content = $endpoint->buildContent();
  $content = drupal_render($content);

  $block->content = $content;
  
  return $block;
}

/**
 * 'Edit form' callback for the content type.
 * This example just returns a form; validation and submission are standard drupal
 * Note that if we had not provided an entry for this in hook_content_types,
 * this could have had the default name
 * ctools_plugin_example_custom_endpoint_content_type_edit_form.
 *
 */
function custom_endpoint_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  
  module_load_include('inc', 'sit_connector_client_endpoint', 'sit_connector_client_endpoint.pages');
  $form += _sit_connector_client_endpoint_form_element($form, $form_state, $conf);
  
  if (!empty($form_state['contexts'])) {
    // Set extended description if both CCK and Token modules are enabled, notifying of unlisted keywords
    if (module_exists('content') && module_exists('token')) {
      $description = t('If checked, context keywords will be substituted in this content. Note that CCK fields may be used as keywords using patterns like <em>%node:field_name-formatted</em>.');
    }
    elseif (!module_exists('token')) {
      $description = t('If checked, context keywords will be substituted in this content. More keywords will be available if you install the Token module, see http://drupal.org/project/token.');
    }
    else {
      $description = t('If checked, context keywords will be substituted in this content.');
    }

    $form['substitute'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use context keywords'),
      '#description' => $description,
      '#default_value' => !empty($conf['substitute']),
    );
    $form['contexts'] = array(
      '#title' => t('Substitutions'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $rows = array();
    foreach ($form_state['contexts'] as $context) {
      foreach (ctools_context_get_converters('%' . check_plain($context->keyword) . ':', $context) as $keyword => $title) {
        $rows[] = array(
          check_plain($keyword),
          t('@identifier: @title', array('@title' => $title, '@identifier' => $context->identifier)),
        );
      }
    }
    $header = array(t('Keyword'), t('Value'));
    $form['contexts']['context'] = array('#markup' => theme('table', array('header' => $header, 'rows' => $rows)));
  }
  return $form;
}

function custom_endpoint_content_type_edit_form_submit(&$form, &$form_state) {
  foreach (array('type', 'method', 'url', 'params', 'substitute') as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
  
  if (!empty($form_state['values']['params'])) {
  	$params = explode("\n", $form_state['values']['params']);
  	foreach ($params as $i => $param) {
  		$params[$i] = check_plain(rtrim($param));
    }
    $form_state['conf']['params'] = $params;
  }
}
