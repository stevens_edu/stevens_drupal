<?php
/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Endpoint content'),
  'icon' => 'icon_endpoint.png',
  'description' => t('The content of the referenced endpoint.'),
  'required context' => new ctools_context_required(t('Endpoint'), 'sit_connector_client_endpoint'),
  'category' => t('Endpoint content'),
  'defaults' => array(
    'links' => TRUE,
    'no_extras' => TRUE,
    'override_title' => FALSE,
    'override_title_text' => '',
    'identifier' => '',
    'link' => TRUE,
    'leave_endpoint_title' => FALSE,
    'build_mode' => 'teaser',
  ),
);

/**
 * Render the endpoint content.
 */
function sit_connector_client_endpoint_endpoint_content_content_type_render($subtype, $conf, $panel_args, $context) {
  if (!empty($context) && empty($context->data)) {
    return;
  }
  
  $endpoint = isset($context->data) ? clone($context->data) : NULL;
  $block = new stdClass();
  $block->module = 'endpoint';
  $block->delta  = $endpoint->eid;

  if (empty($endpoint)) {
    $block->delta   = 'placeholder';
    $block->label = t('endpoint title.');
    $block->content = t('endpoint content goes here.');
  }
  else {
    if (!empty($conf['identifier'])) {
      $endpoint->ctools_template_identifier = $conf['identifier'];
    }

    $block->label = $endpoint->label;
    if (empty($conf['leave_endpoint_title'])) {
      $endpoint->label = NULL;
    }
    $block->content = sit_connector_client_endpoint_endpoint_content_render_endpoint($endpoint, $conf);
  }

  if (!empty($conf['link']) && $endpoint) {
    $block->label_link = "endpoint/$endpoint->eid";
  }

  return $block;
}

function sit_connector_client_endpoint_endpoint_content_render_endpoint($endpoint, $conf) {
  if (empty($endpoint->content)) {
    $endpoint->content = array();
    $endpoint->content = sit_connector_client_endpoint_view($endpoint);
  }

  // Set the proper endpoint part, then unset unused $endpoint part so that a bad
  // theme can not open a security hole.
  $content = $endpoint->content;
  return $content;
}

/**
 * Returns an edit form for the custom type.
 */
function sit_connector_client_endpoint_endpoint_content_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $form['leave_endpoint_title'] = array(
    '#type' => 'checkbox',
    '#default_value' => !empty($conf['leave_endpoint_title']),
    '#title' => t('Leave endpoint title'),
    '#description' => t('Advanced: if checked, do not touch the endpoint title; this can cause the endpoint title to appear twice unless your theme is aware of this.'),
  );

  $form['identifier'] = array(
    '#type' => 'textfield',
    '#default_value' => $conf['identifier'],
    '#title' => t('Template identifier'),
    '#description' => t('This identifier will be added as a template suggestion to display this endpoint: endpoint--panel--IDENTIFIER.tpl.php. Please see the Drupal theming guide for information about template suggestions.'),
  );

  return $form;
}

function sit_connector_client_endpoint_endpoint_content_content_type_edit_form_submit($form, &$form_state) {
  // Copy everything from our defaults.
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

function sit_connector_client_endpoint_endpoint_content_content_type_admin_title($subtype, $conf, $context) {
  return t('"@s" content', array('@s' => $context->identifier));
}

