<?php
function sit_connector_admin_form($form, &$form_state) {
  $form = array();

  $form['site_info'] = array(
    '#type' => 'fieldset',
    '#title' => 'Site information',
    '#collapsible' => TRUE,
    '#collapsible' => TRUE,
  );

  $form['site_info']['sit_siteid'] = array(
    '#type' => 'textfield',
    '#title' => t('SIT Site ID'),
    '#required' => TRUE,
    '#size' => 3,
    '#maxlength' => 3,
    '#default_value' => variable_get('sit_siteid', 0),
    '#description' => t('The SIT site id identifies this site to SIT\'s secondary systems whenever, e.g.,
      API calls are made. The value entered here should correspond to a known SIT site id.'),
  );

  $form['site_info']['sit_themekey'] = array(
    '#type' => 'textfield',
    '#title' => t('Theme key'),
    '#size' => 3,
    '#maxlength' => 3,
    '#required' => TRUE,
    '#default_value' => variable_get('sit_themekey', 'dft'),
    '#description' => t('The theme key becomes a body class that templates use to determine
      how certain elements should be styled. This key should be three lowercased characters long
      and should consist of letters only.'),
  );

  $form['tokens']['sit_host'] = array(
    '#type' => 'textfield',
    '#title' => t('SIT host'),
    '#default_value' => variable_get('sit_host', NULL),
    '#description' => t('If supplied then the hostname for all SIT sites may be published as a token
      replacement, e.g., "http://www.stevens.edu". This is useful in places like quick links, where
      we want to build links like "http://www.stevens.edu/cal" and have the host part dynamically
      update when moving to production. <em>Do not include a trailing slash ("/")</em>.'),
  );
  $form['tokens']['sit_news_host'] = array(
    '#type' => 'textfield',
    '#title' => t('SIT News Hub host'),
    '#default_value' => variable_get('sit_news_host', NULL),
    '#description' => t('If supplied then the hostname for the SIT News site may be published as a token
      replacement, e.g., "http://www.stevens.edu/news". <em>Do not include a trailing slash ("/")</em>.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
    );
  return $form;
}

/**
 * Validation handler for sit_connector_admin_form()
 */
function sit_connector_admin_form_validate($form, &$form_state) {
  if (3 != strlen($form_state['values']['sit_themekey'])) {
    form_set_error('sit_themekey', t('SIT theme key must be exactly three alpha characters'));
  }

  if (!empty($form_state['values']['sit_host']) && !valid_url($form_state['values']['sit_host'])) {
  	form_set_error('sit_host', t('SIT host must be an valid URL'));
  }

  if (!empty($form_state['values']['sit_news_host']) && !valid_url($form_state['values']['sit_news_host'])) {
  	form_set_error('sit_news_host', t('SIT News Hub host must be an valid URL'));
  }
}

/**
 * Submit handler for sit_connector_admin_form()
 */
function sit_connector_admin_form_submit($form, &$form_state) {
  variable_set('sit_themekey', strtolower($form_state['values']['sit_themekey']));
  variable_set('sit_siteid', $form_state['values']['sit_siteid']);

  if (!empty($form_state['values']['sit_host'])) {
  	variable_set('sit_host', $form_state['values']['sit_host']);
  }
  else {
  	variable_del('sit_host');
  }

  if (!empty($form_state['values']['sit_news_host'])) {
  	variable_set('sit_news_host', $form_state['values']['sit_news_host']);
  }
  else {
  	variable_del('sit_news_host');
  }

  drupal_set_message(t('Your settings have been saved'));
}
