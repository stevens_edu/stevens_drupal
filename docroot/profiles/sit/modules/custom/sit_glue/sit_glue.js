(function ($) {

  Drupal.behaviors.sitGlue = {
    attach: function (context, settings) {
      // Hide the results on the Find An Expert block initially, until a search is performed.
      // This can't be done in views without some not-so-trivial hackage on the API.
      if (!$('.block-findexpert-block').hasClass('sit-glue-processed')) {
        $('.block-findexpert-block .views-row').remove();
        $('.block-findexpert-block').addClass('sit-glue-processed');
      }

      if ($.trim($('.ds-right.grid-6').text()) === "" && $('.ds-right.grid-6 img').length === 0) {
        // If the right grid-6 column is empty, remove it and expand the left one.
        $(this).remove();
        $('.ds-left.grid-12').removeClass('grid-12').addClass('grid-18');
      } 
      if ($.trim($('.block-page-node-minipanel').text()) === "" && $('.block-page-node-minipanel img').length === 0) {
        // If the mini panel block is empty, remove it and make the other column full width.
        var parent = $('.block-page-node-minipanel').parent();
        $('.block-page-node-minipanel').remove();
        if ($.trim(parent.text()) === "") {
          parent.siblings('.grid-12').removeClass('grid-12').addClass('grid-18');
        }
      } 

      // Convert the SES Faculty tabs to dropdown menu.
      $('.site-ses.page-about-faculty-and-staff-directoryphp #tabs-0-middle .item-list').after('<h3>Departments</h3><select name="fake-tabs" id="fake-tabs"></select>').hide();
      var label = '';
      $('.site-ses.page-about-faculty-and-staff-directoryphp #tabs-0-middle .item-list li').each(function() {
        label = $(this).text();
        $('select#fake-tabs').append('<option value="' + label + '">' + label + '</option>');
      });
      $('#fake-tabs').change(function() {
        var ind = $(this).attr('selectedIndex');
        $('#tabs-0-middle .item-list li:eq(' + ind + ') a').click();
      });

      // Add little expanding links to the pages with big migrated tables of courses.
      $(".migrated-table table table td").has('a').wrapInner('<div class="inner-expander" />');
      $('.migrated-table table table td a').each(function() {
        $(this).prependTo($(this).parent().parent());
      });
      $('.migrated-table table table td a').click(function(event) {
        event.preventDefault();
        $(this).nextAll('.inner-expander').toggle();
      });

      $('.block-views-exp-expose-events-events-page #edit-field-date-value-wrapper .form-text').hide().parent().datepicker({ 
        dateFormat: 'yy-mm-dd',
        onSelect: function(dateText, inst) {
          $('.block-views-exp-expose-events-events-page .form-text').val(dateText);
          $('#edit-submit-expose-events').trigger('click');
        },
        onChangeMonthYear: function(year, month, inst) {
          // Have to re-set the <a> around the Month/Year with a setTimeout so that it has time to render the text first.
          setTimeout("jQuery('.ui-datepicker-title').wrapInner('<a class=\"month-link\" href=\"#\"></a>');", 100);
        },
        beforeShowDay: markEventDays
      });
      $('.ui-datepicker-title').wrapInner('<a class="month-link" href="#"></a>');
      $('.block-views-exp-expose-events-events-page #edit-field-date-value2-wrapper').hide();
      function monthToInt(monthName, short) {
        if (short) {
          var months = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        } else {
          var months = ["", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        }
        return months.indexOf(monthName);
      }

      function markEventDays(date) {
        splitDate = String(date).split(" ");
        month = monthToInt(splitDate[1], true);
        day = parseFloat(splitDate[2]);
        year = splitDate[3];
        goodDate = month + "/" + day + "/" + year;
        if ($('.view-event-date-data .date-display-single:contains("' + goodDate + '")').length > 0) {
          return [true, 'has-event'];
        } else {
          return [false, ''];
        }
      }

      $('a.month-link').live('click', function(e) {
        e.preventDefault();
        $('.ui-datepicker-calendar a.ui-state-active').removeClass('ui-state-active');
        startDate = $('.ui-datepicker-year').text() + '-' + monthToInt($('.ui-datepicker-month').text(), false) + '-' + 1;
        endDay = $('.ui-datepicker-calendar td:contains("3")').last().text();
        if (endDay == 23) {
          endDay = 28;
        }
        endDate = $('.ui-datepicker-year').text() + '-' + monthToInt($('.ui-datepicker-month').text(), false) + '-' + endDay;
        $('.block-views-exp-expose-events-events-page #edit-field-date-value-wrapper .form-text').val(startDate);
        $('.block-views-exp-expose-events-events-page #edit-field-date-value2-wrapper .form-text').val(endDate);
        $('#edit-submit-expose-events').trigger('click');
      });

      // On node 1280, there are collapsible fieldsets, and we need the correct one to
      // auto-open based on the fragment in the URL. For example, #prospective opens the 
      // Prospective Students fieldset.
      if ($('body').hasClass('page-node-1280')) {
        var hash = window.location.hash;
        if (hash) {
          $('fieldset#' + hash + '-fieldset legend a').click();
        }
      }

      // Media module doesn't let you do different display formatters based on media type, so we're telling it to
      // use the Colorbox formatter by default (for images), and if it's a video instead, we do the below which
      // converts it to a YouTube embed. Not the prettiest code, but much much more straightforward than trying
      // to change formatters programmatically on the fly based on the data type in the field.
      $('.site-hub .node-media .media-colorbox:has(object)').css({'float' : 'none', 'width' : 'auto'});
      if ($('.site-hub .node-media a[href*="youtube"]').length > 0) {
        $('a.media-colorbox').remove();
        var ytiframe = '';
        var embedurl = '';
        var embedcode = '';
        $('.site-hub .node-media a[href*="youtube"]').each(function() {
          embedurl = $(this).attr('href').replace("watch?v=", "embed/");
          embedcode = '<iframe width="800" height="449" src="' + embedurl + '" frameborder="0" allowfullscreen></iframe>'
          $(this).before(embedcode);
          $(this).remove();
        });
      }
      $('.media-colorbox > .media-outer-wrapper').unwrap();
    }
  }
})(jQuery);
