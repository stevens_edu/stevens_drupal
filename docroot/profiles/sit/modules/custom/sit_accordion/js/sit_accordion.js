/**
 *  @file
 *  A simple jQuery Cycle Div Slideshow Rotator.
 */

/**
 * This will set our initial behavior, by starting up each individual slideshow.
 */
(function($) {
  Drupal.behaviors.stevensAccordionMenu = {
    attach: function(context, settings) {
      // add div to the block
      $('#block-menu-menu-secondary > ul').before('<div class="open-close">Stevens Quick Links</div>');
      $('#block-system-main-menu > ul').before('<div class="open-close"></div>');
      // hide main menu items and add classes
      $('div#block-system-main-menu').addClass('menu-closed');
      $('div#block-menu-menu-secondary').addClass('menu-closed');
      //system main menu accordion
      $('#block-system-main-menu').click(function() {
        $(this).stop(true, true);
        if ($(this).is('.menu-closed')) {
          closeSecondaryMenu($('#block-menu-menu-secondary'));
          $(this).switchClass('menu-closed', 'menu-opened', 'slow');
        }
        else {
          $(this).switchClass('menu-opened', 'menu-closed', 'slow');
        }
      });
      // system secondary menu accordion
      $('#block-menu-menu-secondary').click(function() {
        $('#block-system-main-menu').stop(true, true);
        var menu = $(this);
        if (menu.is('.menu-closed')) {
          openSecondaryMenu(menu);
        }
        else {
          closeSecondaryMenu(menu);
        }
      });


      $('#block-menu-menu-secondary, #block-system-main-menu').mouseover(function() {
        $(this).css('cursor', 'pointer');
      });
    }
  }
  function openSecondaryMenu(menu) {
    $('div#block-system-main-menu').switchClass('menu-opened', 'menu-closed', 'slow');
    if ($('body').hasClass('not-sit-school')) {
      $('#block-menu-menu-secondary > ul > li > ul').stop(true, true).slideDown('slow');
    } else {
      $('#block-menu-menu-secondary > ul').stop(true, true).slideDown('slow');
    }
    menu.removeClass('menu-closed');
    menu.addClass('menu-opened');
  }
  function closeSecondaryMenu(menu) {
    if ($('body').hasClass('not-sit-school')) {
      $('#block-menu-menu-secondary > ul > li > ul').stop(true, true).slideUp('slow', function() {
        menu.addClass('menu-closed');
        menu.removeClass('menu-opened');
      });
    } else {
      $('#block-menu-menu-secondary > ul').stop(true, true).slideUp('slow', function() {
        menu.addClass('menu-closed');
        menu.removeClass('menu-opened');
      });
    }
  }
})(jQuery);

