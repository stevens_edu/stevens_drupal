<?php
/*
 * @file
 * Admin settings form and validations
 */
function sit_carousel_general_admin_settings() {
  $form = array();

  $form['carousel_cycle'] = array(
    '#type' => 'fieldset',
    '#title' => t('Carousel Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $effects = array(
    'none' => 'none',
    'blindX' => 'blindX',
    'blindY' => 'blindY',
    'blindZ' => 'blindZ',
    'cover' => 'cover',
    'curtainX' => 'curtainX',
    'curtainY' => 'curtainY',
    'fade' => 'fade',
    'fadeZoom' => 'fadeZoom',
    'growX' => 'growX',
    'growY' => 'growY',
    'scrollUp' => 'scrollUp',
    'scrollDown' => 'scrollDown',
    'scrollLeft' => 'scrollLeft',
    'scrollRight' => 'scrollRight',
    'scrollHorz' => 'scrollHorz',
    'scrollVert' => 'scrollVert',
    'shuffle' => 'shuffle',
    'slideX' => 'slideX',
    'slideY' => 'slideY',
    'toss' => 'toss',
    'turnUp' => 'turnUp',
    'turnDown' => 'turnDown',
    'turnLeft' => 'turnLeft',
    'turnRight' => 'turnRight',
    'uncover' => 'uncover',
    'wipe' => 'wipe',
    'zoom' => 'zoom',
  );
  $form['carousel_cycle']['carousel_cycle_effect'] = array(
    '#type' => 'select',
    '#title' => t('Effect'),
    '#options' => $effects,
    '#default_value' => variable_get('carousel_cycle_effect', 'fade'),
    '#description' => t('The transition effect that will be used to change between images. Not all options below may be relevant depending on the effect. ' . l('Follow this link to see examples of each effect.', 'http://jquery.malsup.com/cycle/browser.html', array('attributes' => array('target' => '_blank')))),
  );
  $form['carousel_cycle']['carousel_pager'] = array(
    '#type' => 'checkbox',
    '#title' => t('Pager'),
    '#default_value' => variable_get('carousel_pager', FALSE),
  );
  $form['carousel_cycle']['carousel_controls'] = array(
    '#type' => 'checkbox',
    '#title' => t('Controls'),
    '#default_value' => variable_get('carousel_controls', FALSE),
  );
  $form['carousel_classes'] = array(
    '#type' => 'textarea',
    '#title' => t('Carousel Classes'),
    '#description' => t('The elements have those classes will carousel. Please enter one class per line'),
    '#cols' => 60,
    '#rows' => 5,
    '#default_value' =>  variable_get('carousel_classes', 'carousel')
  );

  return system_settings_form($form);
}

