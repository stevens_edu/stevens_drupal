/**
 *  @file
 *  A simple jQuery Cycle Div Slideshow Rotator.
 */

/**
 * This will set our initial behavior, by starting up each individual slideshow.
 */
 (function($) {
    Drupal.behaviors.stevensCarouselCycle = {
        attach: function(context, settings) {
			// this is a hack to hide video tag on the iPad
			// var i = 0;
			// $('.banner_carousel').children('div.sit-carousel-column').each(function(){
			// 	
			// 	if(i >= 1){
			// 		// alert('oki');
			// 		$(this).children('video').hide();
			// 	}
			// 	i++;
			// });
            // this code has borrowed from the Omega theme, so we need some good mechenism to avoid this.
            if (Drupal.settings.omega) {
                // alert('oki');
                var current = 'other';
                $('body', context).once('omega-mediaqueries-carousel',
                function() {
                    var primary = $.inArray(Drupal.settings.omega.layouts.primary, Drupal.settings.omega.layouts.order);
                    var dummy = $('<div id="omega-media-query-dummy-carousel"></div>').prependTo('body');

                    dummy.append('<style media="all">#omega-media-query-dummy-carousel { position: relative; z-index: -1; }</style>');
                    dummy.append('<!--[if (lt IE 9)&(!IEMobile)]><style media="all">#omega-media-query-dummy-carousel { z-index: ' + primary + '; }</style><![endif]-->');

                    for (var i in Drupal.settings.omega.layouts.order) {
                        dummy.append('<style media="' + Drupal.settings.omega.layouts.queries[Drupal.settings.omega.layouts.order[i]] + '">#omega-media-query-dummy-carousel { z-index: ' + i + '; }</style>');
                    }
                    var css_index = dummy.css('z-index');
                    // alert(css_index);
                    current = Drupal.settings.omega.layouts.order.hasOwnProperty(css_index) ? Drupal.settings.omega.layouts.order[css_index] : 'mobile';
                });
                // alert(current);
                // -- end of Omega theme code
                // console.log(current);
                Drupal.behaviors.stevensCarouselCycle.create(context, settings, current);
            }
        }
    };

    Drupal.behaviors.stevensCarouselCycle.create = function(context, settings, layout) {
        var classes = settings.stevensCarousel.classes;
        var presets = settings.stevensCarousel.opts;
        $.each(classes,
        function(key, value) {
            if ($('.' + value)[0]) {
                $('.' + value).each(function() {
                    var settings = presets[value].settings;
                    var cycle_settings = {};
                    // if pager exits
                    if (settings.carousel_pager == 1) {
                        // @todo add to drupal settings
                        switch (settings.carousel_pager_type) {
                        case 'full':
                            var markup = '<div id="' + value + '-pagination" class="carousel-pager"></div>';
                            cycle_settings['pagination'] = '#' + value + '-pagination';
                            break;
                        case 'mini':
                            var markup = '<div class=".carousel-pager-mini"><a href="#" id="prev">Prev</a> <a href="#" id="next">Next</a></div>';
                            cycle_settings['next'] = '#next';
                            cycle_settings['prev'] = '#prev';
                            break;
                        }
                        switch (settings.carousel_pager_position) {
                        case 'top':
                            $(this).before(markup);
                            break;
                        case 'bottom':
                            $(this).after(markup);
                            break;
                        }
                    }
                    var items = {};
                    var scroll = {};
                    var visible_items = settings['number_of_items_visible_' + layout];
                    // alert(settings.number_of_items_visible_+layout);
                    if (visible_items) {
                        items['visible'] = parseInt(visible_items);
                    }
                    // if any transitions set
                    if (settings.carousel_effect != 'none') {
                        scroll['fx'] = settings.carousel_effect;
                        if (settings.carousel_effect_duration) {
                            scroll['duration'] = parseInt(settings.carousel_effect_duration);
                        }
                    }
                    if (settings.carousel_pause_on_hover == 1) {
                        scroll['pauseOnHover'] = true;
                    }
                    var scroll_items = settings['number_of_items_scroll_' + layout];
                    if (scroll_items) {
                        scroll['items'] = parseInt(scroll_items);
                    }
                    if (settings.carousel_controls == 1) {
                        $(this).after('<ul class="sit-carousel-controls"><li class="previous"><a id="' + value + '-prev" href="#"><span>Prev</span></a></li><li class="next"><a id="' + value + '-next" href="#"><span>Next</span></a></li></ul>');
                        cycle_settings['next'] = '#' + value + '-next';
                        cycle_settings['prev'] = '#' + value + '-prev';
                    }
                    if (settings.carousel_circular != 1)
                    cycle_settings['circular'] = false;
                    if (settings.carousel_direction) {
                        cycle_settings['direction'] = settings.carousel_direction;
                    }
                    cycle_settings['items'] = items;
					// callback to hide/show video items to fix iPad issue
					// scroll['onBefore'] = function(oldItems, newItems, newSizes, duration){
					// 	// alert('okiu');
					// 	// console.log(newItems);
					// 	$(newItems).children('video').show();
					// 	$(oldItems).children('video').hide();
					// };
                    cycle_settings['scroll'] = scroll;
                    var auto = {};
                    auto['play'] = false;
                    if (settings.carousel_auto == 1) {
                        auto['play'] = true;
                    }
                    if (settings.carousel_pause_duration) {
                        auto['pauseDuration'] = parseInt(settings.carousel_pause_duration);
                    }
                    cycle_settings['auto'] = auto;
                    // console.log(settings.carousel_pause_duration);
                    $(this).carouFredSel(cycle_settings);

                    $('.banner_carousel .media-outer-wrapper').mousedown(function() {
                      // Stop the carousel when video is played. Have to use this hack
                      // because the embed eats the .click().
                      var auto;
                      $('.banner_carousel').trigger('configuration', ['auto', function(value) { auto = value; }]);
                      auto['play'] = false;
                      $('.banner_carousel').trigger('configuration', ['auto', auto, true]);
                    });

                });
            }
        });
    }

    // window
    $(window).resize(function() {
        // console.log(Drupal.omega.getCurrentLayout());
        var current_layout = Drupal.omega.getCurrentLayout();
        var previous_layout = Drupal.omega.getPreviousLayout();
        // alert(current_layout);
        if (previous_layout != current_layout) {
            switch (current_layout) {
            case 'wide':
                Drupal.behaviors.stevensCarouselCycle.create('carousel', Drupal.settings, 'wide');
                break;
            case 'normal':
                Drupal.behaviors.stevensCarouselCycle.create('carousel', Drupal.settings, 'normal');
                break;
            case 'narrow':
                Drupal.behaviors.stevensCarouselCycle.create('carousel', Drupal.settings, 'narrow');
                break;
            case 'mobile':
                Drupal.behaviors.stevensCarouselCycle.create('carousel', Drupal.settings, 'mobile');
                break;
            }
        }
    });
})(jQuery);
