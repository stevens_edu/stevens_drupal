<?php

/**
 * Define this Export UI plugin.
 */
$plugin = array(
  'schema' => 'carousel_settings_presets',
  'access' => 'administer site configuration',
  'menu' => array(
    'menu prefix' => 'admin/config/media',
    'menu item' => 'carousel',
    'menu title' => 'Carousel Settings',
    'menu description' => 'Carousel settings management.',
  ),
  'title singular' => t('preset'),
  'title plural' => t('presets'),
  'title singular proper' => t('Carousel settings preset'),
  'title plural proper' => t('Carousel settings presets'),
  'use wizard' => TRUE,
  'form info' => array(
    'add order' => array(
      'basic' => t('Basic settings'),
    ),
    'edit order' => array(
      'basic' => t('Basic preset settings'),
    ),
    'clone order' => array(
      'basic' => t('Basic preset settings'),
    ),
    'forms' => array(
      'basic' => array(
        'form id' => 'sit_carousel_settings_basic',
      ),
      'edit' => array(
        'form id' => 'sit_carousel_settings_edit',
      ),
    ),
  ),
);

/**
 * Define the preset add/edit form. This is basic preset information
 * form callback. It configures machine name, etc, ...
 */
function sit_carousel_settings_basic($form, &$form_state) {
  $new = $form_state['form type'] == 'add';
  $form['#attributes']['class'][] = $new ? 'add-preset' : 'edit-preset';

  if (!$new) {
    $form['ctools_trail']['#markup'] = str_replace('»', '', $form['ctools_trail']['#markup']);
    $form['wrapper']['#type'] = 'fieldset';
  }

  $form['wrapper']['buttons'] = $form['buttons'];
  unset($form['buttons']);

  $form['wrapper']['machine_name'] = array(
    '#type' => 'machine_name',
    '#title' => t('Machine name'),
    '#required' => TRUE,
    '#description' => t("Enter preset's machine name."),
    '#machine_name' => array(
      'exists' => '_carousel_machine_name_check',
    ),
    '#default_value' => isset($form_state['item']->machine_name) ? $form_state['item']->machine_name : '',
    '#disabled' => !$new && $new = $form_state['form type'] != 'clone',
  );

  $form['wrapper']['basic'] = array(
    '#type' => 'fieldset',
    '#title' => t('Basic options'),
    '#collapsible' => TRUE,
  );

  $effects = array(
    'none' => 'none',
    'scroll' => 'scroll',
    'directscroll' => 'directscroll',
    'fade' => 'fade',
    'crossfade' => 'crossfade',
    'cover' => 'cover',
    'uncover' => 'uncover',
  );

  $form['wrapper']['basic']['carousel_effect'] = array(
    '#type' => 'select',
    '#title' => t('Effect'),
    '#options' => $effects,
    '#default_value' => isset($form_state['item']->settings['carousel_effect']) ? $form_state['item']->settings['carousel_effect'] : 'fade',
    '#description' => t('The transition effect that will be used to change between images. Not all options below may be relevant depending on the effect.'),
  );

  $form['wrapper']['basic']['carousel_effect_duration'] = array(
    '#type' => 'textfield',
    '#title' => t('Effect duration'),
    '#description' => t('Duration of the scroll slide.'),
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => isset($form_state['item']->settings['carousel_effect_duration']) ? $form_state['item']->settings['carousel_effect_duration'] : 500,
  );

  $form['wrapper']['basic']['carousel_pause_duration'] = array(
    '#type' => 'textfield',
    '#title' => t('Pause duration'),
    '#description' => t('Duration of the pause slide.'),
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => isset($form_state['item']->settings['carousel_pause_duration']) ? $form_state['item']->settings['carousel_pause_duration'] : 500,
  );

  $form['wrapper']['basic']['carousel_pager'] = array(
    '#type' => 'checkbox',
    '#title' => t('Pager'),
    '#default_value' => isset($form_state['item']->settings['carousel_pager']) ? $form_state['item']->settings['carousel_pager'] : FALSE,
  );

  $form['wrapper']['basic']['carousel_pager_type'] = array(
    '#type' => 'select',
    '#title' => t('Carousel Pager Type'),
    '#options' => array(
      'full' => 'Full Pager',
      'mini' => 'Mini Pager',
    ),
    '#states' => array(
      'visible' => array(
        'input[name="carousel_pager"]' => array('checked' => TRUE),
      ),
    ),
    '#default_value' => isset($form_state['item']->settings['carousel_pager_type']) ? $form_state['item']->settings['carousel_pager_type'] : 'full',
  );

  $form['wrapper']['basic']['carousel_pager_position'] = array(
    '#type' => 'select',
    '#title' => t('Carousel Pager Position'),
    '#options' => array(
      'top' => 'Top',
      'bottom' => 'Bottom',
    ),
    '#states' => array(
      'visible' => array(
        'input[name="carousel_pager"]' => array('checked' => TRUE),
      ),
    ),
    '#default_value' => isset($form_state['item']->settings['carousel_pager_position']) ? $form_state['item']->settings['carousel_pager_position'] : 'top',
  );

  $form['wrapper']['basic']['carousel_controls'] = array(
    '#type' => 'checkbox',
    '#title' => t('Controls'),
    '#default_value' => isset($form_state['item']->settings['carousel_controls']) ? $form_state['item']->settings['carousel_controls'] : FALSE,
  );
  $form['wrapper']['basic']['carousel_circular'] = array(
    '#type' => 'checkbox',
    '#title' => t('Circular Motion'),
    '#default_value' => isset($form_state['item']->settings['carousel_circular']) ? $form_state['item']->settings['carousel_circular'] : FALSE,
  );
  // settings for the layouts
  $form['wrapper']['basic']['layout'] = array(
    '#type' => 'fieldset',
    '#title' => t('Basic layout options'),
    '#collapsible' => TRUE,
	// '#collapsed' => TRUE
  );
  $layouts = array('wide', 'normal', 'narrow', 'mobile');
  foreach ($layouts as $layout) {
    $form['wrapper']['basic']['layout']['number_of_items_visible_' . $layout] = array(
      '#type' => 'select',
      '#title' => t('Number of items to visible:' . $layout),
      '#options' => array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5),
      '#default_value' => isset($form_state['item']->settings['number_of_items_visible_' . $layout]) ? $form_state['item']->settings['number_of_items_visible_' . $layout] : 4,
    );
    $form['wrapper']['basic']['layout']['number_of_items_scroll_' . $layout] = array(
      '#type' => 'select',
      '#title' => t('Number of items to scroll:' . $layout),
      '#options' => array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5),
      '#default_value' => isset($form_state['item']->settings['number_of_items_scroll_' . $layout]) ? $form_state['item']->settings['number_of_items_scroll_' . $layout] : 1,
    );
  }
  $form['wrapper']['basic']['carousel_direction'] = array(
    '#type' => 'select',
    '#title' => t('Direction'),
    '#options' => array('right' => 'right', 'left' => 'left', 'up' => 'up', 'down' => 'down'),
    '#default_value' => isset($form_state['item']->settings['carousel_direction']) ? $form_state['item']->settings['carousel_direction'] : 'left',
  );
  $form['wrapper']['basic']['carousel_auto'] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto Start'),
    '#default_value' => isset($form_state['item']->settings['carousel_auto']) ? $form_state['item']->settings['carousel_auto'] : FALSE,
  );
  $form['wrapper']['basic']['carousel_pause_on_hover'] = array(
    '#type' => 'checkbox',
    '#title' => t('Pause on Hover'),
    '#default_value' => isset($form_state['item']->settings['carousel_pause_on_hover']) ? $form_state['item']->settings['carousel_pause_on_hover'] : FALSE,
  );
  $form['wrapper']['basic']['carousel_responsive'] = array(
    '#type' => 'checkbox',
    '#title' => t('Responsive carousel'),
    '#default_value' => isset($form_state['item']->settings['carousel_responsive']) ? $form_state['item']->settings['carousel_responsive'] : FALSE,
  );
  return $form;
}

function sit_carousel_settings_basic_submit($form, &$form_state) {
  $form_state['item']->machine_name = $form_state['values']['machine_name'];
  $form_state['item']->settings['carousel_effect'] = $form_state['values']['carousel_effect'];
  $form_state['item']->settings['carousel_effect_duration'] = $form_state['values']['carousel_effect_duration'];
  $form_state['item']->settings['carousel_pause_duration'] = $form_state['values']['carousel_pause_duration'];
  $form_state['item']->settings['carousel_pager'] = $form_state['values']['carousel_pager'];
  $form_state['item']->settings['carousel_pager_type'] = $form_state['values']['carousel_pager_type'];
  $form_state['item']->settings['carousel_pager_position'] = $form_state['values']['carousel_pager_position'];
  $layouts = array('wide', 'normal', 'narrow', 'mobile');
  foreach ($layouts as $layout) {
    $form_state['item']->settings['number_of_items_visible_'.$layout] = $form_state['values']['number_of_items_visible_'.$layout];
    $form_state['item']->settings['number_of_items_scroll_'.$layout] = $form_state['values']['number_of_items_scroll_'.$layout];
  }
  $form_state['item']->settings['carousel_controls'] = $form_state['values']['carousel_controls'];
  $form_state['item']->settings['carousel_circular'] = $form_state['values']['carousel_circular'];
  $form_state['item']->settings['carousel_direction'] = $form_state['values']['carousel_direction'];
  $form_state['item']->settings['carousel_auto'] = $form_state['values']['carousel_auto'];
  $form_state['item']->settings['carousel_pause_on_hover'] = $form_state['values']['carousel_pause_on_hover'];
  $form_state['item']->settings['carousel_responsive'] = $form_state['values']['carousel_responsive'];
  // @todo Clear cached presets.
  cache_clear_all('carousel_settings:export_ui:settings', 'cache');
}

/**
 * Check for existing presetn machine names
 */
function _carousel_machine_name_check($name) {
  $presets = sit_carousel_get_settings_presets();
  return isset($presets[$name]);
}

