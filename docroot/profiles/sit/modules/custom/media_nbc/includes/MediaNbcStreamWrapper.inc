<?php

/**
 *  @file
 *  Create a NBC Stream Wrapper class for the Media/Resource module.
 */

/**
 *  Create an instance like this:
 *  $nbc = new ResourceNbcStreamWrapper('nbc://?v=[video-code]');
 */
class MediaNbcStreamWrapper extends MediaReadOnlyStreamWrapper {
  protected $base_url = 'http://www.nbc.com';

  function getTarget($f) {
    return FALSE;
  }

  static function getMimeType($uri, $mapping = NULL) {
    return 'video/nbc';
  }

  function getOriginalThumbnailPath() {
	//@todo, change this
    return 'http://cdn.blogsdna.com/wp-content/uploads/2009/09/video-thumbnails.png';
  }

  function getLocalThumbnailPath() {
    $parts = $this->get_parameters();
    $local_path = 'public://media-nbc/' . check_plain($parts['v']) . '.jpg';
    if (!file_exists($local_path)) {
      $dirname = drupal_dirname($local_path);
      file_prepare_directory($dirname, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      @copy($this->getOriginalThumbnailPath(), $local_path);
    }
    return $local_path;
  }

  function interpolateUrl() {
    if ($parameters = $this->get_parameters()) {
      return $this->base_url . '/' . $parameters['v'];
    }
  }
}
