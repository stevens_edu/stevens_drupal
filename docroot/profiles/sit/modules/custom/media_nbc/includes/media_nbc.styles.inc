<?php

/**
 * @file media_nbc/includes/media_nbc.styles.inc
 * Styles definitions for Media: NBC.
 */

/**
 * Implementation of Styles module hook_styles_register().
 */
function media_nbc_styles_register() {
  return array(
    'MediaNbcStyles' => array(
      'field_types' => 'file',
      'name' => t('MediaNBC'),
      'description' => t('Media NBC styles.'),
      'path' => drupal_get_path('module', 'media_nbc') .'/includes',
      'file' => 'media_nbc.styles.inc',
    ),
  );
}

/**
 *  Implements hook_styles_containers(). (Deprecated in version 2)
 */
function media_nbc_styles_containers() {
  return array(
    'file' => array(
      'containers' => array(
        'media_nbc' => array(
          'label' => t('NBC Styles'),
          'data' => array(
            'streams' => array(
              'nbc',
            ),
            'mimetypes' => array(
              'video/nbc',
            ),
          ),
          'weight' => 0,
          'filter callback' => 'media_nbc_formatter_filter',
          'themes' => array(
            'field_formatter_styles' => 'media_nbc_field_formatter_styles',
            'styles' => 'media_nbc_styles',
            'preview' => 'media_nbc_preview_style',
          ),
          'description' => t('NBC Styles will display embedded NBC videos and thumbnails to your choosing, such as by resizing, setting colors, and autoplay. You can !manage.', array('!manage' => l(t('manage your NBC styles here'), 'admin/config/media/media-nbc-styles'))),
        ),
      ),
    ),
  );
}

function media_nbc_formatter_filter($variables) {
  if (isset($variables['object'])) {
    $object = $variables['object'];
    return (file_uri_scheme($object->uri) == 'nbc') && ($object->filemime == 'video/nbc');
  }
}

/**
 * Implementation of the File Styles module's hook_file_styles_filter().
 */
function media_nbc_file_styles_filter($object) {
  if ((file_uri_scheme($object->uri) == 'nbc') && ($object->filemime == 'video/nbc')) {
    return 'media_nbc';
  }
}

/**
 *  Implements hook_styles_styles().
 */
function media_nbc_styles_styles() {
  $styles = array(
    'file' => array(
      'containers' => array(
        'media_nbc' => array(
          'styles' => array(
            'nbc_thumbnail' => array(
              'name' => 'nbc_thumbnail',
              'effects' => array(
                array('label' => t('Thumbnail'), 'name' => 'thumbnail', 'data' => array('thumbnail' => 1)),
                array('label' => t('Resize'), 'name' => 'resize', 'data' => array('width' => 100, 'height' => 75)),
              ),
            ),
            'nbc_preview' => array(
              'name' => 'nbc_preview',
              'effects' => array(
                array('label' => t('Autoplay'), 'name' => 'autoplay', 'data' => array('autoplay' => 1)),
                array('label' => t('Resize'), 'name' => 'resize', 'data' => array('width' => 220, 'height' => 165)),
              ),
            ),
            'nbc_full' => array(
              'name' => 'nbc_full',
              'effects' => array(
                array('label' => t('Autoplay'), 'name' => 'autoplay', 'data' => array('autoplay' => 0)),
                array('label' => t('Resize'), 'name' => 'resize', 'data' => array('width' => 640, 'height' => 480)),
                array('label' => t('Full screen'), 'name' => 'fullscreen', 'data' => array('fullscreen' => 1)),
              ),
            ),
          ),
        ),
      ),
    ),
  );

  // Allow any image style to be applied to the thumbnail.
  foreach (image_styles() as $style_name => $image_style) {
    $styles['file']['containers']['media_nbc']['styles']['nbc_thumbnail_' . $style_name] = array(
      'name' => 'nbc_thumbnail_' . $style_name,
      'image_style' => $style_name,
      'effects' => array(
        array('label' => t('Thumbnail'), 'name' => 'thumbnail', 'data' => array('thumbnail' => 1)),
      ),
    );
  }

  return $styles;
}

/**
 *  Implements hook_styles_presets().
 */
function media_nbc_styles_presets() {
  $presets = array(
    'file' => array(
      'square_thumbnail' => array(
        'media_nbc' => array(
          'nbc_thumbnail_square_thumbnail',
        ),
      ),
      'thumbnail' => array(
        'media_nbc' => array(
          'nbc_thumbnail',
        ),
      ),
      'small' => array(
        'media_nbc' => array(
          'nbc_preview',
        ),
      ),
      'large' => array(
        'media_nbc' => array(
          'nbc_full',
        ),
      ),
      'original' => array(
        'media_nbc' => array(
          'nbc_full',
        ),
      ),
    ),
  );
  return $presets;
}

/**
 * Implementation of Styles module hook_styles_default_containers().
 */
function media_nbc_styles_default_containers() {
  // We append NBC to the file containers.
  return array(
    'file' => array(
      'containers' => array(
        'media_nbc' => array(
          'class' => 'MediaNbcStyles',
          'name' => 'media_nbc',
          'label' => t('NBC'),
          'preview' => 'media_nbc_preview_style',
        ),
      ),
    ),
  );
}


/**
 * Implementation of Styles module hook_styles_default_presets().
 */
function media_nbc_styles_default_presets() {
  $presets = array(
    'file' => array(
      'containers' => array(
        'media_nbc' => array(
          'default preset' => 'unlinked_thumbnail',
          'styles' => array(
            'original' => array(
              'default preset' => 'video',
            ),
            'thumbnail' => array(
              'default preset' => 'linked_thumbnail',
            ),
            'square_thumbnail' => array(
              'default preset' => 'linked_square_thumbnail',
            ),
            'medium' => array(
              'default preset' => 'linked_medium',
            ),
            'large' => array(
              'default preset' => 'large_video',
            ),
          ),
          'presets' => array(
            'video' => array(
              array(
                'name' => 'video',
                'settings' => array(),
              ),
            ),
            'large_video' => array(
              array(
                'name' => 'resize',
                'settings' => array(
                  'width' => 640,
                  'height' => 390,
                ),
              ),
              array(
                'name' => 'video',
                'settings' => array(),
              ),
            ),
          ),
        ),
      ),
    ),
  );
  // Allow any image style to be applied to the thumbnail.
  foreach (image_styles() as $style_name => $image_style) {
    $presets['file']['containers']['media_nbc']['presets']['linked_' . $style_name] = array(
      array(
        'name' => 'linkToMedia',
        'settings' => array(),
      ),
      array(
        'name' => 'imageStyle',
        'settings' => array(
          'image_style' => $style_name,
        ),
      ),
      array(
        'name' => 'thumbnail',
        'settings' => array(),
      ),
    );
    $presets['file']['containers']['media_nbc']['presets']['unlinked_' . $style_name] = $presets['file']['containers']['media_nbc']['presets']['linked_' . $style_name];
    array_shift($presets['file']['containers']['media_nbc']['presets']['unlinked_' . $style_name]);
  }
  return $presets;
}
