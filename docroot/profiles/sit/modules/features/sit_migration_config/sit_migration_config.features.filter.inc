<?php
/**
 * @file
 * sit_migration_config.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function sit_migration_config_filter_default_formats() {
  $formats = array();

  // Exported format: Purified HTML
  $formats['purified_html'] = array(
    'format' => 'purified_html',
    'name' => 'Purified HTML',
    'cache' => '1',
    'status' => '1',
    'weight' => '0',
    'filters' => array(
      'htmlpurifier_basic' => array(
        'weight' => '0',
        'status' => '1',
        'settings' => array(
          'htmlpurifier_help' => 1,
          'htmlpurifier_basic_config' => array(
            'Attr.EnableID' => '0',
            'AutoFormat.AutoParagraph' => '1',
            'AutoFormat.Linkify' => '1',
            'AutoFormat.RemoveEmpty' => '0',
            'HTML.Allowed' => 'p, a[href], h2, h3, h4, h5, h6, blockquote, br, ul, ol, li, dl, dt, dd, span, object, strong, em, table, tr, td, th, tbody, thead, tfoot, img[src], abbr, cite, code, pre, p, sup, sub, var',
            'HTML.ForbiddenAttributes' => '',
            'HTML.ForbiddenElements' => '',
            'HTML.SafeObject' => '1',
            'Output.FlashCompat' => '1',
            'URI.DisableExternalResources' => '0',
            'URI.DisableResources' => '0',
            'Null_URI.Munge' => '1',
          ),
        ),
      ),
    ),
  );

  return $formats;
}
