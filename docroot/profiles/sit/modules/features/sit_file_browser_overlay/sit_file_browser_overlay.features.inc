<?php
/**
 * @file
 * sit_file_browser_overlay.features.inc
 */

/**
 * Implements hook_views_api().
 */
function sit_file_browser_overlay_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}
