<?php
/**
 * @file
 * all_sites_global.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function all_sites_global_user_default_roles() {
  $roles = array();

  // Exported role: Administrator
  $roles['Administrator'] = array(
    'name' => 'Administrator',
    'weight' => '7',
    'machine_name' => 'administrator',
  );

  // Exported role: Content Manager
  $roles['Content Manager'] = array(
    'name' => 'Content Manager',
    'weight' => '4',
    'machine_name' => 'content_manager',
  );

  // Exported role: Site Admin
  $roles['Site Admin'] = array(
    'name' => 'Site Admin',
    'weight' => '5',
    'machine_name' => 'site_admin',
  );

  // Exported role: Stevens Admin
  $roles['Stevens Admin'] = array(
    'name' => 'Stevens Admin',
    'weight' => '6',
    'machine_name' => 'stevens_admin',
  );

  return $roles;
}
