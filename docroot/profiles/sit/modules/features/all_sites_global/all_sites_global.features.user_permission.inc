<?php
/**
 * @file
 * all_sites_global.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function all_sites_global_user_default_permissions() {
  $permissions = array();

  // Exported permission: access administration menu
  $permissions['access administration menu'] = array(
    'name' => 'access administration menu',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'admin_menu',
  );

  // Exported permission: access administration pages
  $permissions['access administration pages'] = array(
    'name' => 'access administration pages',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'system',
  );

  // Exported permission: access all webform results
  $permissions['access all webform results'] = array(
    'name' => 'access all webform results',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'webform',
  );

  // Exported permission: access content
  $permissions['access content'] = array(
    'name' => 'access content',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
      2 => 'anonymous user',
      3 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: access content overview
  $permissions['access content overview'] = array(
    'name' => 'access content overview',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'node',
  );

  // Exported permission: access overlay
  $permissions['access overlay'] = array(
    'name' => 'access overlay',
    'roles' => array(
      0 => 'Content Manager',
      1 => 'Site Admin',
      2 => 'Stevens Admin',
    ),
    'module' => 'overlay',
  );

  // Exported permission: access toolbar
  $permissions['access toolbar'] = array(
    'name' => 'access toolbar',
    'roles' => array(
      0 => 'Content Manager',
      1 => 'Site Admin',
    ),
    'module' => 'toolbar',
  );

  // Exported permission: access workbench
  $permissions['access workbench'] = array(
    'name' => 'access workbench',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Content Manager',
      2 => 'Site Admin',
      3 => 'Stevens Admin',
    ),
    'module' => 'workbench',
  );

  // Exported permission: access workbench access by role
  $permissions['access workbench access by role'] = array(
    'name' => 'access workbench access by role',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Content Manager',
      2 => 'Site Admin',
      3 => 'Stevens Admin',
    ),
    'module' => 'workbench_access',
  );

  // Exported permission: administer advanced pane settings
  $permissions['administer advanced pane settings'] = array(
    'name' => 'administer advanced pane settings',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'panels',
  );

  // Exported permission: administer blocks
  $permissions['administer blocks'] = array(
    'name' => 'administer blocks',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'block',
  );

  // Exported permission: administer custom content
  $permissions['administer custom content'] = array(
    'name' => 'administer custom content',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'ctools_custom_content',
  );

  // Exported permission: administer menu
  $permissions['administer menu'] = array(
    'name' => 'administer menu',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'menu',
  );

  // Exported permission: administer mini panels
  $permissions['administer mini panels'] = array(
    'name' => 'administer mini panels',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'panels_mini',
  );

  // Exported permission: administer nodes
  $permissions['administer nodes'] = array(
    'name' => 'administer nodes',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'node',
  );

  // Exported permission: administer pane access
  $permissions['administer pane access'] = array(
    'name' => 'administer pane access',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'panels',
  );

  // Exported permission: administer panels layouts
  $permissions['administer panels layouts'] = array(
    'name' => 'administer panels layouts',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'panels',
  );

  // Exported permission: administer panels styles
  $permissions['administer panels styles'] = array(
    'name' => 'administer panels styles',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'panels',
  );

  // Exported permission: administer shortcuts
  $permissions['administer shortcuts'] = array(
    'name' => 'administer shortcuts',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'shortcut',
  );

  // Exported permission: administer taxonomy
  $permissions['administer taxonomy'] = array(
    'name' => 'administer taxonomy',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: administer workbench
  $permissions['administer workbench'] = array(
    'name' => 'administer workbench',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'workbench',
  );

  // Exported permission: administer workbench access
  $permissions['administer workbench access'] = array(
    'name' => 'administer workbench access',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Site Admin',
      2 => 'Stevens Admin',
    ),
    'module' => 'workbench_access',
  );

  // Exported permission: administer workbench moderation
  $permissions['administer workbench moderation'] = array(
    'name' => 'administer workbench moderation',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: assign workbench access
  $permissions['assign workbench access'] = array(
    'name' => 'assign workbench access',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Site Admin',
      2 => 'Stevens Admin',
    ),
    'module' => 'workbench_access',
  );

  // Exported permission: batch update workbench access
  $permissions['batch update workbench access'] = array(
    'name' => 'batch update workbench access',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Site Admin',
      2 => 'Stevens Admin',
    ),
    'module' => 'workbench_access',
  );

  // Exported permission: bypass node access
  $permissions['bypass node access'] = array(
    'name' => 'bypass node access',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'node',
  );

  // Exported permission: bypass workbench moderation
  $permissions['bypass workbench moderation'] = array(
    'name' => 'bypass workbench moderation',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Site Admin',
      2 => 'Stevens Admin',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: create event content
  $permissions['create event content'] = array(
    'name' => 'create event content',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Content Manager',
      2 => 'Site Admin',
      3 => 'Stevens Admin',
    ),
    'module' => 'node',
  );

  // Exported permission: create mini panels
  $permissions['create mini panels'] = array(
    'name' => 'create mini panels',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'panels_mini',
  );

  // Exported permission: create news content
  $permissions['create news content'] = array(
    'name' => 'create news content',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Content Manager',
      2 => 'Site Admin',
      3 => 'Stevens Admin',
    ),
    'module' => 'node',
  );

  // Exported permission: create url aliases
  $permissions['create url aliases'] = array(
    'name' => 'create url aliases',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'path',
  );

  // Exported permission: customize shortcut links
  $permissions['customize shortcut links'] = array(
    'name' => 'customize shortcut links',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'shortcut',
  );

  // Exported permission: delete any event content
  $permissions['delete any event content'] = array(
    'name' => 'delete any event content',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Site Admin',
      2 => 'Stevens Admin',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any news content
  $permissions['delete any news content'] = array(
    'name' => 'delete any news content',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Site Admin',
      2 => 'Stevens Admin',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own event content
  $permissions['delete own event content'] = array(
    'name' => 'delete own event content',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Content Manager',
      2 => 'Site Admin',
      3 => 'Stevens Admin',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own news content
  $permissions['delete own news content'] = array(
    'name' => 'delete own news content',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Content Manager',
      2 => 'Site Admin',
      3 => 'Stevens Admin',
    ),
    'module' => 'node',
  );

  // Exported permission: delete revisions
  $permissions['delete revisions'] = array(
    'name' => 'delete revisions',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Content Manager',
      2 => 'Site Admin',
      3 => 'Stevens Admin',
    ),
    'module' => 'node',
  );

  // Exported permission: display drupal links
  $permissions['display drupal links'] = array(
    'name' => 'display drupal links',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'admin_menu',
  );

  // Exported permission: ds_switch page
  $permissions['ds_switch page'] = array(
    'name' => 'ds_switch page',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'ds_extras',
  );

  // Exported permission: edit any event content
  $permissions['edit any event content'] = array(
    'name' => 'edit any event content',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Site Admin',
      2 => 'Stevens Admin',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any news content
  $permissions['edit any news content'] = array(
    'name' => 'edit any news content',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Site Admin',
      2 => 'Stevens Admin',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any webform content
  $permissions['edit any webform content'] = array(
    'name' => 'edit any webform content',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own event content
  $permissions['edit own event content'] = array(
    'name' => 'edit own event content',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Content Manager',
      2 => 'Site Admin',
      3 => 'Stevens Admin',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own news content
  $permissions['edit own news content'] = array(
    'name' => 'edit own news content',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Content Manager',
      2 => 'Site Admin',
      3 => 'Stevens Admin',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own webform content
  $permissions['edit own webform content'] = array(
    'name' => 'edit own webform content',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'node',
  );

  // Exported permission: flush caches
  $permissions['flush caches'] = array(
    'name' => 'flush caches',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'admin_menu',
  );

  // Exported permission: moderate content from draft to needs_review
  $permissions['moderate content from draft to needs_review'] = array(
    'name' => 'moderate content from draft to needs_review',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Content Manager',
      2 => 'Site Admin',
      3 => 'Stevens Admin',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: moderate content from needs_review to draft
  $permissions['moderate content from needs_review to draft'] = array(
    'name' => 'moderate content from needs_review to draft',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Site Admin',
      2 => 'Stevens Admin',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: moderate content from needs_review to published
  $permissions['moderate content from needs_review to published'] = array(
    'name' => 'moderate content from needs_review to published',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Site Admin',
      2 => 'Stevens Admin',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: revert revisions
  $permissions['revert revisions'] = array(
    'name' => 'revert revisions',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Content Manager',
      2 => 'Site Admin',
      3 => 'Stevens Admin',
    ),
    'module' => 'node',
  );

  // Exported permission: search content
  $permissions['search content'] = array(
    'name' => 'search content',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
      2 => 'anonymous user',
      3 => 'authenticated user',
    ),
    'module' => 'search',
  );

  // Exported permission: switch shortcut sets
  $permissions['switch shortcut sets'] = array(
    'name' => 'switch shortcut sets',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'shortcut',
  );

  // Exported permission: use page manager
  $permissions['use page manager'] = array(
    'name' => 'use page manager',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'page_manager',
  );

  // Exported permission: use panels caching features
  $permissions['use panels caching features'] = array(
    'name' => 'use panels caching features',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'panels',
  );

  // Exported permission: use panels dashboard
  $permissions['use panels dashboard'] = array(
    'name' => 'use panels dashboard',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'panels',
  );

  // Exported permission: use panels in place editing
  $permissions['use panels in place editing'] = array(
    'name' => 'use panels in place editing',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'panels',
  );

  // Exported permission: use panels locks
  $permissions['use panels locks'] = array(
    'name' => 'use panels locks',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'panels',
  );

  // Exported permission: use text format ds_code
  $permissions['use text format ds_code'] = array(
    'name' => 'use text format ds_code',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'filter',
  );

  // Exported permission: use text format full_html
  $permissions['use text format full_html'] = array(
    'name' => 'use text format full_html',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'filter',
  );

  // Exported permission: use text format purified_html
  $permissions['use text format purified_html'] = array(
    'name' => 'use text format purified_html',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'filter',
  );

  // Exported permission: use workbench_media add form
  $permissions['use workbench_media add form'] = array(
    'name' => 'use workbench_media add form',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Content Manager',
      2 => 'Site Admin',
      3 => 'Stevens Admin',
    ),
    'module' => 'workbench_media',
  );

  // Exported permission: use workbench_moderation my drafts tab
  $permissions['use workbench_moderation my drafts tab'] = array(
    'name' => 'use workbench_moderation my drafts tab',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Content Manager',
      2 => 'Site Admin',
      3 => 'Stevens Admin',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: use workbench_moderation needs review tab
  $permissions['use workbench_moderation needs review tab'] = array(
    'name' => 'use workbench_moderation needs review tab',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Site Admin',
      2 => 'Stevens Admin',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: view all unpublished content
  $permissions['view all unpublished content'] = array(
    'name' => 'view all unpublished content',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Site Admin',
      2 => 'Stevens Admin',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: view date repeats
  $permissions['view date repeats'] = array(
    'name' => 'view date repeats',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'date_repeat_field',
  );

  // Exported permission: view moderation history
  $permissions['view moderation history'] = array(
    'name' => 'view moderation history',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Content Manager',
      2 => 'Site Admin',
      3 => 'Stevens Admin',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: view moderation messages
  $permissions['view moderation messages'] = array(
    'name' => 'view moderation messages',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Content Manager',
      2 => 'Site Admin',
      3 => 'Stevens Admin',
    ),
    'module' => 'workbench_moderation',
  );

  // Exported permission: view own unpublished content
  $permissions['view own unpublished content'] = array(
    'name' => 'view own unpublished content',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Content Manager',
      2 => 'Site Admin',
      3 => 'Stevens Admin',
    ),
    'module' => 'node',
  );

  // Exported permission: view pane admin links
  $permissions['view pane admin links'] = array(
    'name' => 'view pane admin links',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Stevens Admin',
    ),
    'module' => 'panels',
  );

  // Exported permission: view revisions
  $permissions['view revisions'] = array(
    'name' => 'view revisions',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Content Manager',
      2 => 'Site Admin',
      3 => 'Stevens Admin',
    ),
    'module' => 'node',
  );

  // Exported permission: view the administration theme
  $permissions['view the administration theme'] = array(
    'name' => 'view the administration theme',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Content Manager',
      2 => 'Site Admin',
      3 => 'Stevens Admin',
    ),
    'module' => 'system',
  );

  // Exported permission: view workbench access information
  $permissions['view workbench access information'] = array(
    'name' => 'view workbench access information',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Site Admin',
      2 => 'Stevens Admin',
    ),
    'module' => 'workbench_access',
  );

  // Exported permission: view workbench taxonomy pages
  $permissions['view workbench taxonomy pages'] = array(
    'name' => 'view workbench taxonomy pages',
    'roles' => array(
      0 => 'Administrator',
      1 => 'Site Admin',
      2 => 'Stevens Admin',
    ),
    'module' => 'workbench_access',
  );

  return $permissions;
}
