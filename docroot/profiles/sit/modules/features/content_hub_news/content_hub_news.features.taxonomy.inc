<?php
/**
 * @file
 * content_hub_news.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function content_hub_news_taxonomy_default_vocabularies() {
  return array(
    'media_relations' => array(
      'name' => 'Media Relations',
      'machine_name' => 'media_relations',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'news_and_events' => array(
      'name' => 'News and Events',
      'machine_name' => 'news_and_events',
      'description' => '',
      'hierarchy' => '1',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'news_feed' => array(
      'name' => 'News feed',
      'machine_name' => 'news_feed',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
  );
}
