<?php
/**
 * @file
 * all_sites_path_config.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function all_sites_path_config_user_default_permissions() {
  $permissions = array();

  // Exported permission: administer pathauto
  $permissions['administer pathauto'] = array(
    'name' => 'administer pathauto',
    'roles' => array(
      0 => 'Administrator',
    ),
    'module' => 'pathauto',
  );

  // Exported permission: administer redirects
  $permissions['administer redirects'] = array(
    'name' => 'administer redirects',
    'roles' => array(
      0 => 'Administrator',
    ),
    'module' => 'redirect',
  );

  return $permissions;
}
