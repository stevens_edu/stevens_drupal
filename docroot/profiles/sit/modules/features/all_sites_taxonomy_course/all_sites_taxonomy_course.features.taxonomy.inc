<?php
/**
 * @file
 * all_sites_taxonomy_course.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function all_sites_taxonomy_course_taxonomy_default_vocabularies() {
  return array(
    'course_term' => array(
      'name' => 'Course Term',
      'machine_name' => 'course_term',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'metatags' => array(),
    ),
    'major' => array(
      'name' => 'Major',
      'machine_name' => 'major',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'metatags' => array(),
    ),
  );
}
