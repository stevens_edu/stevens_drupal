<?php
/**
 * @file
 * content_hub_carousel.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function content_hub_carousel_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'field_collection_item-field_itemcollection-field_carousel_item_view'
  $field_instances['field_collection_item-field_itemcollection-field_carousel_item_view'] = array(
    'bundle' => 'field_itemcollection',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'viewfield',
        'settings' => array(),
        'type' => 'viewfield_default',
        'weight' => 2,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_carousel_item_view',
    'label' => 'View',
    'required' => 0,
    'settings' => array(
      'allowed_views' => array(
        'carousel_items' => 'carousel_items',
      ),
      'force_default' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'viewfield',
      'settings' => array(),
      'type' => 'viewfield_select',
      'weight' => '3',
    ),
  );

  // Exported field_instance: 'field_collection_item-field_itemcollection-field_carousel_title_link'
  $field_instances['field_collection_item-field_itemcollection-field_carousel_title_link'] = array(
    'bundle' => 'field_itemcollection',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'link',
        'settings' => array(),
        'type' => 'link_default',
        'weight' => 3,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'field_collection_item',
    'field_name' => 'field_carousel_title_link',
    'label' => 'Column Title & Link',
    'required' => 0,
    'settings' => array(
      'attributes' => array(
        'class' => '',
        'configurable_title' => 0,
        'rel' => '',
        'target' => 'default',
        'title' => '',
      ),
      'display' => array(
        'url_cutoff' => '80',
      ),
      'enable_tokens' => 1,
      'title' => 'required',
      'title_maxlength' => '128',
      'title_value' => '',
      'url' => 'optional',
      'user_register_form' => FALSE,
      'validate_url' => 0,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'link',
      'settings' => array(),
      'type' => 'link_field',
      'weight' => '1',
    ),
  );

  // Exported field_instance: 'node-carousel-field_itemcollection'
  $field_instances['node-carousel-field_itemcollection'] = array(
    'bundle' => 'carousel',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => '0',
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_itemcollection',
    'label' => 'Item collections',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'field_collection',
      'settings' => array(),
      'type' => 'field_collection_embed',
      'weight' => '2',
    ),
  );

  // Exported field_instance: 'node-carousel-field_shared_adminunits'
  $field_instances['node-carousel-field_shared_adminunits'] = array(
    'bundle' => 'carousel',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'field_name' => 'field_shared_adminunits',
    'label' => 'Administration Unit',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
      ),
      'type' => 'options_select',
      'weight' => '4',
    ),
    'workbench_access_field' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Administration Unit');
  t('Column Title & Link');
  t('Item collections');
  t('View');

  return $field_instances;
}
