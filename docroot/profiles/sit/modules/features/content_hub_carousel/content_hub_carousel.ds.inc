<?php
/**
 * @file
 * content_hub_carousel.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function content_hub_carousel_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass;
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'field_collection_item|field_itemcollection|default';
  $ds_fieldsetting->entity_type = 'field_collection_item';
  $ds_fieldsetting->bundle = 'field_itemcollection';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'field_carousel_item_nodes' => array(
      'ft' => array(
        'func' => 'theme_ds_field_reset',
      ),
    ),
    'field_carousel_title' => array(
      'ft' => array(
        'func' => 'theme_ds_field_expert',
        'ow' => TRUE,
        'ow-el' => 'h3',
        'ow-cl' => 'column-title',
      ),
    ),
  );
  $export['field_collection_item|field_itemcollection|default'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function content_hub_carousel_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass;
  $ds_layout->api_version = 1;
  $ds_layout->id = 'field_collection_item|field_itemcollection|default';
  $ds_layout->entity_type = 'field_collection_item';
  $ds_layout->bundle = 'field_itemcollection';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = '1col_nowrap';
  $ds_layout->settings = array(
    'hide_empty_regions' => 0,
    'hide_sidebars' => 0,
    'regions' => array(
      'middle' => array(
        0 => 'field_carousel_title',
        1 => 'field_carousel_item_nodes',
      ),
    ),
    'fields' => array(
      'field_carousel_title' => 'middle',
      'field_carousel_item_nodes' => 'middle',
    ),
    'classes' => array(),
  );
  $export['field_collection_item|field_itemcollection|default'] = $ds_layout;

  return $export;
}
