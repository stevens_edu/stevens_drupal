<?php
/**
 * @file
 * content_hub_carousel.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function content_hub_carousel_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_carousel_item_view'
  $field_bases['field_carousel_item_view'] = array(
    'active' => '1',
    'cardinality' => '1',
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_carousel_item_view',
    'foreign keys' => array(),
    'indexes' => array(),
    'module' => 'viewfield',
    'settings' => array(),
    'translatable' => '0',
    'type' => 'viewfield',
  );

  // Exported field_base: 'field_carousel_title_link'
  $field_bases['field_carousel_title_link'] = array(
    'active' => '1',
    'cardinality' => '1',
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_carousel_title_link',
    'foreign keys' => array(),
    'indexes' => array(),
    'module' => 'link',
    'settings' => array(
      'attributes' => array(
        'class' => '',
        'rel' => '',
        'target' => 'default',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'title' => 'optional',
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
    ),
    'translatable' => '0',
    'type' => 'link_field',
  );

  // Exported field_base: 'field_itemcollection'
  $field_bases['field_itemcollection'] = array(
    'active' => '1',
    'cardinality' => '-1',
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_itemcollection',
    'foreign keys' => array(),
    'indexes' => array(),
    'module' => 'field_collection',
    'settings' => array(
      'path' => '',
    ),
    'translatable' => '0',
    'type' => 'field_collection',
  );

  return $field_bases;
}
