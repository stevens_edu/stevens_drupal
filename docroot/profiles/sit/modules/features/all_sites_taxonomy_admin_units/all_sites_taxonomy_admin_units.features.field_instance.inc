<?php
/**
 * @file
 * all_sites_taxonomy_admin_units.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function all_sites_taxonomy_admin_units_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'taxonomy_term-admin_units-field_parent_unit_id'
  $field_instances['taxonomy_term-admin_units-field_parent_unit_id'] = array(
    'bundle' => 'admin_units',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => ' ',
        ),
        'type' => 'number_integer',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_parent_unit_id',
    'label' => 'Parent Unit ID',
    'required' => FALSE,
    'settings' => array(
      'max' => '',
      'min' => '',
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => '12',
    ),
  );

  // Exported field_instance: 'taxonomy_term-admin_units-field_parent_unit_type'
  $field_instances['taxonomy_term-admin_units-field_parent_unit_type'] = array(
    'bundle' => 'admin_units',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 2,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_parent_unit_type',
    'is_property' => FALSE,
    'label' => 'Parent Unit Type',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
      ),
      'type' => 'options_select',
      'weight' => '11',
    ),
    'workbench_access_field' => 0,
  );

  // Exported field_instance: 'taxonomy_term-admin_units-field_unit_id'
  $field_instances['taxonomy_term-admin_units-field_unit_id'] = array(
    'bundle' => 'admin_units',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => ' ',
        ),
        'type' => 'number_integer',
        'weight' => 1,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_unit_id',
    'label' => 'ID',
    'required' => 0,
    'settings' => array(
      'max' => '',
      'min' => '',
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => '2',
    ),
  );

  // Exported field_instance: 'taxonomy_term-admin_units-field_unit_type'
  $field_instances['taxonomy_term-admin_units-field_unit_type'] = array(
    'bundle' => 'admin_units',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_unit_type',
    'label' => 'Type',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => '1',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('ID');
  t('Parent Unit ID');
  t('Parent Unit Type');
  t('Type');

  return $field_instances;
}
