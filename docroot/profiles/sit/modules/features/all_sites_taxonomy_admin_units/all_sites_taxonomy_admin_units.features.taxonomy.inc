<?php
/**
 * @file
 * all_sites_taxonomy_admin_units.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function all_sites_taxonomy_admin_units_taxonomy_default_vocabularies() {
  return array(
    'admin_units' => array(
      'name' => 'Admin units',
      'machine_name' => 'admin_units',
      'description' => '',
      'hierarchy' => '1',
      'module' => 'taxonomy',
      'weight' => '0',
      'metatags' => array(),
    ),
  );
}
