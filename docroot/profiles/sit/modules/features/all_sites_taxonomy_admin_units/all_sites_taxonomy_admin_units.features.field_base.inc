<?php
/**
 * @file
 * all_sites_taxonomy_admin_units.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function all_sites_taxonomy_admin_units_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_parent_unit_id'
  $field_bases['field_parent_unit_id'] = array(
    'active' => '1',
    'cardinality' => '1',
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_parent_unit_id',
    'foreign keys' => array(),
    'indexes' => array(),
    'module' => 'number',
    'settings' => array(),
    'translatable' => '0',
    'type' => 'number_integer',
  );

  // Exported field_base: 'field_parent_unit_type'
  $field_bases['field_parent_unit_type'] = array(
    'active' => '1',
    'cardinality' => '1',
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_parent_unit_type',
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'school' => 'School',
        'department' => 'Department',
        'program' => 'Program',
        'research_center' => 'Research Center',
        'laboratory' => 'Laboratory',
        'office' => 'Office',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => '0',
    'type' => 'list_text',
  );

  // Exported field_base: 'field_unit_id'
  $field_bases['field_unit_id'] = array(
    'active' => '1',
    'cardinality' => '1',
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_unit_id',
    'foreign keys' => array(),
    'indexes' => array(),
    'module' => 'number',
    'settings' => array(),
    'translatable' => '0',
    'type' => 'number_integer',
  );

  // Exported field_base: 'field_unit_type'
  $field_bases['field_unit_type'] = array(
    'active' => '1',
    'cardinality' => '1',
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_unit_type',
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'school' => 'School',
        'department' => 'Department',
        'program' => 'Program',
        'research_center' => 'Research Center',
        'laboratory' => 'Laboratory',
        'office' => 'Office',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => '0',
    'type' => 'list_text',
  );

  return $field_bases;
}
