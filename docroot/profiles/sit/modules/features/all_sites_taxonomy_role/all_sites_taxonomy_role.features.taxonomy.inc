<?php
/**
 * @file
 * all_sites_taxonomy_role.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function all_sites_taxonomy_role_taxonomy_default_vocabularies() {
  return array(
    'role' => array(
      'name' => 'Role',
      'machine_name' => 'role',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
  );
}
