<?php
/**
 * @file
 * content_hub_experts_profile.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function content_hub_experts_profile_taxonomy_default_vocabularies() {
  return array(
    'category_expertise' => array(
      'name' => 'Category Expertise',
      'machine_name' => 'category_expertise',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
  );
}
