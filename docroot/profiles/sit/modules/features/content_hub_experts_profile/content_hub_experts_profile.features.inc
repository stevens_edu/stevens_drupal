<?php
/**
 * @file
 * content_hub_experts_profile.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function content_hub_experts_profile_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function content_hub_experts_profile_node_info() {
  $items = array(
    'experts_profile' => array(
      'name' => t('Experts Profile'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
