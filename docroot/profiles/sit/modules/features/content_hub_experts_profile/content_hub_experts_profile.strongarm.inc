<?php
/**
 * @file
 * content_hub_experts_profile.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function content_hub_experts_profile_strongarm() {
  $export = array();

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_experts_profile';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_experts_profile'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_experts_profile';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_experts_profile'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_experts_profile';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
  );
  $export['node_options_experts_profile'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_experts_profile';
  $strongarm->value = '1';
  $export['node_preview_experts_profile'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_experts_profile';
  $strongarm->value = 1;
  $export['node_submitted_experts_profile'] = $strongarm;

  return $export;
}
