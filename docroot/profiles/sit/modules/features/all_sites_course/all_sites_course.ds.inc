<?php
/**
 * @file
 * all_sites_course.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function all_sites_course_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass;
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|course|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'course';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|course|default'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function all_sites_course_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass;
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|course|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'course';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'hide_empty_regions' => 0,
    'hide_sidebars' => 0,
    'regions' => array(
      'left' => array(
        0 => 'title',
        1 => 'field_course_no',
        2 => 'field_course_prereq',
        3 => 'field_course_coreq',
      ),
      'right' => array(
        0 => 'field_course_credit_hours',
        1 => 'field_course_lect_hours',
        2 => 'field_course_lab_hours',
        3 => 'field_course_study_hours',
      ),
      'footer' => array(
        0 => 'body',
        1 => 'field_course_special_instr',
      ),
    ),
    'fields' => array(
      'title' => 'left',
      'field_course_no' => 'left',
      'field_course_prereq' => 'left',
      'field_course_coreq' => 'left',
      'field_course_credit_hours' => 'right',
      'field_course_lect_hours' => 'right',
      'field_course_lab_hours' => 'right',
      'field_course_study_hours' => 'right',
      'body' => 'footer',
      'field_course_special_instr' => 'footer',
    ),
    'classes' => array(),
    'hide_page_title' => '0',
    'page_option_title' => '',
  );
  $export['node|course|default'] = $ds_layout;

  return $export;
}
