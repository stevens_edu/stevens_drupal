<?php
/**
 * @file
 * all_sites_course.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function all_sites_course_taxonomy_default_vocabularies() {
  return array(
    'program_level' => array(
      'name' => 'Program Level',
      'machine_name' => 'program_level',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
  );
}
