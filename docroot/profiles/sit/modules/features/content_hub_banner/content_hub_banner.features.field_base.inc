<?php
/**
 * @file
 * content_hub_banner.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function content_hub_banner_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_banner_img'
  $field_bases['field_banner_img'] = array(
    'active' => '1',
    'cardinality' => '1',
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_banner_img',
    'foreign keys' => array(
      'fid' => array(
        'columns' => array(
          'fid' => 'fid',
        ),
        'table' => 'file_managed',
      ),
    ),
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'module' => 'image',
    'settings' => array(
      'default_image' => 0,
      'uri_scheme' => 'public',
    ),
    'translatable' => '0',
    'type' => 'image',
  );

  // Exported field_base: 'field_banner_link'
  $field_bases['field_banner_link'] = array(
    'active' => '1',
    'cardinality' => '1',
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_banner_link',
    'foreign keys' => array(),
    'indexes' => array(),
    'module' => 'link',
    'settings' => array(
      'attributes' => array(
        'class' => '',
        'rel' => '',
        'target' => 'default',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'title' => 'optional',
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
    ),
    'translatable' => '0',
    'type' => 'link_field',
  );

  // Exported field_base: 'field_banner_text'
  $field_bases['field_banner_text'] = array(
    'active' => '1',
    'cardinality' => '1',
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_banner_text',
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'module' => 'text',
    'settings' => array(),
    'translatable' => '0',
    'type' => 'text_long',
  );

  // Exported field_base: 'field_banner_video'
  $field_bases['field_banner_video'] = array(
    'active' => '1',
    'cardinality' => '1',
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_banner_video',
    'foreign keys' => array(),
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'module' => 'mediafield',
    'settings' => array(),
    'translatable' => '0',
    'type' => 'media',
  );

  return $field_bases;
}
