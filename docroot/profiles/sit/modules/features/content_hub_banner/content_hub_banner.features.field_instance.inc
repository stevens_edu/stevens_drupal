<?php
/**
 * @file
 * content_hub_banner.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function content_hub_banner_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-banner-field_banner_img'
  $field_instances['node-banner-field_banner_img'] = array(
    'bundle' => 'banner',
    'deleted' => '0',
    'description' => 'File has to be 1200 x 388 pixels.',
    'display' => array(
      'carousel_nodes' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => '0',
      ),
      'sit_carousel' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => '0',
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => '0',
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_banner_img',
    'label' => 'Background Image',
    'required' => 1,
    'settings' => array(
      'alt_field' => 0,
      'file_directory' => 'images/banners',
      'file_extensions' => 'png jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '1200x388',
      'min_resolution' => '1200x388',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'medium',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => '5',
    ),
  );

  // Exported field_instance: 'node-banner-field_banner_link'
  $field_instances['node-banner-field_banner_link'] = array(
    'bundle' => 'banner',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => '',
    'display' => array(
      'carousel_nodes' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => '9',
      ),
      'sit_carousel' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => '9',
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => '9',
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'field_name' => 'field_banner_link',
    'label' => 'Destination',
    'required' => 0,
    'settings' => array(
      'attributes' => array(
        'class' => 'action-link',
        'configurable_title' => 0,
        'rel' => '',
        'target' => 'default',
        'title' => '',
      ),
      'display' => array(
        'url_cutoff' => '80',
      ),
      'enable_tokens' => 1,
      'title' => 'optional',
      'title_maxlength' => '128',
      'title_value' => '',
      'url' => 0,
      'user_register_form' => FALSE,
      'validate_url' => 1,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'link',
      'settings' => array(
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
      ),
      'type' => 'link_field',
      'weight' => '4',
    ),
  );

  // Exported field_instance: 'node-banner-field_banner_text'
  $field_instances['node-banner-field_banner_text'] = array(
    'bundle' => 'banner',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => '',
    'display' => array(
      'carousel_nodes' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => '8',
      ),
      'sit_carousel' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => '8',
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => '8',
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'field_name' => 'field_banner_text',
    'label' => 'Banner Text',
    'required' => 0,
    'settings' => array(
      'text_processing' => '1',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => '3',
      ),
      'type' => 'text_textarea',
      'weight' => '2',
    ),
  );

  // Exported field_instance: 'node-banner-field_banner_video'
  $field_instances['node-banner-field_banner_video'] = array(
    'bundle' => 'banner',
    'deleted' => '0',
    'description' => '',
    'display' => array(
      'carousel_nodes' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'above',
        'module' => NULL,
        'settings' => array(),
        'type' => 'media_large',
        'weight' => 10,
      ),
      'sit_carousel' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'field_name' => 'field_banner_video',
    'label' => 'Video',
    'required' => 0,
    'settings' => array(
      'file_extensions' => 'jpg jpeg gif png txt doc xls pdf ppt pps odt ods odp mp3 mov m4v mp4 mpeg avi ogg wmv ico',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'media',
      'settings' => array(
        'allowed_schemes' => array(
          'nbc' => 'nbc',
          'public' => 0,
          'vimeo' => 'vimeo',
          'youtube' => 'youtube',
        ),
        'allowed_types' => array(
          'audio' => 0,
          'default' => 0,
          'image' => 0,
          'video' => 'video',
        ),
        'browser_plugins' => array(),
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'media_generic',
      'weight' => '3',
    ),
  );

  // Exported field_instance: 'node-banner-field_shared_adminunits'
  $field_instances['node-banner-field_shared_adminunits'] = array(
    'bundle' => 'banner',
    'default_value' => NULL,
    'deleted' => '0',
    'description' => '',
    'display' => array(
      'carousel_nodes' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 11,
      ),
      'sit_carousel' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'field_name' => 'field_shared_adminunits',
    'label' => 'Administration Unit',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
      ),
      'type' => 'options_select',
      'weight' => '7',
    ),
    'workbench_access_field' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Administration Unit');
  t('Background Image');
  t('Banner Text');
  t('Destination');
  t('File has to be 1200 x 388 pixels.');
  t('Video');

  return $field_instances;
}
