<?php
/**
 * @file
 * content_hub_banner.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function content_hub_banner_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function content_hub_banner_node_info() {
  $items = array(
    'banner' => array(
      'name' => t('Banner'),
      'base' => 'node_content',
      'description' => t('Add a front page banner to the page'),
      'has_title' => '1',
      'title_label' => t('Internal Title'),
      'help' => '',
    ),
  );
  return $items;
}
