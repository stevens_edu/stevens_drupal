<?php
/**
 * @file
 * content_hub_banner.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function content_hub_banner_strongarm() {
  $export = array();

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_banner';
  $strongarm->value = array();
  $export['menu_options_banner'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_banner';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_banner'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_banner';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_banner'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_banner';
  $strongarm->value = '0';
  $export['node_preview_banner'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_banner';
  $strongarm->value = 0;
  $export['node_submitted_banner'] = $strongarm;

  return $export;
}
