<?php
/**
 * @file
 * default_page_ct.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function default_page_ct_taxonomy_default_vocabularies() {
  return array(
    'qa' => array(
      'name' => 'QA',
      'machine_name' => 'qa',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'sections' => array(
      'name' => 'Sections',
      'machine_name' => 'sections',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
  );
}
