<?php
/**
 * @file
 * default_page_ct.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function default_page_ct_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass;
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|page|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'page';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'page_node_minipanel' => array(
      'weight' => '6',
      'label' => 'hidden',
      'format' => 'default',
      'ft' => array(
        'func' => 'theme_ds_field_reset',
      ),
    ),
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h1',
        'class' => 'page-title title',
      ),
      'ft' => array(
        'func' => 'theme_ds_field_reset',
      ),
    ),
    'body' => array(
      'ft' => array(
        'func' => 'theme_ds_field_minimal',
      ),
    ),
    'field_highlight' => array(
      'ft' => array(
        'func' => 'theme_ds_field_minimal',
      ),
    ),
    'field_image' => array(
      'ft' => array(
        'func' => 'theme_ds_field_minimal',
      ),
    ),
    'field_intro_text' => array(
      'ft' => array(
        'func' => 'theme_ds_field_minimal',
      ),
    ),
    'field_quick_links' => array(
      'ft' => array(
        'func' => 'theme_ds_field_expert',
        'fis' => TRUE,
        'fis-el' => 'ul',
        'fis-cl' => 'quick-links',
        'fi' => TRUE,
        'fi-el' => 'li',
        'fi-cl' => 'quick-link-item',
      ),
    ),
    'field_temp_highlight' => array(
      'ft' => array(
        'func' => 'theme_ds_field_minimal',
      ),
    ),
    'field_temp_node_sidebar' => array(
      'ft' => array(
        'func' => 'theme_ds_field_minimal',
      ),
    ),
  );
  $export['node|page|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass;
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|page|full';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'page';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'page_node_minipanel' => array(
      'weight' => '7',
      'label' => 'hidden',
      'format' => 'default',
      'ft' => array(
        'func' => 'theme_ds_field_reset',
      ),
    ),
    'webform_block' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'default',
      'ft' => array(
        'func' => 'theme_ds_field_reset',
      ),
    ),
    'title' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h1',
        'class' => 'page-title',
      ),
      'ft' => array(
        'func' => 'theme_ds_field_reset',
      ),
    ),
    'body' => array(
      'ft' => array(
        'func' => 'theme_ds_field_minimal',
      ),
    ),
    'field_highlight' => array(
      'ft' => array(
        'func' => 'theme_ds_field_minimal',
      ),
    ),
    'field_image' => array(
      'ft' => array(
        'func' => 'theme_ds_field_minimal',
      ),
    ),
    'field_intro_text' => array(
      'ft' => array(
        'func' => 'theme_ds_field_minimal',
      ),
    ),
    'field_quick_links' => array(
      'ft' => array(
        'func' => 'theme_ds_field_expert',
        'fis' => TRUE,
        'fis-el' => 'ul',
        'fis-cl' => 'quick-links',
        'fi' => TRUE,
        'fi-el' => 'li',
        'fi-cl' => 'quick-link-item',
      ),
    ),
    'field_temp_highlight' => array(
      'ft' => array(
        'func' => 'theme_ds_field_minimal',
      ),
    ),
    'field_temp_node_sidebar' => array(
      'ft' => array(
        'func' => 'theme_ds_field_minimal',
      ),
    ),
  );
  $export['node|page|full'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass;
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|page|sit_grid_default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'page';
  $ds_fieldsetting->view_mode = 'sit_grid_default';
  $ds_fieldsetting->settings = array(
    'page_node_minipanel' => array(
      'weight' => '6',
      'label' => 'hidden',
      'format' => 'default',
      'ft' => array(
        'func' => 'theme_ds_field_reset',
      ),
    ),
    'title' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h1',
        'class' => 'page-title title',
      ),
      'ft' => array(
        'func' => 'theme_ds_field_reset',
      ),
    ),
    'body' => array(
      'ft' => array(
        'func' => 'theme_ds_field_minimal',
      ),
    ),
    'field_highlight' => array(
      'ft' => array(
        'func' => 'theme_ds_field_minimal',
      ),
    ),
    'field_image' => array(
      'ft' => array(
        'func' => 'theme_ds_field_minimal',
      ),
    ),
    'field_intro_text' => array(
      'ft' => array(
        'func' => 'theme_ds_field_minimal',
      ),
    ),
    'field_quick_links' => array(
      'ft' => array(
        'func' => 'theme_ds_field_expert',
        'fis' => TRUE,
        'fis-el' => 'ul',
        'fis-cl' => 'quick-links',
        'fi' => TRUE,
        'fi-el' => 'li',
        'fi-cl' => 'quick-link-item',
      ),
    ),
    'field_temp_highlight' => array(
      'ft' => array(
        'func' => 'theme_ds_field_minimal',
      ),
    ),
    'field_temp_node_sidebar' => array(
      'ft' => array(
        'func' => 'theme_ds_field_minimal',
      ),
    ),
  );
  $export['node|page|sit_grid_default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass;
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|page|sit_two_column_default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'page';
  $ds_fieldsetting->view_mode = 'sit_two_column_default';
  $ds_fieldsetting->settings = array(
    'page_node_minipanel' => array(
      'weight' => '7',
      'label' => 'hidden',
      'format' => 'default',
      'ft' => array(
        'func' => 'theme_ds_field_reset',
      ),
    ),
    'webform_block' => array(
      'weight' => '5',
      'label' => 'hidden',
      'format' => 'default',
      'ft' => array(
        'func' => 'theme_ds_field_reset',
      ),
    ),
    'title' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h1',
        'class' => 'page-title title',
      ),
      'ft' => array(
        'func' => 'theme_ds_field_reset',
      ),
    ),
    'body' => array(
      'ft' => array(
        'func' => 'theme_ds_field_minimal',
      ),
    ),
    'field_highlight' => array(
      'ft' => array(
        'func' => 'theme_ds_field_minimal',
      ),
    ),
    'field_image' => array(
      'ft' => array(
        'func' => 'theme_ds_field_minimal',
      ),
    ),
    'field_intro_text' => array(
      'ft' => array(
        'func' => 'theme_ds_field_minimal',
      ),
    ),
    'field_quick_links' => array(
      'ft' => array(
        'func' => 'theme_ds_field_expert',
        'fis' => TRUE,
        'fis-el' => 'ul',
        'fis-cl' => 'quick-links',
        'fi' => TRUE,
        'fi-el' => 'li',
        'fi-cl' => 'quick-link-item',
      ),
    ),
    'field_temp_highlight' => array(
      'ft' => array(
        'func' => 'theme_ds_field_minimal',
      ),
    ),
    'field_temp_node_sidebar' => array(
      'ft' => array(
        'func' => 'theme_ds_field_minimal',
      ),
    ),
  );
  $export['node|page|sit_two_column_default'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_custom_fields_info().
 */
function default_page_ct_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass;
  $ds_field->api_version = 1;
  $ds_field->field = 'webform_block';
  $ds_field->label = 'Webform Block';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->properties = array(
    'block' => 'panels_mini|page_webform',
    'block_render' => '1',
  );
  $export['webform_block'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function default_page_ct_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass;
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|page|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'page';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'grid_default';
  $ds_layout->settings = array(
    'hide_empty_regions' => 1,
    'hide_sidebars' => 0,
    'regions' => array(
      'header' => array(
        0 => 'title',
        1 => 'field_image',
        2 => 'field_quick_links',
      ),
      'top_left' => array(
        0 => 'field_intro_text',
      ),
      'top_right' => array(
        0 => 'field_temp_highlight',
      ),
      'bottom_left' => array(
        0 => 'body',
      ),
      'bottom_right' => array(
        0 => 'page_node_minipanel',
        1 => 'field_temp_node_sidebar',
        2 => 'field_highlight',
      ),
    ),
    'fields' => array(
      'title' => 'header',
      'field_image' => 'header',
      'field_quick_links' => 'header',
      'field_intro_text' => 'top_left',
      'field_temp_highlight' => 'top_right',
      'body' => 'bottom_left',
      'page_node_minipanel' => 'bottom_right',
      'field_temp_node_sidebar' => 'bottom_right',
      'field_highlight' => 'bottom_right',
    ),
    'classes' => array(),
    'hide_page_title' => '1',
    'page_option_title' => '',
  );
  $export['node|page|default'] = $ds_layout;

  $ds_layout = new stdClass;
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|page|full';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'page';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'grid_default';
  $ds_layout->settings = array(
    'hide_empty_regions' => 1,
    'hide_sidebars' => 0,
    'regions' => array(
      'header' => array(
        0 => 'field_image',
        1 => 'field_quick_links',
        2 => 'title',
      ),
      'top_left' => array(
        0 => 'field_intro_text',
      ),
      'top_right' => array(
        0 => 'webform_block',
        1 => 'field_temp_highlight',
      ),
      'bottom_left' => array(
        0 => 'body',
      ),
      'bottom_right' => array(
        0 => 'page_node_minipanel',
        1 => 'field_temp_node_sidebar',
        2 => 'field_highlight',
      ),
    ),
    'fields' => array(
      'field_image' => 'header',
      'field_quick_links' => 'header',
      'title' => 'header',
      'field_intro_text' => 'top_left',
      'webform_block' => 'top_right',
      'field_temp_highlight' => 'top_right',
      'body' => 'bottom_left',
      'page_node_minipanel' => 'bottom_right',
      'field_temp_node_sidebar' => 'bottom_right',
      'field_highlight' => 'bottom_right',
    ),
    'classes' => array(),
    'hide_page_title' => '1',
    'page_option_title' => '',
  );
  $export['node|page|full'] = $ds_layout;

  $ds_layout = new stdClass;
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|page|sit_grid_default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'page';
  $ds_layout->view_mode = 'sit_grid_default';
  $ds_layout->layout = 'grid_default';
  $ds_layout->settings = array(
    'hide_empty_regions' => 1,
    'hide_sidebars' => 0,
    'regions' => array(
      'header' => array(
        0 => 'field_image',
        1 => 'field_quick_links',
      ),
      'top_left' => array(
        0 => 'title',
        1 => 'field_intro_text',
      ),
      'top_right' => array(
        0 => 'field_temp_highlight',
      ),
      'bottom_left' => array(
        0 => 'body',
      ),
      'bottom_right' => array(
        0 => 'page_node_minipanel',
        1 => 'field_temp_node_sidebar',
        2 => 'field_highlight',
      ),
    ),
    'fields' => array(
      'field_image' => 'header',
      'field_quick_links' => 'header',
      'title' => 'top_left',
      'field_intro_text' => 'top_left',
      'field_temp_highlight' => 'top_right',
      'body' => 'bottom_left',
      'page_node_minipanel' => 'bottom_right',
      'field_temp_node_sidebar' => 'bottom_right',
      'field_highlight' => 'bottom_right',
    ),
    'classes' => array(),
    'hide_page_title' => '1',
    'page_option_title' => '',
  );
  $export['node|page|sit_grid_default'] = $ds_layout;

  $ds_layout = new stdClass;
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|page|sit_two_column_default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'page';
  $ds_layout->view_mode = 'sit_two_column_default';
  $ds_layout->layout = '2col_default';
  $ds_layout->settings = array(
    'hide_empty_regions' => 1,
    'hide_sidebars' => 0,
    'regions' => array(
      'header' => array(
        0 => 'field_image',
        1 => 'field_quick_links',
        2 => 'title',
      ),
      'left' => array(
        0 => 'field_intro_text',
        1 => 'body',
      ),
      'right' => array(
        0 => 'webform_block',
        1 => 'field_temp_highlight',
        2 => 'page_node_minipanel',
        3 => 'field_temp_node_sidebar',
        4 => 'field_highlight',
      ),
    ),
    'fields' => array(
      'field_image' => 'header',
      'field_quick_links' => 'header',
      'title' => 'header',
      'field_intro_text' => 'left',
      'body' => 'left',
      'webform_block' => 'right',
      'field_temp_highlight' => 'right',
      'page_node_minipanel' => 'right',
      'field_temp_node_sidebar' => 'right',
      'field_highlight' => 'right',
    ),
    'classes' => array(),
    'hide_page_title' => '1',
    'page_option_title' => '',
  );
  $export['node|page|sit_two_column_default'] = $ds_layout;

  return $export;
}

/**
 * Implements hook_ds_view_modes_info().
 */
function default_page_ct_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass;
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'sit_grid_default';
  $ds_view_mode->label = 'SIT Grid Default';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['sit_grid_default'] = $ds_view_mode;

  $ds_view_mode = new stdClass;
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'sit_two_column_default';
  $ds_view_mode->label = 'SIT Two Column Default';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['sit_two_column_default'] = $ds_view_mode;

  return $export;
}
