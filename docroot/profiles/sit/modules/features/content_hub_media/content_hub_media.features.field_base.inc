<?php
/**
 * @file
 * content_hub_media.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function content_hub_media_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_media_carousel_title'
  $field_bases['field_media_carousel_title'] = array(
    'active' => '1',
    'cardinality' => '1',
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_media_carousel_title',
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'module' => 'text',
    'settings' => array(
      'max_length' => '30',
    ),
    'translatable' => '0',
    'type' => 'text',
  );

  // Exported field_base: 'field_media_type'
  $field_bases['field_media_type'] = array(
    'active' => '1',
    'cardinality' => '1',
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_media_type',
    'foreign keys' => array(
      'tid' => array(
        'columns' => array(
          'tid' => 'tid',
        ),
        'table' => 'taxonomy_term_data',
      ),
    ),
    'indexes' => array(
      'tid' => array(
        0 => 'tid',
      ),
    ),
    'module' => 'taxonomy',
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => 'media_type',
          'parent' => '0',
        ),
      ),
    ),
    'translatable' => '0',
    'type' => 'taxonomy_term_reference',
  );

  return $field_bases;
}
