<?php
/**
 * @file
 * all_school_sites_global.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function all_school_sites_global_user_default_roles() {
  $roles = array();

  // Exported role: Contributor
  $roles['Contributor'] = array(
    'name' => 'Contributor',
    'weight' => '3',
    'machine_name' => 'contributor',
  );

  // Exported role: Faculty/Staff
  $roles['Faculty/Staff'] = array(
    'name' => 'Faculty/Staff',
    'weight' => '2',
    'machine_name' => 'faculty_staff',
  );

  return $roles;
}
