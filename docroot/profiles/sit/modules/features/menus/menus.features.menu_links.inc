<?php
/**
 * @file
 * menus.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function menus_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-social:http://buzz.stevens.edu/
  $menu_links['menu-social:http://buzz.stevens.edu/'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'http://buzz.stevens.edu/',
    'router_path' => '',
    'link_title' => 'Buzz',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-49',
    'parent_path' => 'http://www.stevens.edu/news/front',
  );
  // Exported menu link: menu-social:http://itunes.apple.com/us/app/stevens-institute-technology/id372838640?mt=8
  $menu_links['menu-social:http://itunes.apple.com/us/app/stevens-institute-technology/id372838640?mt=8'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'http://itunes.apple.com/us/app/stevens-institute-technology/id372838640?mt=8',
    'router_path' => '',
    'link_title' => 'Download iPhone App',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-49',
    'parent_path' => 'http://www.stevens.edu/sit/',
  );
  // Exported menu link: menu-social:http://research.stevens.edu/
  $menu_links['menu-social:http://research.stevens.edu/'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'http://research.stevens.edu/',
    'router_path' => '',
    'link_title' => 'Research Blog',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-46',
    'parent_path' => 'http://www.stevens.edu/news/front',
  );
  // Exported menu link: menu-social:http://stevensadmissions.typepad.com/
  $menu_links['menu-social:http://stevensadmissions.typepad.com/'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'http://stevensadmissions.typepad.com/',
    'router_path' => '',
    'link_title' => 'Admissions Blog',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-48',
    'parent_path' => 'http://www.stevens.edu/news/front',
  );
  // Exported menu link: menu-social:http://stevensgraduateadmissions.typepad.com/
  $menu_links['menu-social:http://stevensgraduateadmissions.typepad.com/'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'http://stevensgraduateadmissions.typepad.com/',
    'router_path' => '',
    'link_title' => 'Grad Admissions Blog',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-47',
    'parent_path' => 'http://www.stevens.edu/news/front',
  );
  // Exported menu link: menu-social:http://stevenslibrary.blogspot.com/
  $menu_links['menu-social:http://stevenslibrary.blogspot.com/'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'http://stevenslibrary.blogspot.com/',
    'router_path' => '',
    'link_title' => 'Library',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-45',
    'parent_path' => 'http://www.stevens.edu/news/front',
  );
  // Exported menu link: menu-social:http://twitter.com
  $menu_links['menu-social:http://twitter.com'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'http://twitter.com',
    'router_path' => '',
    'link_title' => 'Twitter',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '-48',
  );
  // Exported menu link: menu-social:http://twitter.com/RE_stevens
  $menu_links['menu-social:http://twitter.com/RE_stevens'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'http://twitter.com/RE_stevens',
    'router_path' => '',
    'link_title' => 'Research/Entrepreneurship',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-47',
    'parent_path' => 'http://twitter.com',
  );
  // Exported menu link: menu-social:http://twitter.com/scwLibrary
  $menu_links['menu-social:http://twitter.com/scwLibrary'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'http://twitter.com/scwLibrary',
    'router_path' => '',
    'link_title' => 'Library',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-46',
    'parent_path' => 'http://twitter.com',
  );
  // Exported menu link: menu-social:http://www.facebook.com/DeBaunCenterforPerformingArts
  $menu_links['menu-social:http://www.facebook.com/DeBaunCenterforPerformingArts'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'http://www.facebook.com/DeBaunCenterforPerformingArts',
    'router_path' => '',
    'link_title' => 'Performing Arts',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-47',
    'parent_path' => 'https://www.facebook.com/',
  );
  // Exported menu link: menu-social:http://www.facebook.com/Stevens1870
  $menu_links['menu-social:http://www.facebook.com/Stevens1870'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'http://www.facebook.com/Stevens1870',
    'router_path' => '',
    'link_title' => 'Stevens',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
    'parent_path' => 'https://www.facebook.com/',
  );
  // Exported menu link: menu-social:http://www.facebook.com/StevensRE
  $menu_links['menu-social:http://www.facebook.com/StevensRE'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'http://www.facebook.com/StevensRE',
    'router_path' => '',
    'link_title' => 'Research/Entrepreneurship',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-49',
    'parent_path' => 'https://www.facebook.com/',
  );
  // Exported menu link: menu-social:http://www.facebook.com/stevensathletics
  $menu_links['menu-social:http://www.facebook.com/stevensathletics'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'http://www.facebook.com/stevensathletics',
    'router_path' => '',
    'link_title' => 'Athletics',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-48',
    'parent_path' => 'https://www.facebook.com/',
  );
  // Exported menu link: menu-social:http://www.stevens.edu/news/
  $menu_links['menu-social:http://www.stevens.edu/news/'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'http://www.stevens.edu/news/',
    'router_path' => '',
    'link_title' => 'News',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
    'parent_path' => 'http://www.stevens.edu/news/front',
  );
  // Exported menu link: menu-social:http://www.stevens.edu/news/content/stevens-releases-iphone-app
  $menu_links['menu-social:http://www.stevens.edu/news/content/stevens-releases-iphone-app'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'http://www.stevens.edu/news/content/stevens-releases-iphone-app',
    'router_path' => '',
    'link_title' => 'Video',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-48',
    'parent_path' => 'http://www.stevens.edu/sit/',
  );
  // Exported menu link: menu-social:http://www.stevens.edu/news/front
  $menu_links['menu-social:http://www.stevens.edu/news/front'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'http://www.stevens.edu/news/front',
    'router_path' => '',
    'link_title' => 'RSS',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '-46',
  );
  // Exported menu link: menu-social:http://www.stevens.edu/sit/
  $menu_links['menu-social:http://www.stevens.edu/sit/'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'http://www.stevens.edu/sit/',
    'router_path' => '',
    'link_title' => 'Stevens',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '-50',
  );
  // Exported menu link: menu-social:http://www.youtube.com/EdwinAStevens70
  $menu_links['menu-social:http://www.youtube.com/EdwinAStevens70'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'http://www.youtube.com/EdwinAStevens70',
    'router_path' => '',
    'link_title' => 'University News',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
    'parent_path' => 'http://youtube.com',
  );
  // Exported menu link: menu-social:http://www.youtube.com/reatstevens
  $menu_links['menu-social:http://www.youtube.com/reatstevens'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'http://www.youtube.com/reatstevens',
    'router_path' => '',
    'link_title' => 'Research/Entrepreneurship',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-47',
    'parent_path' => 'http://youtube.com',
  );
  // Exported menu link: menu-social:http://www.youtube.com/user/StevensAthletics
  $menu_links['menu-social:http://www.youtube.com/user/StevensAthletics'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'http://www.youtube.com/user/StevensAthletics',
    'router_path' => '',
    'link_title' => 'Athletics',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-49',
    'parent_path' => 'http://youtube.com',
  );
  // Exported menu link: menu-social:http://www.youtube.com/user/stevenslibrary
  $menu_links['menu-social:http://www.youtube.com/user/stevenslibrary'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'http://www.youtube.com/user/stevenslibrary',
    'router_path' => '',
    'link_title' => 'SC Williams Library',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-48',
    'parent_path' => 'http://youtube.com',
  );
  // Exported menu link: menu-social:http://youtube.com
  $menu_links['menu-social:http://youtube.com'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'http://youtube.com',
    'router_path' => '',
    'link_title' => 'YouTube',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '-47',
  );
  // Exported menu link: menu-social:https://market.android.com/details?id=com.u360mobile.stevens
  $menu_links['menu-social:https://market.android.com/details?id=com.u360mobile.stevens'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'https://market.android.com/details?id=com.u360mobile.stevens',
    'router_path' => '',
    'link_title' => 'Download Android App',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
    'parent_path' => 'http://www.stevens.edu/sit/',
  );
  // Exported menu link: menu-social:https://twitter.com/FollowStevens
  $menu_links['menu-social:https://twitter.com/FollowStevens'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'https://twitter.com/FollowStevens',
    'router_path' => '',
    'link_title' => 'Stevens',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
    'parent_path' => 'http://twitter.com',
  );
  // Exported menu link: menu-social:https://twitter.com/StevensGradAdm
  $menu_links['menu-social:https://twitter.com/StevensGradAdm'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'https://twitter.com/StevensGradAdm',
    'router_path' => '',
    'link_title' => 'Grad Admissions',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-48',
    'parent_path' => 'http://twitter.com',
  );
  // Exported menu link: menu-social:https://twitter.com/stevensducks
  $menu_links['menu-social:https://twitter.com/stevensducks'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'https://twitter.com/stevensducks',
    'router_path' => '',
    'link_title' => 'Athletics',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-49',
    'parent_path' => 'http://twitter.com',
  );
  // Exported menu link: menu-social:https://www.facebook.com/
  $menu_links['menu-social:https://www.facebook.com/'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'https://www.facebook.com/',
    'router_path' => '',
    'link_title' => 'Facebook',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '-49',
  );
  // Exported menu link: menu-social:https://www.facebook.com/scwLibrary
  $menu_links['menu-social:https://www.facebook.com/scwLibrary'] = array(
    'menu_name' => 'menu-social',
    'link_path' => 'https://www.facebook.com/scwLibrary',
    'router_path' => '',
    'link_title' => 'SC Williams Library',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'https://www.facebook.com/',
  );
  // Exported menu link: user-menu:http://www.stevens.edu/calendar
  $menu_links['user-menu:http://www.stevens.edu/calendar'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'http://www.stevens.edu/calendar',
    'router_path' => '',
    'link_title' => 'Calendar',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-48',
  );
  // Exported menu link: user-menu:http://www.stevens.edu/mystevens
  $menu_links['user-menu:http://www.stevens.edu/mystevens'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'http://www.stevens.edu/mystevens',
    'router_path' => '',
    'link_title' => 'My Stevens Pipeline',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-49',
  );
  // Exported menu link: user-menu:http://www.stevens.edu/sit/people_finder.cfm
  $menu_links['user-menu:http://www.stevens.edu/sit/people_finder.cfm'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'http://www.stevens.edu/sit/people_finder.cfm',
    'router_path' => '',
    'link_title' => 'People Finder',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-47',
  );
  // Exported menu link: user-menu:http://www.stevens.edu/sit/stevens_index.cfm
  $menu_links['user-menu:http://www.stevens.edu/sit/stevens_index.cfm'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'http://www.stevens.edu/sit/stevens_index.cfm',
    'router_path' => '',
    'link_title' => 'A-Z Index',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
  );
  // Exported menu link: user-menu:http://www.stevenscampusstore.com/
  $menu_links['user-menu:http://www.stevenscampusstore.com/'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'http://www.stevenscampusstore.com/',
    'router_path' => '',
    'link_title' => 'Campus Store',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-46',
  );
  // Exported menu link: user-menu:https://secure.entango.com/donate/E8WCknqSPEc
  $menu_links['user-menu:https://secure.entango.com/donate/E8WCknqSPEc'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'https://secure.entango.com/donate/E8WCknqSPEc',
    'router_path' => '',
    'link_title' => 'Make a Gift',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-45',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('A-Z Index');
  t('Admissions Blog');
  t('Athletics');
  t('Buzz');
  t('Calendar');
  t('Campus Store');
  t('Download Android App');
  t('Download iPhone App');
  t('Facebook');
  t('Grad Admissions');
  t('Grad Admissions Blog');
  t('Library');
  t('Make a Gift');
  t('My Stevens Pipeline');
  t('News');
  t('People Finder');
  t('Performing Arts');
  t('RSS');
  t('Research Blog');
  t('Research/Entrepreneurship');
  t('SC Williams Library');
  t('Stevens');
  t('Twitter');
  t('University News');
  t('Video');
  t('YouTube');


  return $menu_links;
}
