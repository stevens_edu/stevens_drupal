<?php
/**
 * @file
 * all_sites_curriculum.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function all_sites_curriculum_strongarm() {
  $export = array();

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_curriculum';
  $strongarm->value = array();
  $export['menu_options_curriculum'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_curriculum';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_curriculum'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_curriculum';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_curriculum'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_curriculum';
  $strongarm->value = '0';
  $export['node_preview_curriculum'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_curriculum';
  $strongarm->value = 0;
  $export['node_submitted_curriculum'] = $strongarm;

  return $export;
}
