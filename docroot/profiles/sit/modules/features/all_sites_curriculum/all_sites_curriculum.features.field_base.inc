<?php
/**
 * @file
 * all_sites_curriculum.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function all_sites_curriculum_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_coreq_instructions'
  $field_bases['field_coreq_instructions'] = array(
    'active' => '1',
    'cardinality' => '1',
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_coreq_instructions',
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'module' => 'text',
    'settings' => array(),
    'translatable' => '0',
    'type' => 'text_long',
  );

  // Exported field_base: 'field_course_ref'
  $field_bases['field_course_ref'] = array(
    'active' => '1',
    'cardinality' => '1',
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_course_ref',
    'foreign keys' => array(
      'nid' => array(
        'columns' => array(
          'nid' => 'nid',
        ),
        'table' => 'node',
      ),
    ),
    'indexes' => array(
      'nid' => array(
        0 => 'nid',
      ),
    ),
    'module' => 'node_reference',
    'settings' => array(
      'referenceable_types' => array(
        'banner' => 0,
        'bio' => 0,
        'carousel' => 0,
        'course' => 'course',
        'curriculum' => 0,
        'event' => 0,
        'experts_profile' => 0,
        'fe_seminar_series_2011_2012' => 0,
        'media' => 0,
        'news' => 0,
        'page' => 0,
        'publication' => 0,
        'webform' => 0,
      ),
      'view' => array(
        'args' => array(),
        'display_name' => '',
        'view_name' => '',
      ),
    ),
    'translatable' => '0',
    'type' => 'node_reference',
  );

  // Exported field_base: 'field_curriculum_id'
  $field_bases['field_curriculum_id'] = array(
    'active' => '1',
    'cardinality' => '1',
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_curriculum_id',
    'foreign keys' => array(),
    'indexes' => array(),
    'module' => 'number',
    'settings' => array(),
    'translatable' => '0',
    'type' => 'number_integer',
  );

  // Exported field_base: 'field_is_default'
  $field_bases['field_is_default'] = array(
    'active' => '1',
    'cardinality' => '1',
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_is_default',
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        0 => 'No',
        1 => 'Yes',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => '0',
    'type' => 'list_boolean',
  );

  // Exported field_base: 'field_prereq_instructions'
  $field_bases['field_prereq_instructions'] = array(
    'active' => '1',
    'cardinality' => '1',
    'deleted' => '0',
    'entity_types' => array(),
    'field_name' => 'field_prereq_instructions',
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'module' => 'text',
    'settings' => array(),
    'translatable' => '0',
    'type' => 'text_long',
  );

  return $field_bases;
}
