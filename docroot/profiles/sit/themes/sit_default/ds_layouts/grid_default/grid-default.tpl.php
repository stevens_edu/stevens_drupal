<?php
/**
 * @file
 * Display Suite example layout template.
 *
 * Available variables:
 *
 * Layout:
 * - $classes: String of classes that can be used to style this layout.
 * - $contextual_links: Renderable array of contextual links.
 *
 * Regions:
 *
 * - $top_left: Rendered content for the "Left" region.
 * - $top_left_classes: String of classes that can be used to style the "Left" region.
 *
 * - $top_right: Rendered content for the "Right" region.
 * - $top_right_classes: String of classes that can be used to style the "Right" region.
 */
$top_left_classes = ''; 
$top_right_classes = '';
$bottom_left_classes = '';
$bottom_right_classes = '';

if ($top_right) {
	$top_left_classes = ' grid-12 alpha';
	$top_right_classes = ' grid-6 omega';
}

if ($bottom_right) {
	$bottom_left_classes = ' grid-12 alpha';
	$bottom_right_classes = ' grid-6 omega';
}
?>
<div class="<?php print $classes; ?> clearfix">

  <?php if (isset($title_suffix['contextual_links'])): ?>
    <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

  <!-- regions -->

  <?php if ($header): ?>
		<?php print $header; ?>
  <?php endif; ?>
  
  <?php if ($top_left OR $top_right): ?>
	  <div class="ds-top-group">
	
		  <?php if ($top_left): ?>
		    <div class="ds-top-left<?php print $top_left_classes; ?>">
		      <?php print $top_left; ?>
		    </div>
		  <?php endif; ?>
		
		  <?php if ($top_right): ?>
		    <div class="ds-top-right<?php print $top_right_classes; ?>">
		      <?php print $top_right; ?>
		    </div>
		  <?php endif; ?>
	  
	  </div>
  <?php endif; ?>

  <?php if ($bottom_left OR $bottom_right): ?>
	  <div class="ds-bottom-group">

		  <?php if ($bottom_left): ?>
		    <div class="ds-bottom-left<?php print $bottom_left_classes; ?>">
		      <?php print $bottom_left; ?>
		    </div>
		  <?php endif; ?>
		
		  <?php if ($bottom_right): ?>
		    <div class="ds-bottom-right<?php print $bottom_right_classes; ?>">
		      <?php print $bottom_right; ?>
		    </div>
		  <?php endif; ?>

	  </div>
  <?php endif; ?>

  <?php if ($footer): ?>
    <div class="ds-footer<?php print $footer_classes; ?>">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

  <!-- These comments are required for the Drush command. You can remove them in your own copy -->
  <!-- /regions -->

</div>
