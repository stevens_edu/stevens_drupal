<?php

/**
 * implementation of hook_panels_layouts()
 */
// Plugin definition
$plugin = array(
  'title' => t('One Column Clean'),
  'category' => t('Custom'),
  'icon' => 'onecol_clean.png',
  'theme' => 'panels_onecol_clean',
  'regions' => array('middle' => t('Content')),
);
