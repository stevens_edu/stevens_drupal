<?php

/**
 * implementation of hook_panels_layouts()
 */
// Plugin definition
$plugin = array(
  'title' => t('No Wrap'),
  'category' => t('Custom'),
  'icon' => 'nowrap.png',
  'theme' => 'panels_nowrap',
  'regions' => array('middle' => t('Content')),
);
