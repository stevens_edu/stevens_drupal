<?php
/**
 * @file views-view-table--clone-of-bios.tpl.php
 * Template to display bios
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * @ingroup views_templates
 */
?>
<?php foreach($rows as $rowId => $row): ?>
<h1 class="title page-title">
<?php //echo "<pre>"; var_dump($row); echo "</pre>";?>
  <?php print $row['field_bio_salutation'] . " " . $row['field_bio_firstname'] . " " . $row['field_bio_middlename'] . $row['field_bio_lastname']; ?></h1>


<table cellpadding=0 cellspacing=0 border=0 width=100%>
<tr><Td valign=top width=140px><?php echo $row['field_bio_image']; ?></td>
<td width=15px></td>
<Td align=left valign=top>
<table cellpadding=5 cellspacing=0 border=0>
<tr><td>

<div>
 <?php if(!isset($row['field_bio_title'])){
		$row['field_bio_title'] = "";
	}
 ?>
 <?php print $row['field_bio_title']; ?></div>

  <?php if(isset($row['field_bio_building']) && $row['field_bio_building'] != ""): ?>

<div><?php print (isset($row['field_bio_building']) ? "Building:&nbsp;" : ""); ?><?php print $row['field_bio_building']; ?></div>
  
  <?php endif; ?>

  <?php if(isset($row['field_bio_room']) && $row['field_bio_room'] != ""): ?>

<div><?php print (isset($row['field_bio_room']) ? "Room:&nbsp;" : ""); ?>
		<?php print $row['field_bio_room']; ?>

</div>
  <?php endif; ?>

  <?php if(isset($row['field_bio_phone']) && $row['field_bio_phone'] != ""): ?>
	  
<div><?php print (isset($row['field_bio_phone']) ? "Phone:&nbsp;" : ""); ?><?php print $row['field_bio_phone']; ?></div>
	
  <?php endif; ?>
	  
  <?php if(isset($row['field_bio_fax']) && $row['field_bio_fax'] != ""): ?>
	  <div><?php print (isset($row['field_bio_fax']) ? "Fax:&nbsp;" : ""); ?><?php print $row['field_bio_fax']; ?></div>
	  </div>
  <?php endif; ?>
	  
  <?php if(isset($row['field_bio_email']) && $row['field_bio_email'] != ""): ?>
	  <div>
<?php print (isset($row['field_bio_email']) ? "Email:&nbsp" : ""); ?></span><?php print $row['field_bio_email']; ?></div>
	  </div>
  <?php endif; ?>


  <?php if(isset($row['field_bio_website']) && $row['field_bio_website'] != ""): ?>

	  <div>
<?php echo "<a href='".$row['field_bio_website']."'>Personal Webpage</a>"; ?>
	  </div>
  <?php endif; ?>

</td></tr>
</table>
</td></tr>
</table>
<br>
  <?php if(isset($row['field_bio_education']) && $row['field_bio_education'] != ""): ?>
  <div>
    <h3 class="label-above">Education:</h3>
	<p><?php print $row['field_bio_education']; ?></p>
  </div>
  <?php endif; ?>

<?php if(isset($row['field_bio_research_interests']) && $row['field_bio_research_interests'] != ""): ?>	
  <div>
    <h3 class="label-above">Research:</h3>
	<p><?php print $row['field_bio_research_interests']; ?></p>
  </div>
<?php endif; ?>
  
<?php if(isset($row['body']) && $row['body'] != ""): ?>	
  <div>
    <h3 class="label-above">General Information:</h3>
	<p><?php print $row['body']; ?></p>
  </div>
<?php endif; ?>

  <?php if(isset($row['field_bio_experience']) && $row['field_bio_experience'] != ""): ?>
  <div>
    <h3 class="label-above">Experience:</h3>
	<?php print $row['field_bio_experience']; ?>
  </div>
  <?php endif; ?>

 <?php if(isset($row['field_bio_acad_actserv']) && $row['field_bio_acad_actserv'] != ""): ?>  
  <div>
    <h3 class="label-above">Academic Activities/Services</h3>
	<?php print $row['field_bio_acad_actserv']; ?>
  </div>
 <?php endif; ?>
  
  <?php if(isset($row['field_bio_pro_actserv']) && $row['field_bio_pro_actserv'] != ""): ?>
	  <div>
		<h3 class="label-above">Professional Service</h3>
			<?php print $row['field_bio_pro_actserv']; ?>
		</ul>
	  </div>
	<?php endif; ?>


<?php if(isset($row['consulting']) && $row['consulting'] != ""): ?>	
  <div>
    <h3 class="label-above">Consulting Service</h3>
	<?php print $row['consulting']; ?>
  </div>
<?php endif; ?>


  <?php if(isset($row['field_bio_technogenesis']) && $row['field_bio_technogenesis'] != ""): ?>
	  <div>
		<h3 class="label-above">Technogenesis Service</h3>
			<?php print $row['field_bio_technogenesis']; ?>
		</ul>
	  </div>
	<?php endif; ?>


<?php if(isset($row['field_bio_awards']) && $row['field_bio_awards'] != ""): ?>	
  <div>
    <h3 class="label-above">Honors & Awards</h3>
	<?php print $row['field_bio_awards']; ?>
  </div>
<?php endif; ?>

<?php if(isset($row['field_bio_appt_thesis_suprv']) && $row['field_bio_appt_thesis_suprv'] != ""): ?>  
  <div>
    <h3 class="label-above">Appointments</h3>
	<?php print $row['field_bio_appt_thesis_suprv']; ?>
  </div>
 <?php endif; ?>

<?php if(isset($row['field_bio_pro_orgs']) && $row['field_bio_pro_orgs'] != ""): ?>  
  <div>
    <h3 class="label-above">Professional Societies</h3>
	<?php print $row['field_bio_pro_orgs']; ?>
  </div>
 <?php endif; ?>
 
 <?php if(isset($row['field_bio_funding']) && $row['field_bio_funding'] != ""): ?>  
  <div>
    <h3 class="label-above">Grants, Contracts & Funds</h3>
	<?php print $row['field_bio_funding']; ?>
  </div>
 <?php endif; ?>

 <?php if(isset($row['field_bio_patents']) && $row['field_bio_patents'] != ""): ?>  
  <div>
    <h3 class="label-above">Patents & Inventions</h3>
	<?php print $row['field_bio_patents']; ?>
  </div>
 <?php endif; ?>
 
<?php
$view = views_get_view('bio_publications');
print $view->execute_display('publications', $row['nid']);

?>


 
 <?php

//Need to the count of view results first
// Accomplishing this by counting the number of words in the result
// If the number of words is > 70, we know what at least 1  record was found
// Otherwise the content is simply the empty markup.

$publications=views_embed_view('bio_publications','publications', $row['nid']);
$countExplodedPublications = count(explode(' ', $publications));
//echo "abc<pre>";var_dump(count(explode(' ',$publications)));echo "</pre>def";die();
if($countExplodedPublications >= 70)
{
        echo '<h3 class="label-above">Publications:</h3>';
        echo $publications;
}

        //Need to the count of view results first
        // Accomplishing this by counting the number of words in the result
        // If the number of words is > 70, we know what at least 1  record was found
        // Otherwise the content is simply the empty markup.
        $courses = views_embed_view('courses','bio_courses', $row['nid']);

        //echo "abc<pre>";var_dump(count(explode(' ',$courses)));echo "</pre>def";die();
        $countExplodedCourses = count(explode(' ', $courses));
        if($countExplodedCourses >= 70 ):
 ?>
         <div>
                  <h3 class="label-above">Courses:</h3>
                  <?php print $courses;?>
         </div>
 <?php endif; ?>

 <?php endforeach; ?>

