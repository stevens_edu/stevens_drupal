<?php

/**
 * @file media_youtube/includes/themes/media-youtube-video.tpl.php
 *
 * Template file for theme('media_youtube_video').
 *
 * Variables available:
 *  $uri - The uri to the YouTube video, such as youtube://v/xsy7x8c9.
 *  $video_id - The unique identifier of the YouTube video.
 *  $width - The width to render.
 *  $height - The height to render.
 *  $autoplay - If TRUE, then start the player automatically when displaying.
 *  $fullscreen - Whether to allow fullscreen playback.
 *
 * Note that we set the width & height of the outer wrapper manually so that
 * the JS will respect that when resizing later.
 */
?>
<!--<div class="media-outer-wrapper media-youtube-outer-wrapper" id="media-youtube-<?php print $id; ?>">

    
</div>-->
<div class="media-outer-wrapper media-youtube-outer-wrapper" id="media-youtube-<?php print $id; ?>" width="<?php print $width; ?>" height="<?php print $height; ?>">
  <div class="media-youtube-preview-wrapper" id="<?php print $video_id . "_" . $id; ?>">
    <iframe class="media-youtube-player" <?php print $api_id_attribute; ?>width="<?php print $width; ?>" height="<?php print $height; ?>" src="<?php print $url; ?>" frameborder="0" allowfullscreen></iframe>
  </div>
</div>