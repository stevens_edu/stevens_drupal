<div<?php print $attributes; ?>>
  <div class="logo-img">
    <img src="<?php print base_path() . path_to_theme() . '/logo.png'; ?>" />
  </div>
  <?php print $content; ?>
</div>
