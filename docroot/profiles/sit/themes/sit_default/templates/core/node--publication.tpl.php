<?php if ($teaser): ?>

	<p class="publication-info">
	
		<?php if ($pub_authors): ?>
			<span class="pub-authors"><?php print $pub_authors; ?> </span>
		<?php endif; ?>
	
		<?php if ($pub_month OR $pub_year): ?>
			<span class="pub-date">(<?php if ($pub_month): ?><?php print $pub_month . ' '; ?><?php endif; ?><?php if ($pub_year): ?><?php print $pub_year; ?><?php endif; ?>) </span>
		<?php endif; ?>
	
		<?php if ($pub_book_title): ?>
			"<strong class="pub-book-title"><?php print $pub_book_title; ?></strong>"<?php if ($pub_conference || $pub_venue || $pub_editors || $pub_publication || $pub_publisher || $pub_type || $pub_volume || $pub_issue || $pub_page_number || $pub_url): ?>, <?php else: ?>.<?php endif; ?>
		<?php endif; ?>
		
		<?php if ($pub_conference): ?>
			<span class="pub-conference"><?php print $pub_conference; ?><?php if ($pub_venue || $pub_editors || $pub_publication || $pub_publisher || $pub_type || $pub_volume || $pub_issue || $pub_page_number || $pub_url): ?>, <?php else: ?>.<?php endif; ?></span>
		<?php endif; ?>
	
		<?php if ($pub_venue): ?>
			<span class="pub-venue"><?php print $pub_venue; ?>. </span>
		<?php endif; ?>
	
		<?php if ($pub_editors): ?>
			<span class="pub-editors"><?php print $pub_editors; ?><?php if ($pub_publication || $pub_publisher || $pub_type || $pub_volume || $pub_issue || $pub_page_number || $pub_url): ?>, <?php else: ?>.<?php endif; ?></span>
		<?php endif; ?>
	
		<?php if ($pub_publication): ?>
			<em class="pub-publications"><?php print $pub_publication; ?><?php if ($pub_publisher || $pub_type || $pub_volume || $pub_issue || $pub_page_number || $pub_url): ?>, <?php else: ?>.<?php endif; ?></em>
		<?php endif; ?>
	
		<?php if ($pub_publisher): ?>
			<span class="pub-publisher"><?php print $pub_publisher; ?><?php if ($pub_type || $pub_volume || $pub_issue || $pub_page_number || $pub_url): ?>, <?php else: ?>.<?php endif; ?></span>
		<?php endif; ?>
	
		<?php if ($pub_type): ?>
			<span class="pub-type"><?php print $pub_type; ?><?php if ($pub_volume || $pub_issue || $pub_page_number || $pub_url): ?>, <?php else: ?>.<?php endif; ?></span>
		<?php endif; ?>
	
		<?php if ($pub_volume OR $pub_issue): ?>
			<span class="pub-volume"><?php if ($pub_volume): ?>Vol. <?php print $pub_volume; ?><?php endif; ?><?php if ($pub_issue): ?>(<?php print ' ' . $pub_issue; ?>)<?php endif; ?><?php if ($pub_page_number || $pub_url): ?>, <?php else: ?>.<?php endif; ?></span>
		<?php endif; ?>
	
		<?php if ($pub_page_number): ?>
			<span class="pub-page-number">p.&nbsp;<?php print $pub_page_number; ?>. </span>
		<?php endif; ?>
	
		<?php if ($pub_url): ?>
			<span class="pub-url"><?php print $pub_url; ?><?php if ($pub_url_size): ?> <?php print $pub_url_size; ?>&nbsp;<abbr title="Portable Document Format">PDF</abbr><?php endif; ?>.</span>
		<?php endif; ?>
	
	</p>

<?php else: ?>

	<article<?php print $attributes; ?>>
	  <?php print $user_picture; ?>
	  
	  <?php if (!$page && $title): ?>
	  <header>
	    <?php print render($title_prefix); ?>
	    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
	    <?php print render($title_suffix); ?>
	  </header>
	  <?php endif; ?>
	  
	  <?php if ($display_submitted): ?>
	  <footer class="submitted"><?php print $date; ?> -- <?php print $name; ?></footer>
	  <?php endif; ?>  
	  
	  <div<?php print $content_attributes; ?>>
	    <?php
	      // We hide the comments and links now so that we can render them later.
	      hide($content['comments']);
	      hide($content['links']);
	      print render($content);
	    ?>
	  </div>
	  
	  <div class="clearfix">
	    <?php if (!empty($content['links'])): ?>
	      <nav class="links node-links clearfix"><?php print render($content['links']); ?></nav>
	    <?php endif; ?>
	
	    <?php print render($content['comments']); ?>
	  </div>
	</article>

<?php endif; ?>