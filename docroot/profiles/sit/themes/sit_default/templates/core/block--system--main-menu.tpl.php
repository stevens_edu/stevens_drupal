<?php $tag = $block->subject ? 'section' : 'div'; ?>
<<?php print $tag; ?><?php print $attributes; ?>>
  <?php print render($title_prefix); ?>
  <?php if ($block->subject): ?>
    <h2<?php print $title_attributes; ?>><?php print $block->subject; ?></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php print $content ?>
</<?php print $tag; ?>>

<?php if ($secondary_menu): //Add secondary menu for school sites ?>

  <div id="block-menu-menu-secondary" class="block block-menu menu-block secondary-menu-block block-menu-secondary block-menu-menu-secondary even block-without-title">
    <ul class="menu">
      <li class="first expanded engage"><span>Engage</span>
      <ul class="menu">
        <li class="first leaf prospective-students"><a href="/sit/about/engage#prospective">Prospective Students</a></li>
        <li class="leaf current-students"><a href="/sit/about/engage#current">Current Students</a></li>
        <li class="leaf international-students"><a href="/sit/about/engage#international">International Students</a></li>
        <li class="leaf faculty-staff"><a href="/sit/about/engage#faculty">Faculty &amp; Staff</a></li>
        <li class="leaf alumni"><a href="/sit/about/engage#alumni">Alumni</a></li>
        <li class="leaf working-at-stevens"><a href="/sit/about/engage#working">Working at Stevens</a></li>
        <li class="last leaf apply"><a href="/sit/about/engage#apply">Apply</a></li>
      </ul>
      </li>
      <li class="expanded academics"><span>Academics</span>
      <ul class="menu">
        <li class="first leaf academics-overview academics-overview"><a href="http://www.stevens.edu/provost/academics">Academics Overview</a></li>
        <li class="leaf schaefer-school-of-engineering-sciences schaefer-school-of-engineering-sciences"><a href="/ses">Schaefer School of Engineering &amp; Sciences</a></li>
        <li class="leaf school-of-systems-enterprises school-of-systems-enterprises"><a href="/sse">School of Systems &amp; Enterprises</a></li>
        <li class="leaf school-of-business school-of-business"><a href="/business">School of Business</a></li>
        <li class="leaf college-of-arts-and-letters college-of-arts-and-letters"><a href="/cal">College of Arts and Letters</a></li>
        <li class="leaf academic-entrepreneurship academic-entrepreneurship"><a href="http://www.stevens.edu/entrepreneurship">Academic Entrepreneurship</a></li>
        <li class="last leaf office-of-the-provost office-of-the-provost"><a href="http://www.stevens.edu/provost">Office of the Provost</a></li>
      </ul>
      </li>
      <li class="expanded admissions"><span>Admissions</span>
      <ul class="menu">
        <li class="first leaf undergraduate undergraduate"><a href="http://www.stevens.edu/admissions">Undergraduate</a></li>
        <li class="leaf graduate graduate"><a href="http://www.stevens.edu/graduate">Graduate</a></li>
        <li class="leaf webcampus webcampus"><a href="http://www.stevens.edu/webcampus">WebCampus</a></li>
        <li class="leaf international international"><a href="/sit/international/index.cfm">International</a></li>
        <li class="leaf veterans veterans"><a href="http://www.stevens.edu/veterans">Veterans</a></li>
        <li class="leaf washington-dc washington-dc"><a href="http://www.stevens.edu/dc">Washington DC</a></li>
        <li class="last leaf pre-college pre-college"><a href="/sit/admissions/highschool/pre-collegeprograms.cfm">Pre-College</a></li>
      </ul>
      </li>
      <li class="expanded research"><span>Research</span>
      <ul class="menu">
        <li class="first leaf research-enterprise research-enterprise"><a href="http://www.stevens.edu/research">Research Enterprise</a></li>
        <li class="leaf sponsored-research sponsored-research"><a href="http://www.stevens.edu/osr">Sponsored Research</a></li>
        <li class="leaf research-centers research-centers"><a href="http://www.stevens.edu/research/centers.html">Research Centers</a></li>
        <li class="last leaf research-at-stevens research-at-stevens"><a href="http://www.stevens.edu/research/research.html">Research at Stevens</a></li>
      </ul>
      </li>
      <li class="expanded athletics"><span>Athletics</span>
      <ul class="menu">
        <li class="first leaf stevens-athletics stevens-athletics"><a href="http://www.stevensducks.com/">Stevens Athletics</a></li>
        <li class="leaf men-039-s-sports men-039-s-sports"><a href="http://www.stevensducks.com/sports/2012/10/10/Msports.aspx">Men&apos;s Sports</a></li>
        <li class="leaf women-039-s-sports women-039-s-sports"><a href="http://www.stevensducks.com/sports/2012/10/10/Womensports.aspx</a></li>
        <li class="leaf recreation recreation"><a href="http://stevensrec.com/">Recreation</a></li>
        <li class="last leaf facilities facilities"><a href="http://www.stevensducks.com/facilities">Facilities</a></li>
      </ul>
      </li>
      <li class="expanded university-life"><span>University Life</span>
      <ul class="menu">
        <li class="first leaf ug-student-life ug-student-life"><a href="http://www.stevens.edu/student_life">UG Student Life</a></li>
        <li class="leaf grad-student-life grad-student-life"><a href="http://www.stevens.edu/gsa">Grad Student Life</a></li>
        <li class="leaf career-development career-development"><a href="http://www.stevens.edu/ocd">Career Development</a></li>
        <li class="leaf co-op-education co-op-education"><a href="http://www.stevens.edu/co-op">Co-op Education</a></li>
        <li class="leaf residence-life residence-life"><a href="http://www.stevens.edu/housing">Residence Life</a></li>
        <li class="leaf administration administration"><a href="/sit/administration-and-enrollment/index.cfm">Administration</a></li>
        <li class="last leaf performing-arts performing-arts"><a href="http://www.stevens.edu/debaun">Performing Arts</a></li>
      </ul>
      </li>
      <li class="expanded news"><span>News</span>
      <ul class="menu">
        <li class="first leaf stevens-news stevens-news"><a href="/news">Stevens News</a></li>
        <li class="leaf multimedia multimedia"><a href="/news/multimedia">Multimedia</a></li>
        <li class="leaf events events"><a href="/news/events">Events</a></li>
        <li class="last leaf stevens-experts stevens-experts"><a href="/news/stevens-experts">Stevens Experts</a></li>
      </ul>
      </li>
      <li class="last expanded about"><span>About</span>
      <ul class="menu">
        <li class="first leaf about-stevens about-stevens"><a href="/sit/about/index.cfm">About Stevens</a></li>
        <li class="leaf rankings rankings"><a href="/sit/about/rankings-recognition">Rankings</a></li>
        <li class="leaf leadership"><a href="/sit/trustees/trustee-profiles.cfm">Leadership</a></li>
        <li class="leaf governance governance"><a href="http://www.stevens.edu/provost/policies/">Governance</a></li>
        <li class="leaf visiting-campus visiting-campus"><a href="/sit/about/visit">Visiting Campus</a></li>
        <li class="last leaf brand-graphic-standards brand-graphic-standards"><a href="/sit/branding-identity">Brand &amp; Graphic Standards</a></li>
      </ul>
      </li>
    </ul>
  </div>

<?php endif; ?>
