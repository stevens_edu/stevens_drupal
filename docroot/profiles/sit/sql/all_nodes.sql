--ACCESS=access administration pages
(
  SELECT
                                    n.nid,
                                    n.created AS Date_Created,
                                    n.changed AS Last_Modified,
                                    n.status AS Published,
                                    u.name as Author,
                                    n.type AS Content_Type,
                                    adf.field_shared_adminunits_tid as admin_unit_id_from_adf,
                                    adf.entity_id as entity_id,
                                    ttd.name AS admin_unit_name,
                                    ttd2.name AS admin_unit_parent_name,
                                    n.title,
                                    ul.source AS alias_source,
                                    CONCAT('http://www.stevens.edu/sit/',ul.alias) AS Link,
                                    ttd.tid AS admin_unit_id,
                                    ttd2.tid AS admin_unit_parent_id
                                  FROM
                                      field_data_field_shared_adminunits AS adf
                                  LEFT JOIN
                                      taxonomy_term_data AS ttd
                                  ON 
                                      ttd.tid = adf.field_shared_adminunits_tid
                                  LEFT JOIN
                                    node as n
                                  ON
                                    n.nid = adf.entity_id
                                  LEFT JOIN
                                    url_alias AS ul
                                  ON 
                                      CONCAT('node/',adf.entity_id) = ul.source

                                  LEFT JOIN
                                    taxonomy_term_hierarchy AS tth
                                  ON
                                    tth.tid = ttd.tid
                                  LEFT JOIN
                                    taxonomy_term_data AS ttd2
                                  ON
                                    ttd2.tid = tth.parent

                                  LEFT JOIN
                                    users AS u
                                  ON 
                                    n.uid = u.uid
 
                                  WHERE
                                      1 = 1
--IF=:published                                     
                                      AND n.status = :published
--END

--IF=:unpublished                                     
                                      AND n.status <> :unpublished
--END

--IF=:node_author                                
                                      AND u.name IN (:node_author)
--END

--IF=:contentType
                                      AND n.type IN (:contentType)
--END

--IF=:startDate
                                      AND n.created >= :startDate 
--END

--IF=:endDate
                                      AND n.created <= :endDate
--END

--IF=:changedStartDate
                                      AND n.changed >= :changedStartDate 
--END

--IF=:changedEndDate
                                      AND n.changed <= :changedEndDate
--END

--IF=:adminUnit
                                      AND (ttd.name IN (:adminUnit)
                                      OR ttd2.name IN (:adminUnit))
--END
                                      
                                  ORDER BY
                                      admin_unit_parent_name,
                                      admin_unit_name                       
)
--IF=:adminUnit
;
--ELSE
UNION
(SELECT
                                    n.nid,
                                    n.created AS Date_Created,
                                    n.changed AS Last_Modified,
                                    n.status AS Published,
                                    u.name as Author,
                                    n.type AS Content_Type,
                                    NULL as admin_unit_id_from_adf,
                                    n.nid as entity_id,
                                    NULL AS admin_unit_name,
                                    NULL AS admin_unit_parent_name,
                                    n.title,
                                    ul.source,
                                    CONCAT('http://www.stevens.edu/sit/',ul.alias) AS Link,
                                    NULL AS admin_unit_id,
                                    NULL AS admin_unit_parent_id
                                  FROM
                                    node AS n
                                  LEFT JOIN
                                    url_alias AS ul
                                  ON 
                                    CONCAT('node/',n.nid) = ul.source
                                  LEFT JOIN
                                    users AS u
                                  ON 
                                    n.uid = u.uid
                                  WHERE
                                  1 = 1 
                                  AND
                                    n.nid NOT IN (
                                                      SELECT
                                                          entity_id
                                                      FROM
                                                          field_data_field_shared_adminunits
                                                  )
--IF=:published                                     
                                    AND n.status = :published
--END

--IF=:unpublished                                     
                                    AND n.status <> :unpublished
--END

--IF=:node_author                                  
                                    AND u.name IN (:node_author)                                      
--END

--IF=:contentType
                                    AND n.type IN (:contentType)
--END

--IF=:startDate
                                    AND n.created >= :startDate 
--END

--IF=:endDate
                                    AND n.created <= :endDate
--END

--IF=:changedStartDate
                                    AND n.changed >= :changedStartDate 
--END

--IF=:changedEndDate
                                    AND n.changed <= :changedEndDate
--END                               
)
--END
--INFO
type[startDate]=unixtime
type[endDate]=unixtime
type[changedStartDate]=unixtime
type[changedEndDate]=unixtime
