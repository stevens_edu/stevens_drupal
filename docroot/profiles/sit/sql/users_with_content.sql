--ACCESS=access administration pages
								  SELECT
                    DISTINCT(u.name) as Author
                                  FROM
										node as n
                                  LEFT OUTER JOIN
                                    users AS u
                                  ON 
                                    n.uid = u.uid

                                  WHERE
                                      n.status = 1
                                      AND u.name IS NOT NULL
                                      AND u.name <> ''
								  ORDER BY
									  u.name
