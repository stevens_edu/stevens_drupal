--ACCESS=access administration pages
SELECT u.uid,
	   u.name AS Username,
	   u.mail AS EMail,
       u.login AS 'LastLogin',
       u.status AS Active,
       ttd.name AS 'EditorOf'
FROM 
       workbench_access_user as wau

JOIN
	users AS u
ON
	u.uid = wau.uid
    
JOIN
  taxonomy_term_data as ttd
ON
	ttd.tid = wau.access_id

  WHERE 1 = 1 
  AND u.status = 1

ORDER BY
  u.name
