--ACCESS=access administration pages
SELECT
  DISTINCT(ttd.name) AS admin_unit_name
FROM
  field_data_field_shared_adminunits AS adf
LEFT JOIN
  taxonomy_term_data AS ttd
ON 
  ttd.tid = adf.field_shared_adminunits_tid
ORDER BY
  admin_unit_name
