--ACCESS=access administration pages
SELECT u.uid,
	   u.name AS Username,
	   u.mail AS EMail,
       u.login AS 'LastLogin',
       u.status AS Active,
       ttd.name AS 'EditorOf'
FROM 
       workbench_access_user as wau

JOIN
	users AS u
ON
	u.uid = wau.uid
    
JOIN
  taxonomy_term_data as ttd
ON
	ttd.tid = wau.access_id

  WHERE 1 = 1 
  AND u.status = 1

--IF=:user
  AND u.name IN (:user)
--END

--IF=:lastLoginBefore
  AND u.login <= :lastLoginBefore
--END

--IF=:lastLoginAfter
  AND u.login >= :lastLoginAfter
--END

--IF=:adminUnit
  AND ttd.name IN (:adminUnit)
--END

ORDER BY
  u.name

--INFO
type[lastLoginBefore]=unixtime
type[lastLoginAfter]=unixtime
