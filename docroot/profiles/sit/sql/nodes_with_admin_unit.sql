
--ACCESS=access administration pages
SELECT
  n.nid,
  (n.created/86400)+25569 AS Date_Created,
  (n.changed/86400)+25569 AS Last_Modified,
  u.name as Author,
  adf.entity_type AS Content_Type,
  adf.field_shared_adminunits_tid as admin_unit_id_from_adf,
  adf.entity_id as entity_id,
  ttd.name AS admin_unit_name,
  ttd2.name AS admin_unit_parent_name,
  n.title,
  ul.source AS alias_source,
  CONCAT('http://www.stevens.edu/sit/',ul.alias) AS Link,
  ttd.tid AS admin_unit_id,
  ttd2.tid AS admin_unit_parent_id
FROM
    field_data_field_shared_adminunits AS adf
LEFT JOIN
    taxonomy_term_data AS ttd
ON 
    ttd.tid = adf.field_shared_adminunits_tid
LEFT JOIN
  node as n
ON
  n.nid = adf.entity_id
LEFT JOIN
  url_alias AS ul
ON 
    CONCAT('node/',adf.entity_id) = ul.source

LEFT JOIN
  taxonomy_term_hierarchy AS tth
ON
  tth.tid = ttd.tid
LEFT JOIN
  taxonomy_term_data AS ttd2
ON
  ttd2.tid = tth.parent

LEFT JOIN
  users AS u
ON 
  n.uid = u.uid

WHERE
    n.status = 1
    AND adf.entity_type <> 'curriculum'
    AND adf.entity_type <> 'publication'
    
ORDER BY
    admin_unit_parent_name,
    admin_unit_name;
                                
