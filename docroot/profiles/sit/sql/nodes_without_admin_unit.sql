--ACCESS=access administration pages
SELECT
  n.nid,
  (n.created/86400)+25569 AS Date_Created,
  (n.changed/86400)+25569 AS Last_Modified,
  u.name as Author,
  n.type AS Content_Type,
  NULL as admin_unit_id_from_adf,
  n.nid as entity_id,
  NULL AS admin_unit_name,
  NULL AS admin_unit_parent_name,
  n.title,
  ul.source,
  CONCAT('http://www.stevens.edu/$siteUrlId/',ul.alias) AS Link,
  NULL AS admin_unit_id,
  NULL AS admin_unit_parent_id
FROM
  node AS n
LEFT JOIN
  url_alias AS ul
ON 
  CONCAT('node/',n.nid) = ul.source
LEFT JOIN
  users AS u
ON 
  n.uid = u.uid
WHERE 
  n.nid NOT IN (
                    SELECT
                        entity_id
                    FROM
                        field_data_field_shared_adminunits
                )
AND
   n.status = 1
    AND n.type <> 'curriculum'
    AND n.type <> 'publication'
    ;
                                
