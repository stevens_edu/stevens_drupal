<?php
/**
 * This file holds common NON-PROD settings for all sites
 */

// Allow Bearer authorization without caching, as it competes with Shield.
if (strpos($_SERVER['HTTP_AUTHORIZATION'], 'Bearer') === 0) {
  $conf['shield_user'] = NULL;
  $GLOBALS['conf']['cache'] = false;
}

// Ensure that SWEBB doesn't blast the real listserv except in prod.
$conf['swebb_to'] = 'it-web@stevens.edu';
