<?php

function fs_importer_index() {
  $items = array();
  foreach (fs_importer_get_importers() as $key => $importer) {
    $items[] = l($importer['name'], 'admin/importers/' . $key);
  }
  return theme('item_list', array('items' => $items));
}

function fs_importer_settings_form($form, &$form_state, $importer_key) {
  $importers = fs_importer_get_importers();
  $importer = $importers[$importer_key];
  $form['importer'] = array(
    '#type' => 'value',
    '#value' => $importer,
  );
  $form['fs_importer__' . $importer_key . '__settings_url'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('fs_importer__' . $importer_key . '__settings_url', ''),
    '#title' => t('URL'),
  );
  $form['fs_importer__' . $importer_key . '__enabled'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('fs_importer__' . $importer_key . '__enabled', FALSE),
    '#title' => t('Enable Cron Import'),
  );
  $data_queue_items = fs_importer_get_data_queue_items($importer);
  $process_queue_items = fs_importer_get_process_queue_items($importer);
  $form['actions'] = array();
  $form['actions']['test'] = array(
    '#type' => 'submit',
    '#value' => t('Test Importer'),
    '#name' => 'test',
    '#ajax' => array(
      'callback' => 'fs_importer_test_settings',
      'wrapper' => 'fs-importer-test',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );
  $form['actions']['manual'] = array(
    '#type' => 'submit',
    '#value' => t('Manually Queue Importer'),
    '#name' => 'manual',
  );
  $form['actions']['cleanup'] = array(
    '#type' => 'submit',
    '#value' => t('Cleanup Failed Queue Items (!item_count)', array('!item_count' => format_plural((count($data_queue_items) + count($process_queue_items)), 'item', '@count items'))),
    '#name' => 'cleanup',
  );
  $form['test'] = array(
    '#markup' => '<div id="fs-importer-test"></div>',
    '#weight' => 1000,
  );
  $form['#submit'][] = 'fs_importer_settings_form_submit';
  $form = $importer['settings callback']($form, $form_state);
  return system_settings_form($form);
}

function fs_importer_settings_form_submit($form, &$form_state) {
  $importer = $form_state['values']['importer'];
  switch ($form_state['clicked_button']['#name']) {
    case 'save_settings':

      break;
    case 'manual':
      $batch = array(
        'operations' => array(),
        'title' => t('Import: !importer', array('!importer' => $importer['name'])),
        'init_message' => t('Beginning import: !importer', array('!importer' => $importer['name'])),
        'progress_message' => t('Processed @current out of @total.'),
        'error_message' => t('Doctor import has encountered an error.'),
        'file' => 'fs_importer.admin.inc',
      );
      $batch['operations'][] = array('fs_importer_get_data', array($importer, TRUE));
      batch_set($batch);
      break;
    case 'cleanup':
      $data_queue_items = fs_importer_get_data_queue_items($importer);
      foreach ($data_queue_items as $item) {
        db_delete('queue')
          ->condition('item_id', $item['item_id'])
          ->execute();
      }
      $process_queue_items = fs_importer_get_process_queue_items($importer);
      foreach ($process_queue_items as $item) {
        db_delete('queue')
          ->condition('item_id', $item['item_id'])
          ->execute();
      }
      break;
  }
}

function fs_importer_test_settings(&$form, &$form_state) {
  $importer = $form_state['values']['importer'];
  $error = FALSE;
  $url = variable_get('fs_importer__' . $importer['machine name'] . '__settings_url', FALSE);
  if (!$url) {
    $error = TRUE;
  }
  $data = array();
  try {
    $data = $importer['data callback']($url);
  } catch (Exception $e) {
    $error = TRUE;
  }
  if ($error) {
    return '<div id="fs-importer-test">' . kpr('Provided settings resulted in an error.', TRUE) . '</div>';
  } else {
    return '<div id="fs-importer-test">' . kpr($data, TRUE) . '</div>';
  }
}
