<?php

function hook_fs_importer_info() {
  return array(
    'sample_importer' => array(
      // Optional. How often to run data callback and queue items for import. In seconds, defaults to 0, which means run on every cron run
      'frequency' => 60 * 60,
      // Required
      'name' => t('Sample Importer'),
      // Optional.
      'settings callback' => 'fs_importer_sample_importer_settings',
      // Optional. Returns TRUE if the import process should proceed, FALSE if not. If not specified, import process will always proceed if scheduled.
      'queue callback' => 'fs_importer_sample_importer_queue',
      // Required. Should return an array of items to pass to queue up for processing via process callback.
      'data callback' => 'fs_importer_sample_importer_data',
      // Required. Processes items from queue.
      'process callback' => 'fs_importer_sample_importer_process',
    ),
  );
}

function fs_importer_sample_importer_settings($form, &$form_state) {
  return $form;
}

function fs_importer_sample_importer_data() {
  /**
   * Get data from somewhere, transform it, whatever. End result must be an array of things. The things could be
   * associative arrays, objects, etc. Each thing gets passed to the process callback.
   */
  return array();
}

function fs_importer_sample_importer_process($item) {
  // Do something with the item here.
}
