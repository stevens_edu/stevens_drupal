(function ($) {
  $(document).ready(function() {
    function liveChatTitle() {
      if ($('#iFrameResizer1').attr('title', 'Live Chat').length == 0) {
        setTimeout(liveChatTitle, 50);
      }
    };

    liveChatTitle();
  });
}(jQuery));
