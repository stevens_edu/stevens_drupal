<?php

function stevens_faculty_import_fs_importer_info() {
  return array(
    'faculty' => array(
      'name' => t('Faculty Importer'),
      'frequency' => 60 * 60 * 24,
      'data callback' => 'stevens_faculty_import_fs_importer_data',
      'process callback' => 'stevens_faculty_import_fs_importer_process',
    ),
  );
}

function stevens_faculty_import_fs_importer_data($url) {
  $data = array();
  $json_data = file_get_contents($url);
  $json_data = str_replace(array('<?xml version="1.0"?>', '<text>', '</text>'), '', $json_data); //Because they have weird XML parsing of this.
  $data = drupal_json_decode($json_data);
  $facultys = array();
  $keys = array();
  foreach ($data['Physicians'] as $faculty) {
    if (!empty($faculty['physicianID'])) {
      $facultys[] = $faculty;
      $keys[] = $faculty['physicianID'];
    }
  }
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'faculty')
    ->fieldCondition('field_pb_physician_id', 'value', $keys, 'NOT IN');
  $results = $query->execute();
  if (isset($results['node'])) {
    foreach ($results['node'] as $existing) {
      $facultys[] = array('delete' => $existing->nid);
    }
  }
  return $facultys;
}

function stevens_faculty_import_fs_importer_process($item) {
  if (isset($item['delete'])) {
    node_delete($item['delete']);
    return;
  }
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'faculty')
    ->fieldCondition('field_pb_physician_id', 'value', $item['physicianID']);
  $results = $query->execute();
  $existing = FALSE;
  if (isset($results['node'])) {
    $nodes = node_load_multiple(array_keys($results['node']));
    foreach ($nodes as $node) {
      $wrapper = entity_metadata_wrapper('node', $node);
      if ($wrapper->field_pb_physician_id->value() === $item['physicianID']) {
        $existing = $wrapper;
      }
    }
  }
  if ($existing) {
    $wrapper = $existing;
  } else {
    $node = array(
      'type' => 'faculty',
      'uid' => 1,
      'status' => 1,
    );
    $entity = entity_create('node', $node);
    $wrapper = entity_metadata_wrapper('node', $entity);
  }
  if ($wrapper) {
    $api_specialty = taxonomy_vocabulary_machine_name_load("api_specialty");
    $title = stevens_faculty_import_build_name($item);
    $wrapper->title->set($title);
    $wrapper->field_h1_title->set($title);
    $wrapper->field_alphabetical_name->set(stevens_faculty_import_build_alphabetical_name($item));

    if ($item['pict_id']) {
      $wrapper->field_pb_picture_id->set($item['pict_id']);
    }
    if ($item['physicianID']) {
      $wrapper->field_pb_physician_id->set($item['physicianID']);
    }
    if ($item['lastName']) {
      $wrapper->field_pb_last_name->set($item['lastName']);
    }
    if ($item['firstname']) {
      $wrapper->field_pb_first_name->set($item['firstname']);
    }
    if (isset($item['middleInitial'])) {
      $wrapper->field_pb_middle_initial->set($item['middleInitial']);
    }
    if ($item['degree']) {
      $wrapper->field_pb_degree->set($item['degree']);
    }
    if (isset($item['department'])) {
      $wrapper->field_pb_department->set($item['department']);
    }
    if ($item['photoURL']) {
      $wrapper->field_pb_photo_url->set(html_entity_decode($item['photoURL']));
    }
    if ($item['physicianURL']) {
      $wrapper->field_pb_physician_url->set(html_entity_decode($item['physicianURL']));
    }

    if ($item['Specialties']) {
      $specialties = array();
      usort($item['Specialties'], 'stevens_faculty_import_sort_by_sort_order');
      foreach ($item['Specialties'] as $specialty) {
        $specialties[] = stevens_faculty_import_api_sync_get_term($specialty, $api_specialty->vid);
      }
      $wrapper->field_pb_specialty_type->set($specialties);
    }

    if ($item['ClinicalInterests']) {
      stevens_faculty_import_api_sync_sync_relations('ClinicalInterests', 'condition', 'field_related_condition', $wrapper, $item);
      stevens_faculty_import_api_sync_sync_relations('ClinicalInterests', 'test_and_treatment', 'field_related_test_and_treatment', $wrapper, $item);
    }
    $wrapper->author->set(1);
    $wrapper->save();
  }
}

function stevens_faculty_import_sort_by_sort_order($a, $b) {
  $asort = intval($a['SortOrder']);
  $bsort = intval($b['SortOrder']);
  if ($asort === $bsort) {
    return 0;
  }
  return $asort > $bsort ? 1 : -1;
}


/**
 * Build the faculty node title field
 *
 * @param array $faculty faculty array from the API
 *
 * @return int Return the name of the faculty
 */
function stevens_faculty_import_build_name($faculty) {
  $name = "";
  if (array_key_exists("middleInitial", $faculty)) {
    $name = $faculty['firstname']." ".$faculty['middleInitial']." ".$faculty['lastName'];
  } else {
    $name = $faculty['firstname']." ".$faculty['lastName'];
  }

  if (array_key_exists("degree", $faculty)) {
    $name .= ", ".$faculty['degree'];
  }

  return $name;
}

/**
 * Build the alphabetical name field
 *
 * @param array $faculty faculty array from the API
 *
 * @return int Return the alphabetical name
 */
function stevens_faculty_import_build_alphabetical_name($faculty) {
  $name = "";
  if (array_key_exists("middleInitial", $faculty)) {
    $name = $faculty['lastName'].", ".$faculty['firstname']." ".$faculty['middleInitial'];
  } else {
    $name = $faculty['lastName'].", ".$faculty['firstname'];
  }

  return $name;
}

/**
 * Get term from the API and check to see if it already exists. If it does,
 * provide the term ID. If it does not, create it and return the new term ID.
 *
 * @param array $faculty_term faculty term from the API
 * @param int $vid Vocabulary ID to work with
 *
 * @return int Return the node ID
 */
function stevens_faculty_import_api_sync_get_term($faculty_term, $vid) {
  //Check to see if term exists
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'taxonomy_term')
    ->propertyCondition('vid', $vid)
    ->propertyCondition('name', $faculty_term['Name']);
  $result = $query->execute();
  if (isset($result['taxonomy_term'])) {
    //If so, return tid
    $tids = array_keys($result['taxonomy_term']);
    return $tids[0];
  } else {
    //If not, create term and return tid
    $term = new stdClass();
    $term->vid = $vid;
    $term->name = $faculty_term['Name'];
    if (array_key_exists("ID", $faculty_term)) {
      $term->field_pb_id[LANGUAGE_NONE][0]['value'] = $faculty_term['ID'];
    }
    taxonomy_term_save($term);
    return $term->tid;
  }
}

function stevens_faculty_import_api_sync_sync_relations($key, $bundle, $field, $wrapper, $faculty) {
  $relation_lookup = &drupal_static($key . $bundle . $field . __FUNCTION__);
  if (!(isset($relation_lookup))) {
    $query = new EntityFieldQuery();
    $query
      ->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', $bundle);
    $results = $query->execute();
    if (isset($results['node'])) {
      $relations = node_load_multiple(array_keys($results['node']));
      foreach ($relations as $relation) {
        $relation_lookup[$relation->nid] = array($relation->title, _stevens_faculty_import_api_sync_sync_relations_normalize_title($relation->title));
      }
    }
  }

  foreach ($faculty[$key] as $faculty_relation) {
    $normalized_title = _stevens_faculty_import_api_sync_sync_relations_normalize_title($faculty_relation['Name']);
    $relation_nid = FALSE;
    foreach ($relation_lookup as $nid => $lookup) {
      if ($lookup[1] === $normalized_title) {
        $relation_nid = $nid;
      }
    }
    if ($relation_nid) {
      $found = FALSE;
      if (isset($wrapper->{$field})) {
        foreach ($wrapper->{$field}->raw() as $related) {
          if ($related == $relation_nid) {
            $found = TRUE;
          }
        }
      }
      if (!$found) {
        $wrapper->{$field}[] = $relation_nid;
      }
    }
  }
}

function _stevens_faculty_import_api_sync_sync_relations_normalize_title($title) {
  // Remove non-ascii, whitespace
  $title = preg_replace("/[^a-z]/ui", '', $title);
  // Lowercase
  $title = strtolower($title);
  return $title;
}
