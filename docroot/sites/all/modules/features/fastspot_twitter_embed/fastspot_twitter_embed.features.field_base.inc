<?php
/**
 * @file
 * fastspot_twitter_embed.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function fastspot_twitter_embed_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_data_widget_id'.
  $field_bases['field_data_widget_id'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_data_widget_id',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 45,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_twitter_user'.
  $field_bases['field_twitter_user'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_twitter_user',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 15,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  return $field_bases;
}
