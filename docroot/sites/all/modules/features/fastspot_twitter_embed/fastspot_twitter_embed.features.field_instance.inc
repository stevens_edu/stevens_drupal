<?php
/**
 * @file
 * fastspot_twitter_embed.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function fastspot_twitter_embed_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-twitter_embed-field_data_widget_id'.
  $field_instances['node-twitter_embed-field_data_widget_id'] = array(
    'bundle' => 'twitter_embed',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'You must create a "User Timeline" widget using whichever Twitter account you want to appear here. Do that by following this link: https://twitter.com/settings/widgets/new/user
1) Make sure "Exclude Replies" is checked if you want to exclude them. 
2) Uncheck "Auto-expand Photos" 
3) Once created, paste the "data-widget-id" into this field.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_data_widget_id',
    'label' => 'Data Widget ID',
    'required' => 1,
    'settings' => array(
      'better_formats' => array(
        'allowed_formats' => array(
          'filtered_html' => 'filtered_html',
          'full_html' => 'full_html',
          'intro_text' => 'intro_text',
          'plain_text' => 'plain_text',
        ),
        'allowed_formats_toggle' => 0,
        'default_order_toggle' => 0,
        'default_order_wrapper' => array(
          'formats' => array(
            'filtered_html' => array(
              'weight' => -9,
            ),
            'full_html' => array(
              'weight' => -10,
            ),
            'intro_text' => array(
              'weight' => -8,
            ),
            'plain_text' => array(
              'weight' => -7,
            ),
          ),
        ),
      ),
      'linkit' => array(
        'button_text' => 'Search',
        'enable' => 0,
        'profile' => '',
      ),
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-twitter_embed-field_twitter_user'.
  $field_instances['node-twitter_embed-field_twitter_user'] = array(
    'bundle' => 'twitter_embed',
    'default_value' => array(
      0 => array(
        'value' => 'FollowStevens',
      ),
    ),
    'deleted' => 0,
    'description' => 'Will be linked to the twitter account automatically',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_twitter_user',
    'label' => 'Twitter User',
    'required' => 1,
    'settings' => array(
      'better_formats' => array(
        'allowed_formats' => array(
          'filtered_html' => 'filtered_html',
          'full_html' => 'full_html',
          'intro_text' => 'intro_text',
          'plain_text' => 'plain_text',
        ),
        'allowed_formats_toggle' => 0,
        'default_order_toggle' => 0,
        'default_order_wrapper' => array(
          'formats' => array(
            'filtered_html' => array(
              'weight' => -9,
            ),
            'full_html' => array(
              'weight' => -10,
            ),
            'intro_text' => array(
              'weight' => -8,
            ),
            'plain_text' => array(
              'weight' => -7,
            ),
          ),
        ),
      ),
      'linkit' => array(
        'button_text' => 'Search',
        'enable' => 0,
        'profile' => '',
      ),
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Data Widget ID');
  t('Twitter User');
  t('Will be linked to the twitter account automatically');
  t('You must create a "User Timeline" widget using whichever Twitter account you want to appear here. Do that by following this link: https://twitter.com/settings/widgets/new/user
1) Make sure "Exclude Replies" is checked if you want to exclude them. 
2) Uncheck "Auto-expand Photos" 
3) Once created, paste the "data-widget-id" into this field.');

  return $field_instances;
}
