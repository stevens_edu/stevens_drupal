<?php
/**
 * @file
 * fastspot_twitter_embed.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function fastspot_twitter_embed_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function fastspot_twitter_embed_node_info() {
  $items = array(
    'twitter_embed' => array(
      'name' => t('Twitter Embed'),
      'base' => 'node_content',
      'description' => t('Embed a twitter user\'s feed'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
