<?php
/**
 * @file
 * fastspot_twitter_embed.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function fastspot_twitter_embed_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|twitter_embed|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'twitter_embed';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_twitter_user',
        1 => 'field_data_widget_id',
      ),
    ),
    'fields' => array(
      'field_twitter_user' => 'ds_content',
      'field_data_widget_id' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|twitter_embed|default'] = $ds_layout;

  return $export;
}
