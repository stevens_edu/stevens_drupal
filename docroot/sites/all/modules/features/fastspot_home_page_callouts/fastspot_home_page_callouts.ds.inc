<?php
/**
 * @file
 * fastspot_home_page_callouts.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function fastspot_home_page_callouts_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|home_page_callouts|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'home_page_callouts';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|home_page_callouts|default'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function fastspot_home_page_callouts_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|home_page_callouts|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'home_page_callouts';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_hpc_spotlight_image',
        2 => 'field_hpc_spotlight_heading',
        3 => 'field_hpc_spotlight_description',
        4 => 'field_hpc_spotlight_link',
        5 => 'body',
        6 => 'field_hpc_people',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_hpc_spotlight_image' => 'ds_content',
      'field_hpc_spotlight_heading' => 'ds_content',
      'field_hpc_spotlight_description' => 'ds_content',
      'field_hpc_spotlight_link' => 'ds_content',
      'body' => 'ds_content',
      'field_hpc_people' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|home_page_callouts|default'] = $ds_layout;

  return $export;
}
