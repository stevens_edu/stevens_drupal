<?php
/**
 * @file
 * fastspot_home_page_callouts.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function fastspot_home_page_callouts_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_home_page_spotlight|node|home_page_callouts|form';
  $field_group->group_name = 'group_home_page_spotlight';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'home_page_callouts';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Home Page Spotlight',
    'weight' => '1',
    'children' => array(
      0 => 'field_hpc_spotlight_image',
      1 => 'field_hpc_spotlight_heading',
      2 => 'field_hpc_spotlight_link',
      3 => 'field_hpc_spotlight_description',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-home-page-spotlight field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_home_page_spotlight|node|home_page_callouts|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Home Page Spotlight');

  return $field_groups;
}
