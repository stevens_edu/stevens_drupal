<?php

/**
 * @file
 * fastspot_home_page.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function fastspot_home_page_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['fieldblock-69d5b231f685366a8cf85fa7aa5bbdf5'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => '69d5b231f685366a8cf85fa7aa5bbdf5',
    'module' => 'fieldblock',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'stevens' => array(
        'region' => 'content_bottom',
        'status' => 1,
        'theme' => 'stevens',
        'weight' => -48,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['fieldblock-6e469fcbdc595a2500dbb8e192e0424f'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => '6e469fcbdc595a2500dbb8e192e0424f',
    'module' => 'fieldblock',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'stevens' => array(
        'region' => 'feature',
        'status' => 1,
        'theme' => 'stevens',
        'weight' => -47,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
