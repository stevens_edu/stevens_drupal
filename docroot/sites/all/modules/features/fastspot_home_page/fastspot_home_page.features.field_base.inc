<?php

/**
 * @file
 * fastspot_home_page.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function fastspot_home_page_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_home_callouts'.
  $field_bases['field_home_callouts'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_home_callouts',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'events_by_category' => 'events_by_category',
          'highlight_group' => 'highlight_group',
          'home_page_callouts' => 'home_page_callouts',
          'home_page_social' => 'home_page_social',
          'homepage_events' => 'homepage_events',
          'homepage_gallery_callout' => 'homepage_gallery_callout',
          'homepage_news' => 'homepage_news',
          'homepage_numbers' => 'homepage_numbers',
          'media_gallery_callout' => 'media_gallery_callout',
          'program_finder' => 'program_finder',
          'statistics_callout' => 'statistics_callout',
          'stevens_flip_card_list' => 'stevens_flip_card_list',
          'stevens_video' => 'stevens_video',
          'text_callout' => 'text_callout',
          'topic_single' => 'topic_single',
        ),
      ),
      'target_type' => 'node',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'field_home_feature'.
  $field_bases['field_home_feature'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_home_feature',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'home_page_feature' => 'home_page_feature',
        ),
      ),
      'target_type' => 'node',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  return $field_bases;
}
