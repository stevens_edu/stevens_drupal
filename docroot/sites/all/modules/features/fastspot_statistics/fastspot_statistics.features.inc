<?php
/**
 * @file
 * fastspot_statistics.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function fastspot_statistics_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function fastspot_statistics_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function fastspot_statistics_image_default_styles() {
  $styles = array();

  // Exported image style: hompage_statistics_1220x910.
  $styles['hompage_statistics_1220x910'] = array(
    'label' => 'Hompage Statistics 1220x910',
    'effects' => array(
      59 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 1220,
          'height' => 910,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'hompage_statistics_1220x910',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: hompage_statistics_1440x1070.
  $styles['hompage_statistics_1440x1070'] = array(
    'label' => 'Hompage Statistics 1440x1070',
    'effects' => array(
      60 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 1440,
          'height' => 1070,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'hompage_statistics_1440x1070',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: hompage_statistics_500x400.
  $styles['hompage_statistics_500x400'] = array(
    'label' => 'Hompage Statistics 500x400',
    'effects' => array(
      56 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 500,
          'height' => 400,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'hompage_statistics_500x400',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: hompage_statistics_740x550.
  $styles['hompage_statistics_740x550'] = array(
    'label' => 'Hompage Statistics 740x550',
    'effects' => array(
      57 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 740,
          'height' => 550,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'hompage_statistics_740x550',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: hompage_statistics_980x730.
  $styles['hompage_statistics_980x730'] = array(
    'label' => 'Hompage Statistics 980x730',
    'effects' => array(
      58 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 980,
          'height' => 730,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'hompage_statistics_980x730',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: statistic_image.
  $styles['statistic_image'] = array(
    'label' => 'Statistic Image',
    'effects' => array(
      55 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 110,
          'height' => 210,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function fastspot_statistics_node_info() {
  $items = array(
    'statistic' => array(
      'name' => t('Statistic'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'statistics_callout' => array(
      'name' => t('Statistics'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
