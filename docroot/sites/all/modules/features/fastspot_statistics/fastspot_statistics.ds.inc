<?php
/**
 * @file
 * fastspot_statistics.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function fastspot_statistics_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|statistics_callout|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'statistics_callout';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|statistics_callout|default'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function fastspot_statistics_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|statistics_callout|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'statistics_callout';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_statistics_reference',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_statistics_reference' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|statistics_callout|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|statistic|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'statistic';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_statistic_image',
        1 => 'field_statistic_description',
        2 => 'field_statistic_qualifier',
        3 => 'field_statistic',
        4 => 'field_statistic_link',
      ),
    ),
    'fields' => array(
      'field_statistic_image' => 'ds_content',
      'field_statistic_description' => 'ds_content',
      'field_statistic_qualifier' => 'ds_content',
      'field_statistic' => 'ds_content',
      'field_statistic_link' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|statistic|teaser'] = $ds_layout;

  return $export;
}
