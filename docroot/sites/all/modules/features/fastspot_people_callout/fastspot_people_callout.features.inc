<?php
/**
 * @file
 * fastspot_people_callout.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function fastspot_people_callout_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function fastspot_people_callout_image_default_styles() {
  $styles = array();

  // Exported image style: people_250x376.
  $styles['people_250x376'] = array(
    'label' => 'People 250x376',
    'effects' => array(
      61 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 250,
          'height' => 376,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'people_250x376',
        ),
        'weight' => 0,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function fastspot_people_callout_node_info() {
  $items = array(
    'people_callout' => array(
      'name' => t('People Callout'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
