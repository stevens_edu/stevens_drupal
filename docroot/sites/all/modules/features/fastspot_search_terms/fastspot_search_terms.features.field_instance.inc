<?php
/**
 * @file
 * fastspot_search_terms.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function fastspot_search_terms_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-search_terms-field_st_grad_program'.
  $field_instances['node-search_terms-field_st_grad_program'] = array(
    'bundle' => 'search_terms',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_st_grad_program',
    'label' => 'Graduate Program',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-search_terms-field_st_undergrad_program'.
  $field_instances['node-search_terms-field_st_undergrad_program'] = array(
    'bundle' => 'search_terms',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_st_undergrad_program',
    'label' => 'Undergraduate Program',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Graduate Program');
  t('Undergraduate Program');

  return $field_instances;
}
