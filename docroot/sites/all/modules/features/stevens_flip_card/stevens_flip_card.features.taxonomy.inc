<?php
/**
 * @file
 * stevens_flip_card.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function stevens_flip_card_taxonomy_default_vocabularies() {
  return array(
    'flip_card_terms' => array(
      'name' => 'Flip Card Terms',
      'machine_name' => 'flip_card_terms',
      'description' => 'Terms used in flip card component organization and filtering',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
