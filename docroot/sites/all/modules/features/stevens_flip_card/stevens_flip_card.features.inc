<?php
/**
 * @file
 * stevens_flip_card.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function stevens_flip_card_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function stevens_flip_card_node_info() {
  $items = array(
    'stevens_flip_card_item' => array(
      'name' => t('Flip Card Item'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'stevens_flip_card_list' => array(
      'name' => t('Flip Card List'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
