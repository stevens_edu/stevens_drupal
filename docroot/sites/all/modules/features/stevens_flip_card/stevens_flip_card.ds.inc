<?php
/**
 * @file
 * stevens_flip_card.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function stevens_flip_card_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|stevens_flip_card_list|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'stevens_flip_card_list';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_cards',
        1 => 'body',
      ),
    ),
    'fields' => array(
      'field_cards' => 'ds_content',
      'body' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|stevens_flip_card_list|default'] = $ds_layout;

  return $export;
}
