<?php
/**
 * @file
 * stevens_flip_card.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function stevens_flip_card_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_flipcard_back|node|stevens_flip_card_item|form';
  $field_group->group_name = 'group_flipcard_back';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'stevens_flip_card_item';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Back',
    'weight' => '2',
    'children' => array(
      0 => 'body',
      1 => 'field_flip_link',
      2 => 'field_flip_tags',
      3 => 'field_rev_title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-flipcard-back field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_flipcard_back|node|stevens_flip_card_item|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_flipcard_front|node|stevens_flip_card_item|form';
  $field_group->group_name = 'group_flipcard_front';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'stevens_flip_card_item';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Front',
    'weight' => '1',
    'children' => array(
      0 => 'field_header_image',
      1 => 'field_source_name',
      2 => 'field_source_year_attribution',
      3 => 'field_statistic_figure',
      4 => 'field_statistic_label',
      5 => 'field_statistic_label_position',
      6 => 'field_front_title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-flipcard-front field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_flipcard_front|node|stevens_flip_card_item|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Back');
  t('Front');

  return $field_groups;
}
