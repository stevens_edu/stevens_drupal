<?php
/**
 * @file
 * stevens_events_feed_view.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function stevens_events_feed_view_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'events';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Events';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'views_xml_style';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: All Day Event */
  $handler->display->display_options['fields']['field_event_all_day']['id'] = 'field_event_all_day';
  $handler->display->display_options['fields']['field_event_all_day']['table'] = 'field_data_field_event_all_day';
  $handler->display->display_options['fields']['field_event_all_day']['field'] = 'field_event_all_day';
  $handler->display->display_options['fields']['field_event_all_day']['label'] = 'all_day';
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = 'description';
  /* Field: Content: Category */
  $handler->display->display_options['fields']['field_event_categories']['id'] = 'field_event_categories';
  $handler->display->display_options['fields']['field_event_categories']['table'] = 'field_data_field_event_categories';
  $handler->display->display_options['fields']['field_event_categories']['field'] = 'field_event_categories';
  $handler->display->display_options['fields']['field_event_categories']['label'] = 'category';
  $handler->display->display_options['fields']['field_event_categories']['delta_offset'] = '0';
  /* Field: Content: End Time */
  $handler->display->display_options['fields']['field_event_end_time']['id'] = 'field_event_end_time';
  $handler->display->display_options['fields']['field_event_end_time']['table'] = 'field_data_field_event_end_time';
  $handler->display->display_options['fields']['field_event_end_time']['field'] = 'field_event_end_time';
  $handler->display->display_options['fields']['field_event_end_time']['label'] = 'end_time';
  $handler->display->display_options['fields']['field_event_end_time']['settings'] = array(
    'format_type' => 'long',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Content: Location */
  $handler->display->display_options['fields']['field_event_location']['id'] = 'field_event_location';
  $handler->display->display_options['fields']['field_event_location']['table'] = 'field_data_field_event_location';
  $handler->display->display_options['fields']['field_event_location']['field'] = 'field_event_location';
  $handler->display->display_options['fields']['field_event_location']['label'] = 'location';
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = 'guid';
  /* Field: Content: Start Time */
  $handler->display->display_options['fields']['field_event_start_time']['id'] = 'field_event_start_time';
  $handler->display->display_options['fields']['field_event_start_time']['table'] = 'field_data_field_event_start_time';
  $handler->display->display_options['fields']['field_event_start_time']['field'] = 'field_event_start_time';
  $handler->display->display_options['fields']['field_event_start_time']['label'] = 'start_time';
  $handler->display->display_options['fields']['field_event_start_time']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['field_event_start_time']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_event_start_time']['settings'] = array(
    'format_type' => 'long',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Content: Author uid */
  $handler->display->display_options['fields']['uid']['id'] = 'uid';
  $handler->display->display_options['fields']['uid']['table'] = 'node';
  $handler->display->display_options['fields']['uid']['field'] = 'uid';
  $handler->display->display_options['fields']['uid']['label'] = 'author_uid';
  /* Field: Content: Path */
  $handler->display->display_options['fields']['path']['id'] = 'path';
  $handler->display->display_options['fields']['path']['table'] = 'node';
  $handler->display->display_options['fields']['path']['field'] = 'path';
  $handler->display->display_options['fields']['path']['label'] = 'link';
  $handler->display->display_options['fields']['path']['alter']['text'] = 'http://www.stevens.edu[path]';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'event' => 'event',
  );

  /* Display: Feed */
  $handler = $view->new_display('feed', 'Feed', 'feed_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Events feed for external Stevens Website';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '25';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'rss';
  $handler->display->display_options['row_plugin'] = 'rss_fields';
  $handler->display->display_options['row_options']['title_field'] = 'title';
  $handler->display->display_options['row_options']['link_field'] = 'path';
  $handler->display->display_options['row_options']['description_field'] = 'body';
  $handler->display->display_options['row_options']['creator_field'] = 'uid';
  $handler->display->display_options['row_options']['date_field'] = 'field_event_start_time';
  $handler->display->display_options['row_options']['guid_field_options'] = array(
    'guid_field' => 'nid',
    'guid_field_is_permalink' => 0,
  );
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'event' => 'event',
    'events_by_category' => 'events_by_category',
  );
  /* Filter criterion: Content: Start Time (field_event_start_time) */
  $handler->display->display_options['filters']['field_event_start_time_value']['id'] = 'field_event_start_time_value';
  $handler->display->display_options['filters']['field_event_start_time_value']['table'] = 'field_data_field_event_start_time';
  $handler->display->display_options['filters']['field_event_start_time_value']['field'] = 'field_event_start_time_value';
  $handler->display->display_options['filters']['field_event_start_time_value']['operator'] = 'not empty';
  $handler->display->display_options['path'] = 'rss/events';
  $handler->display->display_options['sitename_title'] = 0;

  /* Display: Data export */
  $handler = $view->new_display('views_data_export', 'Data export', 'views_data_export_1');
  $export['events'] = $view;

  return $export;
}
