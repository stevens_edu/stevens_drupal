<?php
/**
 * @file
 * fastspot_image_styles.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function fastspot_image_styles_image_default_styles() {
  $styles = array();

  // Exported image style: admission_feature_500x281.
  $styles['admission_feature_500x281'] = array(
    'label' => 'Admission Feature 500x281',
    'effects' => array(
      37 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 500,
          'height' => 281,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'admission_feature_500x281',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: admission_feature_740x480.
  $styles['admission_feature_740x480'] = array(
    'label' => 'Admission Feature 740x480',
    'effects' => array(
      38 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 740,
          'height' => 480,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'admission_feature_740x480',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: articles_callout_500x281.
  $styles['articles_callout_500x281'] = array(
    'label' => 'Articles Callout 500x281',
    'effects' => array(
      19 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 500,
          'height' => 281,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'articles_callout_500x281',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: articles_callout_740x480.
  $styles['articles_callout_740x480'] = array(
    'label' => 'Articles Callout 740x480',
    'effects' => array(
      20 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 740,
          'height' => 480,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'articles_callout_740x480',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: highlight_group_1220x600.
  $styles['highlight_group_1220x600'] = array(
    'label' => 'Highlight Group 1220x600',
    'effects' => array(
      11 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 1220,
          'height' => 600,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'highlight_group_1220x600',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: highlight_group_1440x600.
  $styles['highlight_group_1440x600'] = array(
    'label' => 'Highlight Group 1440x600',
    'effects' => array(
      12 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 1440,
          'height' => 600,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'highlight_group_1440x600',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: highlight_group_470x352.
  $styles['highlight_group_470x352'] = array(
    'label' => 'Highlight Group 470x352',
    'effects' => array(
      9 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 470,
          'height' => 352,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'highlight_group_470x352',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: highlight_group_980x500.
  $styles['highlight_group_980x500'] = array(
    'label' => 'Highlight Group 980x500',
    'effects' => array(
      10 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 980,
          'height' => 500,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'highlight_group_980x500',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: highlight_item_294x165.
  $styles['highlight_item_294x165'] = array(
    'label' => 'Highlight Item 294x165',
    'effects' => array(
      14 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 294,
          'height' => 165,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'highlight_item_294x165',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: highlight_item_470x470.
  $styles['highlight_item_470x470'] = array(
    'label' => 'Highlight Item 470x470',
    'effects' => array(
      13 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 470,
          'height' => 470,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'highlight_item_470x470',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: home_feature_1200x700.
  $styles['home_feature_1200x700'] = array(
    'label' => 'Home Feature 1200x700',
    'effects' => array(
      23 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 1200,
          'height' => 700,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'home_feature_1200x700',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: home_feature_500x300.
  $styles['home_feature_500x300'] = array(
    'label' => 'Home Feature 500x300',
    'effects' => array(
      24 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 500,
          'height' => 300,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'home_feature_500x300',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: home_feature_persona_250x172.
  $styles['home_feature_persona_250x172'] = array(
    'label' => 'Home Feature Persona 250x172',
    'effects' => array(
      26 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 250,
          'height' => 172,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'home_feature_persona_250x172',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: home_feature_persona_80x80.
  $styles['home_feature_persona_80x80'] = array(
    'label' => 'Home Feature Persona 80x80',
    'effects' => array(
      25 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 80,
          'height' => 80,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'home_feature_persona_80x80',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: home_page_callout_100x100.
  $styles['home_page_callout_100x100'] = array(
    'label' => 'Home Page Callout 100x100',
    'effects' => array(
      48 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 100,
          'height' => 100,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'home_page_callout_100x100',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: home_page_callout_300x300.
  $styles['home_page_callout_300x300'] = array(
    'label' => 'Home Page Callout 300x300',
    'effects' => array(
      49 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 300,
          'height' => 300,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'home_page_callout_300x300',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: home_page_callout_322x300.
  $styles['home_page_callout_322x300'] = array(
    'label' => 'Home Page Callout 322x300',
    'effects' => array(
      46 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 322,
          'height' => 300,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'home_page_callout_322x300',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: home_page_callout_400x300.
  $styles['home_page_callout_400x300'] = array(
    'label' => 'Home Page Callout 400x300',
    'effects' => array(
      47 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 400,
          'height' => 300,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'home_page_callout_400x300',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: home_page_callout_500x375.
  $styles['home_page_callout_500x375'] = array(
    'label' => 'Home Page Callout 500x375',
    'effects' => array(
      44 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 500,
          'height' => 375,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'home_page_callout_500x375',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: home_page_callout_740x555.
  $styles['home_page_callout_740x555'] = array(
    'label' => 'Home Page Callout 740x555',
    'effects' => array(
      45 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 740,
          'height' => 555,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'home_page_callout_740x555',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: home_page_carousel_item_500x333.
  $styles['home_page_carousel_item_500x333'] = array(
    'label' => 'Home Page Carousel Item 500x333',
    'effects' => array(
      39 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 500,
          'height' => 333,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'home_page_carousel_item_500x333',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: home_page_carousel_item_740x416.
  $styles['home_page_carousel_item_740x416'] = array(
    'label' => 'Home Page Carousel Item 740x416',
    'effects' => array(
      42 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 740,
          'height' => 416,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'home_page_carousel_item_740x416',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: home_page_carousel_item_740x494.
  $styles['home_page_carousel_item_740x494'] = array(
    'label' => 'Home Page Carousel Item 740x494',
    'effects' => array(
      40 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 740,
          'height' => 494,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'home_page_carousel_item_740x494',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: home_page_carousel_item_980x551.
  $styles['home_page_carousel_item_980x551'] = array(
    'label' => 'Home Page Carousel Item 980x551',
    'effects' => array(
      43 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 980,
          'height' => 551,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'home_page_carousel_item_980x551',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: home_page_carousel_item_980x653.
  $styles['home_page_carousel_item_980x653'] = array(
    'label' => 'Home Page Carousel Item 980x653',
    'effects' => array(
      41 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 980,
          'height' => 653,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'home_page_carousel_item_980x653',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: image_feature_1024x336.
  $styles['image_feature_1024x336'] = array(
    'label' => 'Image Feature 1024x336',
    'effects' => array(
      5 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 1024,
          'height' => 336,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'image_feature_1024x336',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: image_feature_1280x420.
  $styles['image_feature_1280x420'] = array(
    'label' => 'Image Feature 1280x420',
    'effects' => array(
      1 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 1280,
          'height' => 420,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'image_feature_1280x420',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: image_feature_294x196.
  $styles['image_feature_294x196'] = array(
    'label' => 'Image Feature 294x196',
    'effects' => array(
      2 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 294,
          'height' => 196,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'image_feature_294x196',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: image_feature_470x264.
  $styles['image_feature_470x264'] = array(
    'label' => 'Image Feature 470x264',
    'effects' => array(
      3 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 470,
          'height' => 264,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'image_feature_470x264',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: image_feature_700x394.
  $styles['image_feature_700x394'] = array(
    'label' => 'Image Feature 700x394',
    'effects' => array(
      4 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 700,
          'height' => 394,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'image_feature_700x394',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: media_gallery_280x187.
  $styles['media_gallery_280x187'] = array(
    'label' => 'Media Gallery 280x187',
    'effects' => array(
      33 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 280,
          'height' => 187,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'media_gallery_280x187',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: media_gallery_294x196.
  $styles['media_gallery_294x196'] = array(
    'label' => 'Media Gallery 294x196',
    'effects' => array(
      36 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 294,
          'height' => 196,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'media_gallery_294x196',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: media_gallery_330x220.
  $styles['media_gallery_330x220'] = array(
    'label' => 'Media Gallery 330x220',
    'effects' => array(
      34 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 330,
          'height' => 220,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'media_gallery_330x220',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: media_gallery_350x234.
  $styles['media_gallery_350x234'] = array(
    'label' => 'Media Gallery 350x234',
    'effects' => array(
      32 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 350,
          'height' => 234,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'media_gallery_350x234',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: media_gallery_470x314.
  $styles['media_gallery_470x314'] = array(
    'label' => 'Media Gallery 470x314',
    'effects' => array(
      35 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 470,
          'height' => 314,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'media_gallery_470x314',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: news_item_174x116.
  $styles['news_item_174x116'] = array(
    'label' => 'News Item 174x116',
    'effects' => array(
      51 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 174,
          'height' => 116,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'news_item_174x116',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: news_item_80x80.
  $styles['news_item_80x80'] = array(
    'label' => 'News Item 80x80',
    'effects' => array(
      50 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 80,
          'height' => 80,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'news_item_80x80',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: testimonial_294x165.
  $styles['testimonial_294x165'] = array(
    'label' => 'Testimonial 294x165',
    'effects' => array(
      21 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 294,
          'height' => 165,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'testimonial_294x165',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: testimonial_350x350.
  $styles['testimonial_350x350'] = array(
    'label' => 'Testimonial 350x350',
    'effects' => array(
      22 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 350,
          'height' => 350,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'testimonial_350x350',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: topic_callout_294x196.
  $styles['topic_callout_294x196'] = array(
    'label' => 'Topic Callout 294x196',
    'effects' => array(
      6 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 294,
          'height' => 196,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'topic_callout_294x196',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: topic_callout_400x267.
  $styles['topic_callout_400x267'] = array(
    'label' => 'Topic Callout 400x267',
    'effects' => array(
      8 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 400,
          'height' => 267,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'topic_callout_400x267',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: topic_callout_470x313.
  $styles['topic_callout_470x313'] = array(
    'label' => 'Topic Callout 470x313',
    'effects' => array(
      7 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 470,
          'height' => 313,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'topic_callout_470x313',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: topic_card_286x400.
  $styles['topic_card_286x400'] = array(
    'label' => 'Topic Card 286x400',
    'effects' => array(
      15 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 286,
          'height' => 400,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'topic_card_286x400',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: topic_card_290x164.
  $styles['topic_card_290x164'] = array(
    'label' => 'Topic Card 290x164',
    'effects' => array(
      18 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 290,
          'height' => 164,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'topic_card_290x164',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: topic_card_400x300.
  $styles['topic_card_400x300'] = array(
    'label' => 'Topic Card 400x300',
    'effects' => array(
      16 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 400,
          'height' => 300,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'topic_card_400x300',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: topic_card_460x260.
  $styles['topic_card_460x260'] = array(
    'label' => 'Topic Card 460x260',
    'effects' => array(
      17 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 460,
          'height' => 260,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'topic_card_460x260',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: video_list_200x114.
  $styles['video_list_200x114'] = array(
    'label' => 'Video List 200x114',
    'effects' => array(
      30 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 200,
          'height' => 114,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'video_list_200x114',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: video_list_252x143.
  $styles['video_list_252x143'] = array(
    'label' => 'Video List 252x143',
    'effects' => array(
      31 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 253,
          'height' => 143,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'video_list_252x143',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: video_list_294x196.
  $styles['video_list_294x196'] = array(
    'label' => 'Video List 294x196',
    'effects' => array(
      27 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 294,
          'height' => 196,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'video_list_294x196',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: video_list_330x187.
  $styles['video_list_330x187'] = array(
    'label' => 'Video List 330x187',
    'effects' => array(
      29 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 330,
          'height' => 187,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'video_list_330x187',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: video_list_470x266.
  $styles['video_list_470x266'] = array(
    'label' => 'Video List 470x266',
    'effects' => array(
      28 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 470,
          'height' => 266,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'video_list_470x266',
        ),
        'weight' => 0,
      ),
    ),
  );

  return $styles;
}
