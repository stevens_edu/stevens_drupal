<?php
/**
 * @file
 * fastspot_accordion.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function fastspot_accordion_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|accordion_callout|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'accordion_callout';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_accordion_items',
      ),
    ),
    'fields' => array(
      'field_accordion_items' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|accordion_callout|default'] = $ds_layout;

  return $export;
}
