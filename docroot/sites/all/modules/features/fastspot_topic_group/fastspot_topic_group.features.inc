<?php
/**
 * @file
 * fastspot_topic_group.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function fastspot_topic_group_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function fastspot_topic_group_image_default_styles() {
  $styles = array();

  // Exported image style: topic_card_1220.
  $styles['topic_card_1220'] = array(
    'label' => 'Topic Card - 1220',
    'effects' => array(
      70 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 295,
          'height' => 395,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'topic_card_1220',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: topic_card_740.
  $styles['topic_card_740'] = array(
    'label' => 'Topic Card - 740',
    'effects' => array(
      68 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 350,
          'height' => 350,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'topic_card_740',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: topic_card_980.
  $styles['topic_card_980'] = array(
    'label' => 'Topic Card - 980',
    'effects' => array(
      69 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 236,
          'height' => 316,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'topic_card_980',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: topic_card_fallback.
  $styles['topic_card_fallback'] = array(
    'label' => 'Topic Card - Fallback',
    'effects' => array(
      18 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 460,
          'height' => 260,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'topic_card_fallback',
        ),
        'weight' => 0,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function fastspot_topic_group_node_info() {
  $items = array(
    'topic_group' => array(
      'name' => t('Topic Group'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
