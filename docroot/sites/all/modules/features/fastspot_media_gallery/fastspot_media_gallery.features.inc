<?php
/**
 * @file
 * fastspot_media_gallery.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function fastspot_media_gallery_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function fastspot_media_gallery_node_info() {
  $items = array(
    'media_gallery_callout' => array(
      'name' => t('Media Gallery'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
