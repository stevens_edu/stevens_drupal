<?php
/**
 * @file
 * fastspot_important_dates.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function fastspot_important_dates_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|important_dates_callout|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'important_dates_callout';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|important_dates_callout|default'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function fastspot_important_dates_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|important_dates_callout|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'important_dates_callout';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_important_dates_link',
        2 => 'field_important_dates',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_important_dates_link' => 'ds_content',
      'field_important_dates' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|important_dates_callout|default'] = $ds_layout;

  return $export;
}
