<?php
/**
 * @file
 * stevens_downloadable_views.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function stevens_downloadable_views_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'export_testimonials';
  $view->description = 'A downloadable list of all testimonials.';
  $view->tag = 'default';
  $view->base_table = 'field_collection_item';
  $view->human_name = 'Export Testimonials';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Export Testimonials';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['pager']['options']['expose']['items_per_page'] = TRUE;
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options'] = '10, 50, 100, 500';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all'] = TRUE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'item_id' => 'item_id',
    'field_testimonial_title' => 'field_testimonial_title',
    'field_testimonial_quote' => 'field_testimonial_quote',
    'field_testimonial_name' => 'field_testimonial_name',
    'field_testimonial_link' => 'field_testimonial_link',
    'field_testimonial_color' => 'field_testimonial_color',
    'field_testimonial_image' => 'field_testimonial_image',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'item_id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_testimonial_title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_testimonial_quote' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_testimonial_name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_testimonial_link' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_testimonial_color' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_testimonial_image' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Header: Global: Result summary */
  $handler->display->display_options['header']['result']['id'] = 'result';
  $handler->display->display_options['header']['result']['table'] = 'views';
  $handler->display->display_options['header']['result']['field'] = 'result';
  /* Field: Field collection item: Field collection item ID */
  $handler->display->display_options['fields']['item_id']['id'] = 'item_id';
  $handler->display->display_options['fields']['item_id']['table'] = 'field_collection_item';
  $handler->display->display_options['fields']['item_id']['field'] = 'item_id';
  $handler->display->display_options['fields']['item_id']['exclude'] = TRUE;
  /* Field: Field collection item: Name */
  $handler->display->display_options['fields']['field_testimonial_name']['id'] = 'field_testimonial_name';
  $handler->display->display_options['fields']['field_testimonial_name']['table'] = 'field_data_field_testimonial_name';
  $handler->display->display_options['fields']['field_testimonial_name']['field'] = 'field_testimonial_name';
  /* Field: Field collection item: Title */
  $handler->display->display_options['fields']['field_testimonial_title']['id'] = 'field_testimonial_title';
  $handler->display->display_options['fields']['field_testimonial_title']['table'] = 'field_data_field_testimonial_title';
  $handler->display->display_options['fields']['field_testimonial_title']['field'] = 'field_testimonial_title';
  /* Field: Field collection item: Quote */
  $handler->display->display_options['fields']['field_testimonial_quote']['id'] = 'field_testimonial_quote';
  $handler->display->display_options['fields']['field_testimonial_quote']['table'] = 'field_data_field_testimonial_quote';
  $handler->display->display_options['fields']['field_testimonial_quote']['field'] = 'field_testimonial_quote';
  /* Field: Field collection item: Link */
  $handler->display->display_options['fields']['field_testimonial_link']['id'] = 'field_testimonial_link';
  $handler->display->display_options['fields']['field_testimonial_link']['table'] = 'field_data_field_testimonial_link';
  $handler->display->display_options['fields']['field_testimonial_link']['field'] = 'field_testimonial_link';
  $handler->display->display_options['fields']['field_testimonial_link']['label'] = 'Link Text';
  $handler->display->display_options['fields']['field_testimonial_link']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_testimonial_link']['type'] = 'link_title_plain';
  /* Field: Field collection item: Link */
  $handler->display->display_options['fields']['field_testimonial_link_1']['id'] = 'field_testimonial_link_1';
  $handler->display->display_options['fields']['field_testimonial_link_1']['table'] = 'field_data_field_testimonial_link';
  $handler->display->display_options['fields']['field_testimonial_link_1']['field'] = 'field_testimonial_link';
  $handler->display->display_options['fields']['field_testimonial_link_1']['label'] = 'Link URL';
  $handler->display->display_options['fields']['field_testimonial_link_1']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_testimonial_link_1']['type'] = 'link_plain';
  /* Field: Field collection item: Color */
  $handler->display->display_options['fields']['field_testimonial_color']['id'] = 'field_testimonial_color';
  $handler->display->display_options['fields']['field_testimonial_color']['table'] = 'field_data_field_testimonial_color';
  $handler->display->display_options['fields']['field_testimonial_color']['field'] = 'field_testimonial_color';
  /* Field: Field collection item: Image */
  $handler->display->display_options['fields']['field_testimonial_image']['id'] = 'field_testimonial_image';
  $handler->display->display_options['fields']['field_testimonial_image']['table'] = 'field_data_field_testimonial_image';
  $handler->display->display_options['fields']['field_testimonial_image']['field'] = 'field_testimonial_image';
  $handler->display->display_options['fields']['field_testimonial_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_testimonial_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_testimonial_image']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Sort criterion: Field collection item: Name (field_testimonial_name) */
  $handler->display->display_options['sorts']['field_testimonial_name_value']['id'] = 'field_testimonial_name_value';
  $handler->display->display_options['sorts']['field_testimonial_name_value']['table'] = 'field_data_field_testimonial_name';
  $handler->display->display_options['sorts']['field_testimonial_name_value']['field'] = 'field_testimonial_name_value';
  /* Filter criterion: Field collection item: Name (field_testimonial_name) */
  $handler->display->display_options['filters']['field_testimonial_name_value']['id'] = 'field_testimonial_name_value';
  $handler->display->display_options['filters']['field_testimonial_name_value']['table'] = 'field_data_field_testimonial_name';
  $handler->display->display_options['filters']['field_testimonial_name_value']['field'] = 'field_testimonial_name_value';
  $handler->display->display_options['filters']['field_testimonial_name_value']['operator'] = 'not empty';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Result summary */
  $handler->display->display_options['header']['result']['id'] = 'result';
  $handler->display->display_options['header']['result']['table'] = 'views';
  $handler->display->display_options['header']['result']['field'] = 'result';
  $handler->display->display_options['header']['result']['empty'] = TRUE;
  $handler->display->display_options['header']['result']['content'] = '<p>There are @total testimonials to <a href="/export/testimonials/download">export in CSV format</a>.</p>
<p>The table below is a preview of what the file will contain.</p>';
  $handler->display->display_options['path'] = 'export/testimonials';

  /* Display: Data export */
  $handler = $view->new_display('views_data_export', 'Data export', 'views_data_export_1');
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'views_data_export_csv';
  $handler->display->display_options['style_options']['provide_file'] = 1;
  $handler->display->display_options['style_options']['parent_sort'] = 0;
  $handler->display->display_options['style_options']['quote'] = 1;
  $handler->display->display_options['style_options']['trim'] = 1;
  $handler->display->display_options['style_options']['replace_newlines'] = 0;
  $handler->display->display_options['style_options']['header'] = 1;
  $handler->display->display_options['style_options']['keep_html'] = 0;
  $handler->display->display_options['style_options']['encoding'] = 'ISO-8859-1//TRANSLIT';
  $handler->display->display_options['path'] = 'export/testimonials/download';
  $export['export_testimonials'] = $view;

  return $export;
}
