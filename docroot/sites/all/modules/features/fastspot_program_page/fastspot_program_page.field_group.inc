<?php
/**
 * @file
 * fastspot_program_page.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function fastspot_program_page_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_migration|node|program_page|form';
  $field_group->group_name = 'group_migration';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'program_page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Migration Data',
    'weight' => '11',
    'children' => array(
      0 => 'field_migration_meta_description',
      1 => 'field_migration_meta_title',
      2 => 'field_migration_notes',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-migration field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_migration|node|program_page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_program_components|node|program_page|form';
  $field_group->group_name = 'group_program_components';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'program_page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Page Components',
    'weight' => '9',
    'children' => array(
      0 => 'field_content_components',
      1 => 'field_page_callouts',
      2 => 'field_sidebar_components',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-program-components field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_program_components|node|program_page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_program_feature|node|program_page|form';
  $field_group->group_name = 'group_program_feature';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'program_page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Feature Region',
    'weight' => '6',
    'children' => array(
      0 => 'field_program_feature_campus',
      1 => 'field_program_feature_corporate',
      2 => 'field_program_feature_image',
      3 => 'field_program_feature_link',
      4 => 'field_program_feature_online',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-program-feature field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_program_feature|node|program_page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_program_options|node|program_page|form';
  $field_group->group_name = 'group_program_options';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'program_page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Page Options',
    'weight' => '10',
    'children' => array(
      0 => 'field_hide_siblings',
      1 => 'field_program_keywords',
      2 => 'field_news_departments',
      3 => 'field_page_schools',
      4 => 'field_page_audiences',
      5 => 'field_page_topics',
      6 => 'field_page_programs',
      7 => 'field_search_keywords',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-program-options field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_program_options|node|program_page|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Feature Region');
  t('Migration Data');
  t('Page Components');
  t('Page Options');

  return $field_groups;
}
