<?php
/**
 * @file
 * fastspot_program_page.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function fastspot_program_page_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|program_finder|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'program_finder';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|program_finder|default'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function fastspot_program_page_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|program_finder|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'program_finder';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_program_finder_intro',
        2 => 'field_program_search_type',
        3 => 'field_program_undergraduate_link',
        4 => 'field_program_graduate_link',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_program_finder_intro' => 'ds_content',
      'field_program_search_type' => 'ds_content',
      'field_program_undergraduate_link' => 'ds_content',
      'field_program_graduate_link' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|program_finder|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|program_page|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'program_page';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_intro',
        1 => 'body',
        2 => 'field_content_components',
        3 => 'field_page_callouts',
        4 => 'field_sidebar_components',
      ),
    ),
    'fields' => array(
      'field_intro' => 'ds_content',
      'body' => 'ds_content',
      'field_content_components' => 'ds_content',
      'field_page_callouts' => 'ds_content',
      'field_sidebar_components' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|program_page|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|program_page|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'program_page';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_program_short_name',
        1 => 'field_program_feature_image',
        2 => 'field_program_degrees_offered',
        3 => 'field_program_keywords',
      ),
    ),
    'fields' => array(
      'field_program_short_name' => 'ds_content',
      'field_program_feature_image' => 'ds_content',
      'field_program_degrees_offered' => 'ds_content',
      'field_program_keywords' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|program_page|teaser'] = $ds_layout;

  return $export;
}
