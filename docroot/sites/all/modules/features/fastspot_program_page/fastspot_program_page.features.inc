<?php
/**
 * @file
 * fastspot_program_page.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function fastspot_program_page_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function fastspot_program_page_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function fastspot_program_page_image_default_styles() {
  $styles = array();

  // Exported image style: program_feature_teaser_331x250.
  $styles['program_feature_teaser_331x250'] = array(
    'label' => 'Program Feature Teaser 331x250',
    'effects' => array(
      54 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 331,
          'height' => 250,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'program_feature_teaser_331x250',
        ),
        'weight' => 0,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function fastspot_program_page_node_info() {
  $items = array(
    'program_finder' => array(
      'name' => t('Program Finder'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'program_page' => array(
      'name' => t('Program Page'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
