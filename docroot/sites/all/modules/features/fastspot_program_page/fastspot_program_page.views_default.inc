<?php
/**
 * @file
 * fastspot_program_page.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function fastspot_program_page_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'program_finder';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Program Finder';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Program Finder';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'entity';
  $handler->display->display_options['row_options']['view_mode'] = 'teaser';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = '<article class="program_finder_block_empty no_results">
      	<div class="program_finder_block_empty_inner">
      		<h3 class="program_finder_block_empty_heading">No Results Found</h3>
      		<a href="#" class="program_finder_block_empty_link">View All<br>Undergraduate Programs</a>
      	</div>
      </article>
      <article class="program_finder_block_empty">
      	<div class="program_finder_block_empty_inner"></div>
      </article>
      <article class="program_finder_block_empty">
      	<div class="program_finder_block_empty_inner"></div>
      </article>
      <article class="program_finder_block_empty">
      	<div class="program_finder_block_empty_inner"></div>
      </article>';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  $handler->display->display_options['sorts']['title']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'program_page' => 'program_page',
  );
  /* Filter criterion: Content: Program Type (field_program_type) */
  $handler->display->display_options['filters']['field_program_type_value']['id'] = 'field_program_type_value';
  $handler->display->display_options['filters']['field_program_type_value']['table'] = 'field_data_field_program_type';
  $handler->display->display_options['filters']['field_program_type_value']['field'] = 'field_program_type_value';
  $handler->display->display_options['filters']['field_program_type_value']['value'] = array(
    'Undergraduate' => 'Undergraduate',
  );
  $handler->display->display_options['filters']['field_program_type_value']['expose']['operator_id'] = 'field_program_type_value_op';
  $handler->display->display_options['filters']['field_program_type_value']['expose']['label'] = 'Program Type (field_program_type)';
  $handler->display->display_options['filters']['field_program_type_value']['expose']['operator'] = 'field_program_type_value_op';
  $handler->display->display_options['filters']['field_program_type_value']['expose']['identifier'] = 'field_program_type_value';

  /* Display: Block: Undergraduate */
  $handler = $view->new_display('block', 'Block: Undergraduate', 'block');
  $handler->display->display_options['defaults']['empty'] = FALSE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = '<article class="program_finder_block_empty no_results">
      	<div class="program_finder_block_empty_inner">
      		<h3 class="program_finder_block_empty_heading">No Results Found</h3>
      		<a href="/academics/undergraduate-studies/undergraduate-majors" class="program_finder_block_empty_link">View All<br>Undergraduate Programs</a>
      	</div>
      </article>
      <article class="program_finder_block_empty">
      	<div class="program_finder_block_empty_inner"></div>
      </article>
      <article class="program_finder_block_empty">
      	<div class="program_finder_block_empty_inner"></div>
      </article>
      <article class="program_finder_block_empty">
      	<div class="program_finder_block_empty_inner"></div>
      </article>';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'program_page' => 'program_page',
  );
  /* Filter criterion: Content: Program Type (field_program_type) */
  $handler->display->display_options['filters']['field_program_type_value']['id'] = 'field_program_type_value';
  $handler->display->display_options['filters']['field_program_type_value']['table'] = 'field_data_field_program_type';
  $handler->display->display_options['filters']['field_program_type_value']['field'] = 'field_program_type_value';
  $handler->display->display_options['filters']['field_program_type_value']['value'] = array(
    'Undergraduate' => 'Undergraduate',
  );
  $handler->display->display_options['filters']['field_program_type_value']['expose']['operator_id'] = 'field_program_type_value_op';
  $handler->display->display_options['filters']['field_program_type_value']['expose']['label'] = 'Program Type';
  $handler->display->display_options['filters']['field_program_type_value']['expose']['operator'] = 'field_program_type_value_op';
  $handler->display->display_options['filters']['field_program_type_value']['expose']['identifier'] = 'field_program_type_value';
  $handler->display->display_options['filters']['field_program_type_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_program_type_value']['expose']['reduce'] = TRUE;
  /* Filter criterion: Content: Program Keywords (field_program_keywords) */
  $handler->display->display_options['filters']['field_program_keywords_value']['id'] = 'field_program_keywords_value';
  $handler->display->display_options['filters']['field_program_keywords_value']['table'] = 'field_data_field_program_keywords';
  $handler->display->display_options['filters']['field_program_keywords_value']['field'] = 'field_program_keywords_value';
  $handler->display->display_options['filters']['field_program_keywords_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_program_keywords_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_program_keywords_value']['expose']['operator_id'] = 'field_program_keywords_value_op';
  $handler->display->display_options['filters']['field_program_keywords_value']['expose']['label'] = 'Find a Program:';
  $handler->display->display_options['filters']['field_program_keywords_value']['expose']['operator'] = 'field_program_keywords_value_op';
  $handler->display->display_options['filters']['field_program_keywords_value']['expose']['identifier'] = 'field_program_keywords_value';
  $handler->display->display_options['filters']['field_program_keywords_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );

  /* Display: Block:Graduate */
  $handler = $view->new_display('block', 'Block:Graduate', 'block_1');
  $handler->display->display_options['defaults']['empty'] = FALSE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = '<article class="program_finder_block_empty no_results">
      	<div class="program_finder_block_empty_inner">
      		<h3 class="program_finder_block_empty_heading">No Results Found</h3>
      		<a href="/academics/graduate-studies/graduate-programs" class="program_finder_block_empty_link">View All<br>Graduate Programs</a>
      	</div>
      </article>
      <article class="program_finder_block_empty">
      	<div class="program_finder_block_empty_inner"></div>
      </article>
      <article class="program_finder_block_empty">
      	<div class="program_finder_block_empty_inner"></div>
      </article>
      <article class="program_finder_block_empty">
      	<div class="program_finder_block_empty_inner"></div>
      </article>';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'program_page' => 'program_page',
  );
  /* Filter criterion: Content: Program Type (field_program_type) */
  $handler->display->display_options['filters']['field_program_type_value']['id'] = 'field_program_type_value';
  $handler->display->display_options['filters']['field_program_type_value']['table'] = 'field_data_field_program_type';
  $handler->display->display_options['filters']['field_program_type_value']['field'] = 'field_program_type_value';
  $handler->display->display_options['filters']['field_program_type_value']['value'] = array(
    'Graduate' => 'Graduate',
  );
  $handler->display->display_options['filters']['field_program_type_value']['expose']['operator_id'] = 'field_program_type_value_op';
  $handler->display->display_options['filters']['field_program_type_value']['expose']['label'] = 'Program Type';
  $handler->display->display_options['filters']['field_program_type_value']['expose']['operator'] = 'field_program_type_value_op';
  $handler->display->display_options['filters']['field_program_type_value']['expose']['identifier'] = 'field_program_type_value';
  $handler->display->display_options['filters']['field_program_type_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_program_type_value']['expose']['reduce'] = TRUE;
  /* Filter criterion: Content: Program Keywords (field_program_keywords) */
  $handler->display->display_options['filters']['field_program_keywords_value']['id'] = 'field_program_keywords_value';
  $handler->display->display_options['filters']['field_program_keywords_value']['table'] = 'field_data_field_program_keywords';
  $handler->display->display_options['filters']['field_program_keywords_value']['field'] = 'field_program_keywords_value';
  $handler->display->display_options['filters']['field_program_keywords_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_program_keywords_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_program_keywords_value']['expose']['operator_id'] = 'field_program_keywords_value_op';
  $handler->display->display_options['filters']['field_program_keywords_value']['expose']['label'] = 'Program Keywords:';
  $handler->display->display_options['filters']['field_program_keywords_value']['expose']['operator'] = 'field_program_keywords_value_op';
  $handler->display->display_options['filters']['field_program_keywords_value']['expose']['identifier'] = 'field_program_keywords_value';
  $handler->display->display_options['filters']['field_program_keywords_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );

  /* Display: Block: Business Undergrad */
  $handler = $view->new_display('block', 'Block: Business Undergrad', 'block_4');
  $handler->display->display_options['defaults']['empty'] = FALSE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = '<article class="program_finder_block_empty no_results">
      	<div class="program_finder_block_empty_inner">
      		<h3 class="program_finder_block_empty_heading">No Results Found</h3>
      		<a href="/academics/undergraduate-studies/undergraduate-majors" class="program_finder_block_empty_link">View All<br>Undergraduate Programs</a>
      	</div>
      </article>
      <article class="program_finder_block_empty">
      	<div class="program_finder_block_empty_inner"></div>
      </article>
      <article class="program_finder_block_empty">
      	<div class="program_finder_block_empty_inner"></div>
      </article>
      <article class="program_finder_block_empty">
      	<div class="program_finder_block_empty_inner"></div>
      </article>';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'program_page' => 'program_page',
  );
  /* Filter criterion: Content: Program Type (field_program_type) */
  $handler->display->display_options['filters']['field_program_type_value']['id'] = 'field_program_type_value';
  $handler->display->display_options['filters']['field_program_type_value']['table'] = 'field_data_field_program_type';
  $handler->display->display_options['filters']['field_program_type_value']['field'] = 'field_program_type_value';
  $handler->display->display_options['filters']['field_program_type_value']['value'] = array(
    'Undergraduate' => 'Undergraduate',
  );
  $handler->display->display_options['filters']['field_program_type_value']['expose']['operator_id'] = 'field_program_type_value_op';
  $handler->display->display_options['filters']['field_program_type_value']['expose']['label'] = 'Program Type';
  $handler->display->display_options['filters']['field_program_type_value']['expose']['operator'] = 'field_program_type_value_op';
  $handler->display->display_options['filters']['field_program_type_value']['expose']['identifier'] = 'field_program_type_value';
  $handler->display->display_options['filters']['field_program_type_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_program_type_value']['expose']['reduce'] = TRUE;
  /* Filter criterion: Content: Program Keywords (field_program_keywords) */
  $handler->display->display_options['filters']['field_program_keywords_value']['id'] = 'field_program_keywords_value';
  $handler->display->display_options['filters']['field_program_keywords_value']['table'] = 'field_data_field_program_keywords';
  $handler->display->display_options['filters']['field_program_keywords_value']['field'] = 'field_program_keywords_value';
  $handler->display->display_options['filters']['field_program_keywords_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_program_keywords_value']['value'] = 'Business';
  $handler->display->display_options['filters']['field_program_keywords_value']['expose']['operator_id'] = 'field_program_keywords_value_op';
  $handler->display->display_options['filters']['field_program_keywords_value']['expose']['label'] = 'Find a Program:';
  $handler->display->display_options['filters']['field_program_keywords_value']['expose']['operator'] = 'field_program_keywords_value_op';
  $handler->display->display_options['filters']['field_program_keywords_value']['expose']['identifier'] = 'field_program_keywords_value';
  $handler->display->display_options['filters']['field_program_keywords_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );

  /* Display: Block: Business Grad */
  $handler = $view->new_display('block', 'Block: Business Grad', 'block_3');
  $handler->display->display_options['defaults']['empty'] = FALSE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = '<article class="program_finder_block_empty no_results">
      	<div class="program_finder_block_empty_inner">
      		<h3 class="program_finder_block_empty_heading">No Results Found</h3>
      		<a href="/academics/graduate-studies/graduate-programs" class="program_finder_block_empty_link">View All<br>Graduate Programs</a>
      	</div>
      </article>
      <article class="program_finder_block_empty">
      	<div class="program_finder_block_empty_inner"></div>
      </article>
      <article class="program_finder_block_empty">
      	<div class="program_finder_block_empty_inner"></div>
      </article>
      <article class="program_finder_block_empty">
      	<div class="program_finder_block_empty_inner"></div>
      </article>';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'program_page' => 'program_page',
  );
  /* Filter criterion: Content: Program Type (field_program_type) */
  $handler->display->display_options['filters']['field_program_type_value']['id'] = 'field_program_type_value';
  $handler->display->display_options['filters']['field_program_type_value']['table'] = 'field_data_field_program_type';
  $handler->display->display_options['filters']['field_program_type_value']['field'] = 'field_program_type_value';
  $handler->display->display_options['filters']['field_program_type_value']['value'] = array(
    'Graduate' => 'Graduate',
  );
  $handler->display->display_options['filters']['field_program_type_value']['expose']['operator_id'] = 'field_program_type_value_op';
  $handler->display->display_options['filters']['field_program_type_value']['expose']['label'] = 'Program Type';
  $handler->display->display_options['filters']['field_program_type_value']['expose']['operator'] = 'field_program_type_value_op';
  $handler->display->display_options['filters']['field_program_type_value']['expose']['identifier'] = 'field_program_type_value';
  $handler->display->display_options['filters']['field_program_type_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_program_type_value']['expose']['reduce'] = TRUE;
  /* Filter criterion: Content: Program Keywords (field_program_keywords) */
  $handler->display->display_options['filters']['field_program_keywords_value']['id'] = 'field_program_keywords_value';
  $handler->display->display_options['filters']['field_program_keywords_value']['table'] = 'field_data_field_program_keywords';
  $handler->display->display_options['filters']['field_program_keywords_value']['field'] = 'field_program_keywords_value';
  $handler->display->display_options['filters']['field_program_keywords_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_program_keywords_value']['value'] = 'Business';
  $handler->display->display_options['filters']['field_program_keywords_value']['expose']['operator_id'] = 'field_program_keywords_value_op';
  $handler->display->display_options['filters']['field_program_keywords_value']['expose']['label'] = 'Find a Program:';
  $handler->display->display_options['filters']['field_program_keywords_value']['expose']['operator'] = 'field_program_keywords_value_op';
  $handler->display->display_options['filters']['field_program_keywords_value']['expose']['identifier'] = 'field_program_keywords_value';
  $handler->display->display_options['filters']['field_program_keywords_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $export['program_finder'] = $view;

  return $export;
}
