<?php
/**
 * @file
 * fastspot_faculty_listing_by_program.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function fastspot_faculty_listing_by_program_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|faculty_listing_by_program|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'faculty_listing_by_program';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_faculty_list_program',
      ),
    ),
    'fields' => array(
      'field_faculty_list_program' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|faculty_listing_by_program|default'] = $ds_layout;

  return $export;
}
