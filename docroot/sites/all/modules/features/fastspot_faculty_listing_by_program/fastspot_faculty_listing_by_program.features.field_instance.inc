<?php
/**
 * @file
 * fastspot_faculty_listing_by_program.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function fastspot_faculty_listing_by_program_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'node-faculty_listing_by_program-field_faculty_list_program'.
  $field_instances['node-faculty_listing_by_program-field_faculty_list_program'] = array(
    'bundle' => 'faculty_listing_by_program',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_faculty_list_program',
    'label' => 'Program',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 41,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Program');

  return $field_instances;
}
