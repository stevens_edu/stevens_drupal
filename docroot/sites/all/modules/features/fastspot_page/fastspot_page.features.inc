<?php
/**
 * @file
 * fastspot_page.features.inc
 */

/**
 * Implements hook_default_Workflow().
 */
function fastspot_page_default_Workflow() {
  $workflows = array();

  // Exported workflow: 'draft_done'
  $workflows['draft_done'] = entity_import('Workflow', '{
    "name" : "draft_done",
    "tab_roles" : [],
    "options" : [],
    "states" : {
      "1" : {"sid":"1","wid":"1","weight":"-50","sysid":"1","state":"(creation)","status":"1","name":"(creation)"},
      "6" : {"sid":"6","wid":"1","weight":"-20","sysid":"0","state":"Draft","status":"1","name":"draft"},
      "11" : {"sid":"11","wid":"1","weight":"-19","sysid":"0","state":"Done","status":"1","name":"done"}
    },
    "transitions" : {
      "1" : {"tid":"1","sid":"1","target_sid":"6","roles":{"-1":-1},"wid":"1","name":"1_2","label":"","rdf_mapping":[]},
      "6" : {"tid":"6","sid":"1","target_sid":"11","roles":{"11":"11"},"wid":"1","name":"1_3","label":"","rdf_mapping":[]},
      "11" : {"tid":"11","sid":"6","target_sid":"6","roles":{"-1":-1,"1":"1","2":"2","3":"11"},"wid":"1","name":"2_2","label":"Draft","rdf_mapping":[]},
      "16" : {"tid":"16","sid":"6","target_sid":"11","roles":{"3":"11"},"wid":"1","name":"2_3","label":"","rdf_mapping":[]},
      "21" : {"tid":"21","sid":"11","target_sid":"6","roles":{"-1":-1,"3":"11"},"wid":"1","name":"3_2","label":"Done","rdf_mapping":[]},
      "26" : {"tid":"26","sid":"11","target_sid":"11","roles":{"-1":-1,"1":"1","2":"2","3":"11"},"wid":"1","name":"3_3","label":"","rdf_mapping":[]}
    },
    "label" : "Draft Done",
    "typeMap" : [],
    "rdf_mapping" : [],
    "wid_original" : "1",
    "system_roles" : {
      "-1" : "(author)",
      "1" : "anonymous user",
      "2" : "authenticated user",
      "11" : "Enterprise Administrator",
      "16" : "Content Master",
      "21" : "Content Editor",
      "26" : "News Editor",
      "31" : "Events Admin",
      "36" : "Events Editor",
      "41" : "Personnel Editor",
      "46" : "Program Page Editor",
      "51" : "Subscription Viewer"
    }
  }');

  return $workflows;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function fastspot_page_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function fastspot_page_node_info() {
  $items = array(
    'page' => array(
      'name' => t('Page'),
      'base' => 'node_content',
      'description' => t('Use <em>basic pages</em> for your static content, such as an \'About us\' page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
