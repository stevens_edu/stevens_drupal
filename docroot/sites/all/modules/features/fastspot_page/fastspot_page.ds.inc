<?php
/**
 * @file
 * fastspot_page.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function fastspot_page_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|page|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'page';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_page_feature',
        1 => 'field_intro',
        2 => 'body',
        3 => 'field_content_components',
        4 => 'field_sidebar_components',
        5 => 'field_page_callouts',
      ),
    ),
    'fields' => array(
      'field_page_feature' => 'ds_content',
      'field_intro' => 'ds_content',
      'body' => 'ds_content',
      'field_content_components' => 'ds_content',
      'field_sidebar_components' => 'ds_content',
      'field_page_callouts' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|page|default'] = $ds_layout;

  return $export;
}
