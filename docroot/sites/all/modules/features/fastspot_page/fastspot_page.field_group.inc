<?php
/**
 * @file
 * fastspot_page.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function fastspot_page_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_migration|node|page|form';
  $field_group->group_name = 'group_migration';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Migration Data',
    'weight' => '6',
    'children' => array(
      0 => 'field_migration_meta_description',
      1 => 'field_migration_meta_title',
      2 => 'field_migration_notes',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-migration field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_migration|node|page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_page_components|node|page|form';
  $field_group->group_name = 'group_page_components';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Page Components',
    'weight' => '4',
    'children' => array(
      0 => 'field_content_components',
      1 => 'field_page_callouts',
      2 => 'field_sidebar_components',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-page-components field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_page_components|node|page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_page_options|node|page|form';
  $field_group->group_name = 'group_page_options';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Page Options',
    'weight' => '5',
    'children' => array(
      0 => 'field_hide_siblings',
      1 => 'field_page_audiences',
      2 => 'field_page_programs',
      3 => 'field_page_schools',
      4 => 'field_page_departments',
      5 => 'field_page_topics',
      6 => 'field_search_keywords',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-page-options field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_page_options|node|page|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Migration Data');
  t('Page Components');
  t('Page Options');

  return $field_groups;
}
