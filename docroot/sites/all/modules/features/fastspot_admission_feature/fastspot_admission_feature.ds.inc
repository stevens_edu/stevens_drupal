<?php
/**
 * @file
 * fastspot_admission_feature.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function fastspot_admission_feature_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|admission_feature|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'admission_feature';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_admission_bg_image',
        1 => 'field_admission_carousel_images',
        2 => 'field_admission_action_links',
        3 => 'field_admission_audience_links',
      ),
    ),
    'fields' => array(
      'field_admission_bg_image' => 'ds_content',
      'field_admission_carousel_images' => 'ds_content',
      'field_admission_action_links' => 'ds_content',
      'field_admission_audience_links' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|admission_feature|default'] = $ds_layout;

  return $export;
}
