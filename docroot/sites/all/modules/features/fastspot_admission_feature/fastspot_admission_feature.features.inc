<?php
/**
 * @file
 * fastspot_admission_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function fastspot_admission_feature_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function fastspot_admission_feature_node_info() {
  $items = array(
    'admission_feature' => array(
      'name' => t('Admission'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
