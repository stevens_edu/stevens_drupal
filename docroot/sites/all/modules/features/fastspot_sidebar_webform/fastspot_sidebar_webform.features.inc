<?php
/**
 * @file
 * fastspot_sidebar_webform.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function fastspot_sidebar_webform_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function fastspot_sidebar_webform_node_info() {
  $items = array(
    'webform_sidebar' => array(
      'name' => t('Sidebar Webform'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
