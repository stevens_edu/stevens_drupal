<?php
/**
 * @file
 * fastspot_home_page_gallery.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function fastspot_home_page_gallery_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|homepage_gallery_callout|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'homepage_gallery_callout';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|homepage_gallery_callout|default'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function fastspot_home_page_gallery_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|homepage_gallery_callout|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'homepage_gallery_callout';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_hpg_short_title',
        2 => 'field_hpg_description',
        3 => 'field_hpg_carousel',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_hpg_short_title' => 'ds_content',
      'field_hpg_description' => 'ds_content',
      'field_hpg_carousel' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|homepage_gallery_callout|default'] = $ds_layout;

  return $export;
}
