<?php

/**
 * @file
 * stevens_person_profile.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function stevens_person_profile_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function stevens_person_profile_node_info() {
  $items = array(
    'person_profile' => array(
      'name' => t('Person Spotlight'),
      'base' => 'node_content',
      'description' => t('A page for anyone, including alumni and students.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
