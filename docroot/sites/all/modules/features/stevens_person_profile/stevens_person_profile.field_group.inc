<?php

/**
 * @file
 * stevens_person_profile.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function stevens_person_profile_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_page_components|node|person_profile|form';
  $field_group->group_name = 'group_page_components';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'person_profile';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Page Components',
    'weight' => '12',
    'children' => array(
      0 => 'field_sidebar_components',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-page-components field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_page_components|node|person_profile|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Page Components');

  return $field_groups;
}
