<?php
/**
 * @file
 * stevens_related_news.features.inc
 */

/**
 * Implements hook_views_api().
 */
function stevens_related_news_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function stevens_related_news_image_default_styles() {
  $styles = array();

  // Exported image style: thumbnail_50x50.
  $styles['thumbnail_50x50'] = array(
    'label' => 'Thumbnail 50x50',
    'effects' => array(
      41 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 50,
          'height' => 50,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
