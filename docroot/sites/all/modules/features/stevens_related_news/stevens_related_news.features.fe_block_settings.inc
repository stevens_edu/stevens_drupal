<?php
/**
 * @file
 * stevens_related_news.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function stevens_related_news_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-related_news-block'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'related_news-block',
    'module' => 'views',
    'node_types' => array(
      0 => 'news_item',
    ),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'stevens' => array(
        'region' => 'sidebar_second',
        'status' => 1,
        'theme' => 'stevens',
        'weight' => -40,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
