<?php
/**
 * @file
 * fastspot_collaborators_callout.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function fastspot_collaborators_callout_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function fastspot_collaborators_callout_image_default_styles() {
  $styles = array();

  // Exported image style: collaborator_logo.
  $styles['collaborator_logo'] = array(
    'label' => 'Collaborator Logo',
    'effects' => array(
      53 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 130,
          'height' => 98,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function fastspot_collaborators_callout_node_info() {
  $items = array(
    'collaborators_component' => array(
      'name' => t('Collaborators'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
