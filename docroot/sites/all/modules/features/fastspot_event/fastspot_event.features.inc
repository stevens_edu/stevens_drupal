<?php
/**
 * @file
 * fastspot_event.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function fastspot_event_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function fastspot_event_image_default_styles() {
  $styles = array();

  // Exported image style: event_detail.
  $styles['event_detail'] = array(
    'label' => 'Event Detail',
    'effects' => array(
      66 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 460,
          'height' => 310,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'event_detail',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: event_list.
  $styles['event_list'] = array(
    'label' => 'Event List',
    'effects' => array(
      67 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 125,
          'height' => 125,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'event_list',
        ),
        'weight' => 0,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function fastspot_event_node_info() {
  $items = array(
    'event' => array(
      'name' => t('Event'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
