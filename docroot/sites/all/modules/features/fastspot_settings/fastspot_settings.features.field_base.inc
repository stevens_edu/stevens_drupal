<?php
/**
 * @file
 * fastspot_settings.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function fastspot_settings_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_department'.
  $field_bases['field_department'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_department',
    'indexes' => array(
      'tid' => array(
        0 => 'tid',
      ),
    ),
    'locked' => 0,
    'module' => 'taxonomy',
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => 'departments',
          'parent' => 0,
        ),
      ),
    ),
    'translatable' => 0,
    'type' => 'taxonomy_term_reference',
  );

  return $field_bases;
}
