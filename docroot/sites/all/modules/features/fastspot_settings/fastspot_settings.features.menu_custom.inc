<?php
/**
 * @file
 * fastspot_settings.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function fastspot_settings_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: main-menu.
  $menus['main-menu'] = array(
    'menu_name' => 'main-menu',
    'title' => 'Main menu',
    'description' => 'The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.',
  );
  // Exported menu: menu-audience-navigation.
  $menus['menu-audience-navigation'] = array(
    'menu_name' => 'menu-audience-navigation',
    'title' => 'Audience Navigation',
    'description' => '',
  );
  // Exported menu: menu-contact.
  $menus['menu-contact'] = array(
    'menu_name' => 'menu-contact',
    'title' => 'Contact',
    'description' => '',
  );
  // Exported menu: menu-footer-navigation.
  $menus['menu-footer-navigation'] = array(
    'menu_name' => 'menu-footer-navigation',
    'title' => 'Footer Navigation',
    'description' => '',
  );
  // Exported menu: menu-giving.
  $menus['menu-giving'] = array(
    'menu_name' => 'menu-giving',
    'title' => 'Giving',
    'description' => '',
  );
  // Exported menu: menu-social-media.
  $menus['menu-social-media'] = array(
    'menu_name' => 'menu-social-media',
    'title' => 'Social Media',
    'description' => '',
  );
  // Exported menu: menu-utilities.
  $menus['menu-utilities'] = array(
    'menu_name' => 'menu-utilities',
    'title' => 'Utilities',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Audience Navigation');
  t('Contact');
  t('Footer Navigation');
  t('Giving');
  t('Main menu');
  t('Social Media');
  t('The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.');
  t('Utilities');

  return $menus;
}
