<?php
/**
 * @file
 * fastspot_settings.features.wysiwyg_template.inc
 */

/**
 * Implements hook_wysiwyg_template_default_templates().
 */
function fastspot_settings_wysiwyg_template_default_templates() {
  $templates = array();
  $templates['blockquote'] = array(
    'title' => 'Blockquote',
    'description' => 'A Blockquote with a Citation',
    'weight' => 0,
    'fid' => 0,
    'body' => '<div class="quote"><blockquote><p>“For more than 140 years, Stevens has made an enormous contribution—far exceding its size—to technological innovation that has benefited our society. The next decade promises unprecedented change and opportunity. And Stevens is poised to become a major force in this new era.”</p></blockquote><div class="quote_caption"><cite>Nariman Farvardin, President</cite></div></div>',
    'name' => 'blockquote',
    'content_types' => array(
      'news_item' => 'News Item',
      'page' => 'Page',
    ),
  );
  $templates['image_landscape'] = array(
    'title' => 'Image with Caption - Landscape',
    'description' => '',
    'weight' => 0,
    'fid' => 0,
    'body' => '<div class="block_right content_figure content_figure_landscape"><img class="content_figure_image" src="//spacehold.it/720x480/1.jpg" border="0" /><div class="content_figure_caption">Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</div></div>',
    'name' => 'image_landscape',
    'content_types' => array(
      'news_item' => 'News Item',
      'page' => 'Page',
    ),
  );
  $templates['image_portrait'] = array(
    'title' => 'Image with Caption - Portrait',
    'description' => '',
    'weight' => 0,
    'fid' => 0,
    'body' => '<div class="block_left content_figure content_figure_portrait"><img class="content_figure_image" src="//spacehold.it/305x447/2.jpg" border="0" /><div class="content_figure_caption">Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</div></div>',
    'name' => 'image_portrait',
    'content_types' => array(
      'news_item' => 'News Item',
      'page' => 'Page',
    ),
  );
  $templates['image_wide'] = array(
    'title' => 'Image with Caption - Wide',
    'description' => '',
    'weight' => 0,
    'fid' => 0,
    'body' => '<div class="content_figure content_figure_wide"><img class="content_figure_image" src="//spacehold.it/852x480/1.jpg" border="0" /><div class="content_figure_caption">Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</div></div>',
    'name' => 'image_wide',
    'content_types' => array(
      'news_item' => 'News Item',
      'page' => 'Page',
    ),
  );
  return $templates;
}
