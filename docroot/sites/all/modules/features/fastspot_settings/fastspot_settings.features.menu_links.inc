<?php
/**
 * @file
 * fastspot_settings.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function fastspot_settings_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_about-stevens:node/3.
  $menu_links['main-menu_about-stevens:node/3'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/3',
    'router_path' => 'node/%',
    'link_title' => 'About Stevens',
    'options' => array(
      'identifier' => 'main-menu_about-stevens:node/3',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
  );
  // Exported menu link: main-menu_academics:node/5.
  $menu_links['main-menu_academics:node/5'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/5',
    'router_path' => 'node/%',
    'link_title' => 'Academics',
    'options' => array(
      'identifier' => 'main-menu_academics:node/5',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 2,
    'customized' => 0,
  );
  // Exported menu link: main-menu_admissions:node/4.
  $menu_links['main-menu_admissions:node/4'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/4',
    'router_path' => 'node/%',
    'link_title' => 'Admissions',
    'options' => array(
      'identifier' => 'main-menu_admissions:node/4',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 1,
    'customized' => 0,
  );
  // Exported menu link: main-menu_campus-life:node/7.
  $menu_links['main-menu_campus-life:node/7'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/7',
    'router_path' => 'node/%',
    'link_title' => 'Campus Life',
    'options' => array(
      'identifier' => 'main-menu_campus-life:node/7',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 5,
    'customized' => 0,
  );
  // Exported menu link: main-menu_executive-summary:node/24.
  $menu_links['main-menu_executive-summary:node/24'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/24',
    'router_path' => 'node/%',
    'link_title' => 'Executive Summary',
    'options' => array(
      'identifier' => 'main-menu_executive-summary:node/24',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'main-menu_strategic-plan-2012-2022:node/23',
  );
  // Exported menu link: main-menu_graduate:node/11.
  $menu_links['main-menu_graduate:node/11'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/11',
    'router_path' => 'node/%',
    'link_title' => 'Graduate',
    'options' => array(
      'identifier' => 'main-menu_graduate:node/11',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'main-menu_admissions:node/4',
  );
  // Exported menu link: main-menu_history:node/9.
  $menu_links['main-menu_history:node/9'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/9',
    'router_path' => 'node/%',
    'link_title' => 'History',
    'options' => array(
      'identifier' => 'main-menu_history:node/9',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'main-menu_about-stevens:node/3',
  );
  // Exported menu link: main-menu_mission:node/8.
  $menu_links['main-menu_mission:node/8'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/8',
    'router_path' => 'node/%',
    'link_title' => 'Mission',
    'options' => array(
      'identifier' => 'main-menu_mission:node/8',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'main-menu_about-stevens:node/3',
  );
  // Exported menu link: main-menu_research--entrepeneurship:node/6.
  $menu_links['main-menu_research--entrepeneurship:node/6'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/6',
    'router_path' => 'node/%',
    'link_title' => 'Research & Entrepeneurship',
    'options' => array(
      'identifier' => 'main-menu_research--entrepeneurship:node/6',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 3,
    'customized' => 0,
  );
  // Exported menu link: main-menu_strategic-plan-2012-2022:node/23.
  $menu_links['main-menu_strategic-plan-2012-2022:node/23'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/23',
    'router_path' => 'node/%',
    'link_title' => 'Strategic Plan 2012-2022',
    'options' => array(
      'identifier' => 'main-menu_strategic-plan-2012-2022:node/23',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'main-menu_about-stevens:node/3',
  );
  // Exported menu link: main-menu_summary-summary:node/25.
  $menu_links['main-menu_summary-summary:node/25'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/25',
    'router_path' => 'node/%',
    'link_title' => 'Summary Summary',
    'options' => array(
      'identifier' => 'main-menu_summary-summary:node/25',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'main-menu_executive-summary:node/24',
  );
  // Exported menu link: main-menu_undergraduate:node/10.
  $menu_links['main-menu_undergraduate:node/10'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/10',
    'router_path' => 'node/%',
    'link_title' => 'Undergraduate',
    'options' => array(
      'identifier' => 'main-menu_undergraduate:node/10',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'parent_identifier' => 'main-menu_admissions:node/4',
  );
  // Exported menu link: menu-audience-navigation_future-student:node/20.
  $menu_links['menu-audience-navigation_future-student:node/20'] = array(
    'menu_name' => 'menu-audience-navigation',
    'link_path' => 'node/20',
    'router_path' => 'node/%',
    'link_title' => 'Future Student',
    'options' => array(
      'identifier' => 'menu-audience-navigation_future-student:node/20',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 1,
    'customized' => 0,
  );
  // Exported menu link: menu-audience-navigation_high-school-counselor:node/39.
  $menu_links['menu-audience-navigation_high-school-counselor:node/39'] = array(
    'menu_name' => 'menu-audience-navigation',
    'link_path' => 'node/39',
    'router_path' => 'node/%',
    'link_title' => 'High School Counselor',
    'options' => array(
      'identifier' => 'menu-audience-navigation_high-school-counselor:node/39',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
  );
  // Exported menu link: menu-audience-navigation_parent-or-family-member:node/21.
  $menu_links['menu-audience-navigation_parent-or-family-member:node/21'] = array(
    'menu_name' => 'menu-audience-navigation',
    'link_path' => 'node/21',
    'router_path' => 'node/%',
    'link_title' => 'Parent or Family Member',
    'options' => array(
      'identifier' => 'menu-audience-navigation_parent-or-family-member:node/21',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 2,
    'customized' => 0,
  );
  // Exported menu link: menu-audience-navigation_stevens-alumnusa:node/22.
  $menu_links['menu-audience-navigation_stevens-alumnusa:node/22'] = array(
    'menu_name' => 'menu-audience-navigation',
    'link_path' => 'node/22',
    'router_path' => 'node/%',
    'link_title' => 'Stevens Alumnus/a',
    'options' => array(
      'attributes' => array(),
      'identifier' => 'menu-audience-navigation_stevens-alumnusa:node/22',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 3,
    'customized' => 1,
  );
  // Exported menu link: menu-contact_2012165000:tel:+12012165000.
  $menu_links['menu-contact_2012165000:tel:+12012165000'] = array(
    'menu_name' => 'menu-contact',
    'link_path' => 'tel:+12012165000',
    'router_path' => '',
    'link_title' => '201.216.5000',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-contact_2012165000:tel:+12012165000',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-contact_infostevensedu:mailto:info@stevens.edu.
  $menu_links['menu-contact_infostevensedu:mailto:info@stevens.edu'] = array(
    'menu_name' => 'menu-contact',
    'link_path' => 'mailto:info@stevens.edu',
    'router_path' => '',
    'link_title' => 'info@stevens.edu',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-contact_infostevensedu:mailto:info@stevens.edu',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-contact_request-information:node/32.
  $menu_links['menu-contact_request-information:node/32'] = array(
    'menu_name' => 'menu-contact',
    'link_path' => 'node/32',
    'router_path' => 'node/%',
    'link_title' => 'Request Information',
    'options' => array(
      'identifier' => 'menu-contact_request-information:node/32',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 3,
    'customized' => 0,
  );
  // Exported menu link: menu-contact_website-feedback:node/33.
  $menu_links['menu-contact_website-feedback:node/33'] = array(
    'menu_name' => 'menu-contact',
    'link_path' => 'node/33',
    'router_path' => 'node/%',
    'link_title' => 'Website Feedback',
    'options' => array(
      'identifier' => 'menu-contact_website-feedback:node/33',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 4,
    'customized' => 0,
  );
  // Exported menu link: menu-footer-navigation_campus-store:node/29.
  $menu_links['menu-footer-navigation_campus-store:node/29'] = array(
    'menu_name' => 'menu-footer-navigation',
    'link_path' => 'node/29',
    'router_path' => 'node/%',
    'link_title' => 'Campus Store',
    'options' => array(
      'identifier' => 'menu-footer-navigation_campus-store:node/29',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 2,
    'customized' => 0,
  );
  // Exported menu link: menu-footer-navigation_careers:node/28.
  $menu_links['menu-footer-navigation_careers:node/28'] = array(
    'menu_name' => 'menu-footer-navigation',
    'link_path' => 'node/28',
    'router_path' => 'node/%',
    'link_title' => 'Careers',
    'options' => array(
      'identifier' => 'menu-footer-navigation_careers:node/28',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 1,
    'customized' => 0,
  );
  // Exported menu link: menu-footer-navigation_events:node/31.
  $menu_links['menu-footer-navigation_events:node/31'] = array(
    'menu_name' => 'menu-footer-navigation',
    'link_path' => 'node/31',
    'router_path' => 'node/%',
    'link_title' => 'Events',
    'options' => array(
      'identifier' => 'menu-footer-navigation_events:node/31',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 4,
    'customized' => 0,
  );
  // Exported menu link: menu-footer-navigation_make-a-gift:node/30.
  $menu_links['menu-footer-navigation_make-a-gift:node/30'] = array(
    'menu_name' => 'menu-footer-navigation',
    'link_path' => 'node/30',
    'router_path' => 'node/%',
    'link_title' => 'Make a Gift',
    'options' => array(
      'identifier' => 'menu-footer-navigation_make-a-gift:node/30',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 3,
    'customized' => 0,
  );
  // Exported menu link: menu-giving_give-now:node/18.
  $menu_links['menu-giving_give-now:node/18'] = array(
    'menu_name' => 'menu-giving',
    'link_path' => 'node/18',
    'router_path' => 'node/%',
    'link_title' => 'Give Now',
    'options' => array(
      'identifier' => 'menu-giving_give-now:node/18',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
  );
  // Exported menu link: menu-giving_learn-about-giving:node/19.
  $menu_links['menu-giving_learn-about-giving:node/19'] = array(
    'menu_name' => 'menu-giving',
    'link_path' => 'node/19',
    'router_path' => 'node/%',
    'link_title' => 'Learn about Giving',
    'options' => array(
      'identifier' => 'menu-giving_learn-about-giving:node/19',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 1,
    'customized' => 0,
  );
  // Exported menu link: menu-social-media_facebook:http://www.facebook.com/Stevens1870.
  $menu_links['menu-social-media_facebook:http://www.facebook.com/Stevens1870'] = array(
    'menu_name' => 'menu-social-media',
    'link_path' => 'http://www.facebook.com/Stevens1870',
    'router_path' => '',
    'link_title' => 'Facebook',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-social-media_facebook:http://www.facebook.com/Stevens1870',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
  );
  // Exported menu link: menu-social-media_instagram:https://www.instagram.com/followstevens/.
  $menu_links['menu-social-media_instagram:https://www.instagram.com/followstevens/'] = array(
    'menu_name' => 'menu-social-media',
    'link_path' => 'https://www.instagram.com/followstevens/',
    'router_path' => '',
    'link_title' => 'Instagram',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-social-media_instagram:https://www.instagram.com/followstevens/',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 2,
    'customized' => 1,
  );
  // Exported menu link: menu-social-media_linkedin:http://www.linkedin.com/edu/school?id=18880&trk=edu-hp-follow-name.
  $menu_links['menu-social-media_linkedin:http://www.linkedin.com/edu/school?id=18880&trk=edu-hp-follow-name'] = array(
    'menu_name' => 'menu-social-media',
    'link_path' => 'http://www.linkedin.com/edu/school?id=18880&trk=edu-hp-follow-name',
    'router_path' => '',
    'link_title' => 'LinkedIn',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-social-media_linkedin:http://www.linkedin.com/edu/school?id=18880&trk=edu-hp-follow-name',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 4,
    'customized' => 1,
  );
  // Exported menu link: menu-social-media_social-media-hub:node/27.
  $menu_links['menu-social-media_social-media-hub:node/27'] = array(
    'menu_name' => 'menu-social-media',
    'link_path' => 'node/27',
    'router_path' => 'node/%',
    'link_title' => 'Social Media Hub',
    'options' => array(
      'identifier' => 'menu-social-media_social-media-hub:node/27',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 7,
    'customized' => 0,
  );
  // Exported menu link: menu-social-media_twitter:https://twitter.com/FollowStevens.
  $menu_links['menu-social-media_twitter:https://twitter.com/FollowStevens'] = array(
    'menu_name' => 'menu-social-media',
    'link_path' => 'https://twitter.com/FollowStevens',
    'router_path' => '',
    'link_title' => 'Twitter',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-social-media_twitter:https://twitter.com/FollowStevens',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 1,
    'customized' => 1,
  );
  // Exported menu link: menu-social-media_youtube:http://www.youtube.com/EdwinAStevens70.
  $menu_links['menu-social-media_youtube:http://www.youtube.com/EdwinAStevens70'] = array(
    'menu_name' => 'menu-social-media',
    'link_path' => 'http://www.youtube.com/EdwinAStevens70',
    'router_path' => '',
    'link_title' => 'Youtube',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-social-media_youtube:http://www.youtube.com/EdwinAStevens70',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 3,
    'customized' => 1,
  );
  // Exported menu link: menu-utilities_alumni:node/13.
  $menu_links['menu-utilities_alumni:node/13'] = array(
    'menu_name' => 'menu-utilities',
    'link_path' => 'node/13',
    'router_path' => 'node/%',
    'link_title' => 'Alumni',
    'options' => array(
      'identifier' => 'menu-utilities_alumni:node/13',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 2,
    'customized' => 0,
  );
  // Exported menu link: menu-utilities_apply:node/14.
  $menu_links['menu-utilities_apply:node/14'] = array(
    'menu_name' => 'menu-utilities',
    'link_path' => 'node/14',
    'router_path' => 'node/%',
    'link_title' => 'Apply',
    'options' => array(
      'identifier' => 'menu-utilities_apply:node/14',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 3,
    'customized' => 0,
  );
  // Exported menu link: menu-utilities_athletics:node/17.
  $menu_links['menu-utilities_athletics:node/17'] = array(
    'menu_name' => 'menu-utilities',
    'link_path' => 'node/17',
    'router_path' => 'node/%',
    'link_title' => 'Athletics',
    'options' => array(
      'identifier' => 'menu-utilities_athletics:node/17',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 6,
    'customized' => 0,
  );
  // Exported menu link: menu-utilities_directory:node/16.
  $menu_links['menu-utilities_directory:node/16'] = array(
    'menu_name' => 'menu-utilities',
    'link_path' => 'node/16',
    'router_path' => 'node/%',
    'link_title' => 'Directory',
    'options' => array(
      'identifier' => 'menu-utilities_directory:node/16',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 5,
    'customized' => 0,
  );
  // Exported menu link: menu-utilities_news:node/15.
  $menu_links['menu-utilities_news:node/15'] = array(
    'menu_name' => 'menu-utilities',
    'link_path' => 'node/15',
    'router_path' => 'node/%',
    'link_title' => 'News',
    'options' => array(
      'identifier' => 'menu-utilities_news:node/15',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 4,
    'customized' => 0,
  );
  // Exported menu link: menu-utilities_visit:node/12.
  $menu_links['menu-utilities_visit:node/12'] = array(
    'menu_name' => 'menu-utilities',
    'link_path' => 'node/12',
    'router_path' => 'node/%',
    'link_title' => 'Visit',
    'options' => array(
      'identifier' => 'menu-utilities_visit:node/12',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('201.216.5000');
  t('About Stevens');
  t('Academics');
  t('Admissions');
  t('Alumni');
  t('Apply');
  t('Athletics');
  t('Campus Life');
  t('Campus Store');
  t('Careers');
  t('Directory');
  t('Events');
  t('Executive Summary');
  t('Facebook');
  t('Future Student');
  t('Give Now');
  t('Graduate');
  t('High School Counselor');
  t('History');
  t('Instagram');
  t('Learn about Giving');
  t('LinkedIn');
  t('Make a Gift');
  t('Mission');
  t('News');
  t('Parent or Family Member');
  t('Request Information');
  t('Research & Entrepeneurship');
  t('Social Media Hub');
  t('Stevens Alumnus/a');
  t('Strategic Plan 2012-2022');
  t('Summary Summary');
  t('Twitter');
  t('Undergraduate');
  t('Visit');
  t('Website Feedback');
  t('Youtube');
  t('info@stevens.edu');

  return $menu_links;
}
