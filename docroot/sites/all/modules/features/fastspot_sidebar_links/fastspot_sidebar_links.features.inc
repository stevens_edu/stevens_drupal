<?php
/**
 * @file
 * fastspot_sidebar_links.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function fastspot_sidebar_links_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function fastspot_sidebar_links_node_info() {
  $items = array(
    'link_list_sidebar' => array(
      'name' => t('Link List Sidebar'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
