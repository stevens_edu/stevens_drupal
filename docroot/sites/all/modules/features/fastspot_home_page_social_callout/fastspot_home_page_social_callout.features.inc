<?php
/**
 * @file
 * fastspot_home_page_social_callout.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function fastspot_home_page_social_callout_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function fastspot_home_page_social_callout_node_info() {
  $items = array(
    'home_page_social' => array(
      'name' => t('Home Page Social'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
