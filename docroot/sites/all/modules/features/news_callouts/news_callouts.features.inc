<?php
/**
 * @file
 * news_callouts.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function news_callouts_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function news_callouts_node_info() {
  $items = array(
    'news_by_category' => array(
      'name' => t('News by Category'),
      'base' => 'node_content',
      'description' => t('Draws up to 8 news articles for a given category (news articles must have an image to be drawn).'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'news_by_topic' => array(
      'name' => t('News by Topic'),
      'base' => 'node_content',
      'description' => t('A full width block that will show news by a given program, department, school, or research.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'related_news' => array(
      'name' => t('Related News'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
