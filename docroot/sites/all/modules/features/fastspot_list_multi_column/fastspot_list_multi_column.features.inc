<?php
/**
 * @file
 * fastspot_list_multi_column.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function fastspot_list_multi_column_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function fastspot_list_multi_column_node_info() {
  $items = array(
    'multi_column_callout' => array(
      'name' => t('List Multi-Column'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
