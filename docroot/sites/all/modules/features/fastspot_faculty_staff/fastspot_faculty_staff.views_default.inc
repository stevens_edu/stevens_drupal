<?php
/**
 * @file
 * fastspot_faculty_staff.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function fastspot_faculty_staff_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'personnel';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Personnel';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Personnel';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['links'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Sort criterion: Content: Sort name (field_faculty_sort) */
  $handler->display->display_options['sorts']['field_faculty_sort_value']['id'] = 'field_faculty_sort_value';
  $handler->display->display_options['sorts']['field_faculty_sort_value']['table'] = 'field_data_field_faculty_sort';
  $handler->display->display_options['sorts']['field_faculty_sort_value']['field'] = 'field_faculty_sort_value';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'faculty_staff' => 'faculty_staff',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Sort name (field_faculty_sort) */
  $handler->display->display_options['arguments']['field_faculty_sort_value']['id'] = 'field_faculty_sort_value';
  $handler->display->display_options['arguments']['field_faculty_sort_value']['table'] = 'field_data_field_faculty_sort';
  $handler->display->display_options['arguments']['field_faculty_sort_value']['field'] = 'field_faculty_sort_value';
  $handler->display->display_options['arguments']['field_faculty_sort_value']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_faculty_sort_value']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_faculty_sort_value']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_faculty_sort_value']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_faculty_sort_value']['limit'] = '0';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'faculty_staff' => 'faculty_staff',
  );
  /* Filter criterion: Content: Title */
  $handler->display->display_options['filters']['title']['id'] = 'title';
  $handler->display->display_options['filters']['title']['table'] = 'node';
  $handler->display->display_options['filters']['title']['field'] = 'title';
  $handler->display->display_options['filters']['title']['operator'] = 'contains';
  $handler->display->display_options['filters']['title']['exposed'] = TRUE;
  $handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['label'] = 'Name';
  $handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
  $handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['path'] = 'directory/personnel';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Personnel';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'menu-utilities';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Attachment */
  $handler = $view->new_display('attachment', 'Attachment', 'attachment_1');
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Sort name (field_faculty_sort) */
  $handler->display->display_options['arguments']['field_faculty_sort_value']['id'] = 'field_faculty_sort_value';
  $handler->display->display_options['arguments']['field_faculty_sort_value']['table'] = 'field_data_field_faculty_sort';
  $handler->display->display_options['arguments']['field_faculty_sort_value']['field'] = 'field_faculty_sort_value';
  $handler->display->display_options['arguments']['field_faculty_sort_value']['default_action'] = 'summary';
  $handler->display->display_options['arguments']['field_faculty_sort_value']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_faculty_sort_value']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_faculty_sort_value']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_faculty_sort_value']['summary_options']['count'] = FALSE;
  $handler->display->display_options['arguments']['field_faculty_sort_value']['summary_options']['override'] = TRUE;
  $handler->display->display_options['arguments']['field_faculty_sort_value']['summary_options']['items_per_page'] = '26';
  $handler->display->display_options['arguments']['field_faculty_sort_value']['glossary'] = TRUE;
  $handler->display->display_options['arguments']['field_faculty_sort_value']['limit'] = '1';
  $handler->display->display_options['arguments']['field_faculty_sort_value']['case'] = 'upper';
  $handler->display->display_options['displays'] = array(
    'page' => 'page',
    'default' => 0,
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_1');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $export['personnel'] = $view;

  return $export;
}
