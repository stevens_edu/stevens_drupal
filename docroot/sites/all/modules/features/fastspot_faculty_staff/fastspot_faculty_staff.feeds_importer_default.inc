<?php
/**
 * @file
 * fastspot_faculty_staff.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function fastspot_faculty_staff_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'faculty';
  $feeds_importer->config = array(
    'name' => 'Faculty',
    'description' => 'Bring in faculty over API',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => 0,
        'use_pubsubhubbub' => 0,
        'designated_hub' => '',
        'request_timeout' => '',
        'auto_scheme' => '',
        'accept_invalid_cert' => 0,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsJSONPathParser',
      'config' => array(
        'context' => '*',
        'sources' => array(
          'jsonpath_parser:0' => 'faculty_id',
          'jsonpath_parser:1' => 'salutation',
          'jsonpath_parser:2' => 'first_name',
          'jsonpath_parser:3' => 'middle_name',
          'jsonpath_parser:7' => 'last_name',
          'jsonpath_parser:5' => 'title',
          'jsonpath_parser:6' => 'image_path',
          'jsonpath_parser:8' => 'building',
          'jsonpath_parser:9' => 'room',
          'jsonpath_parser:10' => 'email',
          'jsonpath_parser:11' => 'phone',
          'jsonpath_parser:12' => 'programs..prog_id',
          'jsonpath_parser:13' => 'programs..prog_id',
        ),
        'debug' => array(
          'options' => array(
            'context' => 0,
            'jsonpath_parser:0' => 0,
            'jsonpath_parser:1' => 0,
            'jsonpath_parser:2' => 0,
            'jsonpath_parser:3' => 0,
            'jsonpath_parser:7' => 0,
            'jsonpath_parser:5' => 0,
            'jsonpath_parser:6' => 0,
            'jsonpath_parser:8' => 0,
            'jsonpath_parser:9' => 0,
            'jsonpath_parser:10' => 0,
            'jsonpath_parser:11' => 0,
            'jsonpath_parser:12' => 0,
            'jsonpath_parser:13' => 0,
          ),
        ),
        'allow_override' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'jsonpath_parser:0',
            'target' => 'guid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'jsonpath_parser:1',
            'target' => 'Temporary target 1',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'jsonpath_parser:2',
            'target' => 'Temporary target 2',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'jsonpath_parser:3',
            'target' => 'Temporary target 3',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'jsonpath_parser:7',
            'target' => 'Temporary target 4',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'Blank source 1',
            'target' => 'title',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'jsonpath_parser:5',
            'target' => 'field_faculty_position',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'jsonpath_parser:6',
            'target' => 'field_faculty_image:uri',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'jsonpath_parser:8',
            'target' => 'field_faculty_building',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'jsonpath_parser:9',
            'target' => 'field_faculty_room',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'jsonpath_parser:10',
            'target' => 'field_faculty_email',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'jsonpath_parser:11',
            'target' => 'field_faculty_phone',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'Blank source 2',
            'target' => 'field_faculty_sort',
            'unique' => FALSE,
          ),
          13 => array(
            'source' => 'jsonpath_parser:12',
            'target' => 'field_news_programs',
            'term_search' => '2',
            'autocreate' => 0,
          ),
        ),
        'update_existing' => '2',
        'update_non_existent' => 'delete',
        'input_format' => 'filtered_html',
        'skip_hash_check' => 0,
        'bundle' => 'faculty_staff',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '0',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 1,
  );
  $export['faculty'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'programs';
  $feeds_importer->config = array(
    'name' => 'Programs',
    'description' => 'Pull in Program IDs to associate Programs and Faculty',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
        'request_timeout' => NULL,
        'auto_scheme' => 'http',
        'accept_invalid_cert' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsJSONPathParser',
      'config' => array(
        'context' => '*',
        'sources' => array(
          'jsonpath_parser:0' => 'prog_id',
          'jsonpath_parser:1' => 'prog_id',
          'jsonpath_parser:2' => 'name',
        ),
        'debug' => array(
          'options' => array(
            'context' => 0,
            'jsonpath_parser:0' => 0,
            'jsonpath_parser:1' => 0,
            'jsonpath_parser:2' => 0,
          ),
        ),
        'allow_override' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsTermProcessor',
      'config' => array(
        'vocabulary' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'jsonpath_parser:1',
            'target' => 'field_program_id',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'jsonpath_parser:2',
            'target' => 'name',
            'unique' => 1,
          ),
        ),
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'programs',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '0',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['programs'] = $feeds_importer;

  return $export;
}
