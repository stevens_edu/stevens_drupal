<?php
/**
 * @file
 * fastspot_site_alert_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function fastspot_site_alert_feature_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function fastspot_site_alert_feature_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function fastspot_site_alert_feature_node_info() {
  $items = array(
    'site_alert' => array(
      'name' => t('Site Alert'),
      'base' => 'node_content',
      'description' => t('Published alerts will appear at the top of every page on the site.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
