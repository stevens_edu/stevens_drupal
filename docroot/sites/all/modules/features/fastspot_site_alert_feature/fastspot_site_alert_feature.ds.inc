<?php
/**
 * @file
 * fastspot_site_alert_feature.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function fastspot_site_alert_feature_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|site_alert|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'site_alert';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_alert_date',
        1 => 'body',
      ),
    ),
    'fields' => array(
      'field_alert_date' => 'ds_content',
      'body' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|site_alert|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|site_alert|full';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'site_alert';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_alert_date',
        1 => 'body',
        2 => 'comments',
      ),
    ),
    'fields' => array(
      'field_alert_date' => 'ds_content',
      'body' => 'ds_content',
      'comments' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|site_alert|full'] = $ds_layout;

  return $export;
}
