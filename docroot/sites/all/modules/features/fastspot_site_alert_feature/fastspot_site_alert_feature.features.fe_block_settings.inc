<?php
/**
 * @file
 * fastspot_site_alert_feature.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function fastspot_site_alert_feature_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-site_wide_alert-block'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'site_wide_alert-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'stevens' => array(
        'region' => 'alert',
        'status' => 1,
        'theme' => 'stevens',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
