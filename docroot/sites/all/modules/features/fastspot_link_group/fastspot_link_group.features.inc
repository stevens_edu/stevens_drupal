<?php
/**
 * @file
 * fastspot_link_group.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function fastspot_link_group_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function fastspot_link_group_node_info() {
  $items = array(
    'link_group' => array(
      'name' => t('Link Group'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
