<?php
/**
 * @file
 * instagram_social_blocks.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function instagram_social_blocks_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function instagram_social_blocks_image_default_styles() {
  $styles = array();

  // Exported image style: instagram_large.
  $styles['instagram_large'] = array(
    'label' => 'Instagram Large',
    'effects' => array(
      71 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 500,
          'height' => 500,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'instagram_large',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: instagram_medium.
  $styles['instagram_medium'] = array(
    'label' => 'Instagram Medium',
    'effects' => array(
      72 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 346,
          'height' => 346,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'instagram_medium',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: instagram_small.
  $styles['instagram_small'] = array(
    'label' => 'Instagram Small',
    'effects' => array(
      73 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 288,
          'height' => 288,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'instagram_small',
        ),
        'weight' => 0,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function instagram_social_blocks_node_info() {
  $items = array(
    'instagram_block' => array(
      'name' => t('Instagram Block'),
      'base' => 'node_content',
      'description' => t('Adds an Instagram block to the homepage social feed.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
