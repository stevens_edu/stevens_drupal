<?php
/**
 * @file
 * fastspot_related_news.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function fastspot_related_news_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function fastspot_related_news_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function fastspot_related_news_node_info() {
  $items = array(
    'news_link' => array(
      'name' => t('News Link'),
      'base' => 'node_content',
      'description' => t('To power the In the news section'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
