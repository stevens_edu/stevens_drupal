<?php
/**
 * @file
 * fastspot_related_news.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function fastspot_related_news_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-in_the_news-block'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'in_the_news-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'news',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'stevens' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'stevens',
        'weight' => 0,
      ),
    ),
    'title' => 'In the News',
    'visibility' => 1,
  );

  return $export;
}
