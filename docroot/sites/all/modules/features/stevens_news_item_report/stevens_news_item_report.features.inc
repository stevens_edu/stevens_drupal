<?php
/**
 * @file
 * stevens_news_item_report.features.inc
 */

/**
 * Implements hook_views_api().
 */
function stevens_news_item_report_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
