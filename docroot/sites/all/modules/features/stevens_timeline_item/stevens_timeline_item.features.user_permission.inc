<?php

/**
 * @file
 * stevens_timeline_item.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function stevens_timeline_item_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create timeline_item content'.
  $permissions['create timeline_item content'] = array(
    'name' => 'create timeline_item content',
    'roles' => array(
      'Content Master' => 'Content Master',
      'Enterprise Administrator' => 'Enterprise Administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any timeline_item content'.
  $permissions['delete any timeline_item content'] = array(
    'name' => 'delete any timeline_item content',
    'roles' => array(
      'Content Master' => 'Content Master',
      'Enterprise Administrator' => 'Enterprise Administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own timeline_item content'.
  $permissions['delete own timeline_item content'] = array(
    'name' => 'delete own timeline_item content',
    'roles' => array(
      'Content Master' => 'Content Master',
      'Enterprise Administrator' => 'Enterprise Administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any timeline_item content'.
  $permissions['edit any timeline_item content'] = array(
    'name' => 'edit any timeline_item content',
    'roles' => array(
      'Content Master' => 'Content Master',
      'Enterprise Administrator' => 'Enterprise Administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own timeline_item content'.
  $permissions['edit own timeline_item content'] = array(
    'name' => 'edit own timeline_item content',
    'roles' => array(
      'Content Master' => 'Content Master',
      'Enterprise Administrator' => 'Enterprise Administrator',
    ),
    'module' => 'node',
  );

  return $permissions;
}
