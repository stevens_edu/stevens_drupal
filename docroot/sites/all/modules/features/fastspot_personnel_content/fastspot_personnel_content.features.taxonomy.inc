<?php

/**
 * @file
 * fastspot_personnel_content.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function fastspot_personnel_content_taxonomy_default_vocabularies() {
  return array(
    'organizational_units' => array(
      'name' => 'Organizational Units',
      'machine_name' => 'organizational_units',
      'description' => 'Organization Units initially sourced from soesrv04.  For use in the Personnel content type.',
      'hierarchy' => 1,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
    'tenure_track' => array(
      'name' => 'Tenure Type',
      'machine_name' => 'tenure_track',
      'description' => 'Contains tenure track terms for use in Personnel nodes.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
