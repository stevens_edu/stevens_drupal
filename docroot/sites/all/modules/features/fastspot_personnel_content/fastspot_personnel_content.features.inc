<?php

/**
 * @file
 * fastspot_personnel_content.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function fastspot_personnel_content_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function fastspot_personnel_content_node_info() {
  $items = array(
    'personnel' => array(
      'name' => t('Personnel'),
      'base' => 'node_content',
      'description' => t('An entry in the Personnel Directory and Faculty Listings.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
