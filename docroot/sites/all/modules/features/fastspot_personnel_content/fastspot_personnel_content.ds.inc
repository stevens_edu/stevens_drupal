<?php

/**
 * @file
 * fastspot_personnel_content.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function fastspot_personnel_content_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|personnel|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'personnel';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|personnel|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|personnel|teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'personnel';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|personnel|teaser'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function fastspot_personnel_content_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|personnel|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'personnel';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'body',
        2 => 'field_personnel_sort',
        3 => 'field_personnel_position',
        4 => 'field_personnel_address',
        5 => 'field_personnel_email',
        6 => 'field_personnel_phone',
        7 => 'field_personnel_fax',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'body' => 'ds_content',
      'field_personnel_sort' => 'ds_content',
      'field_personnel_position' => 'ds_content',
      'field_personnel_address' => 'ds_content',
      'field_personnel_email' => 'ds_content',
      'field_personnel_phone' => 'ds_content',
      'field_personnel_fax' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|personnel|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|personnel|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'personnel';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_personnel_position',
        2 => 'field_personnel_address',
        3 => 'field_personnel_email',
        4 => 'field_personnel_phone',
        5 => 'field_personnel_fax',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_personnel_position' => 'ds_content',
      'field_personnel_address' => 'ds_content',
      'field_personnel_email' => 'ds_content',
      'field_personnel_phone' => 'ds_content',
      'field_personnel_fax' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|personnel|teaser'] = $ds_layout;

  return $export;
}
