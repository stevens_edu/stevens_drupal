<?php
/**
 * @file
 * fastspot_curriculum_feature.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function fastspot_curriculum_feature_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-curriculum-field_curriculum_description'.
  $field_instances['node-curriculum-field_curriculum_description'] = array(
    'bundle' => 'curriculum',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_curriculum_description',
    'label' => 'Description',
    'required' => 0,
    'settings' => array(
      'better_formats' => array(
        'allowed_formats' => array(
          'filtered_html' => 0,
          'full_html' => 0,
          'intro_text' => 'intro_text',
          'plain_text' => 0,
        ),
        'allowed_formats_toggle' => 1,
        'default_order_toggle' => 0,
        'default_order_wrapper' => array(
          'formats' => array(
            'filtered_html' => array(
              'weight' => -9,
            ),
            'full_html' => array(
              'weight' => -10,
            ),
            'intro_text' => array(
              'weight' => -8,
            ),
            'plain_text' => array(
              'weight' => -7,
            ),
          ),
        ),
      ),
      'linkit' => array(
        'button_text' => 'Search',
        'enable' => 0,
        'profile' => '',
      ),
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-curriculum-field_curriculum_program'.
  $field_instances['node-curriculum-field_curriculum_program'] = array(
    'bundle' => 'curriculum',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Choosing a program here will pull and cache the curiculum information from the Stevens API. If it returns no records, there may be no information in the database for that curriculum - please reach out to Stevens IT support.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'ds',
        'settings' => array(
          'taxonomy_term_link' => 0,
          'taxonomy_term_separator' => ', ',
        ),
        'type' => 'ds_taxonomy_separator',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_curriculum_program',
    'label' => 'Program',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Choosing a program here will pull and cache the curiculum information from the Stevens API. If it returns no records, there may be no information in the database for that curriculum - please reach out to Stevens IT support.');
  t('Description');
  t('Program');

  return $field_instances;
}
