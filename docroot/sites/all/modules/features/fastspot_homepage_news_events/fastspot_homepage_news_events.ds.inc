<?php
/**
 * @file
 * fastspot_homepage_news_events.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function fastspot_homepage_news_events_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|homepage_events|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'homepage_events';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_announcement_callout_class',
      ),
    ),
    'fields' => array(
      'field_announcement_callout_class' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|homepage_events|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|homepage_news|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'homepage_news';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'body',
        1 => 'field_homepage_news_link',
      ),
    ),
    'fields' => array(
      'body' => 'ds_content',
      'field_homepage_news_link' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|homepage_news|default'] = $ds_layout;

  return $export;
}
