<?php
/**
 * @file
 * fastspot_contact_list.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function fastspot_contact_list_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|contact_list|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'contact_list';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|contact_list|default'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function fastspot_contact_list_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|contact_list|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'contact_list';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_contact_heading',
        1 => 'title',
        2 => 'field_contact_location',
        3 => 'field_contact_social_links',
        4 => 'field_department_leaders',
      ),
    ),
    'fields' => array(
      'field_contact_heading' => 'ds_content',
      'title' => 'ds_content',
      'field_contact_location' => 'ds_content',
      'field_contact_social_links' => 'ds_content',
      'field_department_leaders' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|contact_list|default'] = $ds_layout;

  return $export;
}
