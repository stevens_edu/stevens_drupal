<?php
/**
 * @file
 * fastspot_video_list.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function fastspot_video_list_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function fastspot_video_list_node_info() {
  $items = array(
    'video_list_sidebar' => array(
      'name' => t('Video List'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
