<?php
/**
 * @file
 * fastspot_events_by_category.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function fastspot_events_by_category_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-events_by_category-field_event_categories'.
  $field_instances['node-events_by_category-field_event_categories'] = array(
    'bundle' => 'events_by_category',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_event_categories',
    'label' => 'Category',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 42,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Category');

  return $field_instances;
}
