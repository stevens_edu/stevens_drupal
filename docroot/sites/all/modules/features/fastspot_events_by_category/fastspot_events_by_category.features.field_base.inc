<?php
/**
 * @file
 * fastspot_events_by_category.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function fastspot_events_by_category_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_event_categories'.
  $field_bases['field_event_categories'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_event_categories',
    'indexes' => array(
      'tid' => array(
        0 => 'tid',
      ),
    ),
    'locked' => 0,
    'module' => 'taxonomy',
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => 'event_categories',
          'parent' => 0,
        ),
      ),
    ),
    'translatable' => 0,
    'type' => 'taxonomy_term_reference',
  );

  return $field_bases;
}
