<?php
/**
 * @file
 * fastspot_events_by_category.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function fastspot_events_by_category_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function fastspot_events_by_category_node_info() {
  $items = array(
    'events_by_category' => array(
      'name' => t('Events by Category'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
