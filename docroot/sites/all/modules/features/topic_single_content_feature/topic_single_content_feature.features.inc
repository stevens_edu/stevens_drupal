<?php
/**
 * @file
 * topic_single_content_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function topic_single_content_feature_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function topic_single_content_feature_image_default_styles() {
  $styles = array();

  // Exported image style: topic_single_content_240x160.
  $styles['topic_single_content_240x160'] = array(
    'label' => 'Topic Single Content 240x160',
    'effects' => array(
      64 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 240,
          'height' => 160,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'topic_single_content_240x160',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: topic_single_content_280x187.
  $styles['topic_single_content_280x187'] = array(
    'label' => 'Topic Single Content 280x187',
    'effects' => array(
      63 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 280,
          'height' => 187,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'topic_single_content_280x187',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: topic_single_content_294x196.
  $styles['topic_single_content_294x196'] = array(
    'label' => 'Topic Single Content 294x196',
    'effects' => array(
      66 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 294,
          'height' => 196,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'topic_single_content_294x196',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: topic_single_content_350x234.
  $styles['topic_single_content_350x234'] = array(
    'label' => 'Topic Single Content 350x234',
    'effects' => array(
      62 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 350,
          'height' => 234,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'topic_single_content_350x234',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: topic_single_content_470x313.
  $styles['topic_single_content_470x313'] = array(
    'label' => 'Topic Single Content 470x313',
    'effects' => array(
      65 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 470,
          'height' => 313,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 0,
          'style_name' => 'topic_single_content_470x313',
        ),
        'weight' => 0,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function topic_single_content_feature_node_info() {
  $items = array(
    'topic_single_content' => array(
      'name' => t('Topic Single - Content'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
