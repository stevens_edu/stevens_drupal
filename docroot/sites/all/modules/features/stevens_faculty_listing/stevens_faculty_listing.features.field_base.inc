<?php

/**
 * @file
 * stevens_faculty_listing.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function stevens_faculty_listing_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_organizational_unit'.
  $field_bases['field_organizational_unit'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_organizational_unit',
    'indexes' => array(
      'tid' => array(
        0 => 'tid',
      ),
    ),
    'locked' => 0,
    'module' => 'taxonomy',
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => 'organizational_units',
          'parent' => 0,
        ),
      ),
    ),
    'translatable' => 0,
    'type' => 'taxonomy_term_reference',
  );

  return $field_bases;
}
