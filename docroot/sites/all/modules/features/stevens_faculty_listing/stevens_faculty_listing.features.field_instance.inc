<?php

/**
 * @file
 * stevens_faculty_listing.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function stevens_faculty_listing_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'node-stevens_faculty_listing-field_organizational_unit'.
  $field_instances['node-stevens_faculty_listing-field_organizational_unit'] = array(
    'bundle' => 'stevens_faculty_listing',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_organizational_unit',
    'label' => 'Organizational Unit',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Organizational Unit');

  return $field_instances;
}
