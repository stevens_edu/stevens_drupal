<?php

/**
 * @file
 * stevens_faculty_listing.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function stevens_faculty_listing_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'stevens_faculty_listing';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Faculty Listing';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'field_personnel_tenure_type',
      'rendered' => 1,
      'rendered_strip' => 0,
    ),
  );
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'field_personnel_phone' => 'field_personnel_phone',
    'field_personnel_email' => 'field_personnel_email',
  );
  $handler->display->display_options['row_options']['separator'] = '<br/>';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  /* Relationship: Content: Taxonomy terms on node */
  $handler->display->display_options['relationships']['term_node_tid']['id'] = 'term_node_tid';
  $handler->display->display_options['relationships']['term_node_tid']['table'] = 'node';
  $handler->display->display_options['relationships']['term_node_tid']['field'] = 'term_node_tid';
  $handler->display->display_options['relationships']['term_node_tid']['vocabularies'] = array(
    'tenure_track' => 'tenure_track',
    'audiences' => 0,
    'news_categories' => 0,
    'tags' => 0,
    'programs' => 0,
    'schools' => 0,
    'departments' => 0,
    'sectors_topics' => 0,
    'event_categories' => 0,
    'organizational_units' => 0,
  );
  /* Field: Content: Username */
  $handler->display->display_options['fields']['field_personnel_username']['id'] = 'field_personnel_username';
  $handler->display->display_options['fields']['field_personnel_username']['table'] = 'field_data_field_personnel_username';
  $handler->display->display_options['fields']['field_personnel_username']['field'] = 'field_personnel_username';
  $handler->display->display_options['fields']['field_personnel_username']['label'] = '';
  $handler->display->display_options['fields']['field_personnel_username']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_personnel_username']['element_label_colon'] = FALSE;
  /* Field: Content: Has Faculty Profile */
  $handler->display->display_options['fields']['field_has_faculty_profile']['id'] = 'field_has_faculty_profile';
  $handler->display->display_options['fields']['field_has_faculty_profile']['table'] = 'field_data_field_has_faculty_profile';
  $handler->display->display_options['fields']['field_has_faculty_profile']['field'] = 'field_has_faculty_profile';
  $handler->display->display_options['fields']['field_has_faculty_profile']['label'] = '';
  $handler->display->display_options['fields']['field_has_faculty_profile']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_has_faculty_profile']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_has_faculty_profile']['alter']['text'] = 'https://faculty.stevens.edu/[field_personnel_username]';
  $handler->display->display_options['fields']['field_has_faculty_profile']['element_label_colon'] = FALSE;
  /* Field: Content: Organizational Unit */
  $handler->display->display_options['fields']['field_personnel_ou']['id'] = 'field_personnel_ou';
  $handler->display->display_options['fields']['field_personnel_ou']['table'] = 'field_data_field_personnel_ou';
  $handler->display->display_options['fields']['field_personnel_ou']['field'] = 'field_personnel_ou';
  $handler->display->display_options['fields']['field_personnel_ou']['label'] = '';
  $handler->display->display_options['fields']['field_personnel_ou']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_personnel_ou']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_personnel_ou']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_personnel_ou']['delta_offset'] = '0';
  /* Field: Content: Tenure Type */
  $handler->display->display_options['fields']['field_personnel_tenure_type']['id'] = 'field_personnel_tenure_type';
  $handler->display->display_options['fields']['field_personnel_tenure_type']['table'] = 'field_data_field_personnel_tenure_type';
  $handler->display->display_options['fields']['field_personnel_tenure_type']['field'] = 'field_personnel_tenure_type';
  $handler->display->display_options['fields']['field_personnel_tenure_type']['label'] = '';
  $handler->display->display_options['fields']['field_personnel_tenure_type']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_personnel_tenure_type']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_personnel_tenure_type']['alter']['text'] = '<h2>[field_personnel_tenure_type]</h2>';
  $handler->display->display_options['fields']['field_personnel_tenure_type']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_personnel_tenure_type']['type'] = 'taxonomy_term_reference_plain';
  /* Field: Content: Link to Bio */
  $handler->display->display_options['fields']['field_personnel_link_to_bio']['id'] = 'field_personnel_link_to_bio';
  $handler->display->display_options['fields']['field_personnel_link_to_bio']['table'] = 'field_data_field_personnel_link_to_bio';
  $handler->display->display_options['fields']['field_personnel_link_to_bio']['field'] = 'field_personnel_link_to_bio';
  $handler->display->display_options['fields']['field_personnel_link_to_bio']['label'] = '';
  $handler->display->display_options['fields']['field_personnel_link_to_bio']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_personnel_link_to_bio']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_personnel_link_to_bio']['click_sort_column'] = 'url';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['text'] = '<h6>[title]</h6>';
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['path'] = '[field_has_faculty_profile]';
  $handler->display->display_options['fields']['title']['alter']['external'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Field: Position */
  $handler->display->display_options['fields']['field_personnel_position']['id'] = 'field_personnel_position';
  $handler->display->display_options['fields']['field_personnel_position']['table'] = 'field_data_field_personnel_position';
  $handler->display->display_options['fields']['field_personnel_position']['field'] = 'field_personnel_position';
  $handler->display->display_options['fields']['field_personnel_position']['label'] = '';
  $handler->display->display_options['fields']['field_personnel_position']['element_label_colon'] = FALSE;
  /* Field: Content: Building & Room */
  $handler->display->display_options['fields']['field_personnel_building_room']['id'] = 'field_personnel_building_room';
  $handler->display->display_options['fields']['field_personnel_building_room']['table'] = 'field_data_field_personnel_building_room';
  $handler->display->display_options['fields']['field_personnel_building_room']['field'] = 'field_personnel_building_room';
  $handler->display->display_options['fields']['field_personnel_building_room']['label'] = '';
  $handler->display->display_options['fields']['field_personnel_building_room']['element_label_colon'] = FALSE;
  /* Field: Field: Phone */
  $handler->display->display_options['fields']['field_personnel_phone']['id'] = 'field_personnel_phone';
  $handler->display->display_options['fields']['field_personnel_phone']['table'] = 'field_data_field_personnel_phone';
  $handler->display->display_options['fields']['field_personnel_phone']['field'] = 'field_personnel_phone';
  $handler->display->display_options['fields']['field_personnel_phone']['label'] = 'p.';
  $handler->display->display_options['fields']['field_personnel_phone']['element_label_colon'] = FALSE;
  /* Field: Content: Email */
  $handler->display->display_options['fields']['field_personnel_email']['id'] = 'field_personnel_email';
  $handler->display->display_options['fields']['field_personnel_email']['table'] = 'field_data_field_personnel_email';
  $handler->display->display_options['fields']['field_personnel_email']['field'] = 'field_personnel_email';
  $handler->display->display_options['fields']['field_personnel_email']['label'] = 'e.';
  $handler->display->display_options['fields']['field_personnel_email']['element_label_colon'] = FALSE;
  /* Field: Content: Photo */
  $handler->display->display_options['fields']['field_photo']['id'] = 'field_photo';
  $handler->display->display_options['fields']['field_photo']['table'] = 'field_data_field_photo';
  $handler->display->display_options['fields']['field_photo']['field'] = 'field_photo';
  $handler->display->display_options['fields']['field_photo']['label'] = '';
  $handler->display->display_options['fields']['field_photo']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_photo']['alter']['path'] = '[field_has_faculty_profile]';
  $handler->display->display_options['fields']['field_photo']['alter']['external'] = TRUE;
  $handler->display->display_options['fields']['field_photo']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['field_photo']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_photo']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_photo']['settings'] = array(
    'image_style' => 'home_page_callout_100x100',
    'image_link' => '',
  );
  /* Sort criterion: Taxonomy term: Weight */
  $handler->display->display_options['sorts']['weight']['id'] = 'weight';
  $handler->display->display_options['sorts']['weight']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['sorts']['weight']['field'] = 'weight';
  $handler->display->display_options['sorts']['weight']['relationship'] = 'term_node_tid';
  /* Sort criterion: Content: Sort Name (field_personnel_sort) */
  $handler->display->display_options['sorts']['field_personnel_sort_value']['id'] = 'field_personnel_sort_value';
  $handler->display->display_options['sorts']['field_personnel_sort_value']['table'] = 'field_data_field_personnel_sort';
  $handler->display->display_options['sorts']['field_personnel_sort_value']['field'] = 'field_personnel_sort_value';
  /* Contextual filter: Content: Organizational Unit (field_personnel_ou) */
  $handler->display->display_options['arguments']['field_personnel_ou_tid']['id'] = 'field_personnel_ou_tid';
  $handler->display->display_options['arguments']['field_personnel_ou_tid']['table'] = 'field_data_field_personnel_ou';
  $handler->display->display_options['arguments']['field_personnel_ou_tid']['field'] = 'field_personnel_ou_tid';
  $handler->display->display_options['arguments']['field_personnel_ou_tid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_personnel_ou_tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_personnel_ou_tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_personnel_ou_tid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_personnel_ou_tid']['break_phrase'] = TRUE;
  /* Filter criterion: Content: Published status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'personnel' => 'personnel',
  );
  /* Filter criterion: Content: Tenure Type (field_personnel_tenure_type) */
  $handler->display->display_options['filters']['field_personnel_tenure_type_tid']['id'] = 'field_personnel_tenure_type_tid';
  $handler->display->display_options['filters']['field_personnel_tenure_type_tid']['table'] = 'field_data_field_personnel_tenure_type';
  $handler->display->display_options['filters']['field_personnel_tenure_type_tid']['field'] = 'field_personnel_tenure_type_tid';
  $handler->display->display_options['filters']['field_personnel_tenure_type_tid']['operator'] = 'not empty';
  $handler->display->display_options['filters']['field_personnel_tenure_type_tid']['value'] = '';
  $handler->display->display_options['filters']['field_personnel_tenure_type_tid']['vocabulary'] = 'tenure_track';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Username */
  $handler->display->display_options['fields']['field_personnel_username']['id'] = 'field_personnel_username';
  $handler->display->display_options['fields']['field_personnel_username']['table'] = 'field_data_field_personnel_username';
  $handler->display->display_options['fields']['field_personnel_username']['field'] = 'field_personnel_username';
  $handler->display->display_options['fields']['field_personnel_username']['label'] = '';
  $handler->display->display_options['fields']['field_personnel_username']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_personnel_username']['element_label_colon'] = FALSE;
  /* Field: Content: Has Faculty Profile */
  $handler->display->display_options['fields']['field_has_faculty_profile']['id'] = 'field_has_faculty_profile';
  $handler->display->display_options['fields']['field_has_faculty_profile']['table'] = 'field_data_field_has_faculty_profile';
  $handler->display->display_options['fields']['field_has_faculty_profile']['field'] = 'field_has_faculty_profile';
  $handler->display->display_options['fields']['field_has_faculty_profile']['label'] = '';
  $handler->display->display_options['fields']['field_has_faculty_profile']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_has_faculty_profile']['element_label_colon'] = FALSE;
  /* Field: Content: Organizational Unit */
  $handler->display->display_options['fields']['field_personnel_ou']['id'] = 'field_personnel_ou';
  $handler->display->display_options['fields']['field_personnel_ou']['table'] = 'field_data_field_personnel_ou';
  $handler->display->display_options['fields']['field_personnel_ou']['field'] = 'field_personnel_ou';
  $handler->display->display_options['fields']['field_personnel_ou']['label'] = '';
  $handler->display->display_options['fields']['field_personnel_ou']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_personnel_ou']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_personnel_ou']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_personnel_ou']['delta_offset'] = '0';
  /* Field: Content: Tenure Type */
  $handler->display->display_options['fields']['field_personnel_tenure_type']['id'] = 'field_personnel_tenure_type';
  $handler->display->display_options['fields']['field_personnel_tenure_type']['table'] = 'field_data_field_personnel_tenure_type';
  $handler->display->display_options['fields']['field_personnel_tenure_type']['field'] = 'field_personnel_tenure_type';
  $handler->display->display_options['fields']['field_personnel_tenure_type']['label'] = '';
  $handler->display->display_options['fields']['field_personnel_tenure_type']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_personnel_tenure_type']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_personnel_tenure_type']['alter']['text'] = '<h2>[field_personnel_tenure_type]</h2>';
  $handler->display->display_options['fields']['field_personnel_tenure_type']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_personnel_tenure_type']['type'] = 'taxonomy_term_reference_plain';
  /* Field: Content: Link to Bio */
  $handler->display->display_options['fields']['field_personnel_link_to_bio']['id'] = 'field_personnel_link_to_bio';
  $handler->display->display_options['fields']['field_personnel_link_to_bio']['table'] = 'field_data_field_personnel_link_to_bio';
  $handler->display->display_options['fields']['field_personnel_link_to_bio']['field'] = 'field_personnel_link_to_bio';
  $handler->display->display_options['fields']['field_personnel_link_to_bio']['label'] = '';
  $handler->display->display_options['fields']['field_personnel_link_to_bio']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_personnel_link_to_bio']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_personnel_link_to_bio']['click_sort_column'] = 'url';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['text'] = '<h6>[title]</h6>';
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['path'] = 'https://faculty.stevens.edu/[field_personnel_username]';
  $handler->display->display_options['fields']['title']['alter']['target'] = '_blank';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Field: Position */
  $handler->display->display_options['fields']['field_personnel_position']['id'] = 'field_personnel_position';
  $handler->display->display_options['fields']['field_personnel_position']['table'] = 'field_data_field_personnel_position';
  $handler->display->display_options['fields']['field_personnel_position']['field'] = 'field_personnel_position';
  $handler->display->display_options['fields']['field_personnel_position']['label'] = '';
  $handler->display->display_options['fields']['field_personnel_position']['element_label_colon'] = FALSE;
  /* Field: Content: Building & Room */
  $handler->display->display_options['fields']['field_personnel_building_room']['id'] = 'field_personnel_building_room';
  $handler->display->display_options['fields']['field_personnel_building_room']['table'] = 'field_data_field_personnel_building_room';
  $handler->display->display_options['fields']['field_personnel_building_room']['field'] = 'field_personnel_building_room';
  $handler->display->display_options['fields']['field_personnel_building_room']['label'] = '';
  $handler->display->display_options['fields']['field_personnel_building_room']['element_label_colon'] = FALSE;
  /* Field: Field: Phone */
  $handler->display->display_options['fields']['field_personnel_phone']['id'] = 'field_personnel_phone';
  $handler->display->display_options['fields']['field_personnel_phone']['table'] = 'field_data_field_personnel_phone';
  $handler->display->display_options['fields']['field_personnel_phone']['field'] = 'field_personnel_phone';
  $handler->display->display_options['fields']['field_personnel_phone']['label'] = 'p';
  /* Field: Content: Email */
  $handler->display->display_options['fields']['field_personnel_email']['id'] = 'field_personnel_email';
  $handler->display->display_options['fields']['field_personnel_email']['table'] = 'field_data_field_personnel_email';
  $handler->display->display_options['fields']['field_personnel_email']['field'] = 'field_personnel_email';
  $handler->display->display_options['fields']['field_personnel_email']['label'] = 'e';
  /* Field: Content: Photo */
  $handler->display->display_options['fields']['field_photo']['id'] = 'field_photo';
  $handler->display->display_options['fields']['field_photo']['table'] = 'field_data_field_photo';
  $handler->display->display_options['fields']['field_photo']['field'] = 'field_photo';
  $handler->display->display_options['fields']['field_photo']['label'] = '';
  $handler->display->display_options['fields']['field_photo']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_photo']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_photo']['settings'] = array(
    'image_style' => 'home_page_callout_100x100',
    'image_link' => '',
  );
  $handler->display->display_options['path'] = 'fl_test/%';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_1');
  $export['stevens_faculty_listing'] = $view;

  return $export;
}
