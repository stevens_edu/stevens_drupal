<?php
/**
 * @file
 * stevens_news_rss.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function stevens_news_rss_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'news_rss_feed';
  $view->description = 'An RSS feed of published News Items with contextual filters';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'News RSS Feed';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'News RSS Feed';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'node';
  $handler->display->display_options['row_options']['view_mode'] = 'full';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: News Category (field_news_category) */
  $handler->display->display_options['arguments']['field_news_category_tid']['id'] = 'field_news_category_tid';
  $handler->display->display_options['arguments']['field_news_category_tid']['table'] = 'field_data_field_news_category';
  $handler->display->display_options['arguments']['field_news_category_tid']['field'] = 'field_news_category_tid';
  $handler->display->display_options['arguments']['field_news_category_tid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_news_category_tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_news_category_tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_news_category_tid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_news_category_tid']['break_phrase'] = TRUE;
  /* Contextual filter: Content: Related Programs (field_news_programs) */
  $handler->display->display_options['arguments']['field_news_programs_tid']['id'] = 'field_news_programs_tid';
  $handler->display->display_options['arguments']['field_news_programs_tid']['table'] = 'field_data_field_news_programs';
  $handler->display->display_options['arguments']['field_news_programs_tid']['field'] = 'field_news_programs_tid';
  $handler->display->display_options['arguments']['field_news_programs_tid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_news_programs_tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_news_programs_tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_news_programs_tid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_news_programs_tid']['break_phrase'] = TRUE;
  /* Contextual filter: Content: Related Departments (field_news_departments) */
  $handler->display->display_options['arguments']['field_news_departments_tid']['id'] = 'field_news_departments_tid';
  $handler->display->display_options['arguments']['field_news_departments_tid']['table'] = 'field_data_field_news_departments';
  $handler->display->display_options['arguments']['field_news_departments_tid']['field'] = 'field_news_departments_tid';
  $handler->display->display_options['arguments']['field_news_departments_tid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_news_departments_tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_news_departments_tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_news_departments_tid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_news_departments_tid']['break_phrase'] = TRUE;
  /* Contextual filter: Content: Related Schools (field_news_schools) */
  $handler->display->display_options['arguments']['field_news_schools_tid']['id'] = 'field_news_schools_tid';
  $handler->display->display_options['arguments']['field_news_schools_tid']['table'] = 'field_data_field_news_schools';
  $handler->display->display_options['arguments']['field_news_schools_tid']['field'] = 'field_news_schools_tid';
  $handler->display->display_options['arguments']['field_news_schools_tid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_news_schools_tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_news_schools_tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_news_schools_tid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_news_schools_tid']['break_phrase'] = TRUE;
  /* Contextual filter: Content: Related Research (field_news__research) */
  $handler->display->display_options['arguments']['field_news__research_tid']['id'] = 'field_news__research_tid';
  $handler->display->display_options['arguments']['field_news__research_tid']['table'] = 'field_data_field_news__research';
  $handler->display->display_options['arguments']['field_news__research_tid']['field'] = 'field_news__research_tid';
  $handler->display->display_options['arguments']['field_news__research_tid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_news__research_tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_news__research_tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_news__research_tid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_news__research_tid']['break_phrase'] = TRUE;
  /* Contextual filter: Content: Audiences (field_news_audiences) */
  $handler->display->display_options['arguments']['field_news_audiences_tid']['id'] = 'field_news_audiences_tid';
  $handler->display->display_options['arguments']['field_news_audiences_tid']['table'] = 'field_data_field_news_audiences';
  $handler->display->display_options['arguments']['field_news_audiences_tid']['field'] = 'field_news_audiences_tid';
  $handler->display->display_options['arguments']['field_news_audiences_tid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_news_audiences_tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_news_audiences_tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_news_audiences_tid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_news_audiences_tid']['break_phrase'] = TRUE;
  /* Contextual filter: Content: Other tags (field_news_tags) */
  $handler->display->display_options['arguments']['field_news_tags_tid']['id'] = 'field_news_tags_tid';
  $handler->display->display_options['arguments']['field_news_tags_tid']['table'] = 'field_data_field_news_tags';
  $handler->display->display_options['arguments']['field_news_tags_tid']['field'] = 'field_news_tags_tid';
  $handler->display->display_options['arguments']['field_news_tags_tid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_news_tags_tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_news_tags_tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_news_tags_tid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_news_tags_tid']['break_phrase'] = TRUE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'news_item' => 'news_item',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'news/rss/page';

  /* Display: Feed */
  $handler = $view->new_display('feed', 'Feed', 'feed');
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'rss';
  $handler->display->display_options['row_plugin'] = 'node_rss';
  $handler->display->display_options['path'] = 'news/rss';
  $handler->display->display_options['displays'] = array(
    'default' => 'default',
    'page' => 'page',
  );
  $export['news_rss_feed'] = $view;

  return $export;
}
