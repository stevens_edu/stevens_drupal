<?php
/**
 * @file
 * fastspot_topic_single.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function fastspot_topic_single_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function fastspot_topic_single_node_info() {
  $items = array(
    'topic_single' => array(
      'name' => t('Topic Single'),
      'base' => 'node_content',
      'description' => t('Group of callouts around a single topic'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
