<?php
/**
 * @file
 * fastspot_home_page_statistics_.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function fastspot_home_page_statistics__field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_hp_numbers_description'.
  $field_bases['field_hp_numbers_description'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_hp_numbers_description',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_long',
  );

  // Exported field_base: 'field_hp_numbers_image'.
  $field_bases['field_hp_numbers_image'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_hp_numbers_image',
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => 0,
    'module' => 'image',
    'settings' => array(
      'default_image' => 0,
      'uri_scheme' => 'public',
    ),
    'translatable' => 0,
    'type' => 'image',
  );

  return $field_bases;
}
