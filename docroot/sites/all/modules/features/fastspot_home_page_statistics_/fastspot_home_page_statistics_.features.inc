<?php
/**
 * @file
 * fastspot_home_page_statistics_.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function fastspot_home_page_statistics__ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function fastspot_home_page_statistics__node_info() {
  $items = array(
    'homepage_numbers' => array(
      'name' => t('Home Page By the Numbers'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
