<?php
/**
 * @file
 * fastspot_home_page_feature.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function fastspot_home_page_feature_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_home_feature_persona|field_collection_item|field_home_feature_callouts|form';
  $field_group->group_name = 'group_home_feature_persona';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_home_feature_callouts';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Persona',
    'weight' => '5',
    'children' => array(
      0 => 'field_home_feature_persona_image',
      1 => 'field_home_feature_persona_name',
      2 => 'field_home_feature_persona_desc',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-home-feature-persona field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_home_feature_persona|field_collection_item|field_home_feature_callouts|form'] = $field_group;

  return $export;
}
