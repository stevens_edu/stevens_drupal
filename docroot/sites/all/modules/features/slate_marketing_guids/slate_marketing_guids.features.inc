<?php
/**
 * @file
 * slate_marketing_guids.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function slate_marketing_guids_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function slate_marketing_guids_node_info() {
  $items = array(
    'slate_marketing_degree' => array(
      'name' => t('Slate Marketing Degree'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'slate_marketing_program' => array(
      'name' => t('Slate Marketing Program'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
