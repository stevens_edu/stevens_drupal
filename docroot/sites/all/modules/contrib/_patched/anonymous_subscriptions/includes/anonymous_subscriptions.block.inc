<?php

/**
 * Implements hook_block_info().
 */
function anonymous_subscriptions_block_info() {
  $blocks['anonymous_subscriptions_block'] = array(
    'info' => t('Anonymous subscription email form'),
    'cache' => DRUPAL_CACHE_PER_ROLE,
    'visiblity' => BLOCK_VISIBILITY_NOTLISTED,
    'pages' => "subscription/subscribe\nsubscription/subscribe/*",
  );
  $blocks['anonymous_subscriptions_link'] = array(
    'info' => t('Link to anonymous subscription email form'),
    'cache' => DRUPAL_CACHE_PER_ROLE,
    'visiblity' => BLOCK_VISIBILITY_NOTLISTED,
    'pages' => "subscription/subscribe\nsubscription/subscribe/*",
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function anonymous_subscriptions_block_view($delta = '') {
  global $base_url;

  // Node type checking to see if block content should be displayed.
  if (variable_get('anonymous_subscriptions_per_node', 0) == TRUE) {
    $nid = NULL;
    $node = FALSE;
    $query_string_array = explode('/',$_GET['q']);
    if ($query_string_array[0] == 'node' &&
      is_numeric($query_string_array[1])) {
      $nid = $query_string_array[1];
      $node_types = variable_get('anonymous_subscriptions_node_types', array());
      if (($node = node_load($nid)) == FALSE || !in_array($node->type, $node_types)) {
        return;
      }
    }
    else {
      return;
    }
  }


  switch ($delta) {
    case 'anonymous_subscriptions_block':
      $block['subject'] = t('Subscribe');
      if (user_access('access content')) {
        // Retrieve and process data here.
        $the_form = drupal_get_form('anonymous_subscriptions_form');
        $updates_from = ($node) ? $node->title : variable_get('site_name');
        $content['info'] = array(
          '#type' => 'markup',
          '#markup' => 'To subscribe to updates to ' . $updates_from . ', please fill out the form below.',
          '#weight' => '-150'
        );
        $content['form'] = $the_form;
        $block['content'] = drupal_render($content);
      }

      return $block;
    case 'anonymous_subscriptions_link':
      $block['subject'] = t('Subscribe');
      if (user_access('access content')) {
        // Determine the subscribe link.
        $nid = '';
        $query_string_array = explode('/',$_GET['q']);
        if ($query_string_array[0] == 'node' &&
           is_numeric($query_string_array[1]) && 
           variable_get('anonymous_subscriptions_per_node', 0) == TRUE) {
          $nid = $query_string_array[1];
        }

        $block['content']['sub_link'] = array(
          '#theme' => 'link',
          '#text' => 'Subscribe to Updates',
          '#path' => $base_url . '/subscription/subscribe/' . $nid,
          '#options' => array(
            'attributes' => array('class' => array('as-subscribe-link')),
            'html' => FALSE,
          ),
        );
      }

      return $block;
  }
}

/**
 * Implements hook_form().
 *
 * Block used to display form to register for subscription
 */
function anonymous_subscriptions_form() {

  // If this is a node page, get the NID
  // If not, leave $nid as NULL
  $nid = NULL;
  $query_string_array = explode('/',$_GET['q']);
  if($query_string_array[0] == 'node' &&
     is_numeric($query_string_array[1])) {
    $nid = $query_string_array[1];
  }

  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#default_value' => '',
    '#required' => TRUE,
    '#size' => 15,
  );

  $form['nid'] = array(
    '#type' => 'hidden',
    '#title' => t('Node ID'),
    '#default_value' => $nid,
    '#required' => TRUE,
    '#size' => 15,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Subscribe'),
  );
  $form['#validate'][] = 'anonymous_subscriptions_form_validate';
  $form['#submit'][] = 'anonymous_subscriptions_form_submit';
  return $form;
}

/**
 * Implements hook_form_validate().
 *
 * Validates the subscription form.
 */
function anonymous_subscriptions_form_validate($form, &$form_state) {
  $window = variable_get('anonymous_subscriptions_limit_window', 300);
  $limit = variable_get('anonymous_subscriptions_ip_limit', 20);
  if(flood_is_allowed('failed_subscribe_attempt_ip', $limit, $window)) {
    flood_register_event('failed_subscribe_attempt_ip', $window);
    $email = $form_state['values']['email'];
    $subscription_per_node = variable_get('anonymous_subscriptions_per_node', FALSE);
    $nid = $form_state['values']['nid'];

    if ($email && !valid_email_address($email)) {
      form_set_error('email', 'You must enter a valid email address.');
    } else {
      if ($subscription_per_node) {
        $result = db_select(ANONYMOUS_SUBSCRIPTIONS_TABLE, 's')
          ->fields('s')
          ->condition('email', trim($email), '=')
          ->condition('nid', trim($nid), '=')
          ->execute()
          ->fetchAssoc();
      }
      else {
        $result = db_select(ANONYMOUS_SUBSCRIPTIONS_TABLE, 's')
          ->fields('s')
          ->condition('email', trim($email), '=')
          ->execute()
          ->fetchAssoc(); 
      }
      if ($result) {
        form_set_error('email', 'You are already subscribed to this content.');
      }
    }
  } else {
    form_set_error('email', 'You have made too many attempts to subscribe.');
  }
}

/**
 * Implements hook_form_submit().
 *
 * Handles the registration form submission generating a code and
 * storing that in the database table as well as the default verification
 * status.
 */
function anonymous_subscriptions_form_submit($form, &$form_state) {
  global $base_url;
  $original_base_url = $base_url;
  $site_url = variable_get('anonymous_subscriptions_site_url', '[site:url]');
  $base_url = token_replace($site_url);
  $verification_required = variable_get('anonymous_subscriptions_verify', TRUE);
  $verify_int = (int)(!$verification_required);
  $subscription_per_node = variable_get('anonymous_subscriptions_per_node', FALSE);
  $email = $form['email']['#value'];
  $email = trim($email);
  $code = substr(md5(uniqid(mt_rand(), true)), 0, 8);;

  // If subscription per node is enabled, then include the NID in the INSERT.
  // If not, leave the NID as NULL
  $nid = NULL;
  $node = NULL;
  if ($subscription_per_node) {
    $nid = $form['nid']['#value'];
    $nid = trim($nid);
    $node = node_load($nid);
  }

  db_insert(ANONYMOUS_SUBSCRIPTIONS_TABLE)
    ->fields(array(
      'email' => $email,
      'verified' => $verify_int,
      'code' => $code,
      'nid' => $nid,
    ))
    ->execute();
  $as = new stdClass();
  $as->email = $email;
  $as->code = $code;

  if ($verification_required) {
    drupal_set_message('Thanks for subscribing, in order to receive updates, you will need to verify your email address by clicking on a link in an email we just sent you.');

    $original_subject = variable_get('anonymous_subscriptions_verify_subject_text', '[site:name] -  Please Verify Your Email Address');
    $original_text = variable_get('anonymous_subscriptions_verify_body_text', 'You are receiving this email because your email address was used to subscribe to updates for [site:name]. To verify your email address and begin to receive updates, please visit the following link (or copy it into the address bar of your browser):\n\n[anonymous_subscription:verify_url]');

    $subject = token_replace($original_subject, array('node' => $node, 'anonymous_subscription' => $as));
    $text = token_replace($original_text, array('node' => $node, 'anonymous_subscription' => $as));

    $body = array($text);
    $params = array(
      'email' => $email,
      'subject' => $subject,
      'body' => $body
    );
    drupal_mail('anonymous_subscriptions', 'anonymous_subscriptions_key', $email, language_default(), $params);
  } else {
    drupal_set_message('You are now subscribed to updates.');
    $original_subject = variable_get('anonymous_subscriptions_confirm_subject_text', '[site:name] - Subscription Confirmation');
    $original_text = variable_get('anonymous_subscriptions_confirm_body_text', 'Thank you! Your email address has been verified, and you will now begin to receive email updates from [site:name].\n\nTo unsubscribe, please visit the following URL:\n[anonymous_subscription:unsubscribe_url]');

    $subject = token_replace($original_subject, array('node' => $node, 'anonymous_subscription' => $as));
    $text = token_replace($original_text, array('node' => $node, 'anonymous_subscription' => $as));

    $body = array($text);
    $params = array(
      'email' => $email,
      'subject' => $subject,
      'body' => $body
    );
    drupal_mail('anonymous_subscriptions', 'anonymous_subscriptions_key', $email, language_default(), $params);
  }

  // Setting base URL back to the original as to ensure we get the correct url (after token replacing etc)
  // for our $site_url variable, e.g. if node:url is used we want the correct base_url
  $base_url = $original_base_url;
}
