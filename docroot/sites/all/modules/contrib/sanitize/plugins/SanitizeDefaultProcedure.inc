<?php
/**
 * @file
 * Implementation of the default sanitization procedure.
 */

/**
 * Class defining a default sanitization procedure.
 */
class SanitizeDefaultProcedure extends SanitizeProcedure {
  /**
   * Conduct sanitization of the current site.
   */
  public function sanitize() {
    drush_log(dt('Truncating session data.'));
    $this->trunc('sessions');

    // Database logging.
    drush_log(dt('Truncating database logs.'));
    $this->trunc('watchdog');

    drush_log(dt('Truncating messages.'));
    $this->trunc('message');

    // Webform submissions.
    drush_log(dt('Truncating webform submissions.'));
    $this->trunc('webform_submissions');
    $this->trunc('webform_submitted_data');

    // Poll results.
    drush_log(dt('Truncating poll results.'));
    $this->trunc('poll_vote');

    // Keys.
    $this->resetKeys();
  }

  /**
   * Prepare the export SQL before being written to a file.
   *
   * @param resource $handle
   *   A file handle for the temporary SQL file to be written.
   */
  public function prepareExport($handle) {
    // Sanitize the uid:1 password with our own hash. Defaults to "admin".
    require_once DRUSH_DRUPAL_CORE . '/includes/password.inc';
    $default_pass = drush_get_option('admin-pass', variable_get('sanitize_default_pass', 'admin'));
    $default_hash = user_hash_password($default_pass);
    $admin_user = user_load(1);
    $admin_hash = $admin_user->pass;
    drush_print(dt('Setting a default password: @pass', array('@pass' => $default_pass)));

    stream_filter_register('sanitizereplacetext.*', 'SanitizeReplaceTextFilter');
    stream_filter_append($handle, 'sanitizereplacetext.' . $admin_hash . '|' . $default_hash);
  }

  /**
   * Perform additional cleanup following sanitization.
   */
  public function postSanitize() { 
    drush_log(dt('Regenerating keys post-export...'));
    $this->resetKeys();
  }

  /**
   * Helper function to reset keys.
   */
  protected function resetKeys() {
    // drupal_private_key
    drush_log(dt('Regenerating Drupal private key.'));
    variable_set('drupal_private_key', 0);
    drupal_get_private_key();

    // cron_key
    drush_log(dt('Regenerating cron key.'));
    $cron_key = drupal_random_key();
    variable_set('cron_key', $cron_key);    

    // mimemail_key
    drush_log(dt('Regenerating Mimemail key.'));
    variable_set('mimemail_key', md5(rand()));
  }
}
