<?php
/**
 * @file
 * Stream filter for text replacement on large files.
 *
 * This is invoked by passing the search and replace arguments into the name of
 * the filter in stream_filter_append(), like so:
 *
 * stream_filter_append($stream, 'replacetext.' . $search . '|' . $replace);
 *
 * @see http://stackoverflow.com/questions/3574999/replacing-file-content-in-php
 * @see http://us3.php.net/manual/en/stream.filters.php
 * @see stream_filter_append()
 */

class SanitizeReplaceTextFilter extends php_user_filter {
  protected $search = '';
  protected $replace = '';

  /**
   * Implements php_user_filter::filter().
   */
  public function filter($in, $out, &$consumed, $closing) {
    while ($bucket = stream_bucket_make_writeable($in)) {
      $count = 0;
      $bucket->data = str_replace($this->search, $this->replace, $bucket->data, $count);
      $consumed += $bucket->datalen;
      stream_bucket_append($out, $bucket);
    }
    return PSFS_PASS_ON;
  }

  /**
   * Implements php_user_filter::onCreate().
   */
  public function onCreate() {
    if (strpos($this->filtername, '.') === false) {
      return false;
    }
    list ($name, $arguments) = explode('.', $this->filtername, 2);
    $replace = '';
    $search = $arguments;
    if (strpos($arguments, '|') !== false) {
      list ($search, $replace) = explode('|', $arguments, 2);
    }
    if (strpos($search, ',') !== false) {
      $search = explode(',', $search);
    }
    if (strpos($replace, ',') !== false) {
      $search = explode(',', $replace);
    }
    $this->search = $search;
    $this->replace = $replace;
  }
}
