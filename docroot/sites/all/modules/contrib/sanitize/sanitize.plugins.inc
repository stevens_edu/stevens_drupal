<?php
/**
 * @file
 * SanitizeProcedure CTools plugin declarations.
 *
 * @see sanitize_sanitize_plugins();
 */

/**
 * Helper for sanitize_sanitize_plugins().
 */
function _sanitize_sanitize_plugins() {
  $plugin_path = drupal_get_path('module', 'sanitize') . '/plugins/';

  $info = array();
  $info['SanitizeProcedure'] = array(
    'hidden' => TRUE,
    'handler' => array(
      'class' => 'SanitizeProcedure',
      'file' => 'SanitizeProcedure.inc',
      'path' => $plugin_path,
    ),
  );

  $info['SanitizeDefaultProcedure'] = array(
    'description' => 'Default procedure for sanitizing a Drupal site.',
    'handler' => array(
      'class' => 'SanitizeDefaultProcedure',
      'file' => 'SanitizeDefaultProcedure.inc',
      'path' => $plugin_path,
    ),
  );

  return $info;
}
