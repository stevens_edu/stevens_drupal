<?php
  $type = $content['field_program_search_type']['#items'][0]['value'];
  $type = strtolower($type);
  $type_link = "";
  if ($type !== 'both') {
    $type_link = '<a href="';
    if ($type == 'Graduate') {
      $type_link .= $content['field_program_graduate_link']['#items'][0]['display_url'];
    } else {
      $type_link .= $content['field_program_undergraduate_link']['#items'][0]['display_url'];
    }
    $type_link .= '" class="program_finder_link">View All ' . ucfirst($type) . ' Programs</a>';
  }
?>
<section class="component program_finder js-program_finder<?php if (drupal_is_front_page()) { ?> program_finder_homepage<?php } ?>">
	<div class="fs-row">
		<header class="fs-cell fs-md-half fs-lg-half component_header">
			<h2 class="component_heading"><?php echo $title; ?></h2>
			<div class="component_text"><?php echo render($content['field_program_finder_intro']); ?></div>
		</header>
    <div class="fs-cell fs-md-half fs-lg-half">
			<?php if ($type == 'both') {?>
      <menu class="program_finder_form_buttons">
      	<button class="program_finder_form_button search_undergraduate js-program_finder_button js-swap" data-swap-options='{"collapse":false}' data-swap-group="program_finder" data-swap-target=".js-swap_program_finder_undergraduate" data-program-type="undergraduate">Search Undergraduate</button>
      	<button class="program_finder_form_button search_graduate js-program_finder_button js-swap" data-swap-options='{"collapse":false}' data-swap-group="program_finder" data-swap-target=".js-swap_program_finder_graduate" data-program-type="graduate">Search Graduate</button>
      </menu>
      <form action="/" class="program_finder_form search_form js-program_finder_form">
      	<fieldset>
      		<input type="text" placeholder="e.g. Business" class="search_input program_finder_search_input js-program_finder_search_input" aria-labelledby="program_finder_search_submit">
      		<input type="hidden" name="type" id="program_finder_result_type" value="undergraduate">
      		<button type="submit" class="search_submit program_finder_search_submit" id="program_finder_search_submit">Search</button>
      	</fieldset>
      </form>
      <?php } else { ?>
      <form action="/" class="program_finder_form search_form js-program_finder_form">
      	<fieldset>
      		<input type="text" placeholder="e.g. Business" class="search_input program_finder_search_input js-program_finder_search_input" aria-labelledby="program_finder_search_submit">
      		<input type="hidden" name="type" id="program_finder_result_type" value="<?php echo $type; ?>">
      		<button type="submit" class="search_submit program_finder_search_submit" id="program_finder_search_submit">Search</button>
      	</fieldset>
      </form>
      <?php echo $type_link; ?>
      <?php } ?>
		</div>
	</div>
	<div class="program_finder_results">
		<div class="fs-row">
			<header class="fs-cell program_finder_results_header">
				<?php if ($type == 'both') { ?>
        <a href="<?php echo $content['field_program_undergraduate_link']['#items'][0]['display_url']; ?>" class="program_finder_homepage_link js-swap_program_finder_undergraduate">View All Undergraduate Programs</a><a href="<?php echo $content['field_program_graduate_link']['#items'][0]['display_url']; ?>" class="program_finder_homepage_link js-swap_program_finder_graduate">View All Graduate Programs</a>
        <?php } ?>

				<div class="program_finder_results_message typography">
					<p><span class="inner js-program_finder_results_message">Showing sample results for “<span class="color_red">Business</span>,” start a new search above.</span></p>
				</div>
			</header>
		</div>
		<div class="program_finder_carousel">
			<div class="carousel_controls program_finder_controls">
				<button class="carousel_control program_finder_control program_finder_control_previous">Previous</button>
				<button class="carousel_control program_finder_control program_finder_control_next">Next</button>
			</div>
			<div class="fs-row">
				<div class="fs-cell">

					<div class="program_finder_carousel js-carousel js-program_finder_results" data-carousel-options='{"contained":false,"matchHeight":true,"show":{"500px":2,"980px":4},"controls":{"container":".program_finder_controls","previous":".program_finder_control_previous","next":".program_finder_control_next"}}'>
            <?php
            $display_id = 'block_4';
            if($type=='graduate') {
              $display_id = 'block_3';
            }
            $view = views_get_view('program_finder');

            $view->set_display($display_id);
            $view->execute();
            print $view->render();
            ?>
					</div>
				</div>
			</div>
		</div>

  </div>
</section>
