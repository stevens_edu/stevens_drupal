<?php
  $query = new EntityFieldQuery;
  $query->entityCondition("entity_type","node")
        ->entityCondition("bundle","faculty_staff")
        ->propertyCondition("status",NODE_PUBLISHED)
        ->fieldCondition("field_news_programs","tid",array($content["field_faculty_list_program"]["#items"][0]["tid"]),"IN")
        ->fieldOrderBy("field_faculty_sort","value","ASC");

  $result = $query->execute();

  if ($result) {
    $faculty_nids = array_keys($result["node"]);
    $faculty = entity_load("node",$faculty_nids);
?>
<div class="directory_list_in_content">
  <h2 class="heading_2"><?php echo $title; ?></h2>
  <?php
    foreach ($faculty as $item) {
      $email = !empty($item->field_faculty_email["und"][0]["value"]) ? $item->field_faculty_email["und"][0]["value"] : "";
  ?>
  <article class="clearfix directory_container">
    <div class="fs-cell fs-md-6 fs-lg-6">
      <div class="directory_office">
        <h4 class="directory_heading"><?php echo $item->title; ?></h4>
        <p><?php echo $item->field_faculty_position["und"][0]['value']; ?></p>
      </div>
    </div>
    <div class="fs-cell fs-md-3 fs-lg-3 directory_detail">
      <?php
        if (!empty($item->field_faculty_building["und"][0]["value"]) || !empty($item->field_faculty_room["und"][0]["value"])) {
      ?>
      <h4 class="color_red bold_uppercase">Location</h4>
      <?php
          if (!empty($item->field_faculty_building["und"][0]["value"])) {
            echo "<p>".$item->field_faculty_building["und"][0]['value']."</p>";
          }
          if (!empty($item->field_faculty_room["und"][0]["value"])) {
            echo "<p>Room ".$item->field_faculty_room["und"][0]['value']."</p>";
          }
        } else {
          echo "&nbsp;";
        }
      ?>
    </div>
    <div class="fs-cell fs-md-3 fs-lg-3 directory_detail">
      <?php
        if (!empty($item->field_faculty_phone["und"][0]["value"]) || $email) {
      ?>
      <h4 class="color_red bold_uppercase">Contact</h4>
      <?php
          if (!empty($item->field_faculty_phone["und"][0]["value"])) {
            echo "<p>p. ".$item->field_faculty_phone["und"][0]['value']."</p>";
          }

          if ($email) {
            echo '<p>e. <a href="mailto:'.$email.'">'.$email.'</a></p>';
          }
        }
      ?>
    </div>
  </article>
  <?php
    }
  ?>
</div>
<?php
  } else {
    // Literally have to return an empty space or Drupal will do it's own thing rather than not display this block.
    echo " ";
  }
