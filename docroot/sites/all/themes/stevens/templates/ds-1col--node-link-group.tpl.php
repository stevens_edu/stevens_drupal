<div class="margined_md link_list link_list_in_content">
	<header class="link_list_header link_list_in_content_header">
		<h2 class="link_list_heading link_list_in_content_heading"><?php echo $title; ?></h2>
	</header>
	<div class="link_list_body link_list_in_content_body">
    <?php foreach ($content['field_link_group_links']['#items'] as $link) { ?>
    <div class="link_list_item link_list_in_content_item">
			<?php
				if (!empty($link['url'])) {
    			echo stevens_theme_links($link,"link_list_link link_list_in_content_link");
				} else {
					echo '<span class="link_list_text link_list_in_content_text">' . $link['title'] . '<span>';
				}
			?>
    </div>
    <?php } ?>
	</div>
</div>
