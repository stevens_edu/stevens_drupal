<section class="component media_gallery">
	<div class="fs-row">
		<header class="component_header">
			<div class="fs-cell">
				<h2 class="component_heading"><?php echo $title; ?></h2>
			</div>
			<div class="fs-cell fs-lg-10">
				<div class="media_gallery_description">
					<div class="typography">
						<div class="intro"><?php echo render($content['body']); ?></div>
					</div>
				</div>
			</div>
		</header>
	</div>
	<div class="fs-row">
		<div class="fs-cell">
			<div class="media_gallery_body media_gallery_grid">
				<div class="js-carousel carousel_block_pager carousel_pager_grouped_right news_feature_carousel" data-carousel-options='{"contained": false, "controls": false, "maxWidth": "739px"}'>
          <?php
            $item_count = count($content['field_media_gallery_items']['#items']);
						$galleryid = uniqid();
            for ($i = 0; $i < $item_count; $i++) {
              $item = $content['field_media_gallery_items'][$i]['entity']['field_collection_item'];
              $item = array_pop($item);
              $image = $item['field_media_gallery_image']['#items'][0]['uri'];
          ?>
          <div class="media_item media_grid_item">
          	<a class="js-lightbox block media_item_link<?php if(isset($item['field_media_gallery_link'])) { echo ' video_link'; } ?>" data-lightbox-gallery="<?php echo $galleryid; ?>" href="<?php if(isset($item['field_media_gallery_link'])) { echo stevens_url_fix(stevens_autoplay_url($item['field_media_gallery_link']['#items'][0])); } else { echo file_create_url($image); } ?>" title="<?php echo $item['field_media_gallery_desc']['#items'][0]['value']; ?>">
          		<figure class="media_item_figure">
          			<picture class="responsive_image media_item_picture">
          				<!--[if IE 9]><video style="display: none;"><![endif]-->
          				<source media="(min-width: 1220px)" srcset="<?php echo image_style_url('media_gallery_350x234',$image); ?>">
          				<source media="(min-width: 980px)" srcset="<?php echo image_style_url('media_gallery_280x187',$image); ?>">
          				<source media="(min-width: 740px)" srcset="<?php echo image_style_url('media_gallery_330x220',$image); ?>">
          				<source media="(min-width: 500px)" srcset="<?php echo image_style_url('media_gallery_470x314',$image); ?>">
          				<source media="(min-width: 0px)" srcset="<?php echo image_style_url('media_gallery_294x196',$image); ?>">
          				<!--[if IE 9]></video><![endif]-->
          				<img class="media_item_image" src="<?php echo image_style_url('media_gallery_294x196',$image); ?>" alt="<?php echo $item['field_media_gallery_image']['#items'][0]['alt']; ?>" draggable="false">
          			</picture>
          		</figure>
          		<div class="media_item_link_icon">
          			<span class="visually_hidden media_item_link_text">Expand Lightbox</span>
          		</div>
          	</a>
          	<div class="callout_body media_item_description">
          		<p><?php echo $item['field_media_gallery_desc']['#items'][0]['value']; ?></p>
          	</div>
          </div>
          <?php
            }
          ?>
				</div>
			</div>
		</div>
	</div>
</section>
