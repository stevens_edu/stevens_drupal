<section class="component topic_single">
  <div class="fs-row">
    <header class="fs-cell component_header">
      <h2 class="component_heading"><?php echo $title; ?></h2>
      <?php if(isset($content['body'])) { ?>
      <div class="component_text"><?php print render($content['body']); ?></div>
      <?php } ?>
    </header>
  </div>
  <div class="topic_single_callouts">
    <?php
      $item_count = count($content['field_topic_single_callout']['#items']);
      for ($i = 0; $i < $item_count; $i++) {
        $item = $content['field_topic_single_callout'][$i]['entity']['field_collection_item'];
        $item = array_pop($item);
        $image = $item['field_topic_single_image']['#items'][0]['uri'];
    ?>
    <article class="callout topic_single_callout">
      <div class="fs-row">
        <div class="fs-cell">
          <figure class="topic_single_figure">
            <picture class="responsive_image topic_single_image">
              <!--[if IE 9]><video style="display: none;"><![endif]-->
              <source media="(min-width: 740px)" srcset="<?php echo image_style_url('topic_callout_400x267',$image); ?>">
              <source media="(min-width: 500px)" srcset="<?php echo image_style_url('topic_callout_470x313',$image); ?>">
              <source media="(min-width: 0px)" srcset="<?php echo image_style_url('topic_callout_294x196',$image); ?>">
              <!--[if IE 9]></video><![endif]-->
              <img src="<?php echo image_style_url('topic_callout_294x196',$image); ?>" alt="<?php echo $item['field_topic_single_image']['#items'][0]['alt']; ?>" draggable="false">
            </picture>
          </figure>
          <div class="callout_contain topic_single_contain">
            <h3 class="topic_single_heading"><?php echo stevens_trim_length($item['field_topic_single_title']['#items'][0]['value'],40); ?></h3>
            <div class="callout_content topic_single_content">
              <?php echo stevens_trim_length($item['field_topic_single_description']['#items'][0]['value'],550); ?>
            </div>
            <?php
              if (isset($item['field_topic_single_link'])) {
              	echo stevens_theme_links($item['field_topic_single_link']['#items'][0],"callout_link topic_single_link");
              }
            ?>
          </div>
        </div>
      </div>
    </article>
    <?php
      }
    ?>
  </div>
</section>