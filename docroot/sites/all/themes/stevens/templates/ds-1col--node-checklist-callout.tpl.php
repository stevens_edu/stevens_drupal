<?php $nid = $node->nid; ?>
<section id="node-<?php echo $nid; ?>" class="margined_lg checklist_container">
  <div class="fs-row">
    <div class="fs-cell">
      <div class="typography clearfix">
        <h3 class="component_header_block_heading"><?php echo $title; ?></h3>
        <a href="#" class="color_red bold_uppercase component_header_block_link print_icon">Print List</a>
      </div>
      <div class="js_checklist fs-cell-12">
        <form class="checklist_form">
          <fieldset>
            <div class="form-item webform-component webform-component-checkboxes webform-component--demo-fieldset--check-boxes">
              <div id="edit-submitted-demo-fieldset-check-boxes" class="form-checkboxes">
                <?php
                $i = 0;
                foreach ($content['field_checklist_items']['#items'] as $item) {
                  ?>
                  <div class="form-item form-type-checkbox form-item-submitted-demo-fieldset-check-boxes-foo check_list">
                    <input type="checkbox" id="edit-submitted-demo-fieldset-check-boxes-<?php echo $nid . $i; ?>"  name="edit-submitted-demo-fieldset-check-boxes-<?php echo $nid . $i; ?>" value="sit" class="form-checkbox">
                    <label class="option" for="edit-submitted-demo-fieldset-check-boxes-<?php echo $nid . $i; ?>"><?php echo $item['value']; ?></label>
                  </div>
                  <?php
                  $i++;
                }
                ?>
              </div>
            </div>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</section>
