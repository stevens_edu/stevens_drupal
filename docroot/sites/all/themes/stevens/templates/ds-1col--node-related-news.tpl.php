<div class="news_list news_sidebar_list news_related_list">
	<header class="news_sidebar_header news_related_header">
		<h2 class="bold_uppercase color_red news_sidebar_heading news_related_heading"><?php echo htmlspecialchars(htmlspecialchars_decode($title)); ?></h2>
	</header>
	<div class="news_sidebar_body news_related_body">
		<?php foreach ($content["field_related_news_articles"]["#items"] as $item) { ?>
		<article class="news_item news_sidebar_item news_related_item">
			<h3 class="news_item_title news_related_item_title">
				<a class="news_item_title_link news_related_item_title_link" href="<?php echo url("node/".$item["entity"]->nid); ?>"><?php echo htmlspecialchars(htmlspecialchars_decode($item["entity"]->title)); ?></a>
			</h3>
		</article>
		<?php } ?>
	</div>
	<?php if (!empty($content["field_related_news_view_all_link"]["#items"][0]["url"])) { ?>
	<footer class="news_sidebar_footer news_related_footer">
		<a class="news_sidebar_footer_button news_related_footer_button" href="<?php echo $content["field_related_news_view_all_link"]["#items"][0]["url"]; ?>"><?php echo htmlspecialchars($content["field_related_news_view_all_link"]["#items"][0]["title"]); ?></a>
	</footer>
	<?php } ?>
</div>