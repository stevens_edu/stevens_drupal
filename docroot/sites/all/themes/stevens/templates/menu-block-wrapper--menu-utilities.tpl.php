<div class="utility_nav nav_hover nav_button_hover">
  <button class="utility_nav_button">Utilities</button>
  <nav class="utility_nav_links">
    <?php echo render($content); ?>
  </nav>
</div>
