<div class="margined_md link_list link_list_sidebar">
	<header class="link_list_header link_list_sidebar_header">
		<h2 class="bold_uppercase color_red link_list_heading link_list_sidebar_heading"><?php echo $title; ?></h2>
	</header>
	<div class="link_list_body link_list_sidebar_body">
    <?php foreach ($content['field_link_list_sidebar_links']['#items'] as $link_item) { ?>
    <div class="link_list_item link_list_sidebar_item">
			<?php echo stevens_theme_links($link_item,"link_list_link link_list_sidebar_link"); ?>
    </div>
    <?php } ?>
	</div>
</div>
