<div class="margined_md social_sidebar">
	<header class="social_sidebar_header">
		<h2 class="bold_uppercase color_red social_sidebar_heading">Connect with Stevens</h2>
	</header>
	<div class="social_sidebar_body">
    <?php echo render($content); ?>
</div>
