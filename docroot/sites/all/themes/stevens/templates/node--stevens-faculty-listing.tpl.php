<div class="typography">
<?php
  $tids = array();
  if (!empty($content["field_organizational_unit"]['#items'])) {
    foreach ($content["field_organizational_unit"]['#items'] as $item) {
      $tids[] = $item['tid'];
    }
  }
  if (empty($tids)) {
    $tid_str = 'all';
  }
  else {
    $tid_str = implode('+', $tids);
  }
  print views_embed_view('stevens_faculty_listing', 'block_1', $tid_str);
?>
</div>
