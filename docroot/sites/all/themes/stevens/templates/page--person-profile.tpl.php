<?php
  include ($directory . "/includes/header.php");
?>
<div class="page_wrapper js-navigation_push">
  <main id="page" class="page" role="main">
    <header class="page_header">
    	<div class="fs-row">
        <?php
          if(user_is_logged_in()) {
        ?>
        <div class="fs-cell">
          <?php print $messages; ?>
          <?php print render($page['help']); ?>
          <?php if ($tabs): ?>
            <div class="tabs"><?php print render($tabs); ?></div>
          <?php endif; ?>
          <?php if ($action_links): ?>
            <ul class="action-links"><?php print render($action_links); ?></ul>
          <?php endif; ?>

        </div>
        <?php
          }
        ?>
    		<div class="fs-cell typography">
                <h1><?php print check_plain($node->field_person_profile_type[LANGUAGE_NONE][0]['value']); ?> Spotlight</h1>
        </div>
        <div class="fs-cell">
          <?php if ($breadcrumb): ?>
            <?php print $breadcrumb; ?>
          <?php endif; ?>
        </div>
        <div class="fs-cell">
    			<hr>
    		</div>
    	</div>
      <?php if ($page['feature']): ?>
      <?php print render($page['feature']); ?>
      <?php endif; ?>
    </header>
  	<!-- Page Content -->
  	<section class="page_content_area">
  		<div class="fs-row">
        <?php if ($page['sidebar_first']): ?>
          <aside class="sidebar sidebar_subnavigation fs-cell-right fs-lg-3" role="region">
            <?php print render($page['sidebar_first']); ?>
          </aside>
        <?php endif; ?>

        <div class="page_content fs-cell fs-lg-9">
          <?php
            if(!user_is_logged_in()) {
              print $messages; 
            }
          ?>
          <div class="typography">
            <?php if ($page['content']): ?>
              <?php print render($page['content']); ?>
            <?php endif; ?>
          </div>

          <?php if ($page['content_components']): ?>
            <!-- In-Content Callouts -->
            <aside class="callouts content_callouts">
                <?php print render($page['content_components']); ?>
            </aside><!-- END: content_callouts -->
          <?php endif; ?>
        </div><!-- END: page_content -->

        <?php if ($page['sidebar_second']): ?>
          <aside class="sidebar sidebar_callouts fs-cell-right fs-lg-3">
          <?php print render($page['sidebar_second']); ?>
          </aside>
        <?php endif; ?>
      </div>
    </section><!-- END: page_content_area -->
    <?php if ($page['content_bottom']): ?>
      <!-- Full Width Components -->
      <section class="components">
        <?php print render($page['content_bottom']); ?>
      </section><!-- END: components -->
    <?php endif; ?>
  </main><!-- END: page -->
  <?php
   include ($directory . "/includes/footer.php");
  ?>
