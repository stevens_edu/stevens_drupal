<?php
  include ($directory . "/includes/header.php");

  // Define categories
  $categories = stevens_events_get_categories();    
  
  // Figure out what months to show, first with start months
  $months = array();
  $query = db_query("SELECT DISTINCT(CONCAT(YEAR(field_event_start_time_value),'-',MONTH(field_event_start_time_value))) AS `date` 
                     FROM field_data_field_event_start_time WHERE field_event_start_time_value >= '".date("Y-m-1")."' 
                     ORDER BY field_event_start_time_value ASC");
  while ($result = $query->fetchAssoc()) {
    $months[] = $result["date"];
  }

  // Now end months
  $query = db_query("SELECT DISTINCT(CONCAT(YEAR(field_event_end_time_value),'-',MONTH(field_event_end_time_value))) AS `date` 
                     FROM field_data_field_event_end_time WHERE field_event_end_time_value >= '".date("Y-m-1")."' 
                     ORDER BY field_event_end_time_value ASC");
  while ($result = $query->fetchAssoc()) {
    if (!in_array($result["date"],$months)) {
      $months[] = $result["date"];
    }
  }

  // Labels
  $category_label = "Filter by Category";
  if ($_GET["category"]) {
    $category_entity = entity_load("taxonomy_term",false,array("tid" => intval($_GET["category"])));
    if (count($category_entity)) {
      $category_entity = current($category_entity);
      $category_label = htmlspecialchars(htmlspecialchars_decode($category_entity->name));
    }
  }
  
  $date_label = $_GET["month"] ? date("M Y",strtotime($_GET["month"])) : "Filter by Date";
?>
<div class="page_wrapper js-navigation_push">
  <main id="page" class="page" role="main">
    <header class="page_header">
      <div class="fs-row">
        <?php
          if (user_is_logged_in()) {
        ?>
        <div class="fs-cell">
          <?php print $messages; ?>
          <?php print render($page['help']); ?>
          <?php if ($tabs): ?>
            <div class="tabs"><?php print render($tabs); ?></div>
          <?php endif; ?>
          <?php if ($action_links): ?>
            <ul class="action-links"><?php print render($action_links); ?></ul>
          <?php endif; ?>

        </div>
        <?php
          }
        ?>
        <div class="fs-cell typography">
          <h1>Events</h1>
        </div>
        <div class="fs-cell">
          <div class="breadcrumb">
            <a href="/" class="breadcrumb_item breadcrumb_link home">Home</a>
            <span class="breadcrumb_item">Events</span>
          </div>
        </div>
        <div class="fs-cell">
          <hr>
        </div>
      </div>
      <div class="calendar_controls_wrapper margined_lg_bottom">    
        <div class="fs-row">
          <div class="fs-cell">
            <div class="background_gray_pale component_controls calendar_controls">
              <form class="calendar_filter_form component_filter_form">
                <fieldset class="calendar_filter_fieldset">
                  <label class="visually_hidden calendar_filter_label" for="filter_category">Filter by Category</label>
                  <select id="filter_category" class="js-dropdown calendar_filter_select" data-dropdown-options='{"label": "<?=$category_label?>", "customClass": "red_dropdown calendar_filter_dropdown filter_category", "links": "true"}'>
                    <?php foreach ($categories as $tid => $category) { ?>
                    <option value="/events/?<?php if ($_GET["month"]) { echo "month=".htmlspecialchars($_GET["month"])."&"; } ?>category=<?php echo $tid; ?>"><?php echo $category; ?></option>
                    <?php } ?>
                  </select>
                </fieldset>

                <fieldset class="calendar_filter_fieldset">
                  <label class="visually_hidden calendar_filter_label" for="filter_date">Filter by Date</label>
                  <select id="filter_date" class="js-dropdown calendar_filter_select" data-dropdown-options='{"label":"<?=$date_label?>", "customClass": "red_dropdown calendar_filter_dropdown filter_date", "links": "true"}'>
                    <?php foreach ($months as $month) { ?>
                    <option value="/events/?<?php if ($_GET["category"]) { echo "category=".urlencode(htmlspecialchars($_GET["category"]))."&"; } ?>month=<?php echo urlencode($month); ?>"><?php echo date("M Y",strtotime($month."-01")); ?></option>
                    <?php } ?>
                  </select>
                </fieldset>

                <?php if ($_GET["month"] || $_GET["category"]) { ?>
                <a href="/events/" class="bold_uppercase controls_filter_clear calendar_filter_clear">Clear Filters</a>
                <?php } ?>
              </form>
    
              <a class="link_arrow calendar_controls_link" href="/directory/office-registrar/academic-calendar">Academic Calendar</a>
            </div>
          </div>
        </div>
      </div>
    </header>

    <div class="components">
      <div class="margined_lg_bottom events_list_wide">
        <?php print render($page['content']); ?>
      </div>
    </div>
    
  </main>
  <?php
   include ($directory . "/includes/footer.php");
  ?>
