<?php
  // Get related program links
  $related_programs = array();

  $tids = array();
  foreach (array_filter((array)$node->field_news_programs["und"]) as $item) {
    $tids[] = $item["tid"];
  }

  if (count($tids)) {
    $query = new EntityFieldQuery;
    $query->entityCondition("entity_type","node")
          ->entityCondition("bundle","program_page")
          ->propertyCondition("status",NODE_PUBLISHED)
          ->fieldCondition("field_page_programs","tid",$tids,"IN")
          ->addTag("random")
          ->range(0,6);
    $result = $query->execute();
  
    if (is_array($result["node"])) {
      $program_node_ids = array_keys($result["node"]);
      $related_programs = entity_load("node",$program_node_ids);
    }
  }

  include ($directory . "/includes/header.php");
?>
<div class="page_wrapper js-navigation_push">
  <main id="page" class="page" role="main">
    <header class="page_header">
      <div class="fs-row">
        <?php
          if(user_is_logged_in()) {
        ?>
        <div class="fs-cell">
          <?php print $messages; ?>
          <?php print render($page['help']); ?>
          <?php if ($tabs): ?>
            <div class="tabs"><?php print render($tabs); ?></div>
          <?php endif; ?>
          <?php if ($action_links): ?>
            <ul class="action-links"><?php print render($action_links); ?></ul>
          <?php endif; ?>

        </div>
        <?php
          }
        ?>
        <div class="fs-cell typography">
          <h1>News</h1>
        </div>
        <div class="fs-cell">
          <?php if ($breadcrumb): ?>
            <?php print $breadcrumb; ?>
          <?php endif; ?>
        </div>
        <div class="fs-cell">
          <hr>
        </div>
      </div>
      <?php if ($page['feature']): ?>
      <?php print render($page['feature']); ?>
      <?php endif; ?>
    </header>
    <!-- Page Content -->
    <section class="page_content_area">
      <div class="fs-row">
        <?php if ($page['sidebar_first']): ?>
          <aside class="sidebar sidebar_subnavigation fs-cell-right fs-lg-3" role="region">
            <?php print render($page['sidebar_first']); ?>
          </aside>
        <?php endif; ?>

        <div class="page_content fs-cell fs-lg-9">
          <?php
            if(!user_is_logged_in()) {
              print $messages;
            }
          ?>
          <div class="typography">
            <?php if ($page['content']): ?>
              <?php print render($page['content']); ?>
            <?php endif; ?>
          </div>
        </div><!-- END: page_content -->

        <?php if (count($related_programs) || $page['sidebar_second']) { ?>
        <aside class="sidebar sidebar_callouts fs-cell-right fs-lg-3">
          <?php if ($page['sidebar_second']): ?>    
            <?php print render($page['sidebar_second']); ?>
          <?php endif; ?>
          <?php
            if (count($related_programs)) {
          ?>
          <div class="margined_md link_list link_list_sidebar">
            <header class="link_list_header link_list_sidebar_header">
              <h2 class="bold_uppercase color_red link_list_heading link_list_sidebar_heading">Related Programs</h2>
            </header>
            <div class="link_list_body link_list_sidebar_body">
              <?php foreach ($related_programs as $program) { ?>
              <div class="link_list_item link_list_sidebar_item">
                <a class="link_list_link link_list_sidebar_link" href="<?php echo url("node/".$program->nid); ?>"><?php echo htmlspecialchars(htmlspecialchars_decode($program->title)); ?></a>
              </div>
              <?php } ?>
            </div>
          </div>
          <?php
            }
	  ?>
        </aside>
        <?php } ?>
      </div>
    </section><!-- END: page_content_area -->
    <?php if ($page['content_bottom']): ?>
      <!-- Full Width Components -->
      <section class="components">
        <?php print render($page['content_bottom']); ?>
      </section><!-- END: components -->
    <?php endif; ?>
  </main><!-- END: page -->
  <?php
   include ($directory . "/includes/footer.php");
  ?>
