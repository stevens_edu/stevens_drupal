<section class="margined_lg dates_section">
	<header class="component_header_block component_header_block_border dates_header">
		<div class="fs-row">
			<div class="fs-cell">
				<div class="component_header_block_wrapper">
					<h2 class="component_heading component_header_block_heading"><?php echo $title; ?></h2>
          <?php if (isset($content['field_important_dates_link'])) { ?>
						<?php echo stevens_theme_links($content['field_important_dates_link']['#items'][0],"link_arrow component_header_block_link"); ?>
          <?php } ?>
				</div>
			</div>
		</div>
	</header>
	<div class="dates_list">
		<div class="fs-row">
      <?php
        for ($i =0; $i < 3; $i++) {
          $item = $content['field_important_dates'][$i]['entity']['field_collection_item'];
          $item = array_pop($item);
          $item_date = new DateTime($item['field_important_date_date']['#items'][0]['value']);
      ?>
      <article class="fs-cell fs-lg-4 dates_item">
      	<a class="dates_item_link" href="<?php echo stevens_url_fix($item['field_important_date_link']['#items'][0]); ?>">
      		<h3 class="dates_item_date"><?php echo $item_date->format("d F Y"); ?></h3>
      		<div class="callout_body dates_item_title"><?php echo $item['field_important_date_link']['#items'][0]['title']; ?></div>
      	</a>
      </article>
      <?php
        }
      ?>
		</div>
	</div>
</section>
