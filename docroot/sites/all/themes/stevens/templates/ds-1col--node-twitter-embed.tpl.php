<article class="callout twitter_embed">
	<div class="callout_container">
		<div class="fs-row">
			<div class="fs-cell">
				<header class="component_header twitter_embed_header">
					<h2 class="component_heading component_header_block_heading twitter_embed_heading"><?php echo $title; ?></h2>
					<a href="https://twitter.com/<?php echo $content['field_twitter_user']['#items'][0]['value']; ?>" class="component_header_block_link twitter_embed_account_link">@<?php echo $content['field_twitter_user']['#items'][0]['value']; ?></a>
				</header>
				<div class="callout_content">
					<a class="twitter-timeline" data-dnt="true" href="https://twitter.com/<?php echo $content['field_twitter_user']['#items'][0]['value']; ?>" data-tweet-limit="3" data-link-color="#9D1535" data-widget-id="<?php echo $content['field_data_widget_id']['#items'][0]['value']; ?>" data-theme="light" data-chrome="nofooter noheader transparent noscrollbars">Tweets by @<?php echo $content['field_twitter_user']['#items'][0]['value']; ?></a>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
				</div>
			</div>
		</div>
	</div>
</article>
