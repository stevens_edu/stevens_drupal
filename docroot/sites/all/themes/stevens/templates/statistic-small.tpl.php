<article class="js-equalize_child numbers_item theme_<?php echo $params['color']; ?> size_small">
	<div class="numbers_item_content">
		<header class="numbers_item_header">
			<h2 class="numbers_item_heading"><span class="large"><?php echo $params['statistic']; ?></span><?php echo $params['qualifier']; ?></h2>
		</header>
		<div class="numbers_item_body">
			<div class="numbers_item_description">
				<?php echo $params['description']; ?>
			</div>
			<?php if(strlen($params['url'])!==0) { ?>
			<a class="numbers_item_link" href="<?php echo $params['url']; ?>"><?php echo $params['link']; ?></a>
			<?php } ?>
		</div>
	</div>
</article>
