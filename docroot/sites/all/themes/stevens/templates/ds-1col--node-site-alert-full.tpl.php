<?php
  $item_date = new DateTime($content['field_alert_date']['#items'][0]['value'] . ' UTC');
  $timezone = new DateTimeZone('America/New_York');
  $item_date->setTimeZone($timezone);

?>
<article id="node-<?php echo $node->nid; ?>" class="site_wide_alert_container js-toggle">
	<div class="close_wrap">
		<a href="#" class="alert_close js-toggle_handle">&times;</a>
	</div>
	<div class="fs-row">
	<div class="fs-cell fs-lg-full site_wide_alert">
		<time class="bold_uppercase color_gray_light" datetime="<?php echo $item_date->format("r"); ?>"><?php echo $item_date->format("d M Y"); ?></time>
		<span class="events_list_item_time"><?php echo $item_date->format("g:i A"); ?></span>
		<div class="directory_heading"><?php echo render($content['body']); ?></div>
	</div>
	</div>
</article>
