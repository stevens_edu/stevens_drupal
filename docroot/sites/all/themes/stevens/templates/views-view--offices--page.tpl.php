<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */

 $parameters = drupal_get_query_parameters();
 $query = "";
 if (isset($parameters['title'])) {
   $query = $parameters['title'];
 // } else {
 //   $query = 'A';
 }
 $filtered_query = filter_var($query, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
 $filtered_query = filter_var($filtered_query, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

 $allrows = views_get_view_result('offices', 'block_1');
 $newrows = "";
?>
<div class="<?php print $classes; ?>">
  <div class="view-content">
    <?php
      $results = false;

      // Letter Pagination
      if (strlen($query) == 1) {
        foreach ($allrows as $result) {
          if (substr($result->node_title,0,1)==$query) {
            $result_node = node_load($result->nid);
            $result_view = node_view($result_node, 'teaser');
            $rendered_node = drupal_render($result_view);
            $newrows .= $rendered_node;
            $results = true;
          }
        }
        echo $newrows;

      // Regular
      } else {
        if ($rows) {
          $results = true;
          echo $rows;
        }
      }

      if (!$results) {
        echo '<div class="no_results_message">No Offices Match Your Search</div><br><br><br>';
      }
    ?>
  </div>
  <?php
    if (strlen($query) !== 1) {
      if ($pager) {
        print $pager;
      }
    }
  ?>
</div>