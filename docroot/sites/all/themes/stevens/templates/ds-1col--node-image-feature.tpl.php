<?php
  $image = $content['field_page_feature_image']['#items'][0]['uri'];
?>
<div class="page_header_component">
	<div class="fs-row">
		<div class="fs-cell">
      <figure class="responsive_image page_header_figure">
      	<picture class="page_header_image">
      		<!--[if IE 9]><video style="display: none;"><![endif]-->
      		<source media="(min-width: 1220px)" srcset="<?php echo image_style_url('image_feature_1280x420',$image); ?>">
      		<source media="(min-width: 980px)" srcset="<?php echo image_style_url('image_feature_1024x336',$image); ?>">
      		<source media="(min-width: 740px)" srcset="<?php echo image_style_url('image_feature_700x394',$image); ?>">
      		<source media="(min-width: 0px)" srcset="<?php echo image_style_url('image_feature_470x264',$image); ?>">
      		<!--[if IE 9]></video><![endif]-->
      		<img src="<?php echo image_style_url('image_feature_470x264',$image); ?>" alt="<?php echo $content['field_page_feature_image']['#items'][0]['alt']; ?>" draggable="false">
      	</picture>
      </figure>
    </div>
  </div>
</div>
