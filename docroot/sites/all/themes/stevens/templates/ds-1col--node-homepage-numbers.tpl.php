<?php
  $image = $content['field_hp_numbers_image']['#items'][0]['uri'];
  $theme_array = array(
    array('small','red'),
    array('large','teal'),
    array('small','orange'),
    array('large','blue'),
    array('small','black'),
    array('small','gray'),
    array('small','gray'),
    array('small','red'),
    array('large','teal'),
    array('small','blue'),
    array('large','black'),
    array('small','orange'),
    array('large','teal'),
    array('small','gray'),
    array('small','blue'),
    array('small','black'),
    array('small','red'),
    array('large','orange'),
  );
  $smallview = views_get_view_result('statistics','block');
  $largeview = views_get_view_result('statistics','block_1');
  $s_count = floor(count($smallview) / 4);
  $l_count = floor(count($largeview) / 2);
  $item_limit = ($s_count > $l_count ? $l_count : $s_count);
  $theme_array = array_slice($theme_array, 0, $item_limit * 6);
?>
<section class="component numbers js-background" data-background-options='{"source":{"0px": "<?php echo image_style_url('hompage_statistics_500x400',$image); ?>", "500px": "<?php echo image_style_url('hompage_statistics_740x550',$image); ?>", "740px": "<?php echo image_style_url('hompage_statistics_980x730',$image); ?>", "980px": "<?php echo image_style_url('hompage_statistics_1220x910',$image); ?>", "1220px": "<?php echo image_style_url('hompage_statistics_1440x1070',$image); ?>"}}' data-top="">

	<header class="component_header numbers_header">
		<div class="fs-row">
			<div class="fs-cell fs-lg-4 fs-xl-3">
				<h2 class="numbers_heading"><?php echo $title; ?></h2>
			</div>
			<div class="fs-cell fs-lg-8 fs-xl-9">
				<div class="numbers_description">
					<?php echo render($content['field_hp_numbers_description']); ?>
				</div>
			</div>
		</div>
	</header>

	<div class="js-equalize numbers_callouts" data-equalize-options='{"target": ".js-equalize_child", "minWidth": "740px"}'>
		<div class="fs-row">
			<div class="fs-cell">
				<div class="js-equalize numbers_set numbers_set_active js-loaded" data-equalize-options='{"target": ".js-equalize_child", "minWidth": "740px"}'>
					<div class="js-carousel numbers_carousel" data-carousel-options='{"contained": false, "matchHeight": true, "controls": false, "maxWidth": "739px", "show": {"500px": 2}}'>
            <?php
              $i = 0;
              $item = NULL;
              $params = array();
              foreach ($theme_array as $theme_item) {
                $item_url = "";
                $item_link = "";
                if ($theme_item[0] == 'small') {
                  $item = array_shift($smallview);
                  $item = node_load($item->nid);
                  if (count($item->field_statistic_link)) {
                    $item_url = stevens_url_fix($item->field_statistic_link[LANGUAGE_NONE][0]);
                    $item_link = $item->field_statistic_link[LANGUAGE_NONE][0]['title'];
                  }
                  $params = array(
                    "color" => $theme_item[1],
                    "statistic" => $item->field_statistic[LANGUAGE_NONE][0]['value'],
                    "qualifier" => $item->field_statistic_qualifier[LANGUAGE_NONE][0]['value'],
                    "description" => $item->field_statistic_description[LANGUAGE_NONE][0]['value'],
                    "url" => $item_url,
                    "link" => $item_link
                  );
                  print theme('statistic_small', array('params' => $params));
                } else {
                  $item = array_shift($largeview);
                  $item = node_load($item->nid);
                  if (count($item->field_statistic_link)) {
                    $item_url = stevens_url_fix($item->field_statistic_link[LANGUAGE_NONE][0]);
                    $item_link = $item->field_statistic_link[LANGUAGE_NONE][0]['title'];
                  }
                  $params = array(
                    "color" => $theme_item[1],
                    "statistic" => $item->field_statistic[LANGUAGE_NONE][0]['value'],
                    "qualifier" => $item->field_statistic_qualifier[LANGUAGE_NONE][0]['value'],
                    "description" => $item->field_statistic_description[LANGUAGE_NONE][0]['value'],
                    "url" => $item_url,
                    "link" => $item_link,
                    "image" => $item->field_statistic_image[LANGUAGE_NONE][0]['uri']
                  );
                  print theme('statistic_large', array('params' => $params));
                }
                if ($i == 5) {
                  echo '</div></div><div class="js-equalize numbers_set" data-equalize-options=\'{"target": ".js-equalize_child", "minWidth": "740px"}\'>';
                } elseif ($i == 11) {
                  echo '</div><div class="js-equalize numbers_set" data-equalize-options=\'{"target": ".js-equalize_child", "minWidth": "740px"}\'>';
                }
                $i++;
              }
            ?>
				</div>
			</div>
		</div>
	</div>
	<footer class="numbers_footer">
		<div class="fs-row">
			<div class="fs-cell">
				<button class="numbers_more_button">Load More Statistics</button>
			</div>
		</div>
	</footer>

</section>
