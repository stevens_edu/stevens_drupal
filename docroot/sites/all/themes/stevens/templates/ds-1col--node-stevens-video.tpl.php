<div class="video_component">
  <div class="video_component--wrapper">
    <div class="video_component--container">
      <iframe class="video_component--video" title="<?php echo $title ?>" src="<?php echo _stevens_youtube_url_to_embed($node->field_video_link[LANGUAGE_NONE][0]['url']); ?>" width="560" height="315" frameborder="0" allowfullscreen></iframe>
    </div>
  </div>
  <a class="video_component--adesc icon_only icon_audio_description" href="<?php echo $node->field_audio_description[LANGUAGE_NONE][0]['url']; ?>"><span class="icon_text">Audio Description</span></a>
</div>
