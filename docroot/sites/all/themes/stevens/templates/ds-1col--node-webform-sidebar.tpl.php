<header class="siderbar_form_header">
  <h2 class="siderbar_form_heading"><?php echo $title; ?></h2>
</header>
<?php if (isset($content['body'])) { ?>
<div class="siderbar_form_description">
  <?php echo render($content['body']); ?>
</div>
<?php } ?>

<?php echo render($content['field_webform_entity']); ?>
