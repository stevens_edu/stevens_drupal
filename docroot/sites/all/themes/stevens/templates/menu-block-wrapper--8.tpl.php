<?php
	$node = menu_get_object();
	$block_title = '';
	$block_link = '';
	$active_trail = menu_get_active_trail();
	if ($node) {
		if ($node->field_hide_siblings['und'][0]['value'] || count($active_trail)==2) {
			$block_title = $node->title;
			$block_link = 'node/' . $node->nid;
		} else {
			$current = end($active_trail);
			$parent = prev($active_trail);
			$parent_node = menu_get_object('node', 1, $parent['link_path']);
			if($parent_node->field_hide_siblings['und'][0]['value'] == "1" ) {
				$block_title = $parent['link_title'];
				$block_link = $parent['link_path'];
			}
			else {
				if (count($active_trail) > 3 && !$current['has_children']) {
					$parent = prev($active_trail);
				}
				$block_title = $parent['link_title'];
				$block_link = $parent['link_path'];
			}
		}
	}
?>
<h2 class="subnavigation_handle">Additional Navigation</h2>
<div class="subnavigation js-navigation" data-navigation-handle=".subnavigation_handle" data-navigation-options='{"labels":{"open":"Close","closed":"Additional Navigation"}}'>
	<nav class="subnavigation_links">
		<?php echo l($block_title, $block_link, array('attributes' => array('class' => array('subnavigation_top_level_page_link')))); ?>
		<?php echo render($content); ?>
	</nav>
</div>
