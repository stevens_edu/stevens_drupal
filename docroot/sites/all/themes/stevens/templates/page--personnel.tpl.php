<?php
  $links = menu_load_links("menu-directory-tabs");
  include ($directory . "/includes/header.php");
  $per_page = 20;
  $page_count = 1;
  $pagination = false;
  $query_string = "" . $_GET["query"];

  $query_string = filter_var($query_string, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
  $query_string = filter_var($query_string, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);

  $current_page = isset($_GET["page"]) ? intval($_GET["page"]) : 1;

  $query = db_select('node', 'n');
  $query->condition('n.type','personnel');
  $query->condition('n.status','1');
  $query->innerJoin('field_data_field_personnel_address', 'fpa', 'n.nid = fpa.entity_id');
  $query->innerJoin('field_data_field_personnel_position', 'fpp', 'n.nid = fpp.entity_id');
  $query->innerJoin('field_data_field_personnel_sort', 'fps', 'n.nid = fps.entity_id');
  $query->fields('n',array('nid'));
  $query->orderBy('fps.field_personnel_sort_value', 'ASC');

  if (strlen($query_string) == 1) {
    $query->condition('fps.field_personnel_sort_value', $query_string . '%','LIKE');
  } elseif (strlen($query_string) > 1) {
    // Rebuild the query using db_query().
    $soundex = fastspot_personnel_content_soundex_translate($query_string);

    $query = db_query('SELECT n.nid FROM node n
      INNER JOIN personnel_soundex ps ON n.nid = ps.nid
      INNER JOIN field_data_field_personnel_address fpa ON n.nid = fpa.entity_id
      INNER JOIN field_data_field_personnel_position fpp ON n.nid = fpp.entity_id
      INNER JOIN field_data_field_personnel_sort fps ON n.nid = fps.entity_id
      WHERE (MATCH (ps.soundex_idx)
      AGAINST (:soundex_terms IN BOOLEAN MODE)
      OR n.title LIKE :query_string
      OR fpa.field_personnel_address_value LIKE :query_string
      OR fpp.field_personnel_position_value LIKE :query_string)
      AND n.status = 1
      ORDER BY CASE
        WHEN n.title LIKE :query_string THEN 1
        WHEN fpp.field_personnel_position_value LIKE :query_string THEN 2
        WHEN fpa.field_personnel_address_value LIKE :query_string THEN 3
        ELSE 4
      END, fps.field_personnel_sort_value ASC',
      array(':soundex_terms' => $soundex, ':query_string' => '%'.$query_string.'%'));
  }

  // Execute if necessary.
  if (strlen($query_string) <= 1) {
    $results = $query->execute()->fetchAll();
  }
  else {
    $results = $query->fetchAll();
  }

  $page_query_count = ceil(count($results) / $per_page);
  $page_count = $page_query_count ? $page_query_count : 1;

  $alphaquery = db_query("SELECT UPPER(SUBSTR(field_personnel_sort_value,1,1)) AS `alpha`
                     FROM field_data_field_personnel_sort
                     GROUP BY SUBSTR(field_personnel_sort_value,1,1)");
  // $alpharesult = $alphaquery->fetchAssoc();
  $available_letters = array();
  foreach($alphaquery as $item) {
    $available_letters[] = $item->alpha;
  }
  $letters = range('A', 'Z');
  $output_ul = ($query_string === '') ? '' : '<li class="directory_controls_all"><a href="/' . request_path() . '">All</a></li>';
  $output_select = ($query_string === '') ? '' : '<option value="">All</option>';
  foreach ($letters as $letter) {
    if (in_array($letter, $available_letters)) {
      $output_ul .= '<li><a href="?query=' . $letter . '"';
      $output_select .= '<option value="' . $letter . '"';
      if ($letter == $query_string) {
        $output_ul .= ' class="active"';
        $output_select .= ' selected="selected"';
      }
      $output_ul .= '><span>' . $letter . '</span></a></li>';
      $output_select .= '>' . $letter . '</option>';
    } else {
      $output_ul .= '<li><span class="disabled">' . $letter . ' </span></li>';
      $output_select .= '<option disabled="" value="' . $letter . '">' . $letter . '</option>';
    }
  }

?>
<div class="page_wrapper js-navigation_push">
  <main id="page" class="page" role="main">
    <header class="page_header">
    	<div class="fs-row">
        <?php
          if(user_is_logged_in()) {
        ?>
        <div class="fs-cell">
          <?php print $messages; ?>
          <?php print render($page['help']); ?>
          <?php if ($tabs): ?>
            <div class="tabs"><?php print render($tabs); ?></div>
          <?php endif; ?>
          <?php if ($action_links): ?>
            <ul class="action-links"><?php print render($action_links); ?></ul>
          <?php endif; ?>
        </div>
        <?php
          }
        ?>
    		<div class="fs-cell typography">
    			<h1>Personnel</h1>
        </div>
        <div class="fs-cell">
          <?php if ($breadcrumb): ?>
            <?php print $breadcrumb; ?>
          <?php endif; ?>
        </div>
        <div class="fs-cell">
          <div class="typography directory_header">
            <p>For general inquiries, please call <a href="tel:+12012165000">(201) 216-5000</a>.</p>
            <p>Members of the Stevens community may search for contact information using the <a href="https://web.stevens.edu/peoplefinder">People Finder</a>.</p>
          </div>
          <div class="page_tabs">
            <?php
              foreach ($links as $link) {
                $link_class = "";
                $current_link = drupal_get_path_alias($link["link_path"]);
                $page_link = drupal_get_path_alias('node/' . $node->nid);
                if ($current_link === $page_link) {
                  $link_class = " page_tab_active";
                }
              ?>
            <a class="page_tab<?php echo $link_class; ?>" href="/<?php echo drupal_get_path_alias($link["link_path"]); ?>"><?php echo $link["link_title"]; ?></a>
            <?php
              }
            ?>
          </div>
    			<hr>
    		</div>
    	</div>
      <div class="page_header_component page_header_article">
        <div class="fs-row">
          <div class="fs-cell">
            <article class="directory_controls component_controls">
              <ul class="fs-xs-hide fs-sm-hide">
                <?php echo $output_ul; ?>
              </ul>
              <div class="fs-md-hide fs-lg-hide fs-xl-hide alpha_drop_wrap">
              <fieldset class="form_fieldset">
                <label for="alpha_drop" class="offscreen">Show only those starting with selected letter</label>
                <select class="js-dropdown alpha_drop" id="alpha_drop">
                  <?php echo $output_select; ?>
                </select>
              </fieldset>
              </div>
              <div class="secondary_nav_item">
                <form action="/<?php echo request_path(); ?>" class="search_form directory_search_form">
                  <input id="edit-query" name="query" value="<?php if(strlen($query_string) > 1) { echo $query_string; } ?>" type="text" class="search_input news_search_input">
                  <button class="search_submit news_search_submit" type="submit">
                    <span class="visually_hidden">Search</span>
                  </button>
                </form>
              </div>
            </article>
          </div>
        </div>
      </div>


    </header>
  	<!-- Page Content -->
  	<section class="page_content_area">
  		<div class="fs-row">

        <div class="page_content fs-cell">
          <?php
            if(!user_is_logged_in()) {
              print $messages;
            }
          ?>

            <!-- In-Content Callouts -->
            <aside class="callouts content_callouts">

              <?php
                if (!count($results)) {
                  echo "<br /><h2>No results</h2><br /><br />";
                } else {
                  $result = array_slice($results, ($current_page - 1) * $per_page, $per_page);
                  foreach ($result as $item) {
                    echo render(node_view(node_load($item->nid), 'teaser'));
                  }
                }
              ?>

              <?php
                if ($page_count > 1) {
                  $pagination = stevens_pagination_array($page_count,$current_page);
              ?>
              <div class="item-list">
                <ul class="pager">
                  <li class="pager-previous<?php if ($current_page == 1) { ?> pager-disabled<?php } ?>">
                    <?php if ($current_page == 1) { ?>
                      <a title="Go to previous page" href="#">previous</a>
                    <?php } else { ?>
                      <a title="Go to previous page" href="/<?php echo request_path(); ?>/?query=<?php echo $query_string; ?>&amp;page=<?php echo ($current_page - 1); ?>">previous</a>
                    <?php } ?>
                  </li>
                  <?php
                    $x = 0;
                    foreach ($pagination as $page_entry) {
                      $x++;

                      if ($page_entry === $current_page || $page_entry == "&hellip;") {
                  ?>
                  <li class="pager-current<?php if ($x == count($pagination)) { ?> pager-item-last<?php } ?>"><?php echo $page_entry; ?></li>
                  <?php
                      } else {
                  ?>
                  <li class="pager-item<?php if ($x == count($pagination)) { ?> pager-item-last<?php } ?>">
                    <a title="Go to page <?php echo $page_entry; ?>" href="/<?php echo request_path(); ?>/?query=<?php echo $query_string; ?>&amp;page=<?php echo $page_entry; ?>"><?php echo $page_entry; ?></a>
                  </li>
                  <?php
                      }
                    }
                  ?>
                  <li class="pager-next<?php if ($current_page == $page_count) { ?> pager-disabled<?php } ?>">
                    <?php if ($current_page == $page_count) { ?>
                      <a title="Go to next page" href="#">next</a>
                    <?php } else { ?>
                      <a title="Go to next page" href="/<?php echo request_path(); ?>/?query=<?php echo $query_string; ?>&amp;page=<?php echo ($current_page + 1) ?>">next</a>
                    <?php } ?>
                  </li>
                </ul>
              </div>
              <?php
                }

                print render($page['content_components']);
              ?>

            </aside><!-- END: content_callouts -->

        </div><!-- END: page_content -->

      </div>
    </section><!-- END: page_content_area -->
    <?php if ($page['content_bottom']): ?>
      <!-- Full Width Components -->
      <section class="components">
        <?php print render($page['content_bottom']); ?>
      </section><!-- END: components -->
    <?php endif; ?>
  </main><!-- END: page -->
  <?php
   include ($directory . "/includes/footer.php");
  ?>
