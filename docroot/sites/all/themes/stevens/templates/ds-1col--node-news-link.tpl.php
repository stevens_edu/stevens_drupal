<?php
  $time = strtotime($content["field_news_link_date"]["#items"][0]["value"]);
?>
<article class="clearfix news_item news_list_item">
  <div class="news_item_content news_list_item_content">
    <header class="news_item_header news_list_item_header">
      <time class="news_item_date news_list_item_date" datetime="<?php echo date("Y-m-d",$time); ?>"><?php echo date("l, F j, Y",$time); ?></time>
      <span class="news_item_cat news_list_item_cat"><?php echo $content["field_news_link_source"]["#items"][0]["safe_value"]; ?></span>
    </header>

    <div class="news_item_body news_list_item_body">
      <h3 class="news_item_title news_list_item_title">
        <a class="news_item_title_link news_list_item_title_link" target="_blank" href="<?php echo stevens_url_fix($content["field_news_link_link"]["#items"][0]); ?>"><?php echo strip_tags(render($content["title"])); ?></a>
      </h3>
    </div>
  </div>
</article>