<article class="clearfix directory_container">

	<div class="fs-cell fs-md-6 fs-lg-6">
		<div class="directory_office">
			<h4 class="directory_heading"><?php echo $title; ?></h4>
      <?php
        if (isset($content['field_office_link'])) {
          $link_title = "View Website";
          $t = $content['field_office_link']['#items'][0]["title"];
          if ($t && $t != $content['field_office_link']['#items'][0]["url"]) {
            $link_title = $t;
          }
          
          $link = stevens_url_fix($content['field_office_link']['#items'][0]);
      ?>
			<a href="<?php echo $link; ?>" class="callout_link"><?php echo $link_title; ?></a>
      <?php
        }
      ?>
		</div>
	</div>

	<div class="fs-cell fs-md-half fs-lg-3 directory_detail">
    <?php if(!empty($content['field_office_location']['#items'][0]['name']) || !empty($content['field_office_location']['#items'][0]['street']) || !empty($content['field_office_location']['#items'][0]['additional']) || !empty($content['field_office_location']['#items'][0]['city'])) { ?>
		<h4 class="color_red bold_uppercase">Location on Campus</h4>
		<?php if(!empty($content['field_office_location']['#items'][0]['name'])) { ?>
      <p><?php echo $content['field_office_location']['#items'][0]['name']; ?></p>
    <?php } ?>

    <?php if(!empty($content['field_office_location']['#items'][0]['street'])) { ?>
      <p><?php echo $content['field_office_location']['#items'][0]['street']; ?></p>
    <?php } ?>

    <?php if(!empty($content['field_office_location']['#items'][0]['additional'])) { ?>
      <p><?php echo $content['field_office_location']['#items'][0]['additional']; ?></p>
    <?php } ?>

    <?php if(!empty($content['field_office_location']['#items'][0]['city'])) { ?>
      <p>
        <?php echo $content['field_office_location']['#items'][0]['city']; ?>,
        <?php echo $content['field_office_location']['#items'][0]['province']; ?>
        <?php echo $content['field_office_location']['#items'][0]['postal_code']; ?>
      </p>
      <?php } ?>
    <?php } else { echo "&nbsp;"; } ?>
	</div>

	<div class="fs-cell fs-md-half fs-lg-3 directory_detail">
    <?php if(!empty($content['field_office_location']['#items'][0]['email']) || !empty($content['field_office_location']['#items'][0]['fax']) || !empty($content['field_office_location']['#items'][0]['phone'])) { ?>

		<h4 class="color_red bold_uppercase">Contact</h4>
    <?php if(!empty($content['field_office_location']['#items'][0]['phone'])) { ?>
      <p>p. <?php echo $content['field_office_location']['#items'][0]['phone']; ?></p>
    <?php } ?>
    <?php if(!empty($content['field_office_location']['#items'][0]['email'])) { ?>
      <p>e. <a href="mailto:<?php echo $content['field_office_location']['#items'][0]['email']; ?>"><?php echo $content['field_office_location']['#items'][0]['email']; ?></a></p>
    <?php } ?>
    <?php if(!empty($content['field_office_location']['#items'][0]['fax'])) { ?>
      <p>f. <?php echo $content['field_office_location']['#items'][0]['fax']; ?></p>
    <?php } ?>
	</div>
  <?php } else { echo "&nbsp;"; }?>

</article>
