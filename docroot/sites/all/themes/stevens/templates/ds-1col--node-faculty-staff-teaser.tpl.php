<article class="clearfix directory_container">
	<div class="fs-cell fs-md-6 fs-lg-6">
		<div class="directory_office">
			<h4 class="directory_heading"><?php echo $title; ?></h4>
			<p><?php echo $content['field_faculty_position']['#items'][0]['value']; ?></p>
		</div>
	</div>
	<div class="fs-cell fs-md-3 fs-lg-3 directory_detail">
    <?php if(isset($content['field_faculty_building'])||isset($content['field_faculty_room'])) { ?>
		<h4 class="color_red bold_uppercase">Location</h4>
		<?php if(isset($content['field_faculty_building'])) { ?>
    <p><?php echo $content['field_faculty_building']['#items'][0]['value']; ?></p>
    <?php } ?>
    <?php if(isset($content['field_faculty_building'])) { ?>
		<p>Room <?php echo $content['field_faculty_room']['#items'][0]['value']; ?></p>
    <?php } ?>
    <?php } else { echo '&nbsp;'; }?>
	</div>
	<div class="fs-cell fs-md-3 fs-lg-3 directory_detail">
    <?php if(isset($content['field_faculty_phone'])||isset($content['field_faculty_email'])) { ?>
		<h4 class="color_red bold_uppercase">Contact</h4>
    <?php if(isset($content['field_faculty_phone'])) { ?>
		<p>p. <?php echo $content['field_faculty_phone']['#items'][0]['value']; ?></p>
    <?php } ?>
    <?php if(isset($content['field_faculty_email'])) { ?>
		<p>e. <a href="mailto:<?php echo $content['field_faculty_email']['#items'][0]['email']; ?>"><?php echo $content['field_faculty_email']['#items'][0]['email']; ?></a></p>
    <?php } ?>
    <?php } ?>
	</div>
</article>
