<article class="callout announcement_callout<?php if ($content['field_announcement_callout_class']['#items'][0]['value']) { echo ' bordered'; } ?>">
	<div class="callout_container announcement_container">
		<h2 class="announcement_heading"><?php echo $title; ?></h2>
		<div class="callout_content announcement_content">
			<?php echo render($content['body']); ?>
		</div>
		<?php if (isset($content['field_announcement_callout_link'])) { ?>
		<?php echo stevens_theme_links($content['field_announcement_callout_link']['#items'][0],"callout_link announcement_link"); ?>
		<?php } ?>
	</div>
</article>
