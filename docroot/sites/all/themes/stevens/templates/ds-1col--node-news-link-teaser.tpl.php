<?php
  $item_date = new DateTime($content['field_news_link_date']['#items'][0]['value']);
?>
<article class="news_item news_sidebar_item news_in_item">
	<header class="news_in_item_header">
		<time class="news_in_item_date" datetime="<?php echo $item_date->format("r"); ?>"><?php echo $item_date->format("d F Y"); ?></time>
		<span class="news_in_item_source"><?php echo $content['field_news_link_source']['#items'][0]['value']; ?></span>
	</header>
	<h3 class="news_in_item_title">
		<a class="news_in_item_title_link" href="<?php echo stevens_url_fix($content['field_news_link_link']['#items'][0]); ?>"><?php echo $title; ?></a>
	</h3>
</article>
