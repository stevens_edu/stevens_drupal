<div class="callout multi_column_list">
	<div class="callout_container multi_column_list_container">
		<h2 class="multi_column_list_heading"><?php echo $title; ?></h2>
		<div class="typography">
			<ul class="callout_content multi_column_list_items">
        <?php foreach ($content['field_multi_column_items']['#items'] as $item) { ?>
				<li class="multi_column_list_item"><?php echo $item['value']; ?></li>
        <?php } ?>
			</ul>
		</div>
	</div>
</div>
