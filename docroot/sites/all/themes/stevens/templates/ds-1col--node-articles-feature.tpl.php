<div class="page_header_component page_header_article">
  <div class="page_header_article_carousel js-carousel" data-carousel-options='{"matchHeight":true,"pagination":false}'>
    <?php
      $item_count = count($content['field_articles_callouts']['#items']);
      for ($i = 0; $i < $item_count; $i++) {
        $item = $content['field_articles_callouts'][$i]['entity']['field_collection_item'];
        $item = array_pop($item);
        $image = $item['field_articles_callout_image']['#items'][0]['uri'];
    ?>
    <article class="page_header_article_carousel_callout background_<?php echo $item['field_articles_callout_color']['#items'][0]['value']; ?>">
      <figure class="page_header_article_figure js-background" data-background-options='{"source":{"0px":"<?php echo image_style_url('articles_callout_500x281',$image); ?>","500px":"<?php echo image_style_url('articles_callout_740x480',$image); ?>"}}'></figure>
      <div class="page_header_article_carousel_callout_content">
        <div class="page_header_article_carousel_callout_content_container">
          <h2 class="page_header_article_carousel_callout_heading"><?php echo stevens_trim_length($item['field_articles_callout_title']['#items'][0]['value'],70); ?></h2>
          <div class="page_header_article_carousel_callout">
            <?php echo stevens_trim_length(render($item['field_articles_callout_desc']),350); ?>
          </div>
          <?php
            foreach ($item['field_articles_callout_links']['#items'] as $link) {
              echo stevens_theme_links($link,"page_header_article_carousel_callout_link");
            }
          ?>
        </div>
      </div>
    </article>
    <?php
      }
    ?>
  </div>
</div>