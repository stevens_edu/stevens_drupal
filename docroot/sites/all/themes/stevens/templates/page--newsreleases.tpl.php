<?php
  $page_title = $node->title;

  // Get a list of categories
  $vocabulary = taxonomy_vocabulary_machine_name_load('news_categories');
  $categories = entity_load('taxonomy_term', FALSE, array('vid' => $vocabulary->vid));
  $category_rel = array();
  foreach ($categories as $category) {
    $name = htmlspecialchars(htmlspecialchars_decode($category->name));
    $category_rel[$category->tid] = $name;
  }

  // Get list of audiences so we can get the TID for Media
  $vocabulary = taxonomy_vocabulary_machine_name_load('audiences');
  $audiences = entity_load('taxonomy_term', FALSE, array('vid' => $vocabulary->vid));
  foreach ($audiences as $audience) {
    if ($audience->name == "Media") {
      $media_tid = $audience->tid;
    }
  }

  $per_page = 10;
  $page_count = 1;

  // Get a list of months
  $months = array();
  $query = db_query("SELECT DISTINCT(CONCAT(YEAR(field_news_date_value),'-',MONTH(field_news_date_value))) AS `date` 
                     FROM field_data_field_news_date JOIN field_data_field_news_audiences
                     ON field_data_field_news_audiences.entity_id = field_data_field_news_date.entity_id
                     WHERE field_data_field_news_audiences.field_news_audiences_tid = '$media_tid'
                     ORDER BY field_news_date_value DESC");
  while ($result = $query->fetchAssoc()) {
    $months[] = $result["date"];
  }

  // Set defaults for filter labels
  $date_label = "Filter by Date";
  $pagination = false;
 
  // Get media releases!
  $query = new EntityFieldQuery;

  // Search query
  if (!empty($_GET["query"])) {
    
    $query_string = addslashes($_GET["query"]);
    $result = db_query("SELECT nid FROM node JOIN field_data_body JOIN field_data_field_news_audiences 
                        ON node.nid = field_data_body.entity_id
                        WHERE field_data_field_news_audiences.entity_id = node.nid AND 
                              field_data_field_news_audiences.field_news_audiences_tid = '$media_tid' AND 
                              field_data_body.entity_type = 'node' AND 
                              node.type = 'news_item' AND 
                              (node.title LIKE '%$query_string%' OR field_data_body.body_value LIKE '%$query_string%')");

    $news = $dates = array();
    while ($node = $result->fetchAssoc()) {
      $item = node_load($node["nid"]);
      $news[] = $item;
      $dates[] = strtotime($item->field_news_date["und"][0]["value"]);
    }
    array_multisort($dates,SORT_DESC,$news);

  // Non-search
  } else {
    $query->entityCondition("entity_type","node")
          ->entityCondition("bundle","news_item")
          ->fieldCondition("field_news_audiences","tid",$media_tid)
          ->propertyCondition("status",NODE_PUBLISHED);
  
    // Process month queries
    if ($_GET["month"]) {
      $time = strtotime($_GET["month"]);
      $date_label = date("F Y",$time);
      $start_date = date("Y-m-01 00:00:00",$time);
      $end_date = date("Y-m-t 23:59:59",$time);
  
      // Add condition to the query
      $query->fieldCondition("field_news_date","value",$end_date,"<=")
            ->fieldCondition("field_news_date","value",$start_date,">=");
    }
  
    // Add pagination if no filters were set
    if (!$_GET["month"]) {
      $current_page = isset($_GET["page"]) ? intval($_GET["page"]) : 1;

      // Get page count
      $page_query = new EntityFieldQuery;
      $page_query->entityCondition("entity_type","node")
                 ->entityCondition("bundle","news_item")
                 ->fieldCondition("field_news_audiences","tid",$media_tid)
                 ->propertyCondition("status",NODE_PUBLISHED)
                 ->count();
      $page_query_count = ceil($page_query->execute() / $per_page);
      $page_count = $page_query_count ? $page_query_count : 1;

      // Add limits to the main query
      $start = ($current_page - 1) * $per_page;
      $query->range($start,$per_page);
    }

    // Process 
    $result = $query->fieldOrderBy("field_news_date","value","DESC")->execute();
    if ($result) {
      $news_nids = array_keys($result["node"]);
      $news = entity_load("node",$news_nids);
    } else {
      $news = array();
    }
  }

  include ($directory . "/includes/header.php");
?>
<div class="page_wrapper js-navigation_push">
  <main id="page" class="page" role="main">
    <header class="page_header">
    	<div class="fs-row">
        <?php
          if(user_is_logged_in()) {
        ?>
        <div class="fs-cell">
          <?php print $messages; ?>
          <?php print render($page['help']); ?>
          <?php if ($tabs): ?>
            <div class="tabs"><?php print render($tabs); ?></div>
          <?php endif; ?>
          <?php if ($action_links): ?>
            <ul class="action-links"><?php print render($action_links); ?></ul>
          <?php endif; ?>

        </div>
        <?php
          }
        ?>
    		<div class="fs-cell typography">
    			<h1><?php echo $page_title; ?></h1>
        </div>
        <div class="fs-cell">
          <?php if ($breadcrumb): ?>
            <?php print $breadcrumb; ?>
          <?php endif; ?>
        </div>
        <div class="fs-cell">
    			<hr>
    		</div>
    	</div>
      <?php if ($page['feature']): ?>
      <?php print render($page['feature']); ?>
      <?php endif; ?>
    </header>
  	<!-- Page Content -->
  	<section class="page_content_area" id="news_list">
  		<div class="fs-row">
        <?php if ($page['sidebar_first']): ?>
          <aside class="sidebar sidebar_subnavigation fs-cell-right fs-lg-3" role="region">
            <?php print render($page['sidebar_first']); ?>
          </aside>
        <?php endif; ?>

        <div class="page_content fs-cell fs-lg-9">
          <?php
            if(!user_is_logged_in()) {
              print $messages;
            }
          ?>
          <aside class="callouts content_callouts">
            <div class="background_gray_pale component_controls news_controls">
              <form class="news_filter_form component_filter_form">
                <fieldset class="news_filter_fieldset">
                  <label class="visually_hidden news_filter_label" for="filter_date">Filter by Date</label>
                  <select id="filter_date" class="js-dropdown news_filter_select" data-dropdown-options='{"label":"<?php echo $date_label; ?>", "customClass": "red_dropdown news_filter_dropdown filter_date", "links": true }'>
                    <option value="/news/news-releases/#news_list">All Months</option>
                    <?php foreach ($months as $month) { ?>
                    <option value="/news/news-releases/?month=<?php echo $month; ?>#news_list"><?php echo date("F Y",strtotime($month)); ?></option>
                    <?php } ?>
                  </select>
                </fieldset>
              </form>

              <form action="/news/news-releases/#news_list" class="search_form news_search_form">
                <input type="text" name="query" class="search_input news_search_input" value="<?php if (!empty($_GET["query"])) { echo htmlspecialchars($_GET["query"]); } ?>">
                <button class="search_submit news_search_submit" type="submit">
                  <span class="visually_hidden">Search</span>
                </button>
              </form>

            </div>

            <div class="news_list news_list_in_content">
              <?php
                if (!count($news)) {
                  echo '<br><div class="no_results_message">No News Releases Match Your Filters</div>';
                } else {
                  foreach ($news as $item) {
                    $time = strtotime($item->field_news_date["und"][0]["value"]." GMT");
                    $categories = array();
                    foreach (array_filter((array)$item->field_news_category["und"]) as $c) {
                      $categories[] = $category_rel[$c["tid"]];
                    }
              ?>
              <article class="clearfix news_item news_list_item">
                <?php if (count($item->field_news_featured_image["und"])) { ?>
                <figure class="bg_black responsive_image news_item_figure news_list_item_figure">
                  <picture class="news_item_picture news_list_item_picture">
                    <!--[if IE 9]><video style="display: none;"><![endif]-->
                    <source media="(min-width: 740px)" srcset="<?php echo image_style_url("news_item_174x116",$item->field_news_featured_image["und"][0]["uri"]); ?>">
                    <source media="(min-width: 0px)" srcset="<?php echo image_style_url("news_item_80x80",$item->field_news_featured_image["und"][0]["uri"]); ?>">
                    <!--[if IE 9]></video><![endif]-->
                    <img class="news_item_image news_list_item_image" src="<?php echo image_style_url("news_item_80x80",$item->field_news_featured_image["und"][0]["uri"]); ?>" alt="<?php echo $item->field_news_featured_image["und"][0]["alt"]; ?>">
                  </picture>
                </figure>
                <?php } ?>

                <div class="news_item_content news_list_item_content">
                  <header class="news_item_header news_list_item_header">
                    <time class="news_item_date news_list_item_date" datetime="<?php echo date("Y-m-d",$time); ?>"><?php echo date("j M Y",$time); ?></time>
                    <span class="news_item_cat news_list_item_cat"><?php echo implode(", ",$categories); ?></span>
                  </header>

                  <div class="news_item_body news_list_item_body">
                    <h3 class="news_item_title news_list_item_title">
                      <a class="news_item_title_link news_list_item_title_link" href="<?php echo url("node/".$item->nid); ?>"><?php echo htmlspecialchars(htmlspecialchars_decode($item->title)); ?></a>
                    </h3>
                    <div class="news_item_description news_list_item_description">
                      <p><?php echo stevens_trim_length(drupal_html_to_text($item->body["und"][0]["safe_value"], array('p', 'br')),175); ?></p>
                    </div>
                  </div>
                </div>
              </article>
                <?php
                  }
                }
              ?>
            </div>

            <?php
              if ($page_count > 1) {
                $pagination = stevens_pagination_array($page_count,$current_page);
            ?>
            <div class="item-list">
              <ul class="pager">
                <li class="pager-previous<?php if ($current_page == 1) { ?> pager-disabled<?php } ?>">
                  <?php if ($current_page == 1) { ?> 
                    <a title="Go to previous page" href="#">previous</a>
                  <?php } else { ?>
                    <a title="Go to previous page" href="/news/news-releases/?page=<?php echo ($current_page - 1); ?>#news_list">previous</a>
                  <?php } ?>
                </li>
                <?php
                  $x = 0;
                  foreach ($pagination as $page_entry) {
                    $x++;

                    if ($page_entry === $current_page || $page_entry == "&hellip;") {
                ?>
                <li class="pager-current<?php if ($x == count($pagination)) { ?> pager-item-last<?php } ?>"><?php echo $page_entry; ?></li>
                <?php
                    } else {
                ?>
                <li class="pager-item<?php if ($x == count($pagination)) { ?> pager-item-last<?php } ?>">
                  <a title="Go to page <?php echo $page_entry; ?>" href="/news/news-releases/?page=<?php echo $page_entry; ?>#news_list"><?php echo $page_entry; ?></a>
                </li>
                <?php
                    }
                  }
                ?>
                <li class="pager-next<?php if ($current_page == $page_count) { ?> pager-disabled<?php } ?>">
                  <?php if ($current_page == $page_count) { ?>
                    <a title="Go to next page" href="#">next</a>
                  <?php } else { ?>
                    <a title="Go to next page" href="/news/news-releases/?page=<?php echo ($current_page + 1) ?>#news_list">next</a>
                  <?php } ?>
                </li>
              </ul>
            </div>
            <?php
              }

              print render($page['content_components']);
            ?>
          </aside>
        </div>

        <?php if ($page['sidebar_second']): ?>
          <aside class="sidebar sidebar_callouts fs-cell-right fs-lg-3">
          <?php print render($page['sidebar_second']); ?>
          </aside>
        <?php endif; ?>
      </div>
    </section><!-- END: page_content_area -->
    <?php if ($page['content_bottom']): ?>
      <!-- Full Width Components -->
      <section class="components">
        <?php print render($page['content_bottom']); ?>
      </section><!-- END: components -->
    <?php endif; ?>
  </main><!-- END: page -->
  <?php
   include ($directory . "/includes/footer.php");
  ?>
