<div class="margined_md video_list">
  <header class="video_list_header">
    <h2 class="bold_uppercase color_red video_list_heading"><?php echo $title; ?></h2>
  </header>
  <div class="video_list_body">
    <?php
      $item_count = count($content['field_video_list']['#items']);
      for ($i = 0; $i < $item_count; $i++) {
        $item = $content['field_video_list'][$i]['entity']['field_collection_item'];
        $item = array_pop($item);
        $image = $item['field_video_list_image']['#items'][0]['uri'];
        $alt = $item['field_video_list_image']['#items'][0]['alt'];
    ?>
    <div class="video_item video_list_item">
      <a class="js-lightbox video_item_link" href="<?php echo stevens_url_fix(stevens_autoplay_url($item['field_video_list_link']['#items'][0])); ?>">
        <figure class="video_item_figure">
          <picture class="responsive_image video_item_picture">
            <!--[if IE 9]><video style="display: none;"><![endif]-->
            <source media="(min-width: 1220px)" srcset="<?php echo image_style_url('video_list_252x143',$image); ?>">
            <source media="(min-width: 980px)" srcset="<?php echo image_style_url('video_list_200x114',$image); ?>">
            <source media="(min-width: 740px)" srcset="<?php echo image_style_url('video_list_330x187',$image); ?>">
            <source media="(min-width: 500px)" srcset="<?php echo image_style_url('video_list_470x266',$image); ?>">
            <source media="(min-width: 0px)" srcset="<?php echo image_style_url('video_list_294x196',$image); ?>">
            <!--[if IE 9]></video><![endif]-->
            <img class="video_item_image" src="<?php echo image_style_url('video_list_294x196',$image); ?>" alt="<?php print check_plain($alt); ?>" draggable="false">
          </picture>
        </figure>
        <div class="video_item_content">
          <h3 class="video_item_title"><?php echo $item['field_video_list_title']['#items'][0]['value']; ?></h3>
        </div>
      </a>
    </div>
    <?php
      }
    ?>
  </div>
  <?php if (isset($content['field_video_list_all_link'])) { ?>
  <footer class="video_list_footer">
    <?php echo stevens_theme_links($content['field_video_list_all_link']['#items'][0],"link_arrow video_list_footer_link"); ?>
  </footer>
  <?php } ?>
</div>
