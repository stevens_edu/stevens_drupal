<?php
  $query = new EntityFieldQuery;
  $query->entityCondition("entity_type","node")
        ->entityCondition("bundle","event")
        ->propertyCondition("status",NODE_PUBLISHED)
        ->fieldCondition("field_event_end_time","value",date("Y-m-d H:i:s"),">=")
        ->range(0,12)
        ->fieldOrderBy("field_event_start_time","value","ASC");

  // By category
  if (count($content["field_event_categories"]["#items"])) {
    $query->fieldCondition("field_event_categories","tid",$content["field_event_categories"]["#items"][0]["tid"]);
  }

  // By program
  if (count($content["field_news_programs"]["#items"])) {
    $tids = array();
    foreach ($content["field_news_programs"]["#items"] as $item) {
      $tids[] = $item["tid"];
    }
    $query->fieldCondition("field_news_programs","tid",$tids,"IN");
  }

  // By department
  if (count($content["field_news_by_topic_departments"]["#items"])) {
    $tids = array();
    foreach ($content["field_news_by_topic_departments"]["#items"] as $item) {
      $tids[] = $item["tid"];
    }
    $query->fieldCondition("field_news_departments","tid",$tids,"IN");
  }

  // By school
  if (count($content["field_news_by_topic_schools"]["#items"])) {
    $tids = array();
    foreach ($content["field_news_by_topic_schools"]["#items"] as $item) {
      $tids[] = $item["tid"];
    }
    $query->fieldCondition("field_news_schools","tid",$tids,"IN");
  }

  // By research
  if (count($content["field_news_by_topic_research"]["#items"])) {
    $tids = array();
    foreach ($content["field_news_by_topic_research"]["#items"] as $item) {
      $tids[] = $item["tid"];
    }
    $query->fieldCondition("field_news__research","tid",$tids,"IN");
  }

  // By audience
  if (count($content["field_news_audiences"]["#items"])) {
    $tids = array();
    foreach ($content["field_news_audiences"]["#items"] as $item) {
      $tids[] = $item["tid"];
    }
    $query->fieldCondition("field_news_audiences","tid",$tids,"IN");
  }

  // By tag
  if (count($content["field_news_tags"]["#items"])) {
    $tids = array();
    foreach ($content["field_news_tags"]["#items"] as $item) {
      $tids[] = $item["tid"];
    }
    $query->fieldCondition("field_news_tags","tid",$tids,"IN");
  }

  $result = $query->execute();

  $category_name = $content["field_event_categories"]["#items"][0]["taxonomy_term"]->name;

  if ($result) {
    $event_nids = array_keys($result["node"]);
    $events = entity_load("node",$event_nids);

    if (count($events)) {
?>
<section class="component_bordered events_list_section component_columns content_loader">
  <header class="component_header_block events_list_header">
    <div class="fs-row">
      <div class="fs-cell">
        <div class="component_header_block_wrapper">
          <h2 class="js-content_swap component_heading component_header_block_heading events_list_heading"><?php echo $title; ?></h2>
          <a class="js-content_swap link_arrow component_header_block_link events_list_all" href="/events" data-content-swap-mobile="All" data-content-swap-default="View All Events">View All Events</a>
        </div>
      </div>
    </div>
  </header>

  <div class="events_list">
    <div class="fs-row">
      <div class="fs-cell">
        <div class="content_set content_set_active js-loaded">
          <?php
            $x = 0;
            foreach ($events as $item) {
              if ($x % 4 == 0) {
                echo '<div class="content_set'.(!$x ? ' content_set_active js-loaded' : '').'">';
              }

              // Figure out how to display the date
              $start = strtotime($item->field_event_start_time["und"][0]["value"]." UTC");
              $end = strtotime($item->field_event_end_time["und"][0]["value"]." UTC");
              $all_day = $item->field_event_all_day["und"][0]["value"];
              list($draw_date,$draw_time) = stevens_parse_date_time($start,$end,$all_day);
          ?>
          <article class="clearfix events_item events_list_item component_columns_item">
            <?php if (!empty($item->field_event_image["und"][0]["uri"])) { ?>
            <figure class="bg_black responsive_image events_item_figure events_list_item_figure">
              <picture class="events_item_picture events_list_item_picture">
                <img class="events_item_image events_list_item_image" src="<?php echo image_style_url("event_list", $item->field_event_image["und"][0]["uri"]); ?>" alt="<?php echo $item->field_event_image["und"][0]["alt"]; ?>">
              </picture>
            </figure>
            <?php } ?>
            <div class="events_item_content events_list_item_content">
              <time class="events_item_date events_list_item_date" datetime="<?php echo date("c",$start); ?>">
                <span class="events_list_item_day"><?php echo $draw_date; ?></span> <span class="events_list_item_time"><?php echo $draw_time; ?></span>
              </time>
              <h3 class="events_item_title events_list_item_title">
                <a class="events_item_title_link events_list_item_title_link" href="<?php echo url("node/".$item->nid); ?>"><?php echo htmlspecialchars(htmlspecialchars_decode($item->title)); ?></a>
              </h3>
              <div class="events_item_description events_list_item_description">
                <p><?php echo $item->field_event_subtitle["und"][0]["value"]; ?></p>
              </div>
            </div>
          </article>
          <?php
            $x++;
              if ($x % 4 == 0) {
                echo '</div>';
              }
            }
            if ($x % 4 !== 0) {
              echo '</div>';
            }
          ?>
        </div>
      </div>
    </div>
  </div>
  <?php if ($x > 4) { ?>
  <footer class="events_list_footer">
    <div class="fs-row">
      <div class="fs-cell">
        <button class="load_more_button load_more_button_full events_list_more">Load More Events</button>
      </div>
    </div>
  </footer>
  <?php } ?>
</section>
<?php
    }
  } else {
    echo " ";
  }
  