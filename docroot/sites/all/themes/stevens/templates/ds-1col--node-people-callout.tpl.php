<?php
  global $people_callout_count;
  $people_callout_count++;
?>
<section class="component people background_white">
	<div class="fs-row">
		<header class="fs-cell component_header">
			<h2 class="component_heading"><?php echo $title; ?></h2>
		</header>
	</div>
	<div class="people_callouts">
		<div class="carousel_controls people_controls people_controls_<?php echo $people_callout_count; ?>">
			<button class="carousel_control people_control people_control_previous people_control_previous_<?php echo $people_callout_count; ?>">Previous</button>
			<button class="carousel_control people_control people_control_next people_control_next_<?php echo $people_callout_count; ?>">Next</button>
		</div>
		<div class="fs-row">
			<div class="fs-cell">
				<div class="people_carousel js-carousel" data-carousel-options='{"contained":false,"matchHeight":true,"show":{"500px":2,"980px":4},"controls":{"container":".people_controls_<?php echo $people_callout_count; ?>","previous":".people_control_previous_<?php echo $people_callout_count; ?>","next":".people_control_next_<?php echo $people_callout_count; ?>"}}'>
          <?php
            $item_count = count($content['field_people_items']['#items']);
            for ($i = 0; $i < $item_count; $i++) {
              $item = $content['field_people_items'][$i]['entity']['field_collection_item'];
              $item = array_pop($item);
              $image = $item['field_people_items_image']['#items'][0]['uri'];
              $alt = $item['field_people_items_image']['#items'][0]['alt'];
          ?>
          <article class="people_callout">
          	<?php if (isset($item['field_people_items_link'])) { ?><a href="<?php echo stevens_url_fix($item['field_people_items_link']['#items'][0]); ?>" class="block_link"><?php } ?>
          		<figure class="people_callout_figure">
          			<picture class="responsive_image people_callout_image">
          				<!--[if IE 9]><video style="display: none;"><![endif]-->
          				<source media="(min-width: 740px)" srcset="<?php echo image_style_url('people_250x376',$image); ?>">
          				<!--[if IE 9]></video><![endif]-->
          				<img src="<?php echo image_style_url('people_250x376',$image); ?>" alt="<?php print check_plain($alt); ?>" draggable="false">
          			</picture>
          		</figure>
          		<div class="people_callout_content">
          			<h3 class="people_callout_name"><?php echo $item['field_people_items_name']['#items'][0]['value']; ?></h3>
                <?php if (isset($item['field_people_items_position'])) { ?>
          			<h4 class="people_callout_title"><?php echo $item['field_people_items_position']['#items'][0]['value']; ?></h4>
                <?php } ?>
          		</div>
          		<?php if (isset($item['field_people_items_link'])) { ?></a><?php } ?>
          </article>
          <?php
            }
          ?>
				</div>
			</div>
		</div>
	</div>
</section>
