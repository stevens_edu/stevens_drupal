<section class="component statistics">
	<div class="fs-row">
		<header class="fs-cell component_header">
			<h2 class="component_heading"><?php echo $title; ?></h2>
		</header>
	</div>
	<div class="statistics_callouts">
		<div class="carousel_controls statistics_controls">
			<button class="carousel_control statistics_control statistics_control_previous">Previous</button>
			<button class="carousel_control statistics_control statistics_control_next">Next</button>
		</div>
		<div class="fs-row">
			<div class="fs-cell">
				<div class="statistics_carousel js-carousel" data-carousel-options='{"contained":false,"matchHeight":true,"show":{"500px":2,"980px":4},"controls":{"container":".statistics_controls","previous":".statistics_control_previous","next":".statistics_control_next"}}'>
          <?php
            $colors = array('blue','red','orange','green','teal');
            $item_count = count($content['field_statistics_reference']['#items']);
            for ($i = 0; $i < $item_count; $i++) {
              $item = $content['field_statistics_reference'][$i]['node'];
              $item = array_shift($item);
          ?>
          <article class="statistics_callout <?php echo $colors[$i % 5]; ?>">
          	<header class="statistics_callout_header">
          		<h2 class="statistics_callout_heading"><span class="large"><?php echo $item['field_statistic']['#items'][0]['value']; ?></span><?php if (isset($item['field_statistic_qualifier'])) { echo $item['field_statistic_qualifier']['#items'][0]['value']; } ?></h2>
          	</header>
          	<div class="statistics_callout_text">
          		<p><?php echo $item['field_statistic_description']['#items'][0]['value']; ?></p>
          	</div>
            <?php if (isset($item['field_statistic_link'])) { ?>
							<?php echo stevens_theme_links($item['field_statistic_link']['#items'][0],"statistics_callout_link"); ?>
            <?php } ?>
          </article>
          <?php
            }
          ?>
				</div>
			</div>
		</div>
	</div>
</section>
