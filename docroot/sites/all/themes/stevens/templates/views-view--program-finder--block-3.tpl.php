<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
 if (isset($view->exposed_raw_input['field_program_keywords_value'])) {
   $filtered_query = "biology";

   $allResults = views_get_view_result('program_finder', 'block_3');

   $query = new EntityFieldQuery;
   $query->entityCondition("entity_type","node")
         ->entityCondition("bundle","search_terms")
         ->propertyCondition("status",NODE_PUBLISHED)
         ->propertyCondition('title', '%' . $filtered_query . '%', 'like');
   $result = $query->range(0,1)->execute();
   if (count($result)) {
     $nid = key($result['node']);
     $search_node = node_load($nid);
     if (isset($search_node->field_st_grad_program[LANGUAGE_NONE][0]['target_id'])) {
       $priority_nid = $search_node->field_st_grad_program[LANGUAGE_NONE][0]['target_id'];
     }
   }
 }
 if (isset($priority_nid) && isset($allResults) && count($allResults)) {
   $priority_node = node_load($priority_nid);
   print render(node_view($priority_node,'teaser'));
   foreach ($allResults as $item) {
     if($item->nid != $priority_nid) {
       $item_node = node_load($item->nid);
       print render(node_view($item_node,'teaser'));
     }
   }
 } else {
   if ($rows) {
     print $rows;
   } elseif ($empty) {
     print $empty;
   }
 }

?>



<?php /* class view */ ?>
