<?php
  // Get categories
  $categories = array();
  if (isset($content["field_news_category"]["#items"])) {
    foreach ($content["field_news_category"]["#items"] as $item) {
      $categories[] = $item["taxonomy_term"]->name;
    }
  }
  $time = strtotime($content["field_news_date"]["#items"][0]["value"]." GMT");
?>
<header class="news_detail_header">
  <time class="news_item_date news_list_item_date" datetime="<?php echo date("Y-m-d",$time); ?>"><?php echo date("j M Y",$time); ?></time>
  <?php if (count($categories)) { ?>
  <span class="light_uppercase color_gray"><?php echo implode(", ",$categories); ?></span>
  <?php } ?>
  <h2 class="color_red news_detail_title"><?php echo $title; ?></h2>
  <?php if (!empty($content["field_news_subtitle"]["#items"][0]["value"])) { ?>
  <h3 class="color_gray_dark calendar_detail_header_subtitle"><?php echo htmlspecialchars($content["field_news_subtitle"]["#items"][0]["value"]); ?></h3>
  <?php } ?>
</header>
<div class="service-links">
  <div class="service-label">Share this story</div>
  <ul class="links">
    <li class="service-links-facebook first">
      <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(stevens_current_url()); ?>" target="_blank" title="Share on Facebook" class="service-links-facebook" rel="nofollow">
        <img typeof="foaf:Image" src="/sites/all/modules/contrib/service_links/images/facebook.png" alt="Facebook logo"> Facebook
      </a>
    </li>
    <li class="service-links-twitter">
      <a href="https://twitter.com/home?status=<?php echo urlencode(stevens_current_url()); ?>" target="_blank" title="Share this on Twitter" class="service-links-twitter" rel="nofollow">
        <img typeof="foaf:Image" src="/sites/all/modules/contrib/service_links/images/twitter.png" alt="Twitter logo"> Twitter
      </a>
    </li>
    <li class="service-links-linkedin last">
      <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo urlencode(stevens_current_url()); ?>&amp;title=<?php echo urlencode($title) ?>" target="_blank" title="Publish this post to LinkedIn" class="service-links-linkedin" rel="nofollow">
        <img typeof="foaf:Image" src="/sites/all/modules/contrib/service_links/images/linkedin.png" alt="LinkedIn logo"> LinkedIn
      </a>
    </li>
  </ul>
</div>
<?php
  if (count($content["field_news_featured_image"]["#items"])) {
?>
<div class="content_figure content_figure_wide">
  <img class="content_figure_image" src="<?php echo image_style_url("news_detail",$content["field_news_featured_image"]["#items"][0]["uri"]); ?>" alt="<?php echo $content["field_news_featured_image"]["#items"][0]["alt"]; ?>">
  <?php if (!empty($content["field_news_featured_caption"]["#items"][0]["value"])) { ?>
  <div class="content_figure_caption"><?php echo $content["field_news_featured_caption"]["#items"][0]["safe_value"]; ?></div>
  <?php } ?>
</div>
<?php
  }

  echo render($content["body"]);
