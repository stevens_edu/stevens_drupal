<?php
  $image = $content['field_highlight_group_image']['#items'][0]['uri'];
  // 470x470 294x165
?><section class="component highlight_group js-background"
	data-background-options='{"source":{"0px":"<?php echo image_style_url('highlight_group_470x352',$image); ?>","740px":"<?php echo image_style_url('highlight_group_980x500',$image); ?>","980px":"<?php echo image_style_url('highlight_group_1220x600',$image); ?>","1220px":"<?php echo image_style_url('highlight_group_1440x600',$image); ?>"}}'>
	<div class="fs-row relative">
		<header class="fs-cell component_header highlight_group_header">
			<h2 class="component_heading highlight_group_heading"><?php echo $title; ?></h2>
      <?php if(isset($content['body'])) { ?>
      <div class="component_content highlight_group_content"><?php print render($content['body']); ?></div>
			<?php } ?>
		</header>
	</div>
	<div class="highlight_group_carousel_container">
		<div class="carousel_controls highlight_group_controls">
			<button class="carousel_control highlight_group_control highlight_group_control_previous">Previous</button>
			<button class="carousel_control highlight_group_control highlight_group_control_next">Next</button>
		</div>
		<div class="fs-row">
			<div class="fs-cell">
				<div class="highlight_group_carousel js-carousel" data-carousel-options='{"contained":false,"matchHeight":true,"show":{"740px":2,"980px":3,"1220px":1},"controls":{"container":".highlight_group_controls","previous":".highlight_group_control_previous","next":".highlight_group_control_next"}}'>
          <?php
            $item_count = count($content['field_highlight_group_item']['#items']);
            $colors = array('teal','orange','red','white','black','gray');
            shuffle($colors);
            for ($i = 0; $i < $item_count; $i++) {
              $item = $content['field_highlight_group_item'][$i]['entity']['field_collection_item'];
              $item = array_pop($item);
              $itemsize = 'small';
              $image = "";
              if(isset($item['field_highlight_item_image'])) {
                $itemsize = 'large';
                $image = $item['field_highlight_item_image']['#items'][0]['uri'];
              }
          ?>
          <article class="callout highlight_group_callout <?php echo $itemsize; ?> <?php echo $colors[$i % 6]; ?>">
            <?php if ($image) { ?>
            <figure class="highlight_group_callout_figure">
              <picture class="responsive_image highlight_group_callout_image">
                <!--[if IE 9]><video style="display: none;"><![endif]-->
                <source media="(min-width: 1220px)" srcset="<?php echo image_style_url('highlight_item_470x470',$image); ?>">
                <!--[if IE 9]></video><![endif]-->
                <img src="<?php echo image_style_url('highlight_item_294x165',$image); ?>" alt="<?php echo $item['field_highlight_item_image']['#items'][0]['alt']; ?>" draggable="false">
              </picture>
            </figure>
            <?php } ?>
          	<div class="callout_contain highlight_group_callout_contain">
          		<h3 class="highlight_group_callout_heading"><?php echo stevens_trim_length($item['field_highlight_item_title']['#items'][0]['value'],25); ?></h3>
          		<div class="callout_content highlight_group_callout_content">
          			<?php echo stevens_trim_length($item['field_highlight_item_description']['#items'][0]['value'],160); ?>
          		</div>
              <?php
                if (isset($item['field_highlight_item_link'])) {
                  echo stevens_theme_links($item['field_highlight_item_link']['#items'][0],"callout_link highlight_group_callout_link");
                }
              ?>
          	</div>
          </article>
          <?php
            }
          ?>
				</div>
			</div>
		</div>
	</div>
</section>