<?php
	$image = $params['image'];
?>
<article class="js-equalize_child numbers_item theme_<?php echo $params['color']; ?> size_large">
	<figure class="numbers_item_figure">
		<picture class="numbers_item_picture">
			<!--[if IE 9]><video style="display: none;"><![endif]-->
			<source media="(min-width: 980px)" srcset="<?php echo image_style_url('statistic_image',$image); ?>">
			<!--[if IE 9]></video><![endif]-->
			<img class="numbers_item_image" src="<?php echo $base_path . $directory; ?>/images/placeholder.png" alt="">
		</picture>
	</figure>
	<div class="numbers_item_content">
		<header class="numbers_item_header">
			<h2 class="numbers_item_heading"><span class="large"><?php echo $params['statistic']; ?></span><?php echo $params['qualifier']; ?></h2>
		</header>
		<div class="numbers_item_body">
			<div class="numbers_item_description">
				<?php echo $params['description']; ?>
			</div>
			<?php if(strlen($params['url'])!==0) { ?>
			<a class="numbers_item_link" href="<?php echo $params['url']; ?>"><?php echo $params['link']; ?></a>
			<?php } ?>
		</div>
	</div>
</article>
