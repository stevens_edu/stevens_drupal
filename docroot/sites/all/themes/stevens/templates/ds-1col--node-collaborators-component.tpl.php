<article class="collaborators clearfix">
  <div class="fs-row">
    <div class="fs-cell fs-lg-12">
      <h2 class="component_heading"><?php echo $title; ?></h2>
      <?php if (isset($content['body'])) { ?>
      <div class="typography">
        <div class="intro">
          <?php echo render($content['body']); ?>
        </div>
      </div>
      <?php } ?>
      <?php if (isset($content['field_collaborators_link'])) { ?>
      <?php echo stevens_theme_links($content['field_collaborators_link']['#items'][0],"callout_link"); ?>
      <?php } ?>
      <div class="logo_container">
        <?php
          $item_count = count($content['field_collaborators_orgs']['#items']);
          for ($i = 0; $i < $item_count; $i++) {
            $item = $content['field_collaborators_orgs'][$i]['entity']['field_collection_item'];
            $item = array_pop($item);
            $image = $item['field_collaborators_logo']['#items'][0]['uri'];
        ?>
        <div class="fs-cell-contained fs-xs-full fs-sm-half fs-md-half fs-lg-fourth logo_col">
          <div class="logo_wrap">
            <?php if (isset($item['field_collaborators_org_link'])) { ?><a href="<?php echo stevens_url_fix($item['field_collaborators_org_link']['#items'][0]); ?>"><?php } ?>
              <div class="logo">
                <img src="<?php echo image_style_url('collaborator_logo',$image); ?>" alt="<?php echo $item['field_collaborators_logo']['#items'][0]['alt']; ?>"/>
              </div>
              <p><?php echo $item['field_collaborators_name']['#items'][0]['value']; ?></p>
            <?php if (isset($item['field_collaborators_org_link'])) { ?></a><?php } ?>
          </div>
        </div>
        <?php
          }
        ?>
      </div>
    </div>
  </div>
</article>
