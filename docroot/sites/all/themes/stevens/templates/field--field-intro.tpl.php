<?php
  foreach ($items as $delta => $item) {
    $intro = strip_tags($item['#markup'],'<em><strong><a>');
    $intro = preg_replace('/\s+/', ' ', $intro);
    $intro = trim($intro, " \t\n\r\0\x0B");
    if (trim($intro)) {
      echo '<p class="intro">';
      echo $intro;
      echo '</p>';
    }
  }
?>
