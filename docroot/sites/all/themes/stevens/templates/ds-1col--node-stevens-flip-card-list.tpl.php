<?php
  $colors = array(
    'grey',
    'red',
    'orange',
    'blue',
    'teal',
  );
  $idx = 0;
?>
<div class="component flip-cards flip-cards--<?php echo $nid ?>">
  <div class="fs-row">
    <header class="fs-cell component_header">
      <h2 class="component_heading"><?php echo $title; ?></h2>
    </header>
  </div>

  <?php if (!empty($termlist)): ?>  
  <div class="fs-row">
    <div class="fs-cell" style="overflow:visible;">
      <div class="background_gray_pale component_controls calendar_controls">
      <form class="component_filter_form">
        <fieldset class="calendar_filter_fieldset">
          <label class="visually_hidden" for="filter_flip_select_<?php echo $nid ?>">Filter Flip Cards</label>
          <select name="flip-filter-select--<?php echo $nid ?>" id="filter_flip_select_<?php echo $nid ?>" class="fs-dropdown-elements js-dropdown" data-dropdown-options='{"label":"Filter by Category", "customClass": "red_dropdown filter_category"}'>
            <?php foreach ($termlist as $term) { ?>
            <option <?php if (isset($term->selected)) print 'selected'; ?> value="<?php echo $term->tid ?>"><?php echo check_plain($term->name) ?></option>
            <?php } ?>
          </select>
        </fieldset>

        <input type="submit" value="Apply Filter"/>

        <?php if ($filtered): ?>
	<a class="link_arrow calendar_controls_link" href="<?php print $current_url ?>">Clear Filter</a>
        <?php endif; ?>
        <?php if ($_GET["month"] || $_GET["category"]) { ?>
        <a href="./" class="bold_uppercase controls_filter_clear calendar_filter_clear">Clear Filters</a>
        <?php } ?>
      </form>
      </div>
    </div>
  </div>
  <?php endif; ?>

  <div class="flip-cards--callouts">
    <div class="fs-row">
      <div class="fs-cell">
        <div class="flip-card--container">
          <?php foreach ($flipcards as $flipcard): ?>
          <article class="flip-card--callout <?php print $colors[$idx % 5]; $idx++; ?>">
            <div class="flip-card--faces">
              <div class="flip-card--front">
                <div class="flip-card--header-img">
                  <?php print render(field_view_field('node', $flipcard->raw(), 'field_header_image', 'default')); ?>
                </div>
                <?php if (!empty($flipcard->field_statistic_figure->value()) || !empty($flipcard->field_statistic_label->value())): ?>
                <div class="flip-card--stat">
                  <?php if ($flipcard->field_statistic_label_position->value() == "After"): ?>
                  <span class="flip-card--stat-figure"><?php print $flipcard->field_statistic_figure->value() ?> </span>
                  <span class="flip-card--stat-label"><?php print $flipcard->field_statistic_label->value() ?></span>
                  <?php else: ?>
                  <span class="flip-card--stat-label"><?php print $flipcard->field_statistic_label->value() ?></span>
                  <span class="flip-card--stat-figure"><?php print $flipcard->field_statistic_figure->value() ?> </span>
                  <?php endif; ?>
                </div>
                <?php endif; ?>
                <h1 class="flip-card--title"><?php print $flipcard->field_front_title->value(); ?></h1>
                <div class="flip-card--source">
                  <span class="flip-card--source-name"><?php print $flipcard->field_source_name->value() ?></span><br/>
                  <span class="flip-card--source-year"><?php print $flipcard->field_source_year_attribution->value() ?></span>
                </div>
              </div>
              <div class="flip-card--back">
                <h1 class="flip-card--reverse-title"><?php print $flipcard->field_rev_title->value() ?></h1>
                <div class="flip-card--body"><?php print $flipcard->body->value()['safe_value'] ?></div>
                <span class="flip-card--link"><a href="<?php print $flipcard->field_flip_link->value()['url'] ?>" class="link_arrow"><?php print check_plain($flipcard->field_flip_link->value()['title']) ?></a></span>
              </div>
            </div>
            <div class="flip-card--controls">
              <button class="icon_only icon_card_flip"><span class="icon_text">Toggle Flip</span></button>
            </div>
          </article>
          <?php endforeach; ?>
        </div>
      </div>
		</div>
	</div>
</div>
