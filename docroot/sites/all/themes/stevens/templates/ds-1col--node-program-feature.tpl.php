<?php
  $image = $content['field_program_feature_image']['#items'][0]['uri'];
?>
<div class="page_header_component page_header_program">
	<div class="fs-row">
		<div class="fs-cell">
			<figure class="page_header_figure">
				<picture class="responsive_image page_header_image">
					<!--[if IE 9]><video style="display: none;"><![endif]-->
          <source media="(min-width: 1220px)" srcset="<?php echo image_style_url('image_feature_1280x420',$image); ?>">
          <source media="(min-width: 980px)" srcset="<?php echo image_style_url('image_feature_1024x336',$image); ?>">
          <source media="(min-width: 740px)" srcset="<?php echo image_style_url('image_feature_700x394',$image); ?>">
          <source media="(min-width: 0px)" srcset="<?php echo image_style_url('image_feature_470x264',$image); ?>">
					<!--[if IE 9]></video><![endif]-->
					<img src="<?php echo image_style_url('image_feature_470x264',$image); ?>" alt="<?php echo $content['field_program_feature_image']['#items'][0]['alt']; ?>" draggable="false">
				</picture>
			</figure>
			<div class="page_header_program_bar">
				<div class="page_header_program_bar_inner">
					<div class="page_header_program_bar_availability">
						<div class="page_header_program_bar_offerings">
							<span class="page_header_program_bar_label">This Program Is Available:</span>
              <?php if ($content['field_program_feature_campus']['#items'][0]['value'] == '1') { ?>
              <span class="page_header_program_bar_link on-campus">On Campus</span>
              <?php } ?>
              <?php if ($content['field_program_feature_online']['#items'][0]['value'] == '1') { ?>
							<span class="page_header_program_bar_link online">Online</span>
              <?php } ?>
              <?php if ($content['field_program_feature_corporate']['#items'][0]['value'] == '1') { ?>
							<span class="page_header_program_bar_link corporate">Corporate</span>
              <?php } ?>
						</div>
					</div>
          <?php echo stevens_theme_links($content['field_program_feature_link']['#items'][0],"link_button_lg background_red page_header_program_bar_button",'Apply Now'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
