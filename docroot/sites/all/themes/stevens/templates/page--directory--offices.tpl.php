<?php
  $links = menu_load_links("menu-directory-tabs");
  include ($directory . "/includes/header.php");
?>
<div class="page_wrapper js-navigation_push">
  <main id="page" class="page" role="main">
    <header class="page_header">
    	<div class="fs-row">
        <?php
          if(user_is_logged_in()) {
        ?>
        <div class="fs-cell">
          <?php print $messages; ?>
          <?php print render($page['help']); ?>
          <?php if ($tabs): ?>
            <div class="tabs"><?php print render($tabs); ?></div>
          <?php endif; ?>
          <?php if ($action_links): ?>
            <ul class="action-links"><?php print render($action_links); ?></ul>
          <?php endif; ?>
        </div>
        <?php
          }
        ?>
    		<div class="fs-cell typography">
    			<h1><?php echo $title; ?></h1>
        </div>
        <div class="fs-cell">
          <?php if ($breadcrumb): ?>
            <?php print $breadcrumb; ?>
          <?php endif; ?>
        </div>
        <div class="fs-cell">
          <div class="typography directory_header">
            <p>For general inquiries, please call <a href="tel:+12012165000">(201) 216-5000</a>.</p>
            <p>Members of the Stevens community may search for contact information using the <a href="https://web.stevens.edu/peoplefinder">People Finder</a>.</p>
          </div>
          <div class="page_tabs">
            <?php foreach ($links as $link) { ?>
            <a class="page_tab<?php if ($link["link_path"] == $_GET["q"]) { ?> page_tab_active<?php } ?>" href="/<?php echo drupal_get_path_alias($link["link_path"]); ?>"><?php echo $link["link_title"]; ?></a>
            <?php } ?>
          </div>
    			<hr>
    		</div>
    	</div>
      <?php
        $view = views_get_view('offices');
        $view->set_display('attachment_1');
        $view->execute();
        print $view->render();
      ?>

    </header>
  	<!-- Page Content -->
  	<section class="page_content_area">
  		<div class="fs-row">

        <div class="page_content fs-cell">
          <?php
            if(!user_is_logged_in()) {
              print $messages;
            }
          ?>
            <?php if ($page['content']): ?>
              <?php print render($page['content']); ?>
            <?php endif; ?>

          <?php if ($page['content_components']): ?>
            <!-- In-Content Callouts -->
            <aside class="callouts content_callouts">
                <?php print render($page['content_components']); ?>
            </aside><!-- END: content_callouts -->
          <?php endif; ?>
        </div><!-- END: page_content -->

      </div>
    </section><!-- END: page_content_area -->
    <?php if ($page['content_bottom']): ?>
      <!-- Full Width Components -->
      <section class="components">
        <?php print render($page['content_bottom']); ?>
      </section><!-- END: components -->
    <?php endif; ?>
  </main><!-- END: page -->
  <?php
   include ($directory . "/includes/footer.php");
  ?>
