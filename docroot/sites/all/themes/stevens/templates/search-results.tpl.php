<?php
  if ($search_results) {
?>
<div class="search_result_message">
  <p>Your search returned <?php echo $search_totals; ?> result<?php if ($search_totals != 1) { ?>s<?php } ?>.</p>
</div>
<div class="search_results" role="list">
  <?php echo $search_results; ?>
</div>
<?php
    echo $pager;
  } else {
  	echo "<h2>".t('Your search yielded no results')."</h2>";
    echo search_help('search#noresults', drupal_help_arg());
  }