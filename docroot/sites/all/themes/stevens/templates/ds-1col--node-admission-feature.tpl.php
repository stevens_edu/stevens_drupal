<div class="page_header_component page_header_admissions">
	<div class="page_header_admissions_carousel js-carousel" data-carousel-options='{"matchHeight":true,"pagination":false,"autoAdvance":true,"autoTime": 5000}'>
    <?php
      foreach($content['field_admission_carousel_images']['#items'] as $item) {
        $image = $item['uri'];
    ?>
		<article class="page_header_admissions_carousel_callout">
			<figure class="page_header_admissions_figure js-background" data-background-options='{"source":{"0px":"<?php echo image_style_url('admission_feature_500x281',$image); ?>","500px":"<?php echo image_style_url('admission_feature_740x480',$image); ?>"}}'></figure>
		</article>
    <?php
      }
    ?>
	</div>
	<div class="page_header_admissions_content">
		<div class="page_header_admissions_content_inner">
			<h2 class="page_header_admissions_heading"><?php echo $title; ?></h2>
			<div class="page_header_admissions_buttons">
        <?php
          $colors = array('red','teal','orange','gray');
          $i = 0;
          foreach($content['field_admission_action_links']['#items'] as $link) {
            echo stevens_theme_links($link,'page_header_admissions_button background_' . $colors[$i]);
            $i++;
          }
        ?>
			</div>
			<div class="page_header_admissions_links">
        <?php
          foreach($content['field_admission_audience_links']['#items'] as $link) {
						echo stevens_theme_links($link, 'page_header_admissions_link');
          }
        ?>
			</div>
		</div>
	</div>
  <?php $image = $content['field_admission_bg_image']['#items'][0]['uri']; ?>
	<div class="page_header_admissions_background_container">
		<figure class="page_header_admissions_background_figure js-background" data-background-options='{"source":{"0px":"<?php echo image_style_url('admission_feature_500x281',$image); ?>","500px":"<?php echo image_style_url('admission_feature_740x480',$image); ?>"}}'></figure>
	</div>
</div>
