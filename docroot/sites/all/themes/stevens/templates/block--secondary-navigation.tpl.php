<div class="secondary_nav">
  <div class="secondary_nav_item nav_hover nav_button_hover">
    <button class="secondary_nav_button apply_button">Apply</button>
    <nav class="secondary_nav_links">
      <?php
        $apply_menu = menu_navigation_links('menu-apply-nav');
        foreach ($apply_menu as $menu_item) {
          echo l($menu_item['title'], $menu_item['href'], array('attributes' => array('class' => array('secondary_nav_link'))));
        }
      ?>
    </nav>
  </div>
  <div class="secondary_nav_item nav_hover nav_button_hover">
    <button class="secondary_nav_button giving_button">Giving</button>
    <nav class="secondary_nav_links">
      <?php
        $giving_menu = menu_navigation_links('menu-giving');
        foreach ($giving_menu as $menu_item) {
          echo l($menu_item['title'], $menu_item['href'], array('attributes' => array('class' => array('secondary_nav_link'))));
        }
      ?>
    </nav>
  </div>
  <div class="secondary_nav_item nav_hover nav_button_hover">
    <button class="secondary_nav_button i_am_a_button">I am a&hellip;</button>
    <nav class="secondary_nav_links">
      <?php
        $audience_menu = menu_navigation_links('menu-audience-navigation');
        foreach ($audience_menu as $menu_item) {
          echo l($menu_item['title'], $menu_item['href'], array('attributes' => array('class' => array('secondary_nav_link'))));
        }
      ?>
      <a href="https://my.stevens.edu" class="secondary_nav_link my_stevens_link">Log in to mySTEVENS</a>
    </nav>
  </div>

  <div class="secondary_nav_item nav_hover nav_button_hover">
    <button class="secondary_nav_button search_button">Search</button>
    <div class="search_dropdown search_module">
      <?
         $block = module_invoke('search', 'block_view', 'search');
         echo render($block["content"]);
      ?>
    </div>
  </div>
</div>
