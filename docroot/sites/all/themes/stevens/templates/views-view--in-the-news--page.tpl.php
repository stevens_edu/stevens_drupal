<div class="news_list news_list_in_content">
  <?php
    if ($rows) {
  ?>
  <div class="view-content">
    <?php echo $rows; ?>
  </div>
  <?php
    } elseif ($empty) {
  ?>
  <div class="view-empty">
    <?php echo $empty; ?>
  </div>
  <?php
    }
    if ($pager) {
      echo $pager;
    }
  ?>
</div>