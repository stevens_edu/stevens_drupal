<div class="news_list news_sidebar_list news_in_list">
	<header class="news_sidebar_header news_in_header">
		<h2 class="bold_uppercase color_red news_sidebar_heading news_in_heading"><?php print $view->human_name; ?></h2>
	</header>
	<div class="news_sidebar_body news_in_body">
    <?php if ($rows): ?>
      <div class="view-content">
        <?php print $rows; ?>
      </div>
    <?php elseif ($empty): ?>
      <div class="view-empty">
        <?php print $empty; ?>
      </div>
    <?php endif; ?>
	</div>
	<footer class="news_sidebar_footer news_in_footer">
		<a class="news_sidebar_footer_button news_in_footer_button" href="/news/in-the-news">All Stevens In The News</a>
	</footer>
</div>
