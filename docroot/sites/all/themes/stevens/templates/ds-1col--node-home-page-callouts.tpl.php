<section class="component homepage_callouts">
	<div class="fs-row">
		<div class="fs-cell fs-lg-7 homepage_callouts_spotlight">
			<div class="homepage_callouts_spotlight_inner">
				<h2 class="homepage_callouts_heading">Spotlight</h2>
        <?php $image = $content['field_hpc_spotlight_image']['#items'][0]['uri']; ?>
        <article class="homepage_callout_spotlight">
        	<figure class="homepage_callout_spotlight_figure">
        		<picture class="responsive_image homepage_callout_spotlight_image">
        			<!--[if IE 9]><video style="display: none;"><![endif]-->
        			<source media="(min-width: 740px)" srcset="<?php echo image_style_url('home_page_callout_300x300',$image); ?>">
        			<source media="(min-width: 0px)" srcset="<?php echo image_style_url('home_page_callout_100x100',$image); ?>">
        			<!--[if IE 9]></video><![endif]-->
        			<img src="<?php echo image_style_url('home_page_callout_100x100',$image); ?>" alt="<?php echo $content['field_hpc_spotlight_image']['#items'][0]['alt']; ?>" draggable="false">
        		</picture>
        	</figure>
        	<h2 class="homepage_callout_spotlight_heading"><?php echo $content['field_hpc_spotlight_heading']['#items'][0]['value']; ?></h2>
        	<div class="homepage_callout_spotlight_content">
        		<div class="homepage_callout_spotlight_text">
        			<?php echo render($content['field_hpc_spotlight_description']); ?>
        		</div>
						<?php
							if (!empty($content['field_hpc_spotlight_link']['#items'][0])) {
								echo stevens_theme_links($content['field_hpc_spotlight_link']['#items'][0],'homepage_callout_spotlight_link');
							}
						?>
        	</div>
        </article>
			</div>
		</div>
		<div class="fs-cell fs-lg-4 fs-lg-push-1 homepage_callouts_people">
			<div class="homepage_callouts_people_inner">
				<h2 class="homepage_callouts_heading">People of Stevens</h2>
				<div class="js-carousel" data-carousel-options='{"matchHeight":true,"show":{"740px":2,"980px":1}}'>
          <?php
            $item_count = count($content['field_hpc_people']['#items']);
            for ($i = 0; $i < $item_count; $i++) {
              $item = $content['field_hpc_people'][$i]['entity']['field_collection_item'];
              $item = array_pop($item);
              $image = $item['field_hpc_people_image']['#items'][0]['uri'];
              $alt = $item['field_hpc_people_image']['#items'][0]['alt'];

              if (!empty($item["field_hpc_people_link"])) {
                echo '<a class="homepage_callout_people" href="'.stevens_url_fix($item["field_hpc_people_link"]["#items"][0]).'">';
              } else {
                echo '<article class="homepage_callout_people">';
              }
          ?>
        	<figure class="homepage_callout_people_figure">
        		<picture class="responsive_image homepage_callout_people_image">
        			<!--[if IE 9]><video style="display: none;"><![endif]-->
        			<source media="(min-width: 1220px)" srcset="<?php echo image_style_url('home_page_callout_400x300',$image); ?>">
        			<source media="(min-width: 980px)" srcset="<?php echo image_style_url('home_page_callout_322x300',$image); ?>">
        			<source media="(min-width: 740px)" srcset="<?php echo image_style_url('home_page_callout_400x300',$image); ?>">
        			<source media="(min-width: 500px)" srcset="<?php echo image_style_url('home_page_callout_740x555',$image); ?>">
        			<source media="(min-width: 0px)" srcset="<?php echo image_style_url('home_page_callout_500x375',$image); ?>">
        			<!--[if IE 9]></video><![endif]-->
        			<img src="<?php echo image_style_url('home_page_callout_500x375',$image); ?>" alt="<?php print check_plain($alt); ?>" draggable="false">
        		</picture>
        		<figcaption class="homepage_callout_people_content">
        			<h3 class="homepage_callout_people_heading"><?php echo $item['field_hpc_people_name']['#items'][0]['value']; ?></h3>
        			<div class="homepage_callout_people_text">
        				<?php echo render($item['field_hpc_people_description']); ?>
        			</div>
        		</figcaption>
          </figure>
          <?php
              if (!empty($item["field_hpc_people_link"])) {
                echo '</a>';
              } else {
                echo '</article>';
              }
            }
          ?>
				</div>
			</div>
		</div>
	</div>
</section>
