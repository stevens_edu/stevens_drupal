<section class="component topic_group">
	<div class="fs-row">
		<div class="fs-cell topic_group_callouts">
      <div class="callout topic_group_callout carousel_black">
      	<header class="topic_group_callout_header">
      		<h3 class="topic_group_callout_heading"><?php echo $title; ?></h3>
      		<div class="topic_group_callout_text typography">
      			<?php echo stevens_trim_length(render($content['body']),200); ?>
      		</div>
          <?php
            if (isset($content['field_topic_group_link'])) {
					   echo stevens_theme_links($content['field_topic_group_link']['#items'][0],"topic_group_callout_link");
            }
          ?>
      	</header>
      	<div class="topic_group_carousel_cards">
      		<div class="topic_group_carousel js-carousel" data-carousel-options='{"matchHeight":true}'>
            <?php
              $item_count = count($content['field_topic_group_cards']['#items']);
              for ($i = 0; $i < $item_count; $i++) {
                $item = $content['field_topic_group_cards'][$i]['entity']['field_collection_item'];
                $item = array_pop($item);
                $image = $item['field_topic_group_image']['#items'][0]['uri'];
            ?>
            <article class="topic_group_carousel_card background_<?php echo $item['field_topic_group_color']['#items'][0]['value']; ?>">
              <figure class="topic_group_carousel_card_figure">
                <picture class="responsive_image video_item_picture">
                  <!--[if IE 9]><video style="display: none;"><![endif]-->
                  <source media="(min-width: 1220px)" srcset="<?php echo image_style_url('topic_card_1220',$image); ?>">
                  <source media="(min-width: 980px)" srcset="<?php echo image_style_url('topic_card_980',$image); ?>">
                  <source media="(min-width: 740px)" srcset="<?php echo image_style_url('topic_card_740',$image); ?>">
                  <source media="(min-width: 0px)" srcset="<?php echo image_style_url('topic_card_fallback',$image); ?>">
                  <!--[if IE 9]></video><![endif]-->
                  <img src="<?php echo image_style_url('topic_card_fallback',$image); ?>" alt="<?php echo $item['field_topic_group_image']['#items'][0]['alt']; ?>" draggable="false">
                </picture>
              </figure>
            	<div class="topic_group_carousel_card_content">
            		<h4 class="topic_group_carousel_card_heading"><?php echo stevens_trim_length($item['field_topic_group_title']['#items'][0]['value'],30) ?></h4>
            		<div class="topic_group_carousel_card_text">
            			<?php echo stevens_trim_length($item['field_topic_group_description']['#items'][0]['value'],150); ?>
            		</div>
            		<?php
                  if (isset($item['field_topic_group_links'])) {
                    foreach($item['field_topic_group_links']['#items'] as $item_link) {
                      echo stevens_theme_links($item_link,"topic_group_carousel_card_link");
                    }
                  }
                ?>
            	</div>
            </article>
            <?php
              }
            ?>
      		</div>
      	</div>
      </div>
		</div>
	</div>
</section>
