<div class="mobile_navigation js-mobile_navigation" aria-hidden="true" data-navigation-handle=".js-mobile_handle" data-navigation-content=".js-navigation_push" data-navigation-options='{"gravity":"right","labels":{"open":"Menu","closed":"Menu"},"type":"overlay"}'>
	<div class="mobile_navigation_container">
		<nav class="mobile_primary_nav">
			<?php
				$main_menu = menu_tree_all_data('menu-primary-navigation');
				$i = 0;
				foreach ($main_menu as $menu_item) {
					$i++;
			?>
			<div class="mobile_primary_nav_item">
				<a href="<?php echo url($menu_item['link']['link_path']); ?>" class="mobile_primary_nav_link js-mobile_target_<?php echo $i; ?>"><?php echo $menu_item['link']['link_title']; ?>
					<span class="js-swap" data-swap-target=".js-mobile_target_<?php echo $i; ?>">Expand</span>
				</a>
				<?php if(count($menu_item['below'])) { ?>
				<div class="mobile_primary_nav_children js-mobile_target_<?php echo $i; ?>">
					<?php foreach ($menu_item['below'] as $item) { ?>
					<a href="<?php echo url($item['link']['link_path']); ?>" class="mobile_primary_nav_child_link"><?php echo $item['link']['link_title']; ?></a>
					<?php } ?>
				</div>
				<?php } ?>
			</div>
			<?php
				}
			?>
		</nav>

		<div class="mobile_search_module">
			<?php
      			$block = module_invoke('search', 'block_view', 'search');
      			echo render($block["content"]);
      		?>
		</div>

		<div class="mobile_utility_nav">
			<nav class="mobile_utility_nav_links">
				<?php
					$utility_menu = menu_navigation_links('menu-utilities');
					foreach ($utility_menu as $menu_item) {
						echo l($menu_item['title'], $menu_item['href'], array('attributes' => array('class' => array('mobile_utility_nav_link'))));
					}
				?>
			</nav>
		</div>

		<div class="mobile_secondary_nav">
			<div class="mobile_secondary_nav_item">
				<button class="mobile_secondary_nav_button apply_now_button js-swap" data-swap-target=".mobile_target_apply">Apply Now</button>
				<nav class="mobile_secondary_nav_links mobile_target_apply">
					<?php
						$apply_menu = menu_navigation_links('menu-apply-nav');
						foreach ($apply_menu as $menu_item) {
							echo l($menu_item['title'], $menu_item['href'], array('attributes' => array('class' => array('mobile_secondary_nav_link'))));
						}
					?>
				</nav>
			</div>
			<div class="mobile_secondary_nav_item">
				<button class="mobile_secondary_nav_button find_program_button js-swap" data-swap-target=".mobile_target_program">Find a Program</button>
				<nav class="mobile_secondary_nav_links mobile_target_program">
					<?php
						$find_menu = menu_navigation_links('menu-find-programs');
						foreach ($find_menu as $menu_item) {
							echo l($menu_item['title'], $menu_item['href'], array('attributes' => array('class' => array('mobile_secondary_nav_link'))));
						}
					?>
				</nav>
			</div>
			<div class="mobile_secondary_nav_item">
				<button class="mobile_secondary_nav_button giving_button js-swap" data-swap-target=".mobile_target_giving">Giving</button>
				<nav class="mobile_secondary_nav_links mobile_target_giving">
					<?php
						$giving_menu = menu_navigation_links('menu-giving');
						foreach ($giving_menu as $menu_item) {
							echo l($menu_item['title'], $menu_item['href'], array('attributes' => array('class' => array('mobile_secondary_nav_link'))));
						}
					?>
				</nav>
			</div>
			<div class="mobile_secondary_nav_item">
				<button class="mobile_secondary_nav_button i_am_a_button js-swap" data-swap-target=".mobile_target_iama">I am a&hellip;</button>
				<nav class="mobile_secondary_nav_links mobile_target_iama">
					<?php
						$audience_menu = menu_navigation_links('menu-audience-navigation');
						foreach ($audience_menu as $menu_item) {
							echo l($menu_item['title'], $menu_item['href'], array('attributes' => array('class' => array('mobile_secondary_nav_link'))));
						}
					?>
				</nav>
			</div>
			<a href="https://my.stevens.edu" class="mobile_my_stevens_link">Log in to mySTEVENS</a>
		</div>
	</div>
</div>
