<?php
  // See if we're tagged to a school
  $school_name = array();
  if (is_array($node->field_page_schools["und"])) {
    foreach ($node->field_page_schools["und"] as $item) {
      $school_name[] = $item["taxonomy_term"]->name;
    }
  }
  include ($directory . "/includes/header.php");
?>
<div class="page_wrapper js-navigation_push">
  <main id="page" class="page" role="main">
    <header class="page_header">
    	<div class="fs-row">
        <?php
          if(user_is_logged_in()) {
        ?>
        <div class="fs-cell">
          <?php print $messages; ?>
          <?php print render($page['help']); ?>
          <?php if ($tabs): ?>
            <div class="tabs"><?php print render($tabs); ?></div>
          <?php endif; ?>
          <?php if ($action_links): ?>
            <ul class="action-links"><?php print render($action_links); ?></ul>
          <?php endif; ?>

        </div>
        <?php
          }
        ?>
    		<div class="fs-cell typography">
          <?php if (count($school_name)) { ?>
          <div class="school_heading"><?=implode(", ",$school_name)?></div>
          <?php } ?>
    			<h1><?php echo $title; ?></h1>
        </div>
        <div class="fs-cell">
          <?php if ($breadcrumb): ?>
            <?php print $breadcrumb; ?>
          <?php endif; ?>
        </div>
        <div class="fs-cell">
    			<hr>
    		</div>
    	</div>
      <?php
        if (count($node->field_program_feature_image) && count($node->field_program_feature_link)) {
          $image = $node->field_program_feature_image[LANGUAGE_NONE][0]['uri'];
      ?>
      <div class="page_header_component page_header_program">
      	<div class="fs-row">
      		<div class="fs-cell">
      			<figure class="page_header_figure">
      				<picture class="responsive_image page_header_image">
      					<!--[if IE 9]><video style="display: none;"><![endif]-->
                <source media="(min-width: 1220px)" srcset="<?php echo image_style_url('image_feature_1280x420',$image); ?>">
                <source media="(min-width: 980px)" srcset="<?php echo image_style_url('image_feature_1024x336',$image); ?>">
                <source media="(min-width: 740px)" srcset="<?php echo image_style_url('image_feature_700x394',$image); ?>">
                <source media="(min-width: 0px)" srcset="<?php echo image_style_url('image_feature_470x264',$image); ?>">
      					<!--[if IE 9]></video><![endif]-->
      					<img src="<?php echo image_style_url('image_feature_470x264',$image); ?>" alt="<?php echo $node->field_program_feature_image['#items'][0]['alt']; ?>" draggable="false">
      				</picture>
      			</figure>
      			<div class="page_header_program_bar">
      				<div class="page_header_program_bar_inner">
                <?php
                  $availaility = $node->field_program_feature_campus[LANGUAGE_NONE][0]['value'] . $node->field_program_feature_online[LANGUAGE_NONE][0]['value'] . $node->field_program_feature_corporate[LANGUAGE_NONE][0]['value'];
                  if(strpos($availaility,'1')!==FALSE) {
                ?>
      					<div class="page_header_program_bar_availability">
      						<div class="page_header_program_bar_offerings">
      							<span class="page_header_program_bar_label">This Program Is Available:</span>
                    <?php if ($node->field_program_feature_campus[LANGUAGE_NONE][0]['value'] == '1') { ?>
                    <span class="page_header_program_bar_link on-campus">On Campus</span>
                    <?php } ?>
                    <?php if ($node->field_program_feature_online[LANGUAGE_NONE][0]['value'] == '1') { ?>
      							<span class="page_header_program_bar_link online">Online</span>
                    <?php } ?>
                    <?php if ($node->field_program_feature_corporate[LANGUAGE_NONE][0]['value'] == '1') { ?>
      							<span class="page_header_program_bar_link corporate">Corporate</span>
                    <?php } ?>
      						</div>
      					</div>
                <?php
                  }
                ?>
                <?php echo stevens_theme_links($node->field_program_feature_link[LANGUAGE_NONE][0],"link_button_lg background_red page_header_program_bar_button",'Apply Now'); ?>
      				</div>
      			</div>
      		</div>
      	</div>
      </div>
      <?php
        }
      ?>
    </header>
  	<!-- Page Content -->
  	<section class="page_content_area">
  		<div class="fs-row">
        <?php if ($page['sidebar_first']): ?>
          <aside class="sidebar sidebar_subnavigation fs-cell-right fs-lg-3" role="region">
            <?php print render($page['sidebar_first']); ?>
          </aside>
        <?php endif; ?>

        <div class="page_content fs-cell fs-lg-9">
          <?php
            if(!user_is_logged_in()) {
              print $messages;
            }
          ?>
          <div class="typography">
            <?php if ($page['content']): ?>
              <?php print render($page['content']); ?>
            <?php endif; ?>
          </div>

          <?php if ($page['content_components']): ?>
            <!-- In-Content Callouts -->
            <aside class="callouts content_callouts">
                <?php print render($page['content_components']); ?>
            </aside><!-- END: content_callouts -->
          <?php endif; ?>
        </div><!-- END: page_content -->

        <?php if ($page['sidebar_second']): ?>
          <aside class="sidebar sidebar_callouts fs-cell-right fs-lg-3">
          <?php print render($page['sidebar_second']); ?>
          </aside>
        <?php endif; ?>
      </div>
    </section><!-- END: page_content_area -->
    <?php if ($page['content_bottom']): ?>
      <!-- Full Width Components -->
      <section class="components">
        <?php print render($page['content_bottom']); ?>
      </section><!-- END: components -->
    <?php endif; ?>
  </main><!-- END: page -->
  <?php
   include ($directory . "/includes/footer.php");
  ?>
