<header class='page_header page_header_home js-equalize' data-equalize-options='{"maxWidth":"740px","target":".js-equalize-page_header_home"}'>
	<div class="page_header_home_carousel js-carousel" data-carousel-options='{"contained":false}'>

    <?php
      $item_count = count($content['field_home_feature_callouts']['#items']);
      for ($i = 0; $i < $item_count; $i++) {
        $item = $content['field_home_feature_callouts'][$i]['entity']['field_collection_item'];
        $item = array_pop($item);
        $image = $item['field_home_feature_image']['#items'][0]['uri'];
        $item_background = '';

        // We only use the video feature on non-mobile platforms
        if ((is_array($item["field_home_feature_mp4_file"]) || is_array($item['field_home_feature_video_link']))) {
          if (is_array($item["field_home_feature_mp4_file"])) {
            $item_background = array("source" => array(
              "mp4" => $item["field_home_feature_mp4_file"]['#items'][0]['display_url'],
              "ogg" => $item["field_home_feature_ogg_file"]['#items'][0]['display_url'],
              "webm" => $item["field_home_feature_webm_file"]['#items'][0]['display_url'],
              "poster" => image_style_url('home_page_video_poster',$image)
            ));
          } elseif (is_array($item['field_home_feature_video_link'])) {
            $item_background = array("autoPlay" => false,
							"source" => array(
              	"video" => $item['field_home_feature_video_link']['#items'][0]['display_url'],
              	"poster" => image_style_url('home_page_video_poster',$image)
            ));
          }
        } else {
          $item_background = array("source" => array(
            "0px" => image_style_url('home_feature_320x300',$image),
            "500px" => image_style_url('home_feature_500x300',$image),
            "740px" => image_style_url('home_feature_900x415',$image),
            "980px" => image_style_url('home_feature_1200x700',$image)
          ));
        }
    ?>
    <div class="page_header_home_block js-background" data-background-options="<?php echo htmlspecialchars(json_encode($item_background)); ?>">
    	<div class="page_header_home_block_content_container">
    		<div class="fs-row">
    			<div class="fs-cell">
            <?php
              if (isset($item['field_home_feature_persona_image']) && isset($item['field_home_feature_persona_name']) && isset($item['field_home_feature_persona_desc'])) {
                $persona_image = $item['field_home_feature_persona_image']['#items'][0]['uri'];
                $persona_alt = $item['field_home_feature_persona_image']['#items'][0]['alt'];
            ?>
						<div class="page_header_home_block_content has_callout background_<?php echo $item['field_home_feature_color']['#items'][0]['value']; ?> js-equalize-page_header_home">
              <article class="page_header_home_persona_callout background_<?php echo $item['field_home_feature_color']['#items'][0]['value']; ?>">
              	<figure class="page_header_home_persona_callout_figure">
              		<picture class="responsive_image news_item_picture news_list_item_picture">
              			<!--[if IE 9]><video style="display: none;"><![endif]-->
              			<source media="(min-width: 980px)" srcset="<?php echo image_style_url('home_feature_persona_250x172',$persona_image); ?>">
              			<source media="(min-width: 0px)" srcset="<?php echo image_style_url('home_feature_persona_80x80',$persona_image); ?>">
              			<!--[if IE 9]></video><![endif]-->
              			<img class="page_header_home_persona_callout_image" src="<?php echo image_style_url('home_feature_persona_80x80',$persona_image); ?>" alt="<?php print check_plain($persona_alt); ?>">
              		</picture>
              	</figure>
              	<div class="page_header_home_persona_callout_content">
              		<div class="page_header_home_persona_callout_content_inner">
              			<div class="page_header_home_persona_callout_label">In This Story</div>
              			<h3 class="page_header_home_persona_callout_heading"><?php echo $item['field_home_feature_persona_name']['#items'][0]['value']; ?></h3>
              			<div class="page_header_home_persona_callout_title">
              				<?php echo render($item['field_home_feature_persona_desc']); ?>
              			</div>
              		</div>
              	</div>
              </article>
              <?php
								} else {
              ?>
						<div class="page_header_home_block_content background_<?php echo $item['field_home_feature_color']['#items'][0]['value']; ?> js-equalize-page_header_home">
							<?php
								}
							?>
    					<div class="page_header_home_block_content_inner">
    						<h2 class="page_header_home_block_heading"><?php echo $item['field_home_feature_title']['#items'][0]['value']; ?></h2>
								<?php echo stevens_theme_links($item['field_home_feature_link']['#items'][0],'page_header_home_block_link'); ?>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    <?php
      }
    ?>
	</div>
</header>
