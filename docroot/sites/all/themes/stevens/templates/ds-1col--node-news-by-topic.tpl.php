<?php
  $query = new EntityFieldQuery;
  $query->entityCondition("entity_type","node")
        ->entityCondition("bundle","news_item")
        ->propertyCondition("status",NODE_PUBLISHED);

  // By category
  if (count($content["field_news_by_category_category"]["#items"])) {
    $tids = array();
    foreach ($content["field_news_by_category_category"]["#items"] as $item) {
      $tids[] = $item["tid"];
    }
    $query->fieldCondition("field_news_category","tid",$tids,"IN");
  }

  // By program
  if (count($content["field_news_by_topic_programs"]["#items"])) {
    $tids = array();
    foreach ($content["field_news_by_topic_programs"]["#items"] as $item) {
      $tids[] = $item["tid"];
    }
    $query->fieldCondition("field_news_programs","tid",$tids,"IN");
  }

  // By department
  if (count($content["field_news_by_topic_departments"]["#items"])) {
    $tids = array();
    foreach ($content["field_news_by_topic_departments"]["#items"] as $item) {
      $tids[] = $item["tid"];
    }
    $query->fieldCondition("field_news_departments","tid",$tids,"IN");
  }

  // By school
  if (count($content["field_news_by_topic_schools"]["#items"])) {
    $tids = array();
    foreach ($content["field_news_by_topic_schools"]["#items"] as $item) {
      $tids[] = $item["tid"];
    }
    $query->fieldCondition("field_news_schools","tid",$tids,"IN");
  }

  // By research
  if (count($content["field_news_by_topic_research"]["#items"])) {
    $tids = array();
    foreach ($content["field_news_by_topic_research"]["#items"] as $item) {
      $tids[] = $item["tid"];
    }
    $query->fieldCondition("field_news__research","tid",$tids,"IN");
  }

  // By audience
  if (count($content["field_news_audiences"]["#items"])) {
    $tids = array();
    foreach ($content["field_news_audiences"]["#items"] as $item) {
      $tids[] = $item["tid"];
    }
    $query->fieldCondition("field_news_audiences","tid",$tids,"IN");
  }

  // By tag
  if (count($content["field_news_tags"]["#items"])) {
    $tids = array();
    foreach ($content["field_news_tags"]["#items"] as $item) {
      $tids[] = $item["tid"];
    }
    $query->fieldCondition("field_news_tags","tid",$tids,"IN");
  }

  $result = $query->fieldOrderBy("field_news_date","value","DESC")->range(0,18)->execute();

  if ($result) {
    $news_nids = array_keys($result["node"]);
    $news = entity_load("node",$news_nids);
?>
<section class="margined_lg_top component_bordered news_list_section news_list_full_width content_loader">
  <header class="component_header_block news_list_header">
    <div class="fs-row">
      <div class="fs-cell">
        <div class="component_header_block_wrapper">
          <h2 class="component_heading component_header_block_heading news_list_heading"><?php echo $title; ?></h2>
        </div>
      </div>
    </div>
  </header>
  
  <div class="news_list">
    <div class="fs-row">
      <div class="fs-cell">
        <?php
          $x = 0;
          foreach ($news as $item) {
            $time = strtotime($item->field_news_date["und"][0]["value"]);
            if ($x % 2 == 0) {
              echo '<div class="content_set'.(!$x ? ' content_set_active js-loaded' : '').'">';
            }
        ?>
        <article class="clearfix news_item news_list_item">
          <?php if (count($item->field_news_featured_image["und"])) { ?>
          <figure class="bg_black responsive_image news_item_figure news_list_item_figure">
            <picture class="news_item_picture news_list_item_picture">
            <!--[if IE 9]><video style="display: none;"><![endif]-->
            <source media="(min-width: 740px)" srcset="<?php echo image_style_url("news_item_174x116",$item->field_news_featured_image["und"][0]["uri"]); ?>">
            <source media="(min-width: 0px)" srcset="<?php echo image_style_url("news_item_80x80",$item->field_news_featured_image["und"][0]["uri"]); ?>">
            <!--[if IE 9]></video><![endif]-->
            <img class="news_item_image news_list_item_image" src="<?php echo image_style_url("news_item_80x80",$item->field_news_featured_image["und"][0]["uri"]); ?>" alt="<?php echo $item->field_news_featured_image["und"][0]["alt"]; ?>">
            </picture>
          </figure>
          <?php } ?>

          <div class="news_item_content news_list_item_content">
            <header class="news_item_header news_list_item_header">
                <time class="news_item_date news_list_item_date" datetime="<?php echo date("Y-m-d",$time); ?>"><?php echo date("j M Y",$time); ?></time>
              </header>

            <div class="news_item_body news_list_item_body">
              <h3 class="news_item_title news_list_item_title">
                <a class="news_item_title_link news_list_item_title_link" href="<?php echo url("node/".$item->nid); ?>"><?php echo htmlspecialchars(htmlspecialchars_decode($item->title)); ?></a>
              </h3>
              <div class="news_item_description news_list_item_description">
                <p><?php echo stevens_trim_length(drupal_html_to_text($item->body["und"][0]["safe_value"], array('p', 'br')),175); ?></p>
              </div>
            </div>
          </div>
        </article>
        <?php
            $x++;
            if ($x % 2 == 0) {
              echo '</div>';
            }
          }
          if ($x % 2 !== 0) {
            echo '</div>';
          }
        ?>
      </div>
    </div>
  </div>
  <?php if ($x > 2) { ?>
  <footer class="news_list_footer">
    <div class="fs-row">
      <div class="fs-cell">
        <button class="load_more_button load_more_button_full news_list_more">Load More News</button>
      </div>
    </div>
  </footer>
  <?php } ?>
</section>
<?php
  } else {
    // Literally have to return an empty space or Drupal will do it's own thing rather than not display this block.
    echo " ";
  }
