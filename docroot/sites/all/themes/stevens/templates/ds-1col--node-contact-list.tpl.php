<section class="margined_md contact_section">
  <header class="contact_header">
    <div class="fs-row">
      <div class="fs-cell">
        <h2 class="light_uppercase contact_heading"><?php echo $content['field_contact_heading']['#items'][0]['value']; ?></h2>
      </div>
    </div>
  </header>
  <div class="contact_body">
    <div class="fs-row">
      <div class="fs-cell fs-lg-5">
        <h3 class="section_break_intro contact_title"><?php echo $title; ?></h3>
      </div>
      <div class="fs-cell fs-lg-7">

        <div class="contain contact_container contact_container_col_2">
          <div class="contact_set contact_set_location">
            <h4 class="bold_uppercase color_red contact_set_header">Location On Campus</h4>
            <div class="contact_set_description">
              <?php
                if (!empty($content['field_contact_location']['#items'][0]['name'])) {
                  echo $content['field_contact_location']['#items'][0]['name'] . '<br>';
                }
                if (!empty($content['field_contact_location']['#items'][0]['street'])) {
                  echo $content['field_contact_location']['#items'][0]['street'] . '<br>' ;
                }
                if (!empty($content['field_contact_location']['#items'][0]['additional'])) { 
                  echo $content['field_contact_location']['#items'][0]['additional'] . '<br>' ;
                }
                echo rtrim(trim($content['field_contact_location']['#items'][0]['city'].", ".$content['field_contact_location']['#items'][0]['province']),",");
                echo " ".$content['field_contact_location']['#items'][0]['postal_code'];
              ?>
            </div>
          </div>
          <div class="contact_set contact_set_types">
            <h4 class="bold_uppercase color_red contact_set_header">Contact Types</h4>
            <div class="contact_set_description">
              <?php if (!empty($content['field_contact_location']['#items'][0]['phone'])) { echo 'p. ' . $content['field_contact_location']['#items'][0]['phone'] . '<br>' ; } ?>
              <?php if (!empty($content['field_contact_location']['#items'][0]['email'])) { echo 'e. <a href="mailto:' . $content['field_contact_location']['#items'][0]['email'] . '">' . $content['field_contact_location']['#items'][0]['email'] .'</a><br>' ; } ?>
              <?php if (!empty($content['field_contact_location']['#items'][0]['fax'])) { echo 'f. ' . $content['field_contact_location']['#items'][0]['fax'] . '<br>' ; } ?>
            </div>
          </div>
        </div>
        <?php if (isset($content['field_contact_social_links'])) { ?>
        <div class="contain contact_container contact_container_border">
          <h4 class="bold_uppercase color_red contact_set_header contact_set_header_inline">Social Media</h4>
          <div class="contact_set_description contact_set_description_inline contact_set_social_list">
            <?php
              foreach ($content['field_contact_social_links']['#items'] as $social_link) {
                $class = '';
                $url = parse_url($social_link['display_url']);
                if (strpos($url['host'],'facebook') !== false) {
                  $class = 'contact_social_link_facebook';
                }
                if (strpos($url['host'],'twitter') !== false) {
                  $class = 'contact_social_link_twitter';
                }
                if (strpos($url['host'],'instagram') !== false) {
                  $class = 'contact_social_link_instagram';
                }
                if (strpos($url['host'],'youtu') !== false) {
                  $class = 'contact_social_link_youtube';
                }
                if (strpos($url['host'],'linkedin') !== false) {
                  $class = 'contact_social_link_linkedin';
                }
                echo stevens_theme_links($social_link,"contact_set_social_link contact_social_link " . $class);
              }
            ?>
          </div>
        </div>
        <?php } ?>
        <?php if (isset($content['field_department_leaders'])) { ?>
        <div class="contain contact_container contact_container_border">
          <h4 class="bold_uppercase color_red contact_set_header">Department Leaders</h4>
          <div class="contact_set_description">
            <?php
              foreach ($content['field_department_leaders']['#items'] as $item) {
                $person = node_load($item['target_id']);
            ?>
            <div class="contain contact_set_list">
              <span class="contact_set_list_item contact_set_list_name"><?php echo $person->title; ?></span>
              <span class="contact_set_list_item contact_set_list_position"><?php echo $person->field_faculty_position['und'][0]['value']; ?></span>
              <span class="contact_set_list_item contact_set_list_phone"><?php if (!empty($person->field_faculty_location['und'][0]['phone'])) { echo 'p. ' . $person->field_faculty_location['und'][0]['phone']; } ?></span>
              <span class="contact_set_list_item contact_set_list_email"><?php if (!empty($person->field_faculty_location['und'][0]['phone'])) { ?>e. <a href="mailto:<?php echo $person->field_faculty_location['und'][0]['email']; ?>"><?php echo $person->field_faculty_location['und'][0]['email']; ?></a><?php } ?></span>
            </div>
            <?php
              }
            ?>
          </div>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
</section>