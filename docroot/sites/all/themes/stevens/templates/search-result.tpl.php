<?php
	$display_url = stevens_trim_length(str_replace(array("http://","https://"),"",$url),40);
?>
<div class="search_result" role="listitem">
	<div class="search_result_content">
		<header class="search_result_header">
			<div class="search_result_title">
				<a class="search_result_title_link" href="<?php echo $url; ?>"><?php echo $title; ?></a>
			</div>
		</header>
		<div class="search_result_description">
			<p><?php echo $snippet; ?></p>
		</div>
		<footer class="search_result_footer">
			<a class="search_result_url" href="<?php echo $url; ?>"><?php echo $display_url; ?></a>
		</footer>
	</div>
</div>