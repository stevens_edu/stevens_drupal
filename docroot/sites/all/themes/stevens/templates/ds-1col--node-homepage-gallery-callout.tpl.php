<section class="homepage_gallery">
	<div class="fs-row">
		<header class="fs-cell component_header">
			<h2 class="component_heading js-content_swap" data-content-swap-mobile="<?php echo $content['field_hpg_short_title']['#items'][0]['value']; ?>" data-content-swap-default="<?php echo $title; ?>"><?php echo $title; ?></h2>
			<div class="component_text"><?php echo $content['field_hpg_description']['#items'][0]['value']; ?></div>
		</header>
		<menu class="fs-cell homepage_gallery_buttons">
      <?php
        $item_count = count($content['field_hpg_carousel']['#items']);
        for ($i = 0; $i < $item_count; $i++) {
          $item = $content['field_hpg_carousel'][$i]['entity']['field_collection_item'];
          $item = array_pop($item);
      ?>
			<button class="homepage_gallery_button js-content_swap js-swap" data-content-swap-mobile="<?php echo $item['field_hpg_mobile_title']['#items'][0]['value']; ?>" data-content-swap-tablet="<?php echo $item['field_hpg_tablet_title']['#items'][0]['value']; ?>" data-content-swap-default="<?php echo $item['field_hpg_desktop_title']['#items'][0]['value']; ?>" data-swap-options='{"collapse":false}' data-swap-group="homepage_gallery" data-swap-target=".js-homepage_gallery_carousel_<?php echo $i + 1; ?>"><?php echo $item['field_hpg_desktop_title']['#items'][0]['value']; ?></button>
      <?php
        }
      ?>
		</menu>
	</div>
	<div class="fs-row homepage_gallery_carousels">
		<div class="fs-cell homepage_gallery_carousels_container">
      <?php
        $item_count = count($content['field_hpg_carousel']['#items']);
        for ($i = 0; $i < $item_count; $i++) {
          $item = $content['field_hpg_carousel'][$i]['entity']['field_collection_item'];
          $item = array_pop($item);
      ?>
      <div class="homepage_gallery_carousel js-homepage_gallery_carousel_<?php echo $i + 1; ?> js-carousel" data-carousel-options='{"matchHeight":true,"show":{"980px":2}}'>

        <?php
          foreach($item['field_hpg_items']['#items'] as $carousel_item) {
            $carousel_item = $carousel_item['entity'];
            switch ($carousel_item->type) {
              case "image_carousel_item":
                $image = $carousel_item->field_hpg_image['und'][0]['uri'];
        ?>
        <article class="homepage_gallery_item image js-toggle">
        	<div class="homepage_gallery_item_inner">
        		<figure class="homepage_gallery_item_figure js-background" data-background-options='{"source":{"0px":"<?php echo image_style_url('home_page_carousel_item_500x333',$image); ?>","500px":"<?php echo image_style_url('home_page_carousel_item_740x494',$image); ?>","740px":"<?php echo image_style_url('home_page_carousel_item_980x653',$image); ?>","980px":"<?php echo image_style_url('home_page_carousel_item_740x416',$image); ?>","1220px":"<?php echo image_style_url('home_page_carousel_item_980x551',$image); ?>"}}'>
        			<figcaption class="homepage_gallery_item_caption js-toggle_handle">
        				<div class="homepage_gallery_item_caption_content">
        					<h3 class="homepage_gallery_item_caption_label"><?php echo $carousel_item->title; ?></h3>
        					<div class="homepage_gallery_item_caption_text typography">
        						<?php echo $carousel_item->field_hpg_image_description['und'][0]['value']; ?>
        					</div>
        					<a href="<?php echo $carousel_item->field_hpg_link['und'][0]['url']; ?>" class="homepage_gallery_item_caption_link"><?php echo $carousel_item->field_hpg_link['und'][0]['title']; ?></a>
        				</div>
        			</figcaption>
        		</figure>
        	</div>
        </article>
        <?php
                break;
              case "video_carousel_item":
                $image = $carousel_item->field_hpg_video_image['und'][0]['uri'];
        ?>
        <article class="homepage_gallery_item js-video_embed" data-video-source="<?php echo $carousel_item->field_hpg_video_link['und'][0]['url']; ?>" data-video-container=".homepage_gallery_item_video_container">
        	<div class="homepage_gallery_item_inner">
        		<div class="homepage_gallery_item_video_container"></div>
        		<figure class="homepage_gallery_item_figure js-background js-video_embed_handle" data-background-options='{"source":{"0px":"<?php echo image_style_url('home_page_carousel_item_500x333',$image); ?>","500px":"<?php echo image_style_url('home_page_carousel_item_740x494',$image); ?>","740px":"<?php echo image_style_url('home_page_carousel_item_980x653',$image); ?>","980px":"<?php echo image_style_url('home_page_carousel_item_740x416',$image); ?>","1220px":"<?php echo image_style_url('home_page_carousel_item_980x551',$image); ?>"}}'></figure>
        	</div>
        </article>
        <?php
                break;
            }
          }
        ?>

      </div>
      <?php
        }
      ?>
		</div>
	</div>
</section>
