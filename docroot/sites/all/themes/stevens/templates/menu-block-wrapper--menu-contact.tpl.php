<div class="fs-cell-contained fs-md-2 fs-lg-3 footer_contact_container">
  <div class="footer_contact">
    <div class="footer_heading">Get in Touch</div>
    <div class="footer_contact_links">
      <? echo render($content); ?>
    </div>
  </div>
</div>
