<?php
  // Put together a string for categories
  $categories = array();
  if (is_array($content["field_event_categories"]["#items"])) {
    foreach ($content["field_event_categories"]["#items"] as $item) {
    	$categories[] = htmlspecialchars($item["taxonomy_term"]->name);
    }
  }

  // Figure out how to display the date
  $start = strtotime($content["field_event_start_time"]["#items"][0]["value"]." UTC");
  $end = strtotime($content["field_event_end_time"]["#items"][0]["value"]." UTC");
  $all_day = $content["field_event_all_day"]["#items"][0]["value"];
  list($draw_date,$draw_time) = stevens_parse_date_time($start,$end,$all_day);
?>
<header class="calendar_detail_header">
  <div class="light_uppercase color_gray calendar_detail_header_cat"><?php echo implode(", ",$categories); ?></div>
  <div class="calendar_detail_header_info">
    <div class="color_red calendar_detail_header_date"><?php echo $draw_date; ?></div>
    <time class="light_uppercase color_gray_dark calendar_detail_header_time" datetime="<?php echo date("c",$start); ?>"><?php echo $draw_time; ?></time>
    <?php if (!empty($content["field_event_location"]["#items"][0]["value"])) { ?>
    <div class="calendar_detail_header_location"><?php echo htmlspecialchars($content["field_event_location"]["#items"][0]["value"]); ?></div>
    <?php } ?>
  </div>
  <h2 class="color_red calendar_detail_header_title"><?php echo $title; ?></h2>
  <?php if (!empty($content["field_event_subtitle"]["#items"][0]["value"])) { ?>
  <h3 class="color_gray_dark calendar_detail_header_subtitle"><?php echo htmlspecialchars($content["field_event_subtitle"]["#items"][0]["value"]); ?></h3>
  <?php } ?>
</header>

<div class="calendar_detail_controls">
  <div class="calendar_detail_links">
    <?php if (array_filter((array)$content["field_event_link"]["#items"][0])) { ?>
    <a class="calendar_detail_link link_button_sm background_red" href="<?php echo stevens_url_fix($content["field_event_link"]["#items"][0]); ?>">RSVP</a>
    <?php } ?>
    <a class="calendar_detail_link link_button_sm background_teal" href="/events/add-to-calendar/?nid=<?php echo $node->nid; ?>">Add to Calendar</a>
  </div>
  <div class="service-links">
    <div class="service-label">Share this story</div>
    <ul class="links">
      <li class="service-links-facebook first">
        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(stevens_current_url()); ?>" target="_blank" title="Share on Facebook" class="service-links-facebook" rel="nofollow">
          <img typeof="foaf:Image" src="/sites/all/modules/contrib/service_links/images/facebook.png" alt="Facebook logo"> Facebook
        </a>
      </li>
      <li class="service-links-twitter">
        <a href="https://twitter.com/home?status=<?php echo urlencode(stevens_current_url()); ?>" target="_blank" title="Share this on Twitter" class="service-links-twitter" rel="nofollow">
          <img typeof="foaf:Image" src="/sites/all/modules/contrib/service_links/images/twitter.png" alt="Twitter logo"> Twitter
        </a>
      </li>
      <li class="service-links-linkedin last">
        <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo urlencode(stevens_current_url()); ?>&amp;title=<?php echo urlencode($title) ?>" target="_blank" title="Publish this post to LinkedIn" class="service-links-linkedin" rel="nofollow">
          <img typeof="foaf:Image" src="/sites/all/modules/contrib/service_links/images/linkedin.png" alt="LinkedIn logo"> LinkedIn
        </a>
      </li>
    </ul>
  </div>
</div>

<?php
  if (!empty($content["field_event_image"]["#items"][0]["uri"])) {
?>
<div class="block_right content_figure content_figure_landscape">
  <img class="content_figure_image" src="<?php echo image_style_url("event_detail",$content["field_event_image"]["#items"][0]["uri"]); ?>" alt="<?php echo $content["field_event_image"]["#items"][0]["alt"]; ?>">
</div>
<?php
  }

  echo render($content["body"]);
?>

<p><a class="link_arrow_back" href="/events/">Back to Events</a></p>
