<article class="clearfix directory_container">
<div class="clearfix directory_position_ent">
        <div class="fs-cell fs-md-8 fs-lg-8">
                <div class="directory_office">
                        <h4 class="directory_heading"><?php echo $title; ?></h4>
                        <p><?php echo $content['field_personnel_position']['#items'][0]['value']; ?></p>
                </div>
        </div>
        <div class="fs-cell fs-md-4 fs-lg-4 directory_detail">
    <?php if(isset($content['field_personnel_address'])) { ?>
                <h4 class="color_red bold_uppercase">Primary Affiliation</h4>
    <p><?php echo $content['field_personnel_address']['#items'][0]['value']; ?></p>
    <?php } else { echo '&nbsp;'; }?>
        </div>
</div>



<?php
if (!empty($node->field_personnel_positions)) {
	foreach ($node->field_personnel_positions[LANGUAGE_NONE] as $position_id => $position_item) {
		$position_ent = entity_load_single('field_collection_item', $position_item['value']);

		// For each additional position, use values from the position
		// entity if they exist; else inherit the parent node's values.
		// Maintains unset status when neither exists, for later checks.

		if(isset($content['field_personnel_position']['#items'][0]['value']))
			$position = $content['field_personnel_position']['#items'][0]['value'];
		if(isset($position_ent->field_personnel_position[LANGUAGE_NONE][0]['value']))
			$position = $position_ent->field_personnel_position[LANGUAGE_NONE][0]['value'];

		if(isset($content['field_personnel_address']['#items'][0]['value']))
			$address = $content['field_personnel_address']['#items'][0]['value'];
		if(isset($position_ent->field_personnel_address[LANGUAGE_NONE][0]['value']))
			$address = $position_ent->field_personnel_address[LANGUAGE_NONE][0]['value'];
?>
<div class="clearfix directory_position_ent">
	<div class="fs-cell fs-md-8 fs-lg-8">
		<div class="directory_office">
			<div class="directory_position_push"></div>
			<p><?php echo $position; ?></p>
		</div>
	</div>
	<div class="fs-cell fs-md-4 fs-lg-4 directory_detail">
	<?php if(isset($address)) { ?>
		<h4 class="color_red bold_uppercase">Primary Affiliation</h4>
		<p><?php echo $address; ?></p>
	<?php } else { echo '&nbsp;'; }?>
	</div>
</div>
<?php } ?>
<?php } ?>
</article>
