<?php
  // Setup arrays for multi-sorting
  $times = array();
  $blocks = array();

  // Manually curated instagram
  $query = new EntityFieldQuery;
  $query->entityCondition("entity_type","node")
        ->entityCondition("bundle","instagram_block")
        ->propertyCondition("status",NODE_PUBLISHED)
        ->range(0,12)
        ->propertyOrderBy("changed","DESC");
  $result = $query->execute();
  if ($result) {
    $instagram_nids = array_keys($result["node"]);
    $instagram_nodes = entity_load("node", $instagram_nids);

    foreach ($instagram_nodes as $instanode) {
      $times[] = intval($instanode->changed);
      $blocks[] = array(
        "type" => "instagram",
        "title" => $instanode->title,
        "image" => $instanode->field_instagram_image["und"][0]["uri"],
        "alt" => $instanode->field_instagram_image["und"][0]["alt"],
        "link" => $instanode->field_instagram_link["und"][0]["url"]
      );
    }
  }

  array_multisort($times,SORT_DESC,$blocks);
?>
<section class="component homepage_social_media">
	<div class="fs-row">
		<header class="fs-cell component_header">
			<h2 class="component_heading homepage_social_media_heading"><?php echo $title; ?></h2>
			<div class="component_text homepage_social_media_text">
        <?php echo $content['field_home_social_description']['#items'][0]['value']; ?>

        <?php if (isset($content['field_home_social_link'])) { ?>
        <?php echo stevens_theme_links($content['field_home_social_link']['#items'][0],'homepage_social_media_link'); ?>
        <?php } ?>
      </div>
		</header>
	</div>
	<div class="homepage_social_media_callouts">
		<div class="carousel_controls homepage_social_media_controls">
			<button class="carousel_control homepage_social_media_control homepage_social_media_control_previous">Previous</button>
			<button class="carousel_control homepage_social_media_control homepage_social_media_control_next">Next</button>
		</div>
		<div class="fs-row homepage_social_media_full">
			<div class="fs-cell homepage_social_media_full">

				<div class="homepage_social_media_carousel js-carousel" data-carousel-options='{"contained":false,"matchHeight":true,"show":{"740px":2,"980px":3,"1220px":4},"controls":{"container":".homepage_social_media_controls","previous":".homepage_social_media_control_previous","next":".homepage_social_media_control_next"}}'>

          <?php
            foreach($blocks as $block) {
              if ($block["type"] == "instagram") {
          ?>
          <article class="homepage_social_media_callout instagram">
            <div class="homepage_social_media_callout_content">
              <?php
                if ($block["link"]) {
                  echo '<a class="homepage_social_media_callout_link" href="'.$block["link"].'" target="_blank">';
                }
              ?>
              <figure class="bg_black responsive_image homepage_social_media_callout_figure">
                <picture class="homepage_social_media_callout_picture">
                  <!--[if IE 9]><video style="display: none;"><![endif]-->
                  <source media="(min-width: 1220px)" srcset="<?php echo image_style_url("instagram_small", $block["image"]); ?>">
                  <source media="(min-width: 740px)" srcset="<?php echo image_style_url("instagram_medium", $block["image"]); ?>">
                  <source media="(min-width: 500px)" srcset="<?php echo image_style_url("instagram_small", $block["image"]); ?>">
                  <!--[if IE 9]></video><![endif]-->
                  <img class="homepage_social_media_callout_image" src="<?php echo image_style_url("instagram_large", $block["image"]); ?>" alt="<?php echo $block["alt"]; ?>">
                </picture>
              </figure>
              <?php
                if ($block["link"]) {
                  echo '</a>';
                }
              ?>
            </div>
          </article>

          <?php
              }
            }
          ?>

				</div>
			</div>
		</div>
	</div>
</section>
