<ul class="accordion_list">
  <?php
    global $accordion_callout_count;
    if (!isset($accordion_callout_count)) {
      $accordion_callout_count = 0;
    }

    $item_count = count($content['field_accordion_items']['#items']);
    $uniqid = uniqid();
    for ($i = 0; $i < $item_count; $i++) {
      $item = $content['field_accordion_items'][$i]['entity']['field_collection_item'];
      $item = array_pop($item);
      $accordion_callout_count++;
  ?>
  <li class="accordion_item" <?php echo (!empty($item['field_accordion_anchor']['#items'][0]['value'])) ? 'id="' . check_plain($item['field_accordion_anchor']['#items'][0]['value']) . '"' : ''; ?>>
  	<div class="acc_wrap clearfix">
  		<h4 class="directory_heading"><button aria-expanded="false" aria-controls="accordion_<?php echo $nid; ?>-<?php echo $accordion_callout_count; ?>" class="js-swap acc_q" data-swap-target=".toggle_target_<?php echo $accordion_callout_count; ?>">
  			<?php echo $item['field_accordion_heading']['#items'][0]['value']; ?>
  		</button></h4>
  		<div class="toggle_target toggle_target_<?php echo $accordion_callout_count; ?> acc_a" id="accordion_<?php echo $nid; ?>-<?php echo $accordion_callout_count; ?>" aria-hidden="true">
  			<div class="callout_body"><?php echo render($item['field_accordion_content']); ?></div>
        <?php
          if(isset($item['field_accordion_link'])) {
            echo stevens_theme_links($item['field_accordion_link']['#items'][0],"callout_link");
          }
        ?>
  		</div>
  	</div>
  </li>
  <?php
    }
  ?>
</ul>
