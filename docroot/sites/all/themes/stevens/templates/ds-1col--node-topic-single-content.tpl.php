<?php
  $image = $content['field_tsc_image']['#items'][0]['uri'];
?>
<article class="callout topic_single_callout topic_single_callout_in_content">
	<figure class="topic_single_figure">
		<picture class="responsive_image topic_single_image">
			<!--[if IE 9]><video style="display: none;"><![endif]-->
			<source media="(min-width: 1220px)" srcset="<?php echo image_style_url('topic_single_content_350x234',$image); ?>">
			<source media="(min-width: 980px)" srcset="<?php echo image_style_url('topic_single_content_280x187',$image); ?>">
			<source media="(min-width: 740px)" srcset="<?php echo image_style_url('topic_single_content_240x160',$image); ?>">
			<source media="(min-width: 500px)" srcset="<?php echo image_style_url('topic_single_content_470x313',$image); ?>">
			<source media="(min-width: 0px)" srcset="<?php echo image_style_url('topic_single_content_294x196',$image); ?>">
			<!--[if IE 9]></video><![endif]-->
			<img src="<?php echo image_style_url('topic_single_content_294x196',$image); ?>" alt="<?php echo $content['field_tsc_image']['#items'][0]['alt']; ?>" draggable="false">
		</picture>
	</figure>
	<div class="callout_contain topic_single_contain">
		<h3 class="topic_single_heading"><?php echo $title; ?></h3>
		<div class="callout_content topic_single_content">
			<?php print render($content['body']); ?>
		</div>
    <?php
      if (isset($content['field_tsc_link'])) {
        echo stevens_theme_links($content['field_tsc_link']['#items'][0],"callout_link topic_single_link");
      }
    ?>
	</div>
</article>
