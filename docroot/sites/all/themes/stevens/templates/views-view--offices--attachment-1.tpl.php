<?php
  $parameters = drupal_get_query_parameters();
  $query = "";
  if (isset($parameters['title'])) {
    $query = $parameters['title'];
  }
  $letters = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
  $rows = views_get_view_result('offices', 'attachment_1');
  $letters_length = count($letters);
  $output_ul = ($query === '') ? '' : '<li class="directory_controls_all"><a href="/directory/offices">All</a></li>';
  $output_select = ($query === '') ? '' : '<option value="">All</option>';
  for ($i=0; $i<$letters_length; $i++) {
    $link = "";
    foreach ($rows as $row) {
      if(strtoupper($row->title_truncated) == $letters[$i]) {
        $link = $row->title_truncated;
      }
    }
    if (strlen($link)) {
      $output_ul .= '<li><a href="/directory/offices?title=' . $letters[$i] . '"';
      if ($letters[$i]==$query) {
        $output_ul .= 'class="active"';
      }
      $output_ul .= '><span>' . $letters[$i] . '</span></a></li>';
      $output_select .= '<option value="' . $letters[$i] . '">' . $letters[$i] . '</option>';
    } else {
      $output_ul .= '<li><span class="disabled">' . $letters[$i] . ' </span></li>';
      $output_select .= '<option disabled="" value="' . $letters[$i] . '">' . $letters[$i] . '</option>';
    }
  }
?>
<div class="page_header_component page_header_article">
  <div class="fs-row">
    <div class="fs-cell">
      <article class="directory_controls component_controls">
        <ul class="fs-xs-hide fs-sm-hide">
          <?php echo $output_ul; ?>
        </ul>
        <div class="fs-md-hide fs-lg-hide fs-xl-hide alpha_drop_wrap">
          <fieldset class="form_fieldset">
            <label for="alpha_drop" class="offscreen">Show only those starting with selected letter</label>
            <select class="js-dropdown alpha_drop" id="alpha_drop">
              <?php echo $output_select; ?>
            </select>
          </fieldset>
        </div>
        <div class="secondary_nav_item">
          <form action="/directory/offices" class="search_form news_search_form directory_search_form">
            <input type="text" id="edit-title" name="title" class="search_input news_search_input"<?php if(strlen($query) > 1) { echo ' value="' . $query . '"'; } ?>>
            <button class="search_submit news_search_submit" type="submit">
              <span class="visually_hidden">Search</span>
            </button>
          </form>
        </div>
      </article>
    </div>
  </div>
</div>
