<div class="typography">
  <div class="curriculum_list">
    <header class="curriculum_list_header">
      <h2 class="curriculum_list_title"><?php echo $title; ?></h2>
    </header>
    <div class="curriculum_list_description">
      <?php echo $content['field_curriculum_description']['#items'][0]['value']; ?>
    </div>
    <?php
      $tid = $content['field_curriculum_program']['#items'][0]['tid'];
      $term = taxonomy_term_load($tid);
      $progid = $term->field_program_id['und'][0]['value'];
      $curriculum = stevens_get_curriculum($progid);
      if (!isset($curriculum['message'])) {
        $terms = array_keys($curriculum);
        $i = 0;
        foreach ($curriculum as $semester) {
          echo '<div class="term_list"><h3 class="term_heading">' . $terms[$i] . '</h3>';
          foreach ($semester as $course) {
            //dpm($course);
            echo '<div class="term_item"><h4 class="term_item_title">';
            if(strlen($course['course_no'])) {
              echo $course['course_no'] . ": ";
            }
            echo $course['course_name'] . '</h4>';
            echo '<div class="term_item_description">' . $course['course_description'] . '</div></div>';
          }
          echo '</div>';
          $i++;
        }
      } else {
        echo '<p>' . $curriculum['message'] . '</p>';
      }

    ?>
  </div>
</div>
