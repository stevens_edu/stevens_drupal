<?php
  $image = $content['field_program_feature_image']['#items'][0]['uri'];
?>
<article class="program_finder_block">
  <a href="<?php echo url('node/' . $node->nid); ?>" class="block_link">
    <figure class="program_finder_block_figure">
      <img src="<?php echo image_style_url('program_feature_teaser_331x250',$image); ?>" class="program_finder_block_image" alt="<?php echo $content['field_program_feature_image ']['#items'][0]['alt']; ?>" draggable="false">
    </figure>
    <div class="program_finder_block_content">
      <h3 class="program_finder_block_name"><?php echo $content['field_program_short_name']['#items'][0]['value']; ?></h3>
      <h4 class="program_finder_block_type"><?php echo $content['field_program_degrees_offered']['#items'][0]['value']; ?></h4>
    </div>
  </a>
</article>
