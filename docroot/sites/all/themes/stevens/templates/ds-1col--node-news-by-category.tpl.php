<?php
  $query = new EntityFieldQuery;
  $query->entityCondition("entity_type","node")
        ->entityCondition("bundle","news_item")
        ->propertyCondition("status",NODE_PUBLISHED);

  // By category
  if (count($content["field_news_by_category_category"]["#items"])) {
    $tids = array();
    foreach ($content["field_news_by_category_category"]["#items"] as $item) {
      $tids[] = $item["tid"];
    }
    $query->fieldCondition("field_news_category","tid",$tids,"IN");
  }

  // By program
  if (count($content["field_news_by_topic_programs"]["#items"])) {
    $tids = array();
    foreach ($content["field_news_by_topic_programs"]["#items"] as $item) {
      $tids[] = $item["tid"];
    }
    $query->fieldCondition("field_news_programs","tid",$tids,"IN");
  }

  // By department
  if (count($content["field_news_by_topic_departments"]["#items"])) {
    $tids = array();
    foreach ($content["field_news_by_topic_departments"]["#items"] as $item) {
      $tids[] = $item["tid"];
    }
    $query->fieldCondition("field_news_departments","tid",$tids,"IN");
  }

  // By school
  if (count($content["field_news_by_topic_schools"]["#items"])) {
    $tids = array();
    foreach ($content["field_news_by_topic_schools"]["#items"] as $item) {
      $tids[] = $item["tid"];
    }
    $query->fieldCondition("field_news_schools","tid",$tids,"IN");
  }

  // By research
  if (count($content["field_news_by_topic_research"]["#items"])) {
    $tids = array();
    foreach ($content["field_news_by_topic_research"]["#items"] as $item) {
      $tids[] = $item["tid"];
    }
    $query->fieldCondition("field_news__research","tid",$tids,"IN");
  }

  // By audience
  if (count($content["field_news_audiences"]["#items"])) {
    $tids = array();
    foreach ($content["field_news_audiences"]["#items"] as $item) {
      $tids[] = $item["tid"];
    }
    $query->fieldCondition("field_news_audiences","tid",$tids,"IN");
  }

  // By tag
  if (count($content["field_news_tags"]["#items"])) {
    $tids = array();
    foreach ($content["field_news_tags"]["#items"] as $item) {
      $tids[] = $item["tid"];
    }
    $query->fieldCondition("field_news_tags","tid",$tids,"IN");
  }

  $result = $query->fieldOrderBy("field_news_date","value","DESC")->range(0,24)->execute();

  $category_name = $content["field_news_by_category_category"]["#items"][0]["taxonomy_term"]->name;

  if ($result) {
    $news_nids = array_keys($result["node"]);
    $news = entity_load("node",$news_nids);
    $filtered_news = array();

    // Filter out news that doesn't have images
    $x = 0;
    foreach ($news as $item) {
      if ($x < 18 && !empty($item->field_news_featured_image["und"][0]["uri"])) {
        $x++;
        $filtered_news[] = $item;
      }
    }

    if (count($filtered_news)) {
      // Get a list of categories
      $vocabulary = taxonomy_vocabulary_machine_name_load('news_categories');
      $categories = entity_load('taxonomy_term', FALSE, array('vid' => $vocabulary->vid));
      $category_rel = array();
      foreach ($categories as $category) {
        $name = htmlspecialchars(htmlspecialchars_decode($category->name));
        $category_rel[$category->tid] = $name;
      }
?>
<section class="component_bordered news_list_section component_columns content_loader">
  <header class="component_header_block news_list_header">
    <div class="fs-row">
      <div class="fs-cell">
        <div class="component_header_block_wrapper">
          <h2 class="component_heading component_header_block_heading news_list_heading"><?php echo $title; ?></h2>
        </div>
      </div>
    </div>
  </header>

  <div class="news_list">
    <div class="fs-row">
      <div class="fs-cell">
        <?php
          $x = 0;
          foreach ($filtered_news as $item) {
            $time = strtotime($item->field_news_date["und"][0]["value"]);
            if ($x % 4 == 0) {
              echo '<div class="content_set'.(!$x ? ' content_set_active js-loaded' : '').'">';
            }

            // Get a category name
            $category_name = "";
            if (is_array($item->field_news_category["und"])) {
              foreach ($item->field_news_category["und"] as $category) {
                if (!$category_name) {
                  $category_name = $category_rel[$category["tid"]];
                }
              }
            }
        ?>
        <article class="clearfix news_item news_list_item component_columns_item">
          <figure class="bg_black responsive_image news_item_figure news_list_item_figure">
            <picture class="news_item_picture news_list_item_picture">
              <!--[if IE 9]><video style="display: none;"><![endif]-->
              <source media="(min-width: 980px)" srcset="<?php echo image_style_url("news_item_174x116",$item->field_news_featured_image["und"][0]["uri"]); ?>">
              <source media="(min-width: 0px)" srcset="<?php echo image_style_url("news_item_80x80",$item->field_news_featured_image["und"][0]["uri"]); ?>">
              <!--[if IE 9]></video><![endif]-->
              <img class="news_item_image news_list_item_image" src="<?php echo image_style_url("news_item_80x80",$item->field_news_featured_image["und"][0]["uri"]); ?>" alt="<?php echo $item->field_news_featured_image["und"][0]["alt"]; ?>">
            </picture>
          </figure>
          <div class="news_item_content news_list_item_content">
            <div class="news_item_body news_list_item_body">
              <h3 class="news_item_title news_list_item_title">
                <a class="news_item_title_link news_list_item_title_link" href="<?php echo url("node/".$item->nid); ?>"><?php echo htmlspecialchars(htmlspecialchars_decode($item->title)); ?></a>
              </h3>
            </div>
            <footer class="news_item_footer news_list_item_footer">
              <time class="news_item_date news_list_item_date" datetime="<?php echo date("Y-m-d",$time); ?>"><?php echo date("j M Y",$time); ?></time>
              <?php if ($category_name) { ?>
              <span class="news_item_cat news_list_item_cat"><?php echo $category_name; ?></span>
              <?php } ?>
            </footer>
          </div>
        </article>
        <?php
            $x++;
            if ($x % 4 == 0) {
              echo '</div>';
            }
          }
          if ($x % 4 !== 0) {
            echo '</div>';
          }
        ?>
      </div>
    </div>
  </div>
  <?php if ($x > 4) { ?>
  <footer class="news_list_footer">
    <div class="fs-row">
      <div class="fs-cell">
        <button class="load_more_button load_more_button_full news_list_more">Load More News</button>
      </div>
    </div>
  </footer>
  <?php } ?>
</section>
<?php
    } else {
      echo " ";
    }
  } else {
    echo " ";
  }
