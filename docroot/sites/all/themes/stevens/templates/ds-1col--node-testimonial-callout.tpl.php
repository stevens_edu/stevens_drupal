<section class="component testimonial">
	<div class="fs-row">
		<header class="fs-cell component_header">
			<h2 class="component_heading"><?php echo $title; ?></h2>
		</header>
	</div>
	<div class="testimonial_callouts">
		<div class="fs-row">
			<div class="fs-cell">
				<div class="testimonial_carousel js-carousel" data-carousel-options='{"matchHeight": true, "show" :1}'>
          <?php
            $item_count = count($content['field_testimonials']['#items']);
            for ($i = 0; $i < $item_count; $i++) {
              $item = $content['field_testimonials'][$i]['entity']['field_collection_item'];
              $item = array_pop($item);
              $image = "";
              if (isset($item['field_testimonial_image'])) {
                $image = $item['field_testimonial_image']['#items'][0]['uri'];
              }
          ?>
          <article class="testimonial_callout background_<?php echo $item['field_testimonial_color']['#items'][0]['value']; ?>">
          	<div class="testimonial_callout_container">
              <?php if (strlen($image)) { ?>
          		<figure class="testimonial_callout_figure"><picture class="responsive_image testimonial_callout_image"><!--[if IE 9]><video style="display: none;"><![endif]--><source media="(min-width: 740px)" srcset="<?php echo image_style_url('testimonial_350x350',$image); ?>"><!--[if IE 9]></video><![endif]--><img src="<?php echo image_style_url('testimonial_294x165',$image); ?>" alt="<?php echo $item['field_testimonial_image']['#items'][0]['alt']; ?>" draggable="false"></picture></figure>
              <?php } ?>
          		<div class="testimonial_callout_content">
          			<h3 class="testimonial_callout_heading"><?php echo $item['field_testimonial_title']['#items'][0]['value']; ?></h3>
          			<div class="testimonial_callout_quote"><?php echo stevens_trim_length(render($item['field_testimonial_quote']),200); ?></div>
          			<h4 class="testimonial_callout_name"><?php echo $item['field_testimonial_name']['#items'][0]['value']; ?></h4>
                <?php if (isset($item['field_testimonial_link'])) { ?>
								<?php echo stevens_theme_links($item['field_testimonial_link']['#items'][0],"testimonial_callout_link"); ?>
                <?php } ?>
          		</div>
          	</div>
          </article>
          <?php
            }
          ?>
				</div>
			</div>
		</div>
	</div>
</section>
