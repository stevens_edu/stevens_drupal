<?php
  include ($directory . "/includes/header.php");

  $url_pieces = array_filter(explode("/",request_uri()));
  $current_search = htmlspecialchars(end($url_pieces));
  if ($current_search == "site") {
    $current_search = "";
  }
?>
<div class="page_wrapper js-navigation_push">
  <main id="page" class="page" role="main">
    <header class="page_header">
    	<div class="fs-row">
        <?php
          if(user_is_logged_in()) {
        ?>
        <div class="fs-cell">
          <?php print $messages; ?>
          <?php print render($page['help']); ?>
          <?php if ($tabs): ?>
            <div class="tabs"><?php print render($tabs); ?></div>
          <?php endif; ?>
          <?php if ($action_links): ?>
            <ul class="action-links"><?php print render($action_links); ?></ul>
          <?php endif; ?>

        </div>
        <?php
          }
        ?>
    		<div class="fs-cell typography">
    			<h1>Search Results</h1>
        </div>
        <div class="fs-cell">
          <div class="breadcrumb">
            <a href="/" class="breadcrumb_item breadcrumb_link home">Home</a>
            <span class="breadcrumb_item">Search Results</span>
          </div>
        </div>
        <div class="fs-cell">
    			<hr>
    		</div>
    	</div>
    </header>

  	<section class="page_content_area">
  		<div class="fs-row">
        <div class="page_content fs-cell">
          <?php
            if(!user_is_logged_in()) {
              print $messages; 
            }
          ?>
          <div class="typography">
            <div class="search_results">
              <?php
                echo render($page["content"]);
              ?>
            </div>
          </div>
        </div>
      </div>
    </section>

  </main>
  <?php
   include ($directory . "/includes/footer.php");
  ?>
