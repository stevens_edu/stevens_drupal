<?php

function stevens_preprocess_html(&$vars) {
	$vars["head_scripts"] = drupal_get_js('header');
	$vars["footer_scripts"] = drupal_get_js('footer');
}

function stevens_preprocess_node(&$vars, $hook) {
  if (!empty($vars['type']) && $vars['type'] == 'stevens_flip_card_list') {
    $term_filter = $_GET['flip-filter-select--' . $vars['nid'] ];
    $filtered = (!empty($term_filter) && is_numeric($term_filter)) ? TRUE : FALSE;

    // Gather list of terms and flipcard metadata wrappers.
    $termlist = array();
    $flipcards = array();
    $element = $vars['elements']['field_cards'];
    $children = array_intersect_key($element, element_children($element));
    foreach ($children as $child) {
      $flipcard = array_shift($child['node'])['#node'];
      $wrapper = entity_metadata_wrapper('node', $flipcard->nid);
      $tags = field_get_items('node', $flipcard, 'field_flip_tags');
      if (empty($tags)) {
        $tags = array();
      }
      $in_filter = FALSE;
      foreach ($tags as $tag) {
        // Add each term object to $termlist, avoiding duplication.
        $term = taxonomy_term_load($tag['tid']);
        $termlist[$term->tid] = $term;
        if ($term->tid === $term_filter) {
          $termlist[$term->tid]->selected = TRUE;
          $in_filter = TRUE;
        }
      }
      // Only render flip cards that are filtered if one is present.
      if (!$filtered || $in_filter) {
        $flipcards[] = $wrapper;
      }
    }
    usort($termlist, '_stevens_term_sort_by_name');
    $vars['termlist'] = $termlist;
    $vars['flipcards'] = $flipcards;
    $vars['filtered'] = $filtered;
    $vars['current_url'] = url(current_path(), array('absolute' => TRUE));
  }
}

/**
 * Helper function to sort taxonomy terms by name.
 */
function _stevens_term_sort_by_name($a, $b) {
  return strcmp(strtolower($a->name), strtolower($b->name));
}

function stevens_preprocess_page(&$vars, $hook) {
	// Give node types their own page templates
	if ((isset($vars['node']->type)) && ($vars['node']->type != "")) {
		$vars['theme_hook_suggestions'][] = 'page__'.$vars['node']->type;
	}

	// Preserve the node title in case it gets overwritten
	if (isset($vars['node_title'])) {
		$vars['title'] = $vars['node_title'];
	}

	// Give page types their own named templates
	$illegal_chars = array(" ", "&");
	$remove_chars  = array(",");
	$title         = str_replace($illegal_chars, "_", strtolower($vars['node']->title));
	$title         = str_replace($remove_chars, "", $title);
	if ($vars['node']->type == 'page') {
		$vars['theme_hook_suggestions'][] = 'page__'.$title;
	}

	// Change page template for news landing
	$alias_parts = array_filter(explode('/', drupal_get_path_alias()));
	if (count($alias_parts) == 1 && $alias_parts[0] == "news") {
		$vars['theme_hook_suggestions'][] = 'page__newslanding';
	}

	// News releases
	$alias_parts = array_filter(explode('/', drupal_get_path_alias()));
	if (count($alias_parts) == 2 && $alias_parts[0] == "news" && $alias_parts[1] == "news-releases") {
		$vars['theme_hook_suggestions'][] = 'page__newsreleases';
	}
}

function stevens_breadcrumb($variables) {
	$breadcrumb           = $variables['breadcrumb'];// Determine if we are to display the breadcrumb.
	$breadcrumb_separator = ' ';
	array_shift($breadcrumb);

	$front_page = url('<front>');

	$open = '<div class="breadcrumb"><a href="'.$front_page.'" class="breadcrumb_item breadcrumb_link home">Home</a>';

	$node = menu_get_object();
	if ($node->type == "event") {
		$open .= '<a href="/events" class="breadcrumb_item breadcrumb_link">Events</a>';
	}

	$close = '</div>';

	$title = '';
	$item  = menu_get_item();
	if (!empty($item['tab_parent'])) {
		// If we are on a non-default tab, use the tab's title.
		$title = check_plain($item['title']);
	} else {
		$title = menu_get_active_title();
	}
	$title = '<span class="breadcrumb_item">'.$title.'</span>';

	if (drupal_get_path_alias() == "news/in-the-news") {
		$title = '<span class="breadcrumb_item">In The News</span>';
	}

	if (!empty($breadcrumb)) {
		$newcrumb = array();
		foreach ($breadcrumb as $crumb) {
			$pattern = '#href="([^"]*)"#';
			preg_match($pattern, $crumb, $crumblink);
			$crumbtitle = strip_tags($crumb, 'a');
			$out        = '<a href="'.$crumblink[1].'" class="breadcrumb_item breadcrumb_link" itemprop="url">'.$crumbtitle.'</a>';
			$newcrumb[] = $out;
		}
		return $open.implode($breadcrumb_separator, $newcrumb).$title.$close;
	} else {
		if (!drupal_is_front_page()) {
			return $open.$title.$close;
		}
	}
	return '';

}

function stevens_preprocess_block(&$variables) {
	$illegal_chars = array(" ", "&");
	$remove_chars  = array(",");
	$block_title   = str_replace($illegal_chars, "_", strtolower($variables['elements']['#block']->title));
	$block_title   = str_replace($remove_chars, "", $block_title);
	if (!empty($block_title) && $block_title !== '<none>') {
		$variables['theme_hook_suggestions'][] = 'block__'.$block_title;
	}
}

function stevens_menu_block_tree_alter(&$tree, &$config) {
	if (($config['admin_title'] == 'Sidebar Navigation') || ($config['admin_title'] == 'Schools Sidebar Navigation') || ($config['admin_title'] == 'Offices and Services Sidebar')) {
		$node = menu_get_object();

		$config['level'] = "1";
		$activetrail     = menu_get_active_trail();
		if (count($activetrail) > 1) {
			$current = array_pop($activetrail);
			if (isset($current['depth'])) {
				$currentdepth = intval($current['depth']);
				$parent       = array_pop($activetrail);
				$parent_node  = menu_get_object('node', 1, $parent['link_path']);
				if (!empty($node->field_hide_siblings['und'][0]['value'])) {
					menu_tree_prune_tree($tree, $currentdepth+1);
				} elseif (!empty($parent_node->field_hide_siblings['und'][0]['value']) && $parent_node->field_hide_siblings['und'][0]['value'] == '1') {
					menu_tree_prune_tree($tree, $currentdepth);
				} else
				if ($currentdepth > 1) {
					if ($current['has_children'] == "0") {
						if ($currentdepth > 2) {
							menu_tree_prune_tree($tree, $currentdepth-1);
							$config['follow'] = 0;
							//$config['parent_mlid'] = $parent['plid'];
						} else {
							menu_tree_prune_tree($tree, $currentdepth);
						}
					} else {
						menu_tree_prune_tree($tree, $currentdepth);
					}
				} else {
					menu_tree_prune_tree($tree, 2);
				}
			}
		}
	}
}

function stevens_menu_tree__main_menu($variables) {
	return $variables['tree'];
}

function stevens_menu_link__menu_block__1($variables) {
	$element  = $variables['element'];
	$sub_menu = '';
	$output   = '<div class="primary_nav_item nav_hover"><a class="primary_nav_link';
	if (in_array('active', $element['#attributes']['class']) || in_array('active-trail', $element['#attributes']['class'])) {
		$output .= ' active';
	}
	$element_title = $element['#title'];
	if (strlen($element_title) > 11) {
		$output .= ' two_lines';
		$last_space    = strrpos($element_title, " ");
		$element_title = substr_replace($element_title, ' <span>', $last_space, 1).'</span>';
	}
	$output .= '" href="'.url($element['#href']).'">'.$element_title.'</a>';

	if ($element['#original_link']['has_children'] == "1") {
		$output .= '<div class="primary_nav_children">';
		$parent     = menu_link_get_preferred($element['#original_link']['link_path'], "menu-primary-navigation");
		$parameters = array(
			'active_trail'      => array($parent['plid']),
			'only_active_trail' => FALSE,
			'min_depth'         => $parent['depth']+1,
			'max_depth'         => $parent['depth']+2,
			'conditions'        => array('plid'        => $parent['mlid']),
		);
		$children    = menu_build_tree($parent['menu_name'], $parameters);
		$menu_render = menu_tree_output($children);
		unset($menu_render['#sorted'], $menu_render['#theme_wrappers']);
		foreach ($menu_render as $child) {
			$output .= '<a class="primary_nav_child_link" href="'.url($child['#href']).'">'.$child['#title'].'</a>';
		}
		$output .= '</div>';
	}

	$output .= '</div>';
	return $output;
}

function stevens_menu_tree__3($variables) {
	return $variables['tree'];
}

function stevens_menu_link__menu_block__3($variables) {
	$element = $variables['element'];
	$output  = '<li>';
	$classes = array('subnavigation_link');
	if (in_array('active', $element['#attributes']['class']) || in_array('active-trail', $element['#attributes']['class'])) {
		$classes[] = 'active';
	}

	$output .= l($element['#title'], $element['#href'], array('attributes' => array('class' => $classes)));

	if (in_array('active-trail', $element['#attributes']['class']) && count($element['#below'])) {
		$output .= '<ul class="subnavigation_child_links">';
		foreach ($element['#below'] as $item) {
			if (isset($item['#title'])) {
				$output .= '<li><a href="'.url($item['#href']).'" class="subnavigation_child_link';
				foreach ($item['#attributes']['class'] as $class) {
					if ($class == 'active') {
						$output .= ' active';
					}
				}
				$output .= '">'.$item['#title'].'</a></li>';
			}
		}
		$output .= '</ul>';

	}
	$output .= "</li>";
	return $output;
}

// Same, but for schools
function stevens_menu_tree__7($variables) {
	return $variables['tree'];
}

function stevens_menu_link__menu_block__7($variables) {
	$element = $variables['element'];
	$output  = '<li>';
	$classes = array('subnavigation_link');
	if (in_array('active', $element['#attributes']['class']) || in_array('active-trail', $element['#attributes']['class'])) {
		$classes[] = 'active';
	}

	$output .= l($element['#title'], $element['#href'], array('attributes' => array('class' => $classes)));

	if (in_array('active-trail', $element['#attributes']['class']) && count($element['#below'])) {
		$output .= '<ul class="subnavigation_child_links">';
		foreach ($element['#below'] as $item) {
			if (isset($item['#title'])) {
				$output .= '<li><a href="'.url($item['#href']).'" class="subnavigation_child_link';
				foreach ($item['#attributes']['class'] as $class) {
					if ($class == 'active') {
						$output .= ' active';
					}
				}
				$output .= '">'.$item['#title'].'</a></li>';
			}
		}
		$output .= '</ul>';

	}
	$output .= "</li>";
	return $output;
}

// Same, but for offices
function stevens_menu_tree__8($variables) {
	return $variables['tree'];
}

function stevens_menu_link__menu_block__8($variables) {
	$element = $variables['element'];
	$output  = '<li>';
	$classes = array('subnavigation_link');
	if (in_array('active', $element['#attributes']['class']) || in_array('active-trail', $element['#attributes']['class'])) {
		$classes[] = 'active';
	}

	$output .= l($element['#title'], $element['#href'], array('attributes' => array('class' => $classes)));

	if (in_array('active-trail', $element['#attributes']['class']) && count($element['#below'])) {
		$output .= '<ul class="subnavigation_child_links">';
		foreach ($element['#below'] as $item) {
			if (isset($item['#title'])) {
				$output .= '<li><a href="'.url($item['#href']).'" class="subnavigation_child_link';
				foreach ($item['#attributes']['class'] as $class) {
					if ($class == 'active') {
						$output .= ' active';
					}
				}
				$output .= '">'.$item['#title'].'</a></li>';
			}
		}
		$output .= '</ul>';

	}
	$output .= "</li>";
	return $output;
}

function stevens_menu_tree__menu_utilities($variables) {
	return $variables['tree'];
}
function stevens_menu_link__menu_utilities($variables) {
	$element = $variables['element'];
	return '<a class="utility_nav_link" href="'.url($element['#href']).'">'.$element['#title'].'</a>';
}

function stevens_menu_tree__menu_social_media($variables) {
	return $variables['tree'];
}
function stevens_menu_link__menu_social_media($variables) {
	$element = $variables['element'];
	$url     = parse_url($element['#href']);
	$class   = 'footer_social_media_hub_link';
	if (isset($url['host'])) {
		if (strpos($url['host'], 'facebook') !== false) {
			$class = 'footer_social_media_link facebook';
		}
		if (strpos($url['host'], 'twitter') !== false) {
			$class = 'footer_social_media_link twitter';
		}
		if (strpos($url['host'], 'weibo') !== false) {
			$class = 'footer_social_media_link weibo';
		}
		if (strpos($url['host'], 'tumblr') !== false) {
			$class = 'footer_social_media_link tumblr';
		}
		if (strpos($url['host'], 'instagram') !== false) {
			$class = 'footer_social_media_link instagram';
		}
		if (strpos($url['host'], 'goog') !== false) {
			$class = 'footer_social_media_link googleplus';
		}
		if (strpos($url['host'], 'youtu') !== false) {
			$class = 'footer_social_media_link youtube';
		}
		if (strpos($url['host'], 'linkedin') !== false) {
			$class = 'footer_social_media_link linkedin';
		}
	}
	return '<a class="'.$class.'" href="'.url($element['#href']).'">'.$element['#title'].'</a>';
}

function stevens_menu_link__menu_block__6($variables) {
	$element = $variables['element'];
	$url     = parse_url($element['#href']);
	$class   = '';
	if (isset($url['host'])) {
		if (strpos($url['host'], 'facebook') !== false) {
			$class = 'social_sidebar_link_facebook';
		}
		if (strpos($url['host'], 'twitter') !== false) {
			$class = 'social_sidebar_link_twitter';
		}
		if (strpos($url['host'], 'weibo') !== false) {
			$class = 'social_sidebar_link_weibo';
		}
		if (strpos($url['host'], 'tumblr') !== false) {
			$class = 'social_sidebar_link_tumblr';
		}
		if (strpos($url['host'], 'instagram') !== false) {
			$class = 'social_sidebar_link_instagram';
		}
		if (strpos($url['host'], 'goog') !== false) {
			$class = 'social_sidebar_link_googleplus';
		}
		if (strpos($url['host'], 'youtu') !== false) {
			$class = 'social_sidebar_link_youtube';
		}
		if (strpos($url['host'], 'linkedin') !== false) {
			$class = 'social_sidebar_link_linkedin';
		}
	}
	if (!empty($class)) {
		return '<div class="social_sidebar_item"><a class="block social_sidebar_link '.$class.'" href="'.url($element['#href']).'">'.$element['#title'].'</a></div>';
	}
	return '</div><footer class="social_sidebar_footer"><a class="link_arrow social_sidebar_footer_link" href="'.url($element['#href']).'">'.$element['#title'].'</a></footer>';
}

function stevens_menu_tree__menu_contact($variables) {
	return $variables['tree'];
}
function stevens_menu_link__menu_contact($variables) {
	$element = $variables['element'];
	$class   = 'footer_contact_link';
	if (stripos($element['#title'], 'Information') !== false) {
		$class .= ' info';
	}
	if (stripos($element['#title'], 'Feedback') !== false) {
		$class .= ' feedback';
	}
	if (stripos($element['#href'], 'tel') !== false) {
		$class .= ' phone';
	}
	if (stripos($element['#href'], 'mailto') !== false) {
		$class .= ' email';
	}
	return '<a class="'.$class.'" href="'.url($element['#href']).'">'.$element['#title'].'</a>';
}

/*

<div class="item-list">
<ul class="pager">
<li class="pager-previous pager-disabled">
<a title="Go to previous page" href="#">previous</a>
</li>
<li class="pager-current">1</li>
<li class="pager-item">
<a title="Go to page 2" href="#">2</a>
</li>
<li class="pager-item">
<a title="Go to page 3" href="#">3</a>
</li>
<li class="pager-item">
<a title="Go to page 4" href="#">4</a>
</li>
<li class="pager-item pager-item-last">
<a title="Go to page 5" href="#">5</a>
</li>
<li class="pager-next">
<a title="Go to next page" href="#">next</a>
</li>
</ul>
</div>

 */

function stevens_pager($variables) {
	//dpm($variables);
	$tags       = $variables['tags'];
	$element    = $variables['element'];
	$parameters = $variables['parameters'];
	$quantity   = $variables['quantity'];
	global $pager_page_array, $pager_total;

	// Calculate various markers within this pager piece:
	// Middle is used to "center" pages around the current page.
	$pager_middle = ceil($quantity/2);
	// current is the page we are currently paged to
	$pager_current = $pager_page_array[$element]+1;
	// first is the first page listed by this pager piece (re quantity)
	$pager_first = $pager_current-$pager_middle+1;
	// last is the last page listed by this pager piece (re quantity)
	$pager_last = $pager_current+$quantity-$pager_middle;
	// max is the maximum page number
	$pager_max = $pager_total[$element];
	// End of marker calculations.

	// Prepare for generation loop.
	$i = $pager_first;
	if ($pager_last > $pager_max) {
		// Adjust "center" if at end of query.
		$i          = $i+($pager_max-$pager_last);
		$pager_last = $pager_max;
	}
	if ($i <= 0) {
		// Adjust "center" if at start of query.
		$pager_last = $pager_last+(1-$i);
		$i          = 1;
	}
	// End of generation loop preparation.

	$li_first = '';
	//theme('pager_first', array('text' => (isset($tags[0]) ? $tags[0] : t('« first')), 'element' => $element, 'parameters' => $parameters));
	$li_previous = theme('pager_previous', array('text' => (isset($tags[1])?$tags[1]:t('‹ previous')), 'element' => $element, 'interval' => 1, 'parameters' => $parameters));
	$li_next     = theme('pager_next', array('text'     => (isset($tags[3])?$tags[3]:t('next ›')), 'element'     => $element, 'interval'     => 1, 'parameters'     => $parameters));
	$li_last     = '';
	//theme('pager_last', array('text' => (isset($tags[4]) ? $tags[4] : t('last »')), 'element' => $element, 'parameters' => $parameters));

	if ($pager_total[$element] > 1) {
		// if ($li_first) {
		//   $items[] = array(
		//     'class' => array('pager-first'),
		//     'data' => $li_first,
		//   );
		// }
		if ($li_previous) {
			$items[] = array(
				'class' => array('pager-previous'),
				'data'  => $li_previous,
			);
		}

		// When there is more than one page, create the pager list.
		if ($i != $pager_max) {
			if ($i > 1) {
				$items[] = array(
					'class' => array('pager-ellipsis'),
					'data'  => '…',
				);
			}
			// Now generate the actual pager piece.
			for (; $i <= $pager_last && $i <= $pager_max; $i++) {
				if ($i < $pager_current) {
					$items[] = array(
						'class' => array('pager-item'),
						'data'  => theme('pager_previous', array('text'  => $i, 'element'  => $element, 'interval'  => ($pager_current-$i), 'parameters'  => $parameters)),
					);
				}
				if ($i == $pager_current) {
					$items[] = array(
						'class' => array('pager-current'),
						'data'  => $i,
					);
				}
				if ($i > $pager_current) {
					$items[] = array(
						'class' => array('pager-item'),
						'data'  => theme('pager_next', array('text'  => $i, 'element'  => $element, 'interval'  => ($i-$pager_current), 'parameters'  => $parameters)),
					);
				}
			}
			if ($i < $pager_max) {
				$items[] = array(
					'class' => array('pager-ellipsis'),
					'data'  => '…',
				);
			}
		}
		// End generation.
		if ($li_next) {
			$items[] = array(
				'class' => array('pager-next'),
				'data'  => $li_next,
			);
		}
		if ($li_last) {
			$items[] = array(
				'class' => array('pager-last'),
				'data'  => $li_last,
			);
		}
		return theme('item_list', array(
				'items'      => $items,
				'attributes' => array('class' => array('pager')),
			));
	}
}

function stevens_pager_next($variables) {
	$text       = $variables['text'];
	$element    = $variables['element'];
	$interval   = $variables['interval'];
	$parameters = $variables['parameters'];
	global $pager_page_array, $pager_total;
	$output = '';

	// If we are anywhere but the last page
	if ($pager_page_array[$element] < ($pager_total[$element]-1)) {
		$page_new = pager_load_array($pager_page_array[$element]+$interval, $element, $pager_page_array);
		// If the next page is the last page, mark the link as such.
		if ($page_new[$element] == ($pager_total[$element]-1)) {
			$output = theme('pager_last', array('text' => $text, 'element' => $element, 'parameters' => $parameters));
		}
		// The next page is not the last page.
		 else {
			$output = theme('pager_link', array('text' => $text, 'page_new' => $page_new, 'element' => $element, 'parameters' => $parameters));
		}
	} else {
		$output = '<li class="pager-next pager-disabled"><a href="#">next</a></li>';
	}

	return $output;
}

function stevens_pager_previous($variables) {
	$text       = $variables['text'];
	$element    = $variables['element'];
	$interval   = $variables['interval'];
	$parameters = $variables['parameters'];
	global $pager_page_array;
	$output = '';

	// If we are anywhere but the first page
	if ($pager_page_array[$element] > 0) {
		$page_new = pager_load_array($pager_page_array[$element]-$interval, $element, $pager_page_array);

		// If the previous page is the first page, mark the link as such.
		if ($page_new[$element] == 0) {
			$output = theme('pager_first', array('text' => $text, 'element' => $element, 'parameters' => $parameters));
		}
		// The previous page is not the first page.
		 else {
			$output = theme('pager_link', array('text' => $text, 'page_new' => $page_new, 'element' => $element, 'parameters' => $parameters));
		}
	} else {
		$output = '<li class="pager-previous pager-disabled"><a href="#">previous</a></li>';
	}

	return $output;
}

/*
 * Implements hook_theme
 */
function stevens_theme($existing, $type, $theme, $path) {
	return array(
		'statistic_large' => array(
			'variables'      => array(
				'params'        => array(),
			),
			'template' => 'templates/statistic-large',
		),
		'statistic_small' => array(
			'variables'      => array(
				'params'        => array(),
			),
			'template' => 'templates/statistic-small',
		),
	);
}

function stevens_theme_links($link, $class = NULL, $title = NULL) {
	$url = $link['url'];
	if (is_array($link['query']) && sizeof($link['query']) > 0) {
		$url .= '?';
		foreach ($link['query'] as $key => $value) {
			$url .= $key.'='.$value;
			if ($value !== end($link['query'])) {$url .= '&';
			}
		}
	}

	if ($title) {
		return '<a href="'.$url.'" class="'.$class.'">'.$title.'</a>';
	} else {
		return '<a href="'.$url.'" class="'.$class.'">'.$link['title'].'</a>';
	}
}

function stevens_url_fix($link) {
	$url = $link['url'];
	if (isset($link["query"]) && array_filter((array) $link['query'])) {
		$url .= '?';
		foreach ($link['query'] as $key => $value) {
			$url .= $key.'='.$value;
			if ($value !== end($link['query'])) {$url .= '&';
			}
		}
	}

	return $url;
}

function stevens_autoplay_url($link) {
	if (!is_array($link["query"])) {
		$link["query"] = array("autoplay" => "true");
	} else {
		$link["query"]["autoplay"] = "true";
	}
	return $link;
}

function stevens_relative_time($utimestamp) {
	if (isset($utimestamp)) {
		$types    = Array('s', 'm', 'h', 'd', 'w', 'm', 'y', 'y10');
		$duration = Array(60, 60, 24, 7, 4.35, 12, 10);
		$gap      = (time()-$utimestamp);
		for ($i = 0; $gap >= $duration[$i]; $i++) {
			$gap /= $duration[$i];
			$gap = round($gap);
		}
		if ($gap != 1) {
			$reltime = $gap.$types[$i];
		}
		return $reltime;
	}
}
function stevens_link_tweets($tweet) {
	$tweet = preg_replace("/([\w]+\:\/\/[\w\-?&;#~=\.\/\@]+[\w\/])/", "<a target=\"_blank\" href=\"$1\">$1</a>", $tweet);
	$tweet = preg_replace("/#([A-Za-z0-9\/\.]*)/", "<a target=\"_new\" href=\"http://twitter.com/search?q=$1\">#$1</a>", $tweet);
	$tweet = preg_replace("/@([A-Za-z0-9\/\.]*)/", "<a href=\"http://www.twitter.com/$1\">@$1</a>", $tweet);
	return $tweet;
}

function stevens_get_curriculum($program) {
	$cacheobj = 'curriculum'.$program;
	if (!cache_get($cacheobj)) {
		$url           = 'https://stevens_edu:answerunicornphonelight@api.stevens.edu/a/programs/'.$program.'/curriculum';
		$request       = drupal_http_request($url);
		$json_response = drupal_json_decode($request->data);
		$out           = cache_set($cacheobj, $json_response, (3600*24));//stores in cache table and expires after 24 hours.
		return ($json_response);
	} else {
		$return = cache_get($cacheobj);
		return ($return->data);
	}
}

function stevens_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == "search_block_form") {
		$form['search_block_form']['#attributes']['class'] = array('search_input');
		$form['search_block_form']['#attributes']['autocomplete'] = array('off');
    $form['#attributes'] = array('class' => 'search_form');
    $form['actions']['submit']['#attributes']['class'][] = 'search_submit';
    $form['actions']['submit']['#value'] = 'Go';
  }
	if (strstr($form_id, 'webform_client_form')) {
		$form['stevens_captcha_element'] = array(
			'#type'         => 'captcha',
			'#captcha_type' => 'image_captcha',
			'#title'        => 'Spurious!',
		);
	}

}
function stevens_trim_length($string, $length) {
	$ns       = "";
	$opentags = array();
	$string   = trim($string);
	if (strlen(html_entity_decode(strip_tags($string))) < $length) {
		return $string;
	}
	if (strpos($string, " ") === false && strlen(html_entity_decode(strip_tags($string))) > $length) {
		return substr($string, 0, $length)."&hellip;";
	}
	$x = 0;
	$z = 0;
	while ($z < $length && $x <= strlen($string)) {
		$char = substr($string, $x, 1);
		$ns .= $char;// Add the character to the new string.
		if ($char == "<") {
			// Get the full tag -- but compensate for bad html to prevent endless loops.
			$tag = "";
			while ($char != ">" && $char !== false) {
				$x++;
				$char = substr($string, $x, 1);
				$tag .= $char;
			}
			$ns .= $tag;

			$tagexp  = explode(" ", trim($tag));
			$tagname = str_replace(">", "", $tagexp[0]);

			// If it's a self contained <br /> tag or similar, don't add it to open tags.
			if ($tagexp[1] != "/" && $tagexp[1] != "/>") {
				// See if we're opening or closing a tag.
				if (substr($tagname, 0, 1) == "/") {
					$tagname = str_replace("/", "", $tagname);
					// We're closing the tag. Kill the most recently opened aspect of the tag.
					$done = false;
					reset($opentags);
					while (current($opentags) && !$done) {
						if (current($opentags) == $tagname) {
							unset($opentags[key($opentags)]);
							$done = true;
						}
						next($opentags);
					}
				} else {
					// Open a new tag.
					$opentags[] = $tagname;
				}
			}
		} elseif ($char == "&") {
			$entity = "";
			while ($char != ";" && $char != " " && $char != "<") {
				$x++;
				$char = substr($string, $x, 1);
				$entity .= $char;
			}
			if ($char == ";") {
				$z++;
				$ns .= $entity;
			} elseif ($char == " ") {
				$z += strlen($entity);
				$ns .= $entity;
			} else {
				$z += strlen($entity);
				$ns .= substr($entity, 0, -1);
				$x -= 2;
			}
		} else {
			$z++;
		}
		$x++;
	}
	while ($x < strlen($string) && !in_array(substr($string, $x, 1), array(" ", "!", ".", ",", "<", "&"))) {
		$ns .= substr($string, $x, 1);
		$x++;
	}
	if (strlen(strip_tags($ns)) < strlen(strip_tags($string))) {
		$ns .= "&hellip;";
	}
	$opentags = array_reverse($opentags);
	foreach ($opentags as $key => $val) {
		$ns .= "</".$val.">";
	}
	return $ns;
}

function stevens_parse_date_time($start, $end, $all_day) {
	$start_year  = date("Y", $start);
	$end_year    = date("Y", $end);
	$start_month = date("M", $start);
	$end_month   = date("M", $end);
	$start_day   = date("j", $start);
	$end_day     = date("j", $end);
	$start_date  = date("Y-m-d", $start);
	$end_date    = date("Y-m-d", $end);
	$start_time  = date("g:iA", $start);
	$end_time    = date("g:iA", $end);

	if ($start_date == $end_date) {
		// Same day, easy peasy
		$draw_date = $start_day." ".$start_month." ".$start_year;
	} else if ($start_year != $end_year || $start_month != $end_month) {
		// Years or months are different, draw the whole thing twice
		$draw_date = $start_day." ".$start_month." ".$start_year." &ndash; ".$end_day." ".$end_month." ".$end_year;
	} else {
		// Just the days are different so we'll draw a date span
		$draw_date = $start_day."&ndash;".$end_day." ".$start_month." ".$start_year;
	}

	if ($all_day) {
		$draw_time = "All Day";
	} else if ($start_time != $end_time) {
		$draw_time = $start_time." &ndash; ".$end_time;
	} else {
		$draw_time = $start_time;
	}

	return array($draw_date, $draw_time);
}

function stevens_current_url() {
	return ($_SERVER["SERVER_PORT"] == 80?"http://":"https://").$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
}

// Returns a pagination array
function stevens_pagination_array($pages, $current_page) {
	// If we have 10 or less pages, just draw them all.
	if ($pages < 8) {
		$start_page = 1;
		$end_page   = $pages;
		// Otherwise we need to figure out where we are...
	} else {
		if ($current_page < 7) {
			$start_page = 1;
			$end_page   = 9;
		} else if ($current_page > $pages-7) {
			$start_page = $pages-9;
			$end_page   = $pages;
		} else {
			$start_page = $current_page-4;
			$end_page   = $current_page+5;
		}
	}

	$page_array = array();
	if ($start_page > 1) {
		$page_array[] = "&hellip;";
	}
	for ($i = $start_page; $i <= $end_page; $i++) {
		$page_array[] = $i;
	}
	if ($end_page < $pages) {
		$page_array[] = "&hellip;";
	}

	return $page_array;
}

/**
 * Implements hook_preprocess_search_results().
 */
function stevens_preprocess_search_results(&$vars) {
	$vars['search_totals'] = $GLOBALS['pager_total_items'][0];
}

/**
 * Implements theme_form_required_marker().
 */
function stevens_form_required_marker($variables) {
  // This is also used in the installer, pre-database setup.
  $t = get_t();
  $attributes = array(
    'class' => 'form-required',
  );
  return '<span' . drupal_attributes($attributes) . '>*</span><span class="visually_hidden">Required</span>';
}

/**
 * Implements theme_form().
 */
function stevens_form($variables) {
  $element = $variables['element'];

  if (isset($element['#action'])) {
    $element['#attributes']['action'] = drupal_strip_dangerous_protocols($element['#action']);
  }
  element_set_attributes($element, array('method', 'id'));
  if (empty($element['#attributes']['accept-charset'])) {
    $element['#attributes']['accept-charset'] = "UTF-8";
  }
  $required_text = (_stevens_form_has_required($element)) ? '<p class="typography">Fields marked with an asterisk (*) are required.</p>' : '';
  // Anonymous DIV to satisfy XHTML compliance.
  return $required_text . '<form' . drupal_attributes($element['#attributes']) . '><div>' . $element['#children'] . '</div></form>';
}

/**
 * Utility funciton to check forms for required fields.
 */
function _stevens_form_has_required($form) {
  if (isset($form['#required']) && $form['#required']) {
    return TRUE;
  }
  foreach (element_children($form) as $key) {
    if (_stevens_form_has_required($form[$key])) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Utility to convert a YouTube URL into a YouTube Embed URL.
 */
function _stevens_youtube_url_to_embed($url) {
  $short_regex = '/youtu.be\/([a-zA-Z0-9_]+)\??/i';
  $long_regex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))(\w+)/i';

  if (preg_match($long_regex, $url, $matches)) {
    $youtube_id = $matches[count($matches) - 1];
  }

  if (preg_match($short_regex, $url, $matches)) {
    $youtube_id = $matches[count($matches) - 1];
  }

  if (!empty($youtube_id)) {
    return 'https://www.youtube.com/embed/' . $youtube_id;
  }
  else {
    return FALSE;
  }
}
