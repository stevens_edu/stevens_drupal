<div class="header_push_mobile"></div>
<div class="site_alert_placeholder"></div> 
<header id="header" class="header" role="banner">
  <div class="header_container">
    <a href="<?php echo $front_page; ?>" class="header_site_link">
      <div class="header_chevron"></div>
      <div class="header_logo" <?php if (drupal_is_front_page()) { echo 'role="heading" aria-level="1"'; } ?>>
        <img src="<?php echo $base_path . $directory; ?>/images/bw_logo.png?v=2" alt="<?php echo $site_name; ?> - <?php echo $site_slogan; ?>">
      </div>
    </a>
    <?php if ($page['header']): ?>
    <?php print render($page['header']); ?>
    <?php endif; ?>

    <button class="mobile_navigation_handle js-mobile_handle">Navigation &amp; Search<span class="mobile_navigation_close">Close</span></button>

  </div>
</header>
<div class="header_push_desktop"></div>
