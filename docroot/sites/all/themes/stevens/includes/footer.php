<!-- Footer -->
<footer id="footer" class="footer" role="contentinfo">
  <div class="footer_start">
    <div class="fs-row">
      <div class="fs-cell">
        <div class="footer_meta">
          <nav class="footer_navigation">
            <a href="https://my.stevens.edu" class="footer_nav_link"><span class="lowercase">my</span>Stevens</a>
            <?php
              $footer_menu = menu_navigation_links('menu-footer-navigation');

              foreach ($footer_menu as $menu_item) {
                $class_list = array();
                $class_list[] = 'footer_nav_link';
                if($menu_item['title'] == 'Events') {
                  $class_list[] = 'footer_events_link';
                }
                echo l($menu_item['title'], $menu_item['href'], array('attributes' => array('class' => $class_list)));
              }
            ?>
          </nav>
        </div>
      </div>
      <?php if ($page['footer_nav']): ?>
        <?php print render($page['footer_nav']); ?>
      <?php endif; ?>
    </div> <!-- end row -->
  </div>
  <div class="fs-row">
    <div class="fs-cell footer_end">
      <div class="footer_logo">
        <a href="/"><img src="/<?php echo drupal_get_path('theme', 'stevens'); ?>/images/footer_logo@2x.png?v=2" alt="Stevens Institute of Technology logo" width="241" height="109"></a>
      </div>
      <div class="footer_location">
          <p>&copy; <?php print(date('Y')); ?> <span class="uppercase">Stevens Institute of Technology</span></p>
          <p>1 Castle Point Terrace, Hoboken, NJ 07030 &nbsp;<a href="tel:+12012165000">201.216.5000</a></p>
          <p><a href="/privacypolicy">Privacy Policy</a> | <a href="/emergency">Emergency Info</a> | <a href="/titleix">Title IX Policy</a></p>
      </div>
      <div class="footer_social">
        <nav>
          <ul class="footer_social_media_links">
            <li class="footer_social_media_link"><a href="https://www.facebook.com/Stevens1870"><img src="/<?php echo drupal_get_path('theme', 'stevens'); ?>/images/icon_facebook@2x.gif" alt="Facebook" width="30" height="30"></a></li>
            <li class="footer_social_media_link"><a href="https://twitter.com/FollowStevens"><img src="/<?php echo drupal_get_path('theme', 'stevens'); ?>/images/icon_twitter@2x.gif" alt="Twitter" width="30" height="30"></a></li>
            <li class="footer_social_media_link"><a href="https://www.instagram.com/followstevens/"><img src="/<?php echo drupal_get_path('theme', 'stevens'); ?>/images/icon_instagram@2x.gif" alt="Instagram" width="30" height="30"></a></li>
            <li class="footer_social_media_link"><a href="http://www.youtube.com/EdwinAStevens70"><img src="/<?php echo drupal_get_path('theme', 'stevens'); ?>/images/icon_youtube@2x.gif" alt="YouTube" width="30" height="30"></a></li>
            <li class="footer_social_media_link"><a href="https://www.linkedin.com/company/stevens-institute-of-technology"><img src="/<?php echo drupal_get_path('theme', 'stevens'); ?>/images/icon_linkedin@2x.gif" alt="LinkedIn" width="30" height="30"></a></li>
            <li class="footer_social_media_hub_link"><a href="https://www.stevens.edu/news/social-media">More<span class="sro"> social media</span></a></li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</footer>
</div>
<?php if ($page['mobile_navigation']): ?>
<?php print render($page['mobile_navigation']); ?>
<?php endif; ?>
