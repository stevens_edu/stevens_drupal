/*-------------------------------------------
	Header
-------------------------------------------*/

	Site.modules.Header = (function($, Site) {
		var $header,
			$alert,
			$mobileNavigation,
			headerHeight,
			$clone,
			$page;

		function init() {
			$header = Site.$body.find("#header");
			$mobileNavigation = Site.$body.find(".mobile_navigation");
			$alert = Site.$body.find(".site_wide_alert");
			headerHeight = $header.find(".header_chevron").outerHeight() + 20;
			addAlertHeight();
			$page = Site.$body.find(".page_wrapper");



			$clone = $header.clone()
				.addClass('slim')
				.attr("id", "header_clone")
				.attr("aria-hidden", "true");

			$clone.find("#search-block-form").attr("id","search-block-form-slim");

			$clone.find("#edit-search-block-form--2").attr("id","edit-search-block-form--2-slim");
			$clone.find("label[for='edit-search-block-form--2']").attr("for","edit-search-block-form--2-slim");

			$clone.find("#edit-actions").attr("id","edit-actions-slim");
			$clone.find("label[for='edit-actions']").attr("for","edit-actions-slim");

			$clone.find("#edit-submit").attr("id","edit-submit-slim");
			$clone.find("label[for='edit-submit']").attr("for","edit-submit-slim");

			$clone.appendTo('.page_wrapper');

			$(".header.slim").find(".mobile_navigation_handle").remove();

			$mobileNavigation.on("activate.swap", ".mobile_secondary_nav_button", mobileScrollTo);

			Site.onScroll.push(scroll);
			Site.onRespond.push(respond);
			Site.scroll();
		}

		function scroll() {
			var scrollTop = Site.$window.scrollTop();
			if (scrollTop > headerHeight) {
				$page.addClass("sticky_header");
			} else {
				$page.removeClass("sticky_header");
			}
		}

		function respond() {
			scroll();
			headerHeight = $header.find(".header_chevron").outerHeight() + 20;
			addAlertHeight();
		}

		function addAlertHeight() {
			if ($alert.length > 0) {
				headerHeight += $alert.outerHeight();
			}
		}

		function mobileScrollTo() {
			$mobileNavigation.animate({ scrollTop: $(this).position().top });
		}

		Site.onInit.push(init);

		return {

		};
	})(jQuery, Site);
