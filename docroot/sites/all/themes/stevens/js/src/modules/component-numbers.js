/*-------------------------------------------
	Numbers
-------------------------------------------*/

	Site.modules.Numbers = (function($, Site) {
		var initialized = false,
			$component,
			$button,
			$set,
			visible,
			$loadedSets;

		function init() {
			$component  = Site.$body.find(".numbers");

			if ($component.length) {
				initialized  = true;
				$button      = $component.find(".numbers_more_button");
				$set         = $component.find(".numbers_set");

				$button.on("click", loadMore);
			}
		}

		function loadMore() {
			$loadedSets  = $component.find(".numbers_set.js-loaded");
			var items    = $($set).length;
			visible      = $loadedSets.size() + 1;

			$(".numbers_set:lt("+visible+")").addClass("js-loaded");

			$(".numbers_set").equalize("destroy");
			$(".numbers_set").equalize();

			$.doTimeout(50, function() {
				$component.find(".numbers_set.js-loaded").addClass("numbers_set_active");
			});

			if (visible === items) {
				$button.hide();
			}
		}

		Site.onInit.push(init);

		return {

		};
	})(jQuery, Site);
