/*-------------------------------------------
	Page
-------------------------------------------*/

	/* global picturefill */

	Site.modules.Page = (function($, Site) {

		function init() {

			/* Plugins
			-------------------------------------------*/

			/* Defaults */
			$.lightbox("defaults", {
				mobile: true
			});

			$.analytics({
				scrollDepth: true
			});

			/* Picturefill */
			picturefill();

			/* Mobile Main Navigation */
			Site.$body.find(".js-mobile_navigation").navigation({
				maxWidth: "1219px"
			});

			/* Mobile Subnavigation */
			Site.$body.find(".js-navigation")
				.navigation({
					maxWidth: "979px"
				})
				.on("open.navigation", function() {
					trackEvent( $(this).data("analytics-open") );
				}).on("close.navigation", function() {
					trackEvent( $(this).data("analytics-close") );
				});

			/* Cookie - Checklist */
			$(".checklist_container").each(function() {
				var listId = $(this).attr('id');
				$(this).find(".form-checkbox").each(function() {
					var $this = $(this),
					checkId = $this.attr('id');
					if ($.cookie("sit-checklist_" + listId + "_" + checkId) === "true") {
						$this.prop("checked", true);
					}
				});
			});

			/* Mobile directory dropdowns */
			Site.$body.find('.alpha_drop').on("change", function(){
				var $this = $(this);
				var $directoryForm = Site.$body.find('.directory_search_form');
				var $searchInput = $directoryForm.find('.search_input');
				$searchInput.val($this.val());
				$directoryForm.submit();
			});

			/* Formstone */
			Site.$body.find(".js-swap").swap();
			Site.$body.find(".js-background").background();
			Site.$body.find(".js-equalize").equalize();
			Site.$body.find(".js-lightbox").lightbox();
			Site.$body.find(".js-carousel").carousel();


			/* Formstone Forms */
			Site.$body.find(".js-checkbox, .js-radio, input[type=checkbox], input[type=radio]").checkbox();
			Site.$body.find(".js-dropdown, .form-item .form-select")
			  .dropdown()
			  .parent()
			  .each(function() {
			    // Label each link according to the button's text.
			    var lbl = $(this).find('select').attr('id') + "_fs_dropdown_button";
			    $(this).find('button').attr('id', lbl);
			    $(this).find('.fs-dropdown-options a.fs-dropdown-item').each(function(index) {
			      var a_lbl = lbl + "link_" + index;
			      $(this).attr('id', a_lbl);
			      $(this).attr('aria-labelledby', lbl + " " + a_lbl);
			    });
			  });
			Site.$body.find("input[type=number]").number();
			Site.$body.find("input[type=range]").range();


			/* Wrapper for Tables */
			Site.$body.find("table").wrap('<div class="table_wrapper"><div class="table_wrapper_inner"></div></div>');


      /* Override search forms to use GET instead of POST */
      $(".search_form, .search-form").not(".directory_search_form, .news_search_form, .program_finder_form").submit(function(e) {
        e.preventDefault();
        e.stopPropagation();
        var $inputElement = $(this).find("input[type=text]");
        var searchVal = encodeURI($inputElement.val());
        document.location.replace("/search/results/" + searchVal);
        return false;
      });

			/* Pikaday Date Picker
			if (Site.touch) {
				Site.$body.find(".js-datepicker, input[type=date]")
					.attr("type", "date")
					.removeAttr("placeholder")
					.on("blur", function() {
						$(this).trigger("datepicker.change");
					});
			} else {
				Site.$body.find(".js-datepicker, input[type=date]")
					.attr("type", "text")
					.pikaday({
						firstDay: 0,
						minDate: new Date("2000-01-01"),
						maxDate: new Date("2020-12-31"),
						yearRange: [ 2000, 2020 ],
						format: "MM/DD/YYYY",
						position: 'bottom left',
						onSelect: function() {
							$(".js-datepicker").trigger("datepicker.change");
						}
					});
			}
			*/

			/* Generic Toggles */
			Site.$body.find(".js-toggle")
				.not(".js-bound")
				.on("click", ".js-toggle_handle", onToggleClick)
				.addClass("js-bound");

			/* Scroll to Anchors/Elements */
			Site.$body.find(".js-scroll_to")
				.not(".js-bound")
				.on("click", onScrollTo)
				.addClass("js-bound");

			/* Video Embed */
			Site.$body.find(".js-video_embed")
				.not(".js-bound")
				.on("click", ".js-video_embed_handle", embedVideo)
				.addClass("js-bound");

			/* Responsive Video */
			$("iframe[src*='vimeo.com'], iframe[src*='youtube.com']", ".typography").each(function() {
				$(this).wrap('<div class="video_frame"></div>');
			});

			/* Scrolling */
			Site.onScroll.push(scroll);
			Site.onResize.push(resize);
			Site.onRespond.push(respond);
			Site.scroll();

			/* Cookie - Checklist */
			$(".checklist_container .form-checkbox").on("change", checkList);

			/* Print - Checklist */
			$(".print_icon").on("click", printClick);

			$(".secondary_nav_button").on("click", secondaryClick);

      /* Flip Cards */
      $('.flip-card--controls button').on('click', function() {
        $(this).closest('.flip-card--callout').toggleClass('flip-card--flipped');
      });

			/* Get Site Alert */
			$.get(
				'/views/ajax',
				{
					view_name: 'site_wide_alert',
					view_display_id: "block"
				},
				function (response) {
					if (response[1] !== undefined) {
						var viewHTML = response[1].data;
						$(".site_alert_placeholder").html(viewHTML);
						$(".site_wide_alert_container").on("click", ".js-toggle_handle", onToggleClick);

						/* Cookie - Site Wide Alert */
						$(".alert_close").on("click", closeAlert);
						if ($.cookie("sit-site-alert_" + $('.site_wide_alert_container').attr('id'))) {
							$(".site_wide_alert_container").addClass("js-toggle_active");
						}
					}
				}
			);

      /* Accordion */
      $(".accordion_item .acc_q").on("click", onAccordionToggle);
      // Check fragment ID on page load.
      var fragid = Site.window.location.hash;
      if ($(fragid).hasClass('accordion_item')) {
        $(fragid + " .acc_q").trigger('click');
      }
      // Check hash change.
      $(Site.window).on('hashchange', function() {
        var fragid = Site.window.location.hash;
        if ($(fragid).hasClass('accordion_item') && !$(fragid + ' .acc_q').hasClass('fs-swap-active')) {
          $(fragid + " .acc_q").trigger('click');
        }
      });
		}

		function scroll() {

		}

		function resize() {
			scroll();

			setTimeout(function(){
				Site.$body.find(".js-background").background("resize");
			}, 50);
		}

		function respond() {
			scroll();
		}

		function onScrollTo(e) {
			Site.killEvent(e);

			var $target = $(e.delegateTarget),
				id = $target.attr("href");

			scrollToElement(id);
		}

		function scrollToElement(id) {
			var $to = $(id);

			if ($to.length) {
				var offset = $to.offset();

				scrollToPosition(offset.top);
			}
		}

		function scrollToPosition(top) {
			$("html, body").animate({ scrollTop: top });
		}

		function onToggleClick(e) {
			Site.killEvent(e);

			var $target     = $(e.delegateTarget),
				activeClass = "js-toggle_active";

			if ($target.hasClass(activeClass)) {
				$target.removeClass(activeClass);
			} else {
				$target.addClass(activeClass);
			}
		}
		
		function onAccordionToggle(e) {
		  Site.killEvent(e);
		  
		  var $target = $(e.delegateTarget),
		      $sibling = $target.parent().parent().find('.toggle_target');
		  
		  $target.attr('aria-expanded', function(index, attr) {
		    return attr === 'false' ? 'true' : 'false';
		  });
		  $sibling.attr('aria-hidden', function(index, attr) {
		    return attr === 'false' ? 'true' : 'false';
		  });		  
		}

		function loadVideo(source) {
			var youtubeParts = source.match( /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i ), // 1
				vimeoParts   = source.match( /(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)/ ), // 3
				url = (youtubeParts !== null) ? "//www.youtube.com/embed/" + youtubeParts[1] : "//player.vimeo.com/video/" + vimeoParts[3];

			return '<div class="video_frame"><iframe src="' + url + '?autoplay=1" seamless="seamless"></iframe></div>';
		}

		function embedVideo(e) {
			var $this = $(e.delegateTarget),
				source = $this.data("video-source"),
				$container = $this.find($($this.data("video-container")));
				$this.addClass("js-video_embedded");
			$container.html(loadVideo(source));
		}

		function trackEvent(data) {
			if ($.type(data) === "string") {
				data = data.split(",");

				$.analytics.apply(this, data);
			}
		}

		function closeAlert() {
			var id = $(".site_wide_alert_container").attr('id');
			$.cookie("sit-site-alert_" + id, true, { expires: 86400000 }); /* 1 day in milliseconds */

			setTimeout(function() {
				Site.modules.Homepage.homeCarousel();
			}, 0);
		}

		function checkList() {
			var $this = $(this),
			checkId = $this.attr('id'),
			listId = $this.parents(".checklist_container").attr('id');
			if ($this.prop("checked")) {
				$.cookie("sit-checklist_" + listId + "_" + checkId, true);
			} else {
				$.cookie("sit-checklist_" + listId + "_" + checkId, false);
			}
		}

		function printClick() {
			var w = window.open('', 'printwindow'),
			is_chrome = Boolean(window.chrome),
			div = $(this).closest(".checklist_container").find(".js_checklist").html();

			$(w.document.write('<html><head><title>Checklist</title><link rel="stylesheet" type="text/css" href="/sites/all/themes/stevens/css/site.css"></head><body>'));
			$(w.document.body).html(div);
			$(w.document.write('</body></html>'));
			if (is_chrome) {
				setTimeout(function() {
					w.print();
					w.close();
				}, 250);
			} else {
				w.print();
				w.close();
			}
			return true;
		}

		function secondaryClick() {
			var $navItems = $(".secondary_nav_item");
			var $thisNavItem = $(this).closest(".secondary_nav_item");

			if ($thisNavItem.hasClass("active")) {
				$thisNavItem.removeClass("active");
			} else {
				$navItems.removeClass("active");
				$thisNavItem.addClass("active");
			}
		}

		/* Hook Into Main init Routine */
		Site.onInit.push(init);

		return {

		};
	})(jQuery, Site);
