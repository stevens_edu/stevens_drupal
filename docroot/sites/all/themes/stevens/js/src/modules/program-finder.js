/*-------------------------------------------
	Program Finder
-------------------------------------------*/


	Site.modules.ProgramFinder = (function($, Site) {
		var $programFinder,
			$programFinderForm,
			$programFinderInput,
			$programFinderResults,
			$programFinderResultsMessage,
			$programFinderResultType,
			$programFinderButton,
			resultType,
			resultsMessagePartOne = "Showing results for “<span class=\"color_red\">",
			resultsMessagePartTwo = "</span>,” start a new search above.",
			minChars = 3,
			value,
			block_type;

		function init() {
			$programFinder = Site.$body.find(".js-program_finder");
			$programFinderForm = $programFinder.find(".program_finder_form");
			$programFinderResults = $programFinder.find(".js-program_finder_results");
			$programFinderResultsMessage = $programFinder.find(".js-program_finder_results_message");
			$programFinderInput = $programFinderForm.find(".js-program_finder_search_input");
			$programFinderResultType = $programFinderForm.find("#program_finder_result_type");
			$programFinderButton = $programFinder.find(".js-program_finder_button");

			$programFinderButton.on("click", updateResultType);

			$programFinderInput.on("keyup", function () {
				$(this).doTimeout('typing', 300, getInput);
			});

			$programFinderForm.on("submit", interceptSubmit);

		}

		function getInput() {
			var value = $programFinderInput.val();
			if (value.length >= minChars) {
				loadContent();
			}
		}

		function updateResultType() {
			var $this = $(this);
			$programFinderResultType.val($this.data("program-type"));
			loadContent();
		}

		function interceptSubmit(e) {
			Site.killEvent(e);
			loadContent();
		}

		function loadContent() {
			value = $programFinderInput.val();
			if (value === '') {
				value = 'Business';
			}
			resultType = $programFinderResultType.val();
			block_type = 'block';
			if (resultType === 'graduate') {
				block_type = 'block_1';
			}

			$.get(
				'/views/ajax',
				{
					view_name: 'program_finder',
					view_display_id: block_type,
					"field_program_keywords_value": value.toLowerCase().trim()
				},
				function (response) {
						if (response[1] !== undefined) {
							var viewHTML = response[1].data;
							$programFinderResultsMessage.html(resultsMessagePartOne + value.trim() + resultsMessagePartTwo);
							$programFinderResults.html(viewHTML);
							$programFinder.find(".js-carousel").carousel("destroy").carousel();
						}
				}
			);
		}

		Site.onInit.push(init);

		return {

		};
	})(jQuery, Site);
