/*-------------------------------------------
	Content Loading
-------------------------------------------*/

	Site.modules.News = (function($, Site) {
		var $component,
			$button,
			$set,
			visible,
			$loadedSets;

		function init() {
			$component  = Site.$body.find(".content_loader");

			if ($component.length) {
					$button      = $component.find(".load_more_button");

					$button.on("click", loadMore);
			}
		}

		function loadMore() {
			$set         = $(this).closest($component).find(".content_set");
			$loadedSets  = $(this).closest($component).find(".content_set.js-loaded");
			var items    = $($set).length;
			visible      = $loadedSets.size() + 1;

			$(this).closest($component).find(".content_set:lt("+visible+")").addClass("js-loaded");

			if (visible === items) {
				$(this).hide();
			}
		}

		Site.onInit.push(init);

		return {

		};
	})(jQuery, Site);
