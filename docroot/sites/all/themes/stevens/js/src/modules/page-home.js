/*-------------------------------------------
	Homepage
-------------------------------------------*/

	Site.modules.Homepage = (function($, Site) {
		var $galleryObject,
			$galleryButtons,
			$galleries,
			$contentSwapObjects,
			$carousel,
			$carouselContainer,
			$siteHeader,
			$siteAlert,
			$carouselBlock,
			$secondaryAlert;

		function init() {

			/* Homepage Gallery Component */
			$galleryObject = Site.$body.find(".homepage_gallery");
			$galleryButtons = $galleryObject.find(".homepage_gallery_button");
			$galleries = $galleryObject.find(".homepage_gallery_carousel");
			$carousel = Site.$body.find(".page_header_home_carousel .fs-carousel-viewport");
			$carouselContainer= Site.$body.find(".page_header_home_carousel");
			$siteHeader = Site.$body.find(".header");
			$siteAlert = Site.$body.find(".site_wide_alert");
			$carouselBlock = $carousel.find(".page_header_home_block_content_container");
			$secondaryAlert = Site.$body.find(".alert-secondary-bg");

			$galleryButtons.on("activate.swap, deactivate.swap", function() {
				var $this = $(this),
					$elem = $($this.data("swap-target"));

				$elem.find(".video_frame").remove();
				$galleries.removeClass("js-video_embedded");
			});

			/* Responsive Content Swapping */
			$contentSwapObjects = Site.$body.find(".js-content_swap");

			$carouselContainer.on("update.carousel", onHomeCarouselUpdate);

			homeCarousel();
			// onHomeCarouselUpdate();

			Site.onRespond.push(respond);
			Site.onResize.push(resize);
			Site.onScroll.push(homeCarousel);

			$carousel.find(".page_header_home_block.fs-carousel-visible").background("play");
		}

		function respond() {
			$contentSwapObjects.each(swapContent);
		}

		function swapContent() {
			var $this = $(this),
				content = false;

			if (Site.minWidth > 500 && Site.minWidth < 980) {
				content = $this.data("content-swap-tablet");
			} else if (Site.minWidth < 740) {
				content = $this.data("content-swap-mobile");
			}

			if (!content) {
				content = $this.data("content-swap-default");
			}

			$this.html(content);
		}

		function homeCarousel() {
			if ($carousel.length) {
				var scrollTop        = Site.$window.scrollTop();
				var carouselHeight   = $carousel.innerHeight();
				var headerHieght     = $siteHeader.innerHeight();
				var alertHeight      = $siteAlert.innerHeight();
				var windowHeight     = Site.window.innerHeight;
				var secondaryAlertHeight = $secondaryAlert.innerHeight();
				var heightOffset     = windowHeight - (carouselHeight + headerHieght + alertHeight + secondaryAlertHeight);
				var blockMargin      = (heightOffset * -1) - scrollTop;

				if (blockMargin > 0) {
					$carouselBlock.css("transform", "translateY(" + (blockMargin * -1) + "px)", "-webkit-transform", "translateY(" + (blockMargin * -1) + "px)");
				} else {
					$carouselBlock.css("transform", "translateY(0)", "-webkit-transform", "translateY(0)");
				}
			}
		}

		function onHomeCarouselUpdate() {
			$carousel.find(".page_header_home_block.fs-carousel-item").not(".fs-carousel-visible").background("pause");

			$carousel.find(".page_header_home_block.fs-carousel-visible").background("play");
		}

		function resize() {
			if ($carousel.length) {
				setTimeout(function() {
					homeCarousel();
				}, 100);
			}
		}

		Site.onInit.push(init);

		return {
			homeCarousel:homeCarousel
		};
	})(jQuery, Site);
