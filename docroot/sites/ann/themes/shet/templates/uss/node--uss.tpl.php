<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <div class="content clearfix"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
    ?>

<?php /* start drop-in */ ?>




<style type="text/css">
img {max-width: 100%; }       
          
@media only screen and (max-width: 600px) {
  .wrapper { width: 95% !important;}
  .logo { width: auto; display: block !important; margin-right: 0 !important;}
  .label { width: auto; display: block !important; }
  .portrait { width: 125px !important; display: block !important;}
  .info {width: auto !important; display: block !important;}
  .description {margin-right: 0 !important; margin-left: 0 !important;}
}
@media only screen and (max-width: 400px) {
  .portrait { width: 75px !important; display: block !important;}
}  
    
</style>
      
<!--- VIEW IN BROWSER------------------------>
      
<table bgcolor="#f0f0f0"
       style="width: 100%;">
    <tr>
        <td style="display: block !important;
                   clear: both !important;
                   margin-top: 0;                          
                   margin-right: auto;                           
                   margin-bottom: 0;                           
                   margin-left: auto;">
            <table align="center"
                   class="wrapper"
                   style="width: 580px;
                          margin-top: 0;                           
                          margin-right: auto;                           
                          margin-bottom: 0;                           
                          margin-left: auto;">
                <tr>
                <td>
            <center>
                    <p style="font-family: Arial, sans-serif;
                            color: #9A989A;
                            font-weight: normal;
                            font-size: 12px;
                            line-height: 1;
                            margin: 3px 0;">
                    <a target="_blank" href="http://www.stevens.edu/news/content/deans-seminar-series-simon-head-senior-fellow-institute-public-knowledge-nyu-taylors-three?utm_source=internal&utm_medium=email&utm_term=fa15&utm_content=dss_head&utm_campaign=events"
                    style="color: #9A989A;
                            text-decoration: underline;">View in browser</a></p>
             </center>
            
	   </td></tr></table>
        </td>
	</tr>
</table>   
      
<!--- HEADER and LOGO------------------------>
    
<table bgcolor="#FFFFFF"
       style="width: 100%;
              margin: 0;
              border: none">
    
    <tr>
        <td style="display: block !important;
                   clear: both !important;
                   margin-top: 0;                           
                   margin-right: auto;                           
                   margin-bottom: 0;                           
                   margin-left: auto;">
            
            <table align="center"
                   class="wrapper"
                   style="width: 580px;
                          margin-top: 0;                           
                          margin-right: auto;                           
                          margin-bottom: 0;                           
                          margin-left: auto;">
                <tr>
                <td>
                <center>
			<table style="margin-top: 8px;
                          margin-right: 10px;
                          margin-bottom: 15px;
                          margin-left: 10px;
                          width: 100%">
                
                <tr> 
                    <td class="logo" 
                        align="center"
                        style="margin-right: 12px;
                               vertical-align: top" valign="top">
                        <img src="/sites/ann/files/uss-logo.gif" alt="Stevens"
                             style="margin: 0;" />
                    </td>
                    <td class="label"
                        align="center"
                        style="margin: 0;">
                        <img src="/sites/ann/files/uss-banner.gif"
                             alt="University Seminar Series"
                             style="margin: 0;" />
                    </td>
				</tr>
            </table>
                    </center>
            </td></tr></table>
		</td>
	</tr>
</table>
      

<!-- TITLE and PORTRAIT --------------------------->
      
<table bgcolor="#F4F4F4"
       style="background: #F4F4F4;
              border-bottom-color: #C6C5C6;
              border-bottom-style: solid;
              border-bottom-width: 2px;
              border-top-color: #C6C5C6;
              border-top-style: solid;
              border-top-width: 2px;
              margin: 0;
              width: 100%;">
    <tr>
      <td style="display: block !important;
                   clear: both !important;
                   margin-top: 0;                           
                 margin-right: auto;                           
                 margin-bottom: 0;                           
                 margin-left: auto;">
            <table align="center"
                   class="wrapper"
                   cellpadding="8"
                   style="width: 580px;
                          margin-top: 0;                           
                          margin-right: auto;                           
                          margin-bottom: 0;                           
                          margin-left: auto;">
                <tr>

                 
<!-- PORTRAIT --------------------------->        
                <td class="portrait"
                    valign="top"
                    style="margin-top: 0px;                           
                           margin-right: auto;                           
                           margin-bottom: 0;                           
                           margin-left: auto;">
                    <span style="border-style: solid;
                                     border-width: 2px;
                                     border-color: #c6c5c6;
                                    display: inline-block;
                                    max-width: 100%;
                                    min-width: 75px;"><a href="http://www.stevens.edu/news/content/deans-seminar-series-simon-head-senior-fellow-institute-public-knowledge-nyu-taylors-three?utm_source=internal&utm_medium=email&utm_term=fa15&utm_content=dss_head&utm_campaign=events"><img src="/sites/ann/files/styles/seminar_headshot/public/images/img-simonhead.jpg?itok=O8prNSe_"
                                align="center"
                               alt="Portrait Image"
                               class="portrait"
                               style="display: inline-block;"></a></span>
                 </td>
                 
<!-- SPEAKER ---------------------------> 
                 <td width="380" class="info">
                         <h3 style="color: #A32638;
                                    font-family: Arial, sans-serif;
                                    font-size: 18px;
                                    font-weight: bold;
                                    line-height: 1.1;
                                    margin-top: 0;
                                    margin-right: 0;
                                    margin-bottom: 2px">Simon Head</h3>
                         <h4 style="color: #A32638;
                                    font-family: Arial, sans-serif;
                                    font-size: 16px;
                                    font-style: normal;
                                    font-weight: normal;
                                    line-height: 1.1;
                                    margin-top: 0;
                                    margin-right: 0;
                                    margin-bottom: 15px;
                                    margin-left: 0;">Senior Fellow, Institute for Public Knowledge, New York University</h4>

<!-- TITLE ---------------------------> 
                         <table style="margin-top: 0;
                                    margin-right: 0;
                                    margin-bottom: 10px;
                                    margin-left: 0;">
                             <tr>
                                <td style="margin: 0;">
							         <h1 style="color: #000;
                                                font-family: Arial, sans-serif;
                                                font-size: 24px;
                                                font-weight: bold;
                                                line-height: 1.1;
                                                margin-top: 0;
                                                margin-right: 0;
                                                margin-bottom: 5px;
                                                margin-left: 0;
                                                ">Taylor's Three Centuries</h1>
  
 <!-- SUBTITLE IF NEEDED--------------------------->                                    
<h2 style="color: #000;
           font-family: Arial, sans-serif;
           font-size: 16px;
           font-style: normal;
           font-weight: normal;
           line-height: 1.1;
           margin-top: 0;
            margin-right: 0;
            margin-bottom: 5px;
            margin-left: 0;">Reflections on creating the CASL, running a hedge fund, composing the world’s ugliest music, solving crimes and working for the world’s most innovative company</h2>
                                </td>
                            </tr>
                        </table>
                     
<!-- INFO --------------------------->    
                        <h3 style="color: #A32638;
                                   font-family: Arial, sans-serif;
                                   font-size: 18px; font-weight: bold;
                                   line-height: 1.1;
                                   margin-top: 0;
                                    margin-right: 0;
                                    margin-bottom: 2px;
                                    margin-left: 0;">Thursday, September 24, 2015, 4-6 PM</h3>
                        <h4 style="color: #A32638;
                                   font-family: Arial, sans-serif;
                                   font-size: 16px;
                                   font-style: normal;
                                   font-weight: normal;
                                   line-height: 1.1; margin-top: 0;
                                    margin-right: 0;
                                    margin-bottom: 15px;
                                    margin-left: 0;">Babbio 122, Stevens Institute of Technology
                           <br style="margin: 0;" />and online via Blackboard Collaborate</h4>

                   <h6 style="color: #9A989A;
                              font-family: Arial, sans-serif;
                              font-size: 14px;
                              font-style: normal;
                              font-weight: normal;
                              line-height: 1.1;
                              margin-top: 0;
                            margin-right: 0;
                            margin-bottom: 15px;
                            margin-left: 0;">
                       <a href="#" style="color: #0059BD; margin: 0;">Add to calendar</a></h6>

<!-- REGISTER BUTTON --------------------------->  
  
            <table cellpadding="8" bgcolor="#F98E2B" style="margin: 10px 0;">
                <tr>
                    <td>
                     <a href="http://www.stevens.edu/provost/dss/rsvp-Simon-Head?utm_source=internal&utm_medium=rsvp-button&utm_term=fa15&utm_content=dss_head&utm_campaign=events"
                        style="color: #FFFFFF;
                               margin: 0 ;
                               cursor: pointer;
                               display: inline-block;
                               font-family: Arial, sans-serif;
                               font-weight: bold;
                               text-align: center;
                               text-decoration: none;">REGISTER NOW</a>
                    </td>
                </tr>
            </table>
                     
    
<h6 style="color: #9A989A;
           font-family: Arial, sans-serif;
           font-size: 14px;
           font-style: normal;
           font-weight: normal;
           line-height: 1.1;
           margin-top: 10px;
         margin-right: 0;
         margin-bottom: 5px;
         margin-left: 0;">Seating limited. Registration required.</h6>


                    </td>
                </tr>
            </table>

                    	   
        </td>
    </tr>
</table>
      
      
<!-- DESCRIPTION --------------------------->
      
<table bgcolor="#FFFFFF"
       style="width: 100%;
              margin: 0;">
    <tr>
        <td style="display: block !important;
                   clear: both !important;
                   margin-top: 0;                           
                   margin-right: auto;                           
                   margin-bottom: 0;                           
                   margin-left: auto;">
            <table align="center"
                   class="wrapper"
                   style="width: 580px;
                          margin-top: 10px;                           
                          margin-right: auto;                           
                          margin-bottom: 0;                           
                          margin-left: auto;">
                <tr>
                <td>

                <table class="description"
                       align="left"
                       style="margin-top: 0;
                            margin-right: 10px;
                            margin-bottom: 0;
                            margin-left: 10px;
                            width: 100%">
                    <tr>
                         <td>	
                             
<!-- SPONSOR ---------------------------> 
                            <h5 style="color: #A32638;
                                       font-family: Arial, sans-serif;
                                       font-size: 16px;
                                       line-height: 1.1;
                                       margin-top: 15px;
                                        margin-right: 0;
                                        margin-bottom: 5px;
                                        margin-left: 0;
                                       text-transform: uppercase">SPONSORED BY</h5>
                             
				            <p style="font-family: Arial, sans-serif;
                                      font-size: 20px; color: #000; font-weight: bold; line-height: 1.3;
                                      margin-top: 0;
                                        margin-right: 0;
                                        margin-bottom: 20px;
                                        margin-left: 0;">Center for Complex Systems and Enterprises</p>

<!-- ABSTRACT ---------------------------> 
				            <h5 style="color: #A32638; font-family: Arial, sans-serif; font-size: 16px; line-height: 1.1;
                                       margin-top: 0;
                                        margin-right: 0;
                                        margin-bottom: 5px;
                                        margin-left: 0;
                                       text-transform: uppercase">ABSTRACT</h5>
                             
                             
				            <p style="font-family: Arial, sans-serif;
                                      font-size: 16px; font-weight: normal; color: #000; line-height: 1.3;
                                      margin-top: 0;
                                        margin-right: 0;
                                        margin-bottom: 15px;
                                       margin-left: 0;">One of Stevens’ most iconic graduates, Frederick Winslow Taylor is widely recognized as the father of scientific management. In celebration of his achievements and legacy, author Simon Head will discuss the influence of Taylor spanning three centuries.</p>
                            
                             <p style="font-family: Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.3; margin: 0 0 15px; padding: 0">The event marks the centennial of Taylor’s death in 1915 and will explore both Taylor's place in history and his legacy in the 21st century. In this lecture, Head will trace Taylor's work and legacy from the early roots of scientific management during the nineteenth and early twentieth centuries, the American "Arsenal of Democracy" during World War II, Japanese "lean production" in the 1970s and 1980s, and the digitized "service economy" of the twenty-first century.</p>

<!-- BIOGRAPHY ---------------------------> 
				            <h5 style="color: #A32638; font-family: Arial, sans-serif;
                                       font-size: 16px; line-height: 1.1; 
                                       margin-top: 0;
                                        margin-right: 0;
                                        margin-bottom: 5px;
                                       margin-left: 0;
                                       text-transform: uppercase">BIOGRAPHY</h5>
				            <p style="font-family: Arial, sans-serif;
                                      font-size: 16px; font-weight: normal; color: #000; line-height: 1.3;
                                      margin-top: 0;
                                        margin-right: 0;
                                        margin-bottom: 15px;
                                       margin-left: 0;">Simon Head is a Senior Fellow at the Institute for Public Knowledge at New York University; a Senior Member at St Antony's College, Oxford; and Director of Programs at The New York Review of Books Foundation. He was a Fellow at the Rothermere American Institute at Oxford, 2005-2011, and Director of the Project on Technology and the Workforce at the Century Foundation, New York City, 1998-2004. He is the author of <i>The New Ruthless Economy: Work and Power in The Digital Age</i> (2005) and <i>Mindless: Why Smarter Machines are Making Dumber Humans</i> (Basic Books, New York, 2014).</p>
                             
                             <p style="font-family: Arial, sans-serif;
                                       font-size: 16px; font-weight: normal; color: #000; line-height: 1.3;
                                       margin-top: 0;
                                        margin-right: 0;
                                        margin-bottom: 15px;
                                       margin-left: 0;">Reception to follow in Babbio Atrium.</p>
                             
<!-- ATTENDANCE IF NEEDED--------------------------->    
                             
                             <h5 style="color: #A32638; font-family: Arial, sans-serif;
                                       font-size: 16px; line-height: 1.1;
                                       margin-top: 0;
                                        margin-right: 0;
                                        margin-bottom: 5px;
                                       margin-left: 0;
                                       text-transform: uppercase">ATTENDANCE</h5>
                            <p style="font-family: Arial, sans-serif;
                                    font-size: 16px; font-weight: bold; color: #000; line-height: 1.3;
                                      margin-top: 0;
                                        margin-right: 0;
                                        margin-bottom: 15px;
                                       margin-left: 0;">This event is free and open to all Stevens' students, faculty, alumni, staff and invited guests. Details for Blackboard Collaborate available at: <a href="#" style="color: #0059BD; margin: 0; padding: 0">stevens.edu/news/abboud</a></p>
                       
				        </td>
				    </tr>
                </table>
                    	   </td></tr></table>
            <div class="clear" style="clear: both; display: block; margin: 0; padding: 0"></div>			

        </td>
    </tr>
</table>




<?php /* end drop-in */ ?>


    <?php
//      print render($content);
    ?>
  </div>

  <div class="clearfix">
    <?php if (!empty($content['links'])): ?>
      <div class="links"><?php print render($content['links']); ?></div>
    <?php endif; ?>

    <?php print render($content['field_workflow']); ?>

    <?php print render($content['comments']); ?>
  </div>

</div>
