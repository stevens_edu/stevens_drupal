<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <div class="content clearfix"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
    ?>
    <div id="logo">
      <img width="250" height="66" src="<?php print $announcement_logo_url; ?>">
    </div>
    <br>
    <table class="inline-headers">
      <?php
        print render($content['field_inline_to']);
        print render($content['field_inline_from']);
      ?>
      <tr>
        <td>
          <div class="field field-type-text field-label-inline clearfix">
            <div class="field-label">
              Date:&nbsp;
            </div>
          </div>
        </td>
        <td>
          <div class="field-items">
            <div class="field-item">
              <?php
                print format_date(((!empty($sent_time)) ? $sent_time : strtotime('now')), 'stevens_ann_mailer_date');
              ?>
            </div>
          </div>
        </td>
      </tr>
      <tr>
        <td>
          <div class="field field-type-text field-label-inline clearfix">
            <div class="field-label">
              Subject:&nbsp;
            </div>
          </div>
        </td>
        <td>
          <div class="field-items">
            <div class="field-item">
              <?php
                print $title;
              ?>
            </div>
          </div>
        </td>
      </tr>
    </table>
    <br>
    <?php
      print render($content);
    ?>
    <p style="font-size: .9em; font-style: italic;">This message has been approved by the leadership of the sender's division.</p>
  </div>

  <div class="clearfix">
    <?php if (!empty($content['links'])): ?>
      <div class="links"><?php print render($content['links']); ?></div>
    <?php endif; ?>

    <?php print render($content['comments']); ?>
  </div>

</div>
