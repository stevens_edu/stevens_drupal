<?php

/**
 * @file
 * Formats an HTML mail for the "renderednode" mailkey. Instead of printing
 * the $body, it prints a rendered node using the nid in $body.
 *
 * Available variables:
 * - $recipient: The recipient of the message
 * - $subject: The message subject
 * - $body: The message body
 * - $css: Internal style sheets
 * - $module: The sending module
 * - $key: The message identifier
 *
 * @see template_preprocess_mimemail_message()
 */
?>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?php if ($css): ?>
    <style type="text/css">
      <!--
      <?php print $css ?>
      -->
    </style>
    <?php endif; ?>
  </head>
  <body id="mimemail-body" <?php if ($module && $key): print 'class="'. $module .'-'. $key .'"'; endif; ?>>
    <div id="center">
      <div id="main">
        <?php
          // The $body variable contains a node ID. Completely sanitize it.
          $nid = strip_tags($body);
          $nid = (int) $nid;
          // If the node exists, render and print it.
          if ($node = node_load($nid)) {
            $build = node_view($node);
            // Hide contextual links and workflow. Everything else in the node template is rendered.
            $build['links']['#access'] = FALSE;
            $build['field_workflow']['#access'] = FALSE;
            print drupal_render($build);
          }
          else {
            print "<p>Node $nid not found.</p>";
          }
        ?>
      </div>
    </div>
  </body>
</html>
