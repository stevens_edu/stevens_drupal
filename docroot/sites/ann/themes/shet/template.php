<?php

/**
 * @file
 * Controls theme.
 */

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return
 *   A string containing the breadcrumb output.
 */
function shet_breadcrumb($variables){
	$breadcrumb = $variables['breadcrumb'];
	$breadcrumb_separator=theme_get_setting('breadcrumb_separator');

	$show_breadcrumb_home = theme_get_setting('breadcrumb_home');
	if (!$show_breadcrumb_home) {
		array_shift($breadcrumb);
	}

	if (!empty($breadcrumb)) {
		$breadcrumb[] = drupal_get_title();
		return '<div class="breadcrumb">' . implode(' <span class="breadcrumb-separator">' . $breadcrumb_separator . '</span>', $breadcrumb) . '</div>';
	}
}

function shet_page_alter($page) {

	if (theme_get_setting('responsive_meta')):
	$mobileoptimized = array(
			'#type' => 'html_tag',
			'#tag' => 'meta',
			'#attributes' => array(
					'name' =>  'MobileOptimized',
					'content' =>  'width'
			)
	);

	$handheldfriendly = array(
			'#type' => 'html_tag',
			'#tag' => 'meta',
			'#attributes' => array(
					'name' =>  'HandheldFriendly',
					'content' =>  'true'
			)
	);

	$viewport = array(
			'#type' => 'html_tag',
			'#tag' => 'meta',
			'#attributes' => array(
					'name' =>  'viewport',
					'content' =>  'width=device-width, initial-scale=1'
			)
	);

	drupal_add_html_head($mobileoptimized, 'MobileOptimized');
	drupal_add_html_head($handheldfriendly, 'HandheldFriendly');
	drupal_add_html_head($viewport, 'viewport');
	endif;

}

function shet_preprocess_html(&$variables) {

	if (!theme_get_setting('responsive_respond')):
	drupal_add_css(path_to_theme() . '/css/basic-layout.css', array('group' => CSS_THEME, 'browsers' => array('IE' => '(lte IE 8)&(!IEMobile)', '!IE' => FALSE), 'preprocess' => FALSE));
	endif;

	drupal_add_css(path_to_theme() . '/css/ie.css', array('group' => CSS_THEME, 'browsers' => array('IE' => '(lte IE 8)&(!IEMobile)', '!IE' => FALSE), 'preprocess' => FALSE));
}

/**
 * Override or insert variables into the html template.
 */
function shet_process_html(&$vars) {
	// Hook into color.module
	if (module_exists('color')) {
		_color_html_alter($vars);
	}

}

/**
 * Override or insert variables into the page template.
 */
function shet_process_page(&$variables) {
	// Hook into color.module.
	if (module_exists('color')) {
		_color_page_alter($variables);
	}

}

function shet_form_alter(&$form, &$form_state, $form_id) {
	if ($form_id == 'search_block_form') {

		unset($form['search_block_form']['#title']);

		$form['search_block_form']['#title_display'] = 'invisible';
		$form_default = t('Search');
		$form['search_block_form']['#default_value'] = $form_default;
		$form['actions']['submit'] = array('#type' => 'image_button', '#src' => base_path() . path_to_theme() . '/images/search-button.png');

		$form['search_block_form']['#attributes'] = array('onblur' => "if (this.value == '') {this.value = '{$form_default}';}", 'onfocus' => "if (this.value == '{$form_default}') {this.value = '';}" );
	}
}

/**
 * Add javascript files for jquery slideshow.
 */
if (theme_get_setting('slideshow_js')):

drupal_add_js(drupal_get_path('theme', $GLOBALS['theme']) . '/js/jquery.cycle.all.js');

//Initialize slideshow using theme settings
$effect=theme_get_setting('slideshow_effect');
$effect_time= (int) theme_get_setting('slideshow_effect_time')*1000;
$slideshow_randomize=theme_get_setting('slideshow_randomize');
$slideshow_wrap=theme_get_setting('slideshow_wrap');
$slideshow_pause=theme_get_setting('slideshow_pause');

drupal_add_js('jQuery(document).ready(function($) {

	$(window).load(function() {

		$("#slideshow img").show();
		$("#slideshow").fadeIn("slow");
		$("#slider-controls-wrapper").fadeIn("slow");

		$("#slideshow").cycle({
			fx:    "'.$effect.'",
			speed:  "slow",
			timeout: "'.$effect_time.'",
			random: '.$slideshow_randomize.',
			nowrap: '.$slideshow_wrap.',
			pause: '.$slideshow_pause.',
			pager:  "#slider-navigation",
			pagerAnchorBuilder: function(idx, slide) {
				return "#slider-navigation li:eq(" + (idx) + ") a";
			},
			slideResize: true,
			containerResize: false,
			height: "auto",
			fit: 1,
			before: function(){
				$(this).parent().find(".slider-item.current").removeClass("current");
			},
			after: onAfter
		});
	});

	function onAfter(curr, next, opts, fwd) {
		var $ht = $(this).height();
		$(this).parent().height($ht);
		$(this).addClass("current");
	}

	$(window).load(function() {
		var $ht = $(".slider-item.current").height();
		$("#slideshow").height($ht);
	});

	$(window).resize(function() {
		var $ht = $(".slider-item.current").height();
		$("#slideshow").height($ht);
	});

	});',
	array('type' => 'inline', 'scope' => 'footer', 'weight' => 5)
);

endif;

/**
 * Define $announcement_logo_url for mail template.
 */
function shet_preprocess(&$variables) {
  // Mimemail requires a non-absolute URL to a file in the public files
  // directory, not the theme directory, to embed images. We need it to be
  // root-relative (not Drupal internal) for the web previewable img src.
  $logo_url = file_create_url('public://stevens-logo-500x132.png');
  $logo_url = parse_url($logo_url);
  $logo_url = $logo_url['path']; // finally, root-relative
  $variables['announcement_logo_url'] = $logo_url;
  $uss_banner_url = file_create_url('public://uss-banner.gif');
  $uss_banner_url = parse_url($uss_banner_url);
  $uss_banner_url = $uss_banner_url['path']; // finally, root-relative
  $variables['uss_banner_url'] = $uss_banner_url;
}

/**
 * Implements hook_preprocess_node().
 */
function shet_preprocess_node(&$variables) {
  $node = $variables['node'];

  if ($node->type == 'announcement') {
    // Set the sent time if available to be used in templates.
    $sent_flag = flag_get_flag('mail_sent')->get_flagging_record($node->nid);
    $variables['sent_time'] = (!empty($sent_flag->timestamp)) ? $sent_flag->timestamp : NULL;
  }
}

/**
 * Returns HTML for a node preview for display during node creation and editing.
 *
 * @see theme_node_preview()
 *
 * @return string
 * The full node, without the trimmed version.
 */

function shet_node_preview($variables) {
  $node = $variables['node'];

  $elements = node_view($node, 'full');
  // Hide contextual links.
  $elements['links']['#access'] = FALSE;
  $full = drupal_render($elements);

  $output = '<div class="preview">' . $full . "</div>\n";

  return $output;
}

/**
 * Returns HTML for an image.
 *
 * @param $variables
 *   An associative array containing:
 *   - path: Either the path of the image file (relative to base_path()) or a
 *     full URL.
 *   - width: The width of the image (if known).
 *   - height: The height of the image (if known).
 *   - alt: The alternative text for text-based browsers. HTML 4 and XHTML 1.0
 *     always require an alt attribute. The HTML 5 draft allows the alt
 *     attribute to be omitted in some cases. Therefore, this variable defaults
 *     to an empty string, but can be set to NULL for the attribute to be
 *     omitted. Usually, neither omission nor an empty string satisfies
 *     accessibility requirements, so it is strongly encouraged for code
 *     calling theme('image') to pass a meaningful value for this variable.
 *     - http://www.w3.org/TR/REC-html40/struct/objects.html#h-13.8
 *     - http://www.w3.org/TR/xhtml1/dtds.html
 *     - http://dev.w3.org/html5/spec/Overview.html#alt
 *   - title: The title text is displayed when the image is hovered in some
 *     popular browsers.
 *   - attributes: Associative array of attributes to be placed in the img tag.
 */
function shet_image($variables) {
  $attributes = $variables['attributes'];

  // Make the src attribute root-relative if possible, so mimemail embeds it.
  $path = file_create_url($variables['path']);
  if (strpos($path, $GLOBALS['base_url']) === 0) {
    $parsed   = parse_url($path);
    $path     = isset($parsed['path'])     ?       $parsed['path']     : ''; 
    $query    = isset($parsed['query'])    ? '?' . $parsed['query']    : ''; 
    $fragment = isset($parsed['fragment']) ? '#' . $parsed['fragment'] : ''; 
  }
  $attributes['src'] = $path . $query . $fragment;

  foreach (array('width', 'height', 'alt', 'title') as $key) {

    if (isset($variables[$key])) {
      $attributes[$key] = $variables[$key];
    }
  }

  return '<img' . drupal_attributes($attributes) . ' />';
}
