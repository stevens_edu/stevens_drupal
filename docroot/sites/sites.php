<?php

$site_names = array(
  'stevens_edu',
  'library',
  'bot',
  'ann',
  'swebb',
);

$env_names = array(
  'dev',
  'dev2',
  'dev3',
  'dev4',
  'stage',
  'sandbox',
  'prod',
  'ra',
  'local',
);

foreach ($site_names as $site) {
  // Subdomain starts with the site name, except for stevens_edu.
  $sub_start = ($site == 'stevens_edu' ? '' : $site);
  // Directory is site name, except for the library.
  $dir = ($site == 'library' ? 'library_v2' : $site);

  foreach ($env_names as $env) {
    // Subdomain ends with the environment name, except for prod.
    $sub_end = ($env == 'prod' || $env == 'local' ? '' : $env);
    // Delimit with a hyphen when necessary.
    $delim = ($sub_start && $sub_end ? '-' : '');
    // Build the complete subdomain.
    $sub = $sub_start . $delim . $sub_end;
    // Use www in lieu of nothing at all.
    if (!$sub) $sub = 'www';
    // Add entry to sites array.
    // Hosts file entries must be added for SITENAME.stevens.local
    $key = ($env == 'local' ? "$sub.stevens.local" : "$sub.stevens.edu");
    $sites[$key] = $dir;
  }
}

// Set stevens_edu as default so drush uses it as the fallback when it has no
// other context such as an alias or sites/* current working directory.
$sites['default'] = 'stevens_edu';
