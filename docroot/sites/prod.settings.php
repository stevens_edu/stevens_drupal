<?php
/**
 * This file holds common PROD setting for all sites
 */

# Error reporting level configuration
$conf['error_level'] = 0; // Error messages to display: 0 - None; 1 - Errors and warnings; 2 - All messages
ini_set("display_errors", "0"); // do not display errors
error_reporting(E_ALL & ~E_NOTICE);  // ignore PHP Notices

# Drupal Performance settings
$conf['cache'] = TRUE;
$conf['cache_lifetime'] = 0;
$conf['page_cache_maximum_age'] = 300; // 5 minutes
$conf['page_compression'] = FALSE;
$conf['preprocess_css'] = TRUE;
$conf['preprocess_js'] = TRUE;

# Disable shield user on production, allowing shield to be enabled but not requiring auth
# See https://docs.acquia.com/articles/password-protect-your-non-production-environments-acquia-hosting
$conf['shield_user'] = NULL;
