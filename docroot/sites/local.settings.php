<?php
/**
 * This file holds common NON-ACQUIA, LOCAL settings for all sites
 */

# Disable SAML
$conf['simplesamlphp_auth_activate'] = FALSE;

# Configure Stage File Proxy
$conf['stage_file_proxy_origin'] = 'https://www.stevens.edu';
$conf['stage_file_proxy_origin_dir'] = 'sites/stevens_edu/files';
$conf['stage_file_proxy_hotlink'] = TRUE;

