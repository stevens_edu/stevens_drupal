<?php
/**
 * This file holds common DEV setting for all sites
 *
 */

# Error reporting level configuration
$conf['error_level'] = 1; // Error messages to display: 0 - None; 1 - Errors and warnings; 2 - All messages
ini_set("display_errors", "1"); // display errors
error_reporting(E_ALL & ~E_NOTICE);  // ignore PHP Notices

# Drupal Performance settings
$conf['cache'] = TRUE;
$conf['cache_lifetime'] = 0;
$conf['page_cache_maximum_age'] = 300; // 5 minutes
$conf['page_compression'] = FALSE;
$conf['preprocess_css'] = FALSE;
$conf['preprocess_js'] = FALSE;
