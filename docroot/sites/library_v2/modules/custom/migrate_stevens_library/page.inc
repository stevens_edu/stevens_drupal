<?php
/**
 * @file
 * Details of page content migration from /library.
 */


/**
 * Migrate Page images to Image nodes.
 */
class StevensLibraryImageFeatureMigration extends DrupalNode7Migration {
  /**
   * Overrides DrupalNode7Migration::__construct().
   */
  public function __construct($arguments) {
    parent::__construct($arguments);

    // Add file mappings.
    $this->addFieldMapping('field_page_feature_image', 'field_image')
         ->sourceMigration('StevensLibraryFile');
    $this->addFieldMapping('field_page_feature_image:file_class')
         ->defaultValue('MigrateFileFid');
    $this->addFieldMapping('field_page_feature_image:preserve_files')
         ->defaultValue(TRUE);
    $this->addFieldMapping('field_page_feature_image:language')
         ->defaultValue(LANGUAGE_NONE);    
  }

  /**
   * Overrides DrupalNode7Migration::prepareRow().
   */
  public function prepareRow($row) {
    // Return FALSE if parent implementation says we should.
    // Note that this removes a row from processing.
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    if (empty($row->field_image)) {
      return FALSE;
    }

    return TRUE;
  }

}

class StevensLibraryQuickLinksMigration extends DrupalNode7Migration {
  /**
   * Overrides DrupalNode7Migration::__construct().
   */
  public function __construct($arguments) {
    parent::__construct($arguments);

    // Add 1:1 mappings.
    $this->addFieldMapping('field_link_list_sidebar_links', 'field_quick_links');
    $this->addFieldMapping('field_link_list_sidebar_links:title', 'field_quick_links:title');
  }

  /**
   * Overrides DrupalNode7Migration::prepareRow().
   */
  public function prepareRow($row) {
    // Return FALSE if parent implementation says we should.
    // Note that this removes a row from processing.
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    if (empty($row->field_quick_links)) {
      return FALSE;
    }

    return TRUE;
  }
}

class StevensLibraryPageMigration extends DrupalNode7Migration {
  /**
   * Overrides DrupalNode7Migration::__construct().
   */
  public function __construct($arguments) {
    parent::__construct($arguments);

    // Add mappings for feature image and quick links.
    $this->addFieldMapping('field_sidebar_components', 'nid')
         ->sourceMigration('StevensLibraryQuickLinks');
    $this->addFieldMapping('field_page_feature', 'nid')
         ->sourceMigration('StevensLibraryImageFeature');
  }

  /**
   * Overrides DrupalNode7Migration::prepareRow().
   */
  public function prepareRow($row) {
    // Return FALSE if parent implementation says we should.
    // Note that this removes a row from processing.
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Aggregate body and sidebar fields, since we can't tell semantically 
    // where the content should end up.
    $body_rows = array($row->body[0]);
    if (!empty($row->field_temp_highlight[0])) {
      $body_rows[] = $row->field_temp_highlight[0];
    }
    if (!empty($row->field_temp_node_sidebar[0])) {
      $body_rows[] = $row->field_temp_node_sidebar[0];
    }
    if (!empty($row->field_highlight[0])) {
      $body_rows[] = $row->field_highlight[0];
    }
    $row->body[0] = implode('<br/>', $body_rows);

    return TRUE;
  }
}
