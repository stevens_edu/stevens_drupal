<?php
/**
 * @file
 * Declares our migrations.
 */
    

/**
 * Implements hook_migrate_api().
 */
function migrate_stevens_library_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'stevenslibrary' => array(
        'title' => t('Stevens Library Migrations'),
      ),
    ),
    'migrations' => array(),
  );

  $common_arguments = array(
    'source_connection' => 'library',
    'source_version' => 7,
    'group_name' => 'stevenslibrary',
  );

  // File migration for /library.

  $file_arguments = $common_arguments + array(
    'machine_name' => 'StevensLibraryFile',
    'description' => t('Import files from library site.'),
    'default_uid' => 1,
    'source_dir' => 'https://www.stevens.edu/library/sites/library/files/',
    'destination_dir' => 'public://legacy_library',
  );
  Migration::registerMigration('DrupalFile7Migration', 
    $file_arguments['machine_name'], $file_arguments);

  // Node migration for library components.

  $library_node_arguments = $common_arguments + array(
    'default_uid' => 1,
    'machine_name' => 'StevensLibraryImageFeature',
    'description' => t('Import image feature nodes'),
    'source_type' => 'page', 
    'destination_type' => 'image_feature',
  );

  // Mapping of term references and other field happens in derived class.
  Migration::registerMigration('StevensLibraryImageFeatureMigration', 
    $library_node_arguments['machine_name'], $library_node_arguments);

  $library_node_arguments = $common_arguments + array(
    'default_uid' => 1,
    'machine_name' => 'StevensLibraryQuickLinks',
    'description' => t('Import Quick Link sidebar nodes'),
    'source_type' => 'page', 
    'destination_type' => 'link_list_sidebar',
    'dependencies' => array('StevensLibraryFile'),
  );

  // Mapping of term references and other field happens in derived class.
  Migration::registerMigration('StevensLibraryQuickLinksMigration', 
    $library_node_arguments['machine_name'], $library_node_arguments);

  // Node migration for library page items.

  $library_node_arguments = $common_arguments + array(
    'default_uid' => 1,
    'machine_name' => 'StevensLibraryPage',
    'description' => t('Import Page nodes'),
    'source_type' => 'page', 
    'destination_type' => 'page',
    'dependencies' => array('StevensLibraryImageFeature', 'StevensLibraryQuickLinks'),
  );

  // Mapping of term references and other field happens in derived class.
  Migration::registerMigration('StevensLibraryPageMigration', 
    $library_node_arguments['machine_name'], $library_node_arguments);

  return $api;
}

