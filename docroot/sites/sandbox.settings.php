<?php
/**
 * This file holds common SANDBOX setting for all sites
 */

# Error reporting level configuration
$conf['error_level'] = 0; // Error messages to display: 0 - None; 1 - Errors and warnings; 2 - All messages
ini_set("display_errors", "0"); // do not display errors
error_reporting(E_ALL & ~E_NOTICE);  // ignore PHP Notices

# Drupal Performance settings
$conf['cache'] = TRUE;
$conf['cache_lifetime'] = 0;
$conf['page_cache_maximum_age'] = 21600; // 6 hours
$conf['page_compression'] = FALSE;
$conf['preprocess_css'] = TRUE;
$conf['preprocess_js'] = TRUE;
