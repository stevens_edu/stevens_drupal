<?php

/**
 * @file
 * Template overrides as well as (pre-)process and alter hooks for the
 * stevens_swebb theme.
 */

/**
 * Theme function override for status messages to enable use of csm in Omega.
 *
 * @see https://www.drupal.org/project/omega/issues/2197945#comment-8917641
 */
function stevens_swebb_status_messages($variables) {
  if (module_exists('messages_alter')) {
    // Avoid a recursive call that would result by invoking
    // theme_messages_alter_alter() directly.
    global $theme;
    $registry_alter = variable_get('messages_alter_theme_altered', array());
    messages_alter_invoke_message_alter();
    $output = theme('messages_alter_status_messages', array('variables' => $variables, 'theme_function' => FALSE ));
    return $output;
  }

  return theme_status_messages($variables);
}

/**
 * A preprocess function for theme('mimemail_message').
 *
 * The $variables array initially contains the following arguments:
 * - $recipient: The recipient of the message
 * - $key:  The mailkey associated with the message
 * - $subject: The message subject
 * - $body:  The message body
 *
 * @see mimemail-message.tpl.php
 */
function stevens_swebb_preprocess_mimemail_message(&$variables) {
  if (strpos($variables['key'], 'newsletter-') === 0) {
    // Specify what template to use, since we use a dynamic key that doesn't
    // exactly match the static key in the template filename.
    $variables['theme_hook_suggestion'] = 'mimemail_message__newsletter';

    $view = views_get_view('bulletin_board');
    $view->set_display('page');
    $view->is_cacheable = FALSE;
    $filter = explode('-', $variables['key']);
    $filter = $filter[1]; // The issue number comes after the hyphen.
    $view->args = array($filter);

    $path = $view->display_handler->display->display_options['path'];

    $variables['view'] = $view;
    $variables['view_on_web_url'] = $GLOBALS['base_url'] . $GLOBALS['base_path'] . $path . '/' . $filter;
  }
}

