<?php

/**
 * @file
 * Default theme implementation to format an HTML mail.
 *
 * Copy this file in your default theme folder to create a custom themed mail.
 * Rename it to mimemail-message--[module]--[key].tpl.php to override it for a
 * specific mail.
 *
 * Available variables:
 * - $recipient: The recipient of the message
 * - $subject: The message subject
 * - $body: The message body
 * - $css: Internal style sheets
 * - $module: The sending module
 * - $key: The message identifier
 *
 * @see template_preprocess_mimemail_message()
 */
?>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?php if ($css): ?>
    <style type="text/css">
      <!--
      <?php print $css ?>
      -->
    </style>
    <?php endif; ?>
  </head>
  <body id="mimemail-body" <?php if ($module && $key): print 'class="'. $module .'-'. $key .'"'; endif; ?>>
    <div id="center">
      <div id="main">
        <div class="view-on-web">
          <?php print l('View on the web', $variables['view_on_web_url']); ?>
        </div>

        <div class="l-page">

          <!--[if mso]>
            <table align="center"><tr><td width="512">
          <![endif]-->

          <div class="l-main">
            <header class="l-header" role="banner">
              <div class="l-branding">
                 <div class="site-logo">
                   <img src="/sites/swebb/themes/stevens_swebb/logo-2022.png" alt="Stevens Logo" width="250" height="66"></a>
                 </div>
              </div>
            </header>

            <div class="l-content" role="main">
              <h1><?php print $variables['view']->get_title(); ?></h1>
              <?php
                print $variables['view']->render();
              ?>
            </div>
          </div>

          <!--[if mso]>
            </td></tr></table>
          <![endif]-->

        </div>
      </div>
    </div>
  </body>
</html>
