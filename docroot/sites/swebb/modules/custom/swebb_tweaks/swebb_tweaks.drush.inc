<?php
/**
 * @file
 * Drush integration for sendbulletin module.
 */

/**
 * Implements hook_drush_command().
 */
function swebb_tweaks_drush_command() {
  $commands['sendbulletin'] = array(
    'description' => 'Creates an issue out of pending posts and sends that issue via email.',
    'aliases' => array('sb'),
    'examples' => array(
      'drush @swebb.[env] sb --user=1' => 'Creates an issue out of pending posts and sends that issue via email.',
    ),
  );

  return $commands;
}

/**
 * Implements drush_COMMAND_NAME().
 */
function drush_swebb_tweaks_sendbulletin() {
  global $user;

  if ($user->uid != 1) {
    return drush_set_error('DRUSH_SB_NOT_USER_1', dt('Must be user 1. Usage: drush @swebb.[env] sb --user=1'));
  }

  // @TODO: Decouple the issue creation logic from the drush interface.
  // Get a list of nodes that don't have assigned terms
  $node_res = db_query("SELECT n.nid FROM node n LEFT JOIN field_data_field_swebb_post_issue pi ON pi.entity_id = n.nid WHERE n.type = 'post' AND pi.field_swebb_post_issue_tid IS NULL");

  // Check for empty set here and terminate if a new issue does not need sending.
  if (!$node_res->rowCount()) {
    drush_log(dt('There are no announcements to send. Skipping issue creation and digest mailing.'), 'ok');
    return;
  }

  // Get the vid, in case no terms exist yet.
  $res_vid = db_query("SELECT vid FROM taxonomy_vocabulary tv WHERE tv.machine_name = 'swebb_issues'")->fetchAssoc();
  $vid = $res_vid['vid'];
  

  // Get the highest number term name from swebb_issues.
  $res = db_query("SELECT MAX(CAST(ttd.name AS UNSIGNED)) AS maxi, ttd.vid FROM taxonomy_term_data ttd INNER JOIN taxonomy_vocabulary tv ON tv.vid = ttd.vid  WHERE tv.machine_name = 'swebb_issues'")->fetchAssoc();
  $max = $res['maxi'];
  $issue_no = (is_numeric($max)) ? $max + 1 : 1;

  // Get the current time of the issue.
  $date = new DateTime();
  $date->setTimezone(new DateTimeZone('UTC'));

  // Create a new issue
  $term = new stdClass();
  $term->name = $issue_no;
  $term->vid = $vid;
  $term->field_swebb_issue_date[LANGUAGE_NONE][0]['value'] = date_format($date, 'Y-m-d H:i:s');
  $term->field_swebb_issue_date[LANGUAGE_NONE][0]['timezone'] = "America/New_York";
  $term->field_swebb_issue_date[LANGUAGE_NONE][0]['timezone_db'] = "UTC";
  $term->field_swebb_issue_date[LANGUAGE_NONE][0]['date_type'] = "datetime";

  taxonomy_term_save($term);
  $tid = $term->tid;

  // Set all NULL post issue tids to new term's tid
  $nodes = array();
  foreach ($node_res as $record) {
    $node = node_load($record->nid);
    $node->field_swebb_post_issue[LANGUAGE_NONE][0]['tid'] = $tid;
    node_save($node);
  }

  // Send the latest
  $result = _swebb_tweaks_send_mail($issue_no);

  if ($result) {
    drush_log(dt('Issue ' . $issue_no . ' has been sent.'), 'ok');
  }
  else {
    drush_set_error('DRUSH_SB_MAIL_ERROR', dt('Mail was not sent.'));
  }
}
