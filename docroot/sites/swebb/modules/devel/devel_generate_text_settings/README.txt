This module allows you to control the length and format of generated content for text and long text fields.

After enabling you will find additional settings at the bottom of the "Devel Generate > Generate content" form.

There are detailed instructions with each field.
