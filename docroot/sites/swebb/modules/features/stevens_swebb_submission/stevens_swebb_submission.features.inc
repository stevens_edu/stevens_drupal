<?php

/**
 * @file
 * stevens_swebb_submission.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function stevens_swebb_submission_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function stevens_swebb_submission_node_info() {
  $items = array(
    'post' => array(
      'name' => t('Submission'),
      'base' => 'node_content',
      'description' => t('An item posted on the bulletin board.'),
      'has_title' => '1',
      'title_label' => t('Subject'),
      'help' => t('Your submission will be included in the next issue of the Stevens Weekly Bulletin Board, which will be mailed to faculty and staff on Monday morning at 3 or 4 AM. Until the next issue is sent, posts are considered "pending" and may be edited or deleted by the author. After mailing has occurred, editing and deleting are no longer possible; this ensures that the web archive reflects what was mailed. Please be sure to proofread, and if this is related to an event, mention the date in the subject line.'),
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
