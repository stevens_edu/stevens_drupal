<?php

/**
 * @file
 * stevens_swebb_submission.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function stevens_swebb_submission_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create field_swebb_post_issue'.
  $permissions['create field_swebb_post_issue'] = array(
    'name' => 'create field_swebb_post_issue',
    'roles' => array(
      'Enterprise Administrator' => 'Enterprise Administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_swebb_post_issue'.
  $permissions['edit field_swebb_post_issue'] = array(
    'name' => 'edit field_swebb_post_issue',
    'roles' => array(
      'Enterprise Administrator' => 'Enterprise Administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_swebb_post_issue'.
  $permissions['edit own field_swebb_post_issue'] = array(
    'name' => 'edit own field_swebb_post_issue',
    'roles' => array(
      'Enterprise Administrator' => 'Enterprise Administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_swebb_post_issue'.
  $permissions['view field_swebb_post_issue'] = array(
    'name' => 'view field_swebb_post_issue',
    'roles' => array(
      'Enterprise Administrator' => 'Enterprise Administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_swebb_post_issue'.
  $permissions['view own field_swebb_post_issue'] = array(
    'name' => 'view own field_swebb_post_issue',
    'roles' => array(
      'Enterprise Administrator' => 'Enterprise Administrator',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
