<?php
/**
 * @file
 * stevens_simplesamlphp_auth.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function stevens_simplesamlphp_auth_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
