<?php

/**
 * @file
 * stevens_bulletin_board_view.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function stevens_bulletin_board_view_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'bulletin_board';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Bulletin Board';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Stevens Weekly Bulletin Board';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'created' => 'created',
    'field_organizational_unit' => 'field_organizational_unit',
  );
  /* No results behavior: Global: Result summary */
  $handler->display->display_options['empty']['result']['id'] = 'result';
  $handler->display->display_options['empty']['result']['table'] = 'views';
  $handler->display->display_options['empty']['result']['field'] = 'result';
  /* Relationship: Content: Issue (field_swebb_post_issue) */
  $handler->display->display_options['relationships']['field_swebb_post_issue_tid']['id'] = 'field_swebb_post_issue_tid';
  $handler->display->display_options['relationships']['field_swebb_post_issue_tid']['table'] = 'field_data_field_swebb_post_issue';
  $handler->display->display_options['relationships']['field_swebb_post_issue_tid']['field'] = 'field_swebb_post_issue_tid';
  $handler->display->display_options['relationships']['field_swebb_post_issue_tid']['required'] = TRUE;
  /* Field: Content: Post date */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'node';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['label'] = '';
  $handler->display->display_options['fields']['created']['element_class'] = 'news_list_item_date';
  $handler->display->display_options['fields']['created']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['created']['date_format'] = 'custom';
  $handler->display->display_options['fields']['created']['custom_date_format'] = 'F d, Y';
  $handler->display->display_options['fields']['created']['second_date_format'] = 'long';
  /* Field: Field: Organizational Unit */
  $handler->display->display_options['fields']['field_organizational_unit']['id'] = 'field_organizational_unit';
  $handler->display->display_options['fields']['field_organizational_unit']['table'] = 'field_data_field_organizational_unit';
  $handler->display->display_options['fields']['field_organizational_unit']['field'] = 'field_organizational_unit';
  $handler->display->display_options['fields']['field_organizational_unit']['label'] = '';
  $handler->display->display_options['fields']['field_organizational_unit']['element_class'] = 'news_list_item_cat';
  $handler->display->display_options['fields']['field_organizational_unit']['element_label_colon'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_class'] = 'news_list_item_title';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_class'] = 'news_list_item_description';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  /* Field: Content: Link Title */
  $handler->display->display_options['fields']['field_link']['id'] = 'field_link';
  $handler->display->display_options['fields']['field_link']['table'] = 'field_data_field_link';
  $handler->display->display_options['fields']['field_link']['field'] = 'field_link';
  $handler->display->display_options['fields']['field_link']['label'] = '';
  $handler->display->display_options['fields']['field_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_link']['click_sort_column'] = 'url';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  /* Contextual filter: Content: Issue (field_swebb_post_issue) */
  $handler->display->display_options['arguments']['field_swebb_post_issue_tid']['id'] = 'field_swebb_post_issue_tid';
  $handler->display->display_options['arguments']['field_swebb_post_issue_tid']['table'] = 'field_data_field_swebb_post_issue';
  $handler->display->display_options['arguments']['field_swebb_post_issue_tid']['field'] = 'field_swebb_post_issue_tid';
  $handler->display->display_options['arguments']['field_swebb_post_issue_tid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_swebb_post_issue_tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_swebb_post_issue_tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_swebb_post_issue_tid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_swebb_post_issue_tid']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['field_swebb_post_issue_tid']['validate']['type'] = 'taxonomy_term';
  $handler->display->display_options['arguments']['field_swebb_post_issue_tid']['validate_options']['vocabularies'] = array(
    'swebb_issues' => 'swebb_issues',
  );
  $handler->display->display_options['arguments']['field_swebb_post_issue_tid']['validate_options']['type'] = 'convert';
  $handler->display->display_options['arguments']['field_swebb_post_issue_tid']['validate']['fail'] = 'empty';
  /* Filter criterion: Content: Published status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'post' => 'post',
  );
  /* Filter criterion: Content: Issue (field_swebb_post_issue) */
  $handler->display->display_options['filters']['field_swebb_post_issue_tid']['id'] = 'field_swebb_post_issue_tid';
  $handler->display->display_options['filters']['field_swebb_post_issue_tid']['table'] = 'field_data_field_swebb_post_issue';
  $handler->display->display_options['filters']['field_swebb_post_issue_tid']['field'] = 'field_swebb_post_issue_tid';
  $handler->display->display_options['filters']['field_swebb_post_issue_tid']['operator'] = 'not empty';
  $handler->display->display_options['filters']['field_swebb_post_issue_tid']['value'] = '';
  $handler->display->display_options['filters']['field_swebb_post_issue_tid']['vocabulary'] = 'swebb_issues';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'bb';

  /* Display: Search */
  $handler = $view->new_display('page', 'Search', 'search');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Result summary */
  $handler->display->display_options['header']['result']['id'] = 'result';
  $handler->display->display_options['header']['result']['table'] = 'views';
  $handler->display->display_options['header']['result']['field'] = 'result';
  $handler->display->display_options['header']['result']['content'] = '@total search results | <a href="/">back to current issue</a><br><br>';
  $handler->display->display_options['defaults']['empty'] = FALSE;
  /* No results behavior: Global: Unfiltered text */
  $handler->display->display_options['empty']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['empty']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['empty'] = TRUE;
  $handler->display->display_options['empty']['area_text_custom']['content'] = 'No search results | <a href="/">back to current issue</a><br><br>';
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Search: Search Terms */
  $handler->display->display_options['arguments']['keys']['id'] = 'keys';
  $handler->display->display_options['arguments']['keys']['table'] = 'search_index';
  $handler->display->display_options['arguments']['keys']['field'] = 'keys';
  $handler->display->display_options['arguments']['keys']['default_action'] = 'default';
  $handler->display->display_options['arguments']['keys']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['keys']['default_argument_options']['argument'] = 'Enter your search terms';
  $handler->display->display_options['arguments']['keys']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['keys']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['keys']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['path'] = 'search/node/%';
  $export['bulletin_board'] = $view;

  return $export;
}
