<?php

/**
 * @file
 * stevens_bulletin_board_view.features.inc
 */

/**
 * Implements hook_views_api().
 */
function stevens_bulletin_board_view_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
