<?php
/**
 * This file holds common GLOBAL setting for all sites
 *
 * $conf['stevens_siteid'] is defined in per site setting.php
 */

# Acquia settings, including database connection strings.
if (file_exists('/var/www/site-php')) {
  require('/var/www/site-php/stevens/' . $conf['stevens_siteid'] . '-settings.inc');
}

# Tell Drupal to use memcached instead of MySQL
# Ensure that cache_form table is stored in non-volatile memory
if (isset($conf['memcache_servers'])) {
  $conf['cache_backends'][] = './sites/all/modules/contrib/memcache/memcache.inc';
  $conf['cache_default_class'] = 'MemCacheDrupal';
  $conf['cache_class_cache_form'] = 'DrupalDatabaseCache';
}

# Avoid database deadlock per https://docs.acquia.com/articles/fixing-database-deadlocks
# Add in stampede protection
$conf['memcache_stampede_protection'] = TRUE;
# Move semaphore out of the database and into memory for performance purposes
$conf['lock_inc'] = './sites/all/modules/contrib/memcache/memcache-lock.inc';

# Override domain detection in Acquia Purge: hardcode the incoming domain.
# (see sites/all/modules/contrib/acquia_purge/DOMAINS.txt)
if (isset($_SERVER['HTTP_HOST']) && (!empty($_SERVER['HTTP_HOST']))) {
  $conf['acquia_purge_domains'] = array($_SERVER['HTTP_HOST']);
}

# Disable http and enable https for Acquia Purge, since we enforce https.
# See https://docs.acquia.com/articles/configuring-acquia-purge
$conf['acquia_purge_http'] = FALSE;
$conf['acquia_purge_https'] = TRUE;

# Logging Client IP in Apache Behind CloudFlare
# See https://docs.acquia.com/article/logging-client-ip-apache-behind-cloudflare
if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
  $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
}

# Media paths settings
$conf['file_public_path'] = 'sites/' . $conf['stevens_siteid'] . '/files';
if (isset($_ENV["AH_SITE_ENVIRONMENT"])) {
  $conf['file_private_path'] = '/mnt/files/stevens.' . $_ENV["AH_SITE_ENVIRONMENT"] . '/sites/' . $conf['stevens_siteid'] . '/files-private';
}

# Webform results temp file - use network volume - see Acquia ticket 323289
if (isset($_ENV['AH_SITE_ENVIRONMENT'])) {
  $conf['webform_export_path'] = "/mnt/gfs/{$_ENV['AH_SITE_GROUP']}.{$_ENV['AH_SITE_ENVIRONMENT']}/tmp/";
}

# Plupload fix: use a tmp location shared among the multiple origin servers
# See https://docs.acquia.com/articles/correcting-broken-uploads-acquia-cloud-enterprise
if (isset($_ENV['AH_SITE_ENVIRONMENT'])) {
  $conf['plupload_temporary_uri'] = "/mnt/gfs/{$_ENV['AH_SITE_GROUP']}.{$_ENV['AH_SITE_ENVIRONMENT']}/tmp";
}

# Image style generation fix. Increase memory limit for large images.
# Note: Bootstrap level isn't high enough for
# file_stream_wrapper_get_instance_by_scheme('public')->getDirectoryPath();
if (strpos($_GET['q'], 'sites/' . $conf['stevens_siteid'] . '/files/styles/') === 0) {
  ini_set('memory_limit', '512M');
}

# Shield module credentials
$conf['shield_allow_cli'] = TRUE;
$conf['shield_user'] = 'dev';
$conf['shield_pass'] = 'Stevens';
$conf['shield_print'] = 'Restricted Environment';

# Syslog identity - distinguish site in Syslog
$conf['syslog_identity'] = 'stevens';

# Set session cookie lifetime. The value 0 means "until the browser is closed".
ini_set('session.cookie_lifetime', 0);

# Per environment settings

if (!isset($_ENV["AH_SITE_ENVIRONMENT"]) || $_ENV["AH_SITE_ENVIRONMENT"] != 'prod') {
  require('./sites/nonprod.settings.php');
}

if (isset($_ENV["AH_SITE_ENVIRONMENT"])) {
  switch ($_ENV["AH_SITE_ENVIRONMENT"]) {
    case 'dev':
    case 'dev2':
    case 'dev3':
    case 'dev4':
    case 'ra':
      require('./sites/dev.settings.php');
      break;
    case 'test':
      require('./sites/test.settings.php');
      break;
    case 'sandbox':
      require('./sites/sandbox.settings.php');
      break;
    case 'prod':
      require('./sites/prod.settings.php');
      break;
  }
}
else {
  require('./sites/local.settings.php');
}

# Global local overrides can be found in docroot/sites/global.settings.local.inc
if (file_exists('./sites/global.settings.local.inc')) {
  require('./sites/global.settings.local.inc');
}

# Per-site local overrides can be found in
# docroot/sites/SITENAME/settings.local.inc. Make sure to ignore them in git!
if (file_exists('./sites/' . $conf['stevens_siteid'] . '/settings.local.inc')) {
  require('./sites/' . $conf['stevens_siteid'] . '/settings.local.inc');
}

