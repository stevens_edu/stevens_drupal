<?php

// Plugin definition
$plugin = array(
  'title' => t('SIT Custom Pages Layout'),
  'category' => t('Custom'),
  'icon' => 'sit_custom_pages.png',
  'theme' => 'panels_sit_custom_pages',
  'admin css' => 'sit_custom_pages.css',
  'css' => 'sit_custom_pages.css',
  'regions' => array(
    'top' => t('Top'),
    'left' => t('Left side'),
    'right' => t('Right side'),
    'bottom' => t('Bottom')
  ),
);
