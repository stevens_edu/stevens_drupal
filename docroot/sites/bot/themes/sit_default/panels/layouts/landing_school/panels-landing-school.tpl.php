<?php
/**
 * @file
 * Template for a 3 column panel layout.
 *
 * This template provides a very simple "one column" panel display layout.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   $content['middle']: The only panel in the layout.
 */
//Contextual classes for layout columns
$left_column_classes = '';
$right_column_classes = '';
$right_top_classes = '';
$right_bottom_first_classes = '';
$right_bottom_last_classes = '';

if ($content['right_top'] OR $content['right_bottom_first'] OR $content['right_bottom_last']) {
	$left_column_classes = ' grid-6';
}
else {
	$left_column_classes = ' grid-24';
}

if ($content['left']) {
	$right_column_classes = ' grid-18';
}
else {
	$right_column_classes = ' grid-24';
}

if ($content['right_bottom_last']) {
	if ($content['left']) {
		$right_bottom_first_classes = ' grid-12 alpha';
	}
	else {
		$right_bottom_first_classes = ' grid-18 alpha';
	}
}

if ($content['right_bottom_first']) {
	$right_bottom_last_classes = ' grid-6 omega';
}

?>
<div class="panel-display panel-landing-school clearfix<?php if (!empty($css_id)) { print ' ' . $css_id; } ?>">

	<?php if ($content['top']): ?>
		<div class="panel-top-row">
			<?php print $content['top']; ?>
		</div>
	<?php endif; ?>

	<?php if ($content['left']): ?>
		<div class="panel-column-left<?php print $left_column_classes; ?>">
			<?php print $content['left']; ?>
		</div>
	<?php endif; ?>
	
	<?php if ($content['right_top'] OR $content['right_bottom_first'] OR $content['right_bottom_last']): ?>
		<div class="panel-column-right<?php print $right_column_classes; ?>">

			<?php if ($content['right_top']): ?>
				<div class="panel-right-top<?php print $right_top_classes; ?>">
					<?php print $content['right_top']; ?>
				</div>
			<?php endif; ?>
		
			<?php if ($content['right_bottom_first']): ?>
				<div class="panel-right-bottom-first clear<?php print $right_bottom_first_classes; ?>">
					<div class="panel-inside">
						<?php print $content['right_bottom_first']; ?>
					</div>
				</div>
			<?php endif; ?>
		
			<?php if ($content['right_bottom_last']): ?>
				<div class="panel-right-bottom-last<?php print $right_bottom_last_classes; ?>">
					<?php print $content['right_bottom_last']; ?>
				</div>
			<?php endif; ?>

		</div>
	<?php endif; ?>

	<?php if ($content['bottom']): ?>
		<div class="panel-bottom-row">
			<?php print $content['bottom']; ?>
		</div>
	<?php endif; ?>

</div>