<?php

/**
 * implementation of hook_panels_layouts()
 */
// Plugin definition
$plugin = array(
  'title' => t('School Landing Layout'),
  'category' => t('Custom'),
  'icon' => 'landing_school.png',
  'admin css' => 'landing_school_admin.css',
  'theme' => 'panels_landing_school',
  'regions' => array(
    'top' => t('top'),
    'left' => t('Left Column'),
    'right_top' => t('Right Top'),
    'right_bottom_first' => t('Right Bottom First'),
    'right_bottom_last' => t('Right Bottom Last'),
    'bottom' => t('Bottom'),
  ),
);
