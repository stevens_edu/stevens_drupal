<?php

// Plugin definition
$plugin = array(
  'title' => t('Two column stacked Clean'),
  'category' => t('Custom'),
  'icon' => 'twocol_stacked_clean.png',
  'theme' => 'panels_twocol_stacked_clean',
  'admin css' => 'twocol_stacked_clean_admin.css',
  'css' => 'twocol_stacked_clean.css',
  'regions' => array(
    'top' => t('Top'),
    'left' => t('Left side'),
    'right' => t('Right side'),
    'bottom' => t('Bottom')
  ),
);
