<?php

/**
 * @file field.tpl.php
 * Default template implementation to display the value of a field.
 *
 * This file is not used and is here as a starting point for customization only.
 * @see theme_field()
 *
 * Available variables:
 * - $items: An array of field values. Use render() to output them.
 * - $label: The item label.
 * - $label_hidden: Whether the label display is set to 'hidden'.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - field: The current template type, i.e., "theming hook".
 *   - field-name-[field_name]: The current field name. For example, if the
 *     field name is "field_description" it would result in
 *     "field-name-field-description".
 *   - field-type-[field_type]: The current field type. For example, if the
 *     field type is "text" it would result in "field-type-text".
 *   - field-label-[label_display]: The current label position. For example, if
 *     the label position is "above" it would result in "field-label-above".
 *
 * Other variables:
 * - $element['#object']: The entity to which the field is attached.
 * - $element['#view_mode']: View mode, e.g. 'full', 'teaser'...
 * - $element['#field_name']: The field name.
 * - $element['#field_type']: The field type.
 * - $element['#field_language']: The field language.
 * - $element['#field_translatable']: Whether the field is translatable or not.
 * - $element['#label_display']: Position of label display, inline, above, or
 *   hidden.
 * - $field_name_css: The css-compatible field name.
 * - $field_type_css: The css-compatible field type.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess_field()
 * @see theme_field()
 */
$field_banner_title = field_get_items('node', $element['#object'], 'field_banner_title');
$banner_title =  $field_banner_title[0]['safe_value'];
$field_banner_text = field_get_items('node', $element['#object'], 'field_banner_text'); 
$banner_text =  $field_banner_text[0]['safe_value'];
$field_banner_link = field_get_items('node', $element['#object'], 'field_banner_link'); 
$banner_link =  $field_banner_link[0]['url'];
?>
<?php foreach ($items as $delta => $item): ?>
  <?php
  	if ($field_banner_link) {
  		print l (render($item), $banner_link, array('html' => TRUE)); 
  	} 
  	else {
  		print render($item);
  	}
  ?>
  <?php if ($field_banner_title OR $field_banner_text): ?>
  	<div class="banner-text-title">
		  <?php if ($field_banner_title): ?>
		  	<h2 class="banner-title"><?php print $banner_title; ?></h2>
		  <?php endif; ?>
	  	<?php if ($field_banner_text): ?>
	  		<div class="banner-text"><?php print $banner_text; ?></div>
	  	<?php endif; ?>
	  </div>
  <?php endif; ?>
<?php endforeach; ?>
