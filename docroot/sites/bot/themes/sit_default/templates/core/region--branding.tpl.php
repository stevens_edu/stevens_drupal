<div<?php print $attributes; ?>>
  <div class="logo-img">
    <a href="/sit">
      <img src="<?php print base_path() . path_to_theme() . '/logo.png'; ?>" />
    </a>
  </div>
  <?php print $content; ?>
</div>