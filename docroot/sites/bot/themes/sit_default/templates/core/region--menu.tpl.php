<div<?php print $attributes; ?>>
  <?php if ($main_menu || $secondary_menu): ?>
  <nav class="navigation">
    <?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('id' => 'main-menu', 'class' => array('links', 'inline', 'clearfix', 'main-menu')), 'heading' => array('text' => t('Main menu'),'level' => 'h2','class' => array('element-invisible')))); ?>
    <?php print theme('links__system_secondary_menu', array('links' => $secondary_menu, 'attributes' => array('id' => 'secondary-menu', 'class' => array('links', 'inline', 'clearfix', 'secondary-menu')), 'heading' => array('text' => t('Secondary menu'),'level' => 'h2','class' => array('element-invisible')))); ?>
  </nav>
  <?php endif; ?>
  <?php
    // print $content;
    // Placeholder div looks better than nothing at all.
    echo '<div style="height: 1px;"></div>';
  ?>
</div>
