<?php
/**
 * @file views-view-table--clone-of-bios.tpl.php
 * Template to display bios
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * @ingroup views_templates
 */
?>
<?php foreach($rows as $rowId => $row): ?>
<h1 class="title page-title"><span thmr="thmr_4064">
<?php //echo "<pre>"; var_dump($row); echo "</pre>";?>
  <?php print $row['field_bio_salutation'] . " " . $row['field_bio_firstname'] . " " . $row['field_bio_middlename'] . $row['field_bio_lastname']; ?></span></h1>

  <?php if(isset($row['field_bio_image']) && $row['field_bio_image'] != ""): ?>
  <div class="bio_image">
    <span thmr="thmr_4065"><span thmr="thmr_4066"><span thmr="thmr_4067"><span thmr="thmr_4068"><?php print $row['field_bio_image']; ?></span></span></span></span>
  </div>
  <?php endif; ?>
  
  <?php if(isset($row['field_bio_body']) && $row['field_bio_body'] != ""): ?>
	<h3><span thmr="thmr_4069"><?php print $row['field_bio_body']; ?></span></h3>
  <?php endif; ?>

  <?php if(isset($row['field_shared_role']) && $row['field_shared_role'] != ""): ?>
	  <div class="bio_head">
		<span thmr="thmr_4070"><?php print $row['field_shared_role']; ?></span>
	  </div>
  <?php endif; ?>

  <?php if(isset($row['field_bio_building']) && $row['field_bio_building'] != ""): ?>
  <div class="bio_head">
    <span thmr="thmr_4071"><span class="label-inline"><?php print (isset($row['field_bio_building']) ? "Building:&nbsp;" : ""); ?></span><?php print $row['field_bio_building']; ?></span>
  </div>
  <?php endif; ?>

  <?php if(isset($row['field_bio_room']) && $row['field_bio_room'] != ""): ?>
  <div class="bio_head">
    <span thmr="thmr_4072"><span class="label-inline"><?php print (isset($row['field_bio_room']) ? "Room:&nbsp;" : ""); ?></span>
		<?php print $row['field_bio_room']; ?>
	</span>
  </div>
  <?php endif; ?>

  <?php if(isset($row['field_bio_phone']) && $row['field_bio_phone'] != ""): ?>
	  <div class="bio_head">
		<span thmr="thmr_4073"><span class=
		"label-inline"><?php (isset($row['field_bio_phone']) ? "Phone:&nbsp;" : ""); ?></span><?php print $row['field_bio_phone']; ?></span>
	  </div>
  <?php endif; ?>
	  
  <?php if(isset($row['field_bio_fax']) && $row['field_bio_fax'] != ""): ?>
	  <div class="bio_head">
		<span thmr="thmr_4074"><span class=
		"label-inline"><?php (isset($row['field_bio_fax']) ? "Fax:&nbsp;" : ""); ?></span><?php print $row['field_bio_fax']; ?></span>
	  </div>
  <?php endif; ?>
	  
  <?php if(isset($row['field_bio_email']) && $row['field_bio_email'] != ""): ?>
	  <div class="bio_head">
		<span thmr="thmr_4075"><span class=
		"label-inline"><?php (isset($row['field_bio_email']) ? "Email:&nbsp" : ""); ?></span><?php print $row['field_bio_email']; ?></span>
	  </div>
  <?php endif; ?>

  <?php if(isset($row['field_bio_appt_thesis_suprv']) && $row['field_bio_appt_thesis_suprv'] != ""): ?>
	  <h3 class="label-above"><span thmr="thmr_4078">Appointments Thesis Supervisor</span></h3>
	  <?php print $row['field_bio_appt_thesis_suprv']; ?>
  <?php endif; ?>
  
  <?php if(isset($row['field_bio_education']) && $row['field_bio_education'] != ""): ?>
  <div>
    <h3 class="label-above"><span thmr="thmr_4076">Education: &nbsp;</span></h3>
	<?php print $row['field_bio_education']; ?>
  </div>
  <?php endif; ?>
  
  <?php if(isset($row['field_bio_experience']) && $row['field_bio_experience'] != ""): ?>
  <div>
    <h3 class="label-above"><span thmr="thmr_4076">Experience: &nbsp;</span></h3>
	<?php print $row['field_bio_experience']; ?>
  </div>
  <?php endif; ?>

  <?php if(isset($row['field_bio_research_blurb']) && $row['field_bio_research_blurb'] != ""): ?>
  <div>
    <h3 class="label-above"><span thmr="thmr_4077">Research:&nbsp;</span></h3>
	<?php print $row['field_bio_research_blurb']; ?>
  </div>
  <?php endif; ?>	  
	  
  <?php if(isset($row['field_bio_pro_actserv']) && $row['field_bio_pro_actserv'] != ""): ?>
	  <div>
		<h3 class="label-above"><span thmr="thmr_4079">Professional Activities/Services</span></h3>
			<?php print $row['field_bio_pro_actserv']; ?>
		</ul>
	  </div>
	<?php endif; ?>

<?php if(isset($row['field_bio_research_interests']) && $row['field_bio_research_interests'] != ""): ?>	
  <div>
    <h3 class="label-above"><span thmr="thmr_4080">Research Projects Interests</span></h3>
	<?php print $row['field_bio_research_interests']; ?>
  </div>
<?php endif; ?>

<?php if(isset($row['field_bio_awards']) && $row['field_bio_awards'] != ""): ?>	
  <div>
    <h3 class="label-above"><span thmr="thmr_4081">Awards and Honors</span></h3>
	<?php print $row['field_bio_awards']; ?>
  </div>
<?php endif; ?>

<?php if(isset($row['field_bio_pro_orgs']) && $row['field_bio_pro_orgs'] != ""): ?>  
  <div>
    <h3 class="label-above"><span thmr="thmr_4082">Professional Organizations and Societies</span></h3>
	<?php print $row['field_bio_pro_orgs']; ?>
  </div>
 <?php endif; ?>
 
 <?php if(isset($row['field_bio_patents']) && $row['field_bio_patents'] != ""): ?>  
  <div>
    <h3 class="label-above"><span thmr="thmr_4082">Patents/Inventions</span></h3>
	<?php print $row['field_bio_patents']; ?>
  </div>
 <?php endif; ?>
 
 <?php if(isset($row['field_bio_funding']) && $row['field_bio_funding'] != ""): ?>  
  <div>
    <h3 class="label-above"><span thmr="thmr_4082">Grants/Contracts/Funds</span></h3>
	<?php print $row['field_bio_funding']; ?>
  </div>
 <?php endif; ?>

 <?php if(isset($row['field_bio_acad_actserv']) && $row['field_bio_acad_actserv'] != ""): ?>  
  <div>
    <h3 class="label-above"><span thmr="thmr_4083">Academic Activities/Services</span></h3>
	<?php print $row['field_bio_acad_actserv']; ?>
  </div>
 <?php endif; ?>
 
 <?php print views_embed_view('bio_publications','publications', $row['nid']);?>
 
 <?php 
    $courses = views_embed_view('courses','bio_courses', $row['nid']);
	if(trim($courses) != ""):
 ?>
	 <div>
		  <h3 class="label-above"><span thmr="thmr_4082">Courses</span></h3>
		  <?php print $courses;?> 
	 </div>
 <?php endif; ?>

 <?php endforeach; ?>
<!--  end -->
<?php // echo "<pre>";var_dump($rows);echo "</pre>"; ?>
