<?php

/**
 * @file
 * Display Suite example layout configuration.
 */

function ds_grid_default() {
  return array(
    'label' => t('SIT Grid Default Layout'),
    'regions' => array(
      'header' => t('Header'),
      'top_left' => t('Top Left'),
      'top_right' => t('Top Right'),
      'bottom_left' => t('Bottom Left'),
      'bottom_right' => t('Bottom Right'),
      'footer' => t('Footer'),
    ),
  );
}
