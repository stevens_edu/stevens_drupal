<?php

/**
 * @file
 * Display Suite example layout configuration.
 */

function ds_2col_default() {
  return array(
    'label' => t('SIT Two Columns Default'),
    'regions' => array(
      'header' => t('Header'),
      'left' => t('Left'),
      'right' => t('Right'),
      'footer' => t('Footer'),
    ),
  );
}
