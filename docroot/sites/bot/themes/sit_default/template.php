<?php

/**
 * @file
 * This file is empty by default because the base theme chain (Alpha & Omega) provides
 * all the basic functionality. However, in case you wish to customize the output that Drupal
 * generates through Alpha & Omega this file is a good place to do so.
 *
 * Alpha comes with a neat solution for keeping this file as clean as possible while the code
 * for your subtheme grows. Please read the README.txt in the /preprocess and /process subfolders
 * for more information on this topic.
 */
function sit_default_preprocess_html(&$vars) {
        // START Facebook comment admins
                //michael forbes, christopher robinson, laura bubeck
        $fbAdminIDs = '1329600025,1422454891,19800933';
        $fbAdmins = array(
                '#tag' => 'meta',
                '#attributes' => array(
                        'property' => 'fb:admins',
                        'content' => $fbAdminIDs,
                ),
        );
        drupal_add_html_head($fbAdmins, 'fbAdmins');
	// END Facebook comment admins

 	//Add class for non IE 7 browser - REMOVED DUE TO CACHE PERSISTENCE
//	$browser = browserclass_get_classes(BROWSERCLASS_BROWSER);
//	if (isset($browser[1]) && $browser[1] != 'ie7') {
		$vars['attributes_array']['class'][] = 'not-ie7';
//	}
	
	//Add browser class common to ie7 and 8
//	if (isset($browser[1]) && $browser[1] == 'ie7' OR $browser[1] == 'ie8') {
//		$vars['attributes_array']['class'][] = 'ie7-and-8';
//	}
	
	if (array_key_exists('sidebar_first', $vars['page']['content']['content'])) {
		//drupal_set_message('there is a first sidebar');
		$vars['attributes_array']['class'][] = 'region-sidebar-first-active';
	}
	else {
		$vars['attributes_array']['class'][] = 'region-sidebar-first-inactive';
	}
		
	//Add body class unique to each website
  $theme_key = variable_get('sit_themekey');
  if ($theme_key) {
    $vars['attributes_array']['class'][] = 'site-' . $theme_key;
  }
  if ($theme_key != 'sit') {
  	$vars['attributes_array']['class'][] = 'not-sit';
  }
 // if ($theme_key == 'hub' OR $theme_key == 'how' OR $theme_key == 'sse' OR $theme_key == 'ses' OR $theme_key == 'cal' OR $theme_key == 'prv' OR $theme_key == 'dof') {
  if ($theme_key != 'sit') {
  	$vars['attributes_array']['class'][] = 'sit-school';
    if ($theme_key != 'prv') {
      $vars['attributes_array']['class'][] = 'full-width-home';
    }
  }
  else {
  	$vars['attributes_array']['class'][] = 'not-sit-school';
  }
  if (!$vars['is_front']) {
		// Add unique classes for each page and website section
		$path = drupal_get_path_alias($_GET['q']);
    if (strpos($path, '/') !== FALSE) {
		  list($section, $sub_section) = explode('/', $path, 3);
    }
    else {
      $section = $path;
      $sub_section = '';
    }
		$vars['attributes_array']['class'][] = 'section-'. $section;
		$vars['attributes_array']['class'][] = 'sub-section-'. ($sub_section ? $sub_section : 'none');
		if (arg(0) == 'node') {
			if (arg(1) == 'add') {
				if ($section == 'node') {
					array_pop($vars['attributes_array']['class']); // Remove 'section-node'
					array_pop($vars['attributes_array']['class']); // Remove 'section-node'
				}
				$vars['attributes_array']['class'] = 'section-node-add'; // Add 'section-node-add'
			}
			elseif (is_numeric(arg(1)) && (arg(2) == 'edit' || arg(2) == 'delete')) {
				if ($section == 'node') {
					array_pop($vars['attributes_array']['class']); // Remove 'section-node'
					array_pop($vars['attributes_array']['class']); // Remove 'section-node'
				}
				$vars['attributes_array']['class'][] = 'section-node-'. arg(2); // Add 'section-node-edit' or 'section-node-delete'
			}
		}
	}
	
	if (arg(0) == 'node' && is_numeric(arg(1)) && !arg(2)) {
		$node = node_load(arg(1));
		$hide_title = field_get_items('node', $node, 'field_title_display');
		if ($hide_title[0]['value'] == 1) {
			$vars['attributes_array']['class'][] = 'hide-page-title'; 
		}
	}
}

function sit_default_preprocess_node(&$vars) {
  //Determine amount of columns on body field
  if (array_key_exists('field_body_columns', $vars)) {
    $columns = field_get_items('node', $vars['node'], 'field_body_columns');
    $vars['classes_array'][] = drupal_html_class($columns[0]['value']);
  }
  
  //Create variables for publications tpl
  if (!strcmp('publication', $vars['type'])) {
//if(0){  	
  	//Determine Publication Author(s)
  	$vars['pub_name'] = '';
  	if (empty($vars['field_authors'])) {
  		$bio = FALSE;
	  	$bio_values = field_get_items('node', $vars['node'], 'field_bio_reference');
	  	if (!empty($bio_values[0]['nid'])) {
	  		$bio = node_load($bio_values[0]['nid']);
	  	}
	  	if ($bio) {
	  	  $bio_first = field_get_items('node', $bio, 'field_bio_firstname');
	  	  $bio_last = field_get_items('node', $bio, 'field_bio_lastname');
	  	  $vars['pub_authors'] = $bio_first[0]['value'] . ' ' . $bio_last[0]['value'];
	  	}
  	}
  	elseif(!empty($vars['field_authors'])) {
	  	$author = field_get_items('node', $vars['node'], 'field_authors');
	  	$vars['pub_authors'] = $author[0]['value'];
  	}
//}//0  	
  	//Determine Publication Month
  	if (!empty($vars['field_month'])) {
			$month = field_get_items('node', $vars['node'], 'field_month');
			$vars['pub_month'] = format_date($month[0]['value'], 'custom', 'M');
  	}
  	else {
  		$vars['pub_month'] = '';
  	}
  		  
  	//Determine Publication Year
  	if (!empty($vars['field_year'])) {
			$year = field_get_items('node', $vars['node'], 'field_year');
			$vars['pub_year'] = $year[0]['value'];
  	}
  	else {
  		$vars['pub_year'] = '';
  	}
  	
  	//Determine node title
  	if (!empty($vars['field_book_title'])) {
			$book_title = field_get_items('node', $vars['node'], 'field_book_title');
			$vars['pub_book_title'] = $book_title[0]['value'];
  	}
  	else {
  		$vars['pub_book_title'] = $vars['title'];
  	}

  	//Determine Publication Conference
  	if (!empty($vars['field_conference'])) {
			$conference = field_get_items('node', $vars['node'], 'field_conference');
			$vars['pub_conference'] = $conference[0]['value'];
  	}
  	else {
  		$vars['pub_conference'] = '';
  	}

  	//Determine Publication Venue
  	if (!empty($vars['field_venue'])) {
			$venue = field_get_items('node', $vars['node'], 'field_venue');
			$vars['pub_venue'] = $venue[0]['value'];
  	}
  	else {
  		$vars['pub_venue'] = '';
  	}

  	//Determine Publication editors
  	if (!empty($vars['field_editors'])) {
			$editors = field_get_items('node', $vars['node'], 'field_editors');
			$vars['pub_editors'] = $editors[0]['value'];
  	}
  	else {
  		$vars['pub_editors'] = '';
  	}

  	//Determine Publication publication
  	if (!empty($vars['field_publication'])) {
			$publication = field_get_items('node', $vars['node'], 'field_publication');
			$vars['pub_publication'] = $publication[0]['value'];
  	}
  	else {
  		$vars['pub_publication'] = '';
  	}

  	//Determine Publication publisher
  	if (!empty($vars['field_publisher'])) {
			$publisher = field_get_items('node', $vars['node'], 'field_publisher');
			$vars['pub_publisher'] = $publisher[0]['value'];
  	}
  	else {
  		$vars['pub_publisher'] = '';
  	}

  	//Determine Publication volume
  	if (!empty($vars['field_volume'])) {
			$volume = field_get_items('node', $vars['node'], 'field_volume');
			$vars['pub_volume'] = $volume[0]['value'];
  	}
  	else {
  		$vars['pub_volume'] = '';
  	}

  	//Determine Publication issue
  	if (!empty($vars['field_issue'])) {
			$issue = field_get_items('node', $vars['node'], 'field_issue');
			$vars['pub_issue'] = $issue[0]['value'];
  	}
  	else {
  		$vars['pub_issue'] = '';
  	}

  	//Determine Publication page_number
  	if (!empty($vars['field_page_number'])) {
			$page_number = field_get_items('node', $vars['node'], 'field_page_number');
			$vars['pub_page_number'] = $page_number[0]['value'];
  	}
  	else {
  		$vars['pub_page_number'] = '';
  	}

  	//Determine Publication pub_type
  	if (!empty($vars['field_pub_type'])) {
			$pub_type = field_get_items('node', $vars['node'], 'field_pub_type');
			$vars['pub_type'] = $pub_type[0]['value'];
  	}
  	else {
  		$vars['pub_type'] = '';
  	}

  	//Determine Publication URL
  	if (!empty($vars['field_publication_url'])) {
			$pub_url = field_get_items('node', $vars['node'], 'field_publication_url');
			$vars['pub_url'] = l(t('Download'), $pub_url[0]['url']);
  	}
  	else {
  		$vars['pub_url'] = '';
  	}

  	//Determine Publication URL Size
  	if (!empty($vars['field_publication_url_size'])) {
			$pub_url_size = field_get_items('node', $vars['node'], 'field_publication_url_size');
			$vars['pub_url_size'] = $pub_url_size[0]['value'];
  	}
  	else {
  		$vars['pub_url_size'] = '';
  	}
  }
}

function sit_default_preprocess_block(&$vars) {
  //Manually add secondary menu on non /sit sites
  $vars['secondary_menu'] = FALSE;
  $theme_key = variable_get('sit_themekey');
  if ($theme_key != 'sit' && array_key_exists('block', $vars)) {
    if (isset($vars['block']->bid) && $vars['block']->bid == 'system-main-menu') {
			$vars['secondary_menu'] = TRUE;
	  }
  }
}

/*
 * Remove stupid panels separator div 
 */
function sit_default_panels_default_style_render_region($vars) {
  $output = '';
  $output .= implode('', $vars['panes']);
  return $output;
}

/**
 * Adding Alpha & Omega classes to first and last view rows for 960 layouts
 */
function sit_default_preprocess_views_view_list(&$vars) {
  $vars['classes_array'][0] = $vars['classes_array'][0] . ' alpha';
  $lastkey = array_pop(array_keys($vars['classes_array']));
  $vars['classes_array'][$lastkey] = $vars['classes_array'][$lastkey] . ' omega';
}

function sit_default_preprocess_views_view_unformatted(&$vars) {
  $keys = array_keys($vars['classes_array']);
  $firstkey = array_shift($keys);
  $lastkey = (!empty($keys)) ? array_pop($keys) : $firstkey;

  if (!empty($firstkey)) {
    $vars['classes_array'][$firstkey] .= ' alpha';

    if (empty($lastkey)) {
      $lastkey = $firstkey;
    }

    $vars['classes_array'][$lastkey] .= ' omega';
  }
}

/**
 * Add theme function to fix the menu issues.
 */
function sit_default_menu_link__main_menu(array$vars) {
  $element = $vars['element'];
  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  if ($element['#href'] == '<nolink>') {
    $element['#attributes']['class'][] = _make_class_name($element['#title']);
    $tag = variable_get('special_menu_items_nolink_tag', '<span>');
    $title = strip_tags(l($element['#title'], $element['#href'], $element['#localized_options']));
    $output = special_menu_items_render_menu_item($tag, $title);
    $element['#attributes']['class'][] = 'nolink';
  }
  else {
    $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  }
  // end of menu_css_names code

  return '<li ' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Add theme function to fix the menu issues.
 */
function sit_default_menu_link__menu_secondary(array$vars) {
  $element = $vars['element'];
  $sub_menu = '';
  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }
  if ($element['#href'] == '<nolink>') {
    $element['#attributes']['class'][] = _make_class_name($element['#title']);
    $tag = variable_get('special_menu_items_nolink_tag', '<span>');
    $title = strip_tags(l($element['#title'], $element['#href'], $element['#localized_options']));
    $output = special_menu_items_render_menu_item($tag, $title);
    $element['#attributes']['class'][] = 'nolink';
  }
  else {
    $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  }
  // end of menu_css_names code

  return '<li ' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}


/**
 * Override superfish menu to add class based on menu name
 */
function sit_default_superfish_build($variables) {
  $output = array('content' => '');
  $id = $variables['id'];
  $menu = $variables['menu'];
  $depth = $variables['depth'];
  $trail = $variables['trail'];
  // Keep $sfsettings untouched as we need to pass it to the child menus.
  $settings = $sfsettings = $variables['sfsettings'];
  $megamenu = $settings['megamenu'];
  $total_children = $parent_children = $single_children = 0;
  $i = 1;

  // Reckon the total number of available menu items.
  foreach ($menu as $menu_item) {
    if (!isset($menu_item['link']['hidden']) || $menu_item['link']['hidden'] == 0) {
      $total_children++;
    }
  }

  foreach ($menu as $menu_item) {

    $show_children = $megamenu_wrapper = $megamenu_column = $megamenu_content = FALSE;
    $item_class = $link_options = $link_class = array();
    $mlid = $menu_item['link']['mlid'];
  
    if (!isset($menu_item['link']['hidden']) || $menu_item['link']['hidden'] == 0) {
      $item_class[] = ($trail && in_array($mlid, $trail)) ? 'active-trail' : '';

      // Add helper classes to the menu items and hyperlinks.
      $settings['firstlast'] = ($settings['dfirstlast'] == 1 && $total_children == 1) ? 0 : $settings['firstlast'];
      $item_class[] = ($settings['firstlast'] == 1) ? (($i == 1) ? 'first' : (($i == $total_children) ? 'last' : 'middle')) : '';
      $settings['zebra'] = ($settings['dzebra'] == 1 && $total_children == 1) ? 0 : $settings['zebra'];
      $item_class[] = ($settings['zebra'] == 1) ? (($i % 2) ? 'odd' : 'even') : '';
      $item_class[] = ($settings['itemcount'] == 1) ? 'sf-item-' . $i : '';
      $item_class[] = ($settings['itemdepth'] == 1) ? 'sf-depth-' . $menu_item['link']['depth'] : '';
      $link_class[] = ($settings['itemdepth'] == 1) ? 'sf-depth-' . $menu_item['link']['depth'] : '';
      $item_class[] = ($settings['liclass']) ? $settings['liclass'] : '';
      $item_class[] = 'menu-' . drupal_html_class($menu_item['link']['link_title']);
      if (strpos($settings['hlclass'], ' ')) {
        $l = explode(' ', $settings['hlclass']);
        foreach ($l as $c) {
          $link_class[] = $c;
        }
      }
      else {
        $link_class[] = $settings['hlclass'];
      }
      $i++;

      // Add hyperlinks description (title) to their text.
      $show_linkdescription = ($settings['linkdescription'] == 1 && !empty($menu_item['link']['localized_options']['attributes']['title'])) ? TRUE : FALSE;
      if ($show_linkdescription) {
        if (!empty($settings['hldmenus'])) {
          $show_linkdescription = (is_array($settings['hldmenus'])) ? ((in_array($mlid, $settings['hldmenus'])) ? TRUE : FALSE) : (($mlid == $settings['hldmenus']) ? TRUE : FALSE);
        }
        if (!empty($settings['hldexclude'])) {
          $show_linkdescription = (is_array($settings['hldexclude'])) ? ((in_array($mlid, $settings['hldexclude'])) ? FALSE : $show_linkdescription) : (($settings['hldexclude'] == $mlid) ? FALSE : $show_linkdescription);
        }
        if ($show_linkdescription) {
          $menu_item['link']['title'] .= '<span class="sf-description">';
          $menu_item['link']['title'] .= (!empty($menu_item['link']['localized_options']['attributes']['title'])) ? $menu_item['link']['localized_options']['attributes']['title'] : array();
          $menu_item['link']['title'] .= '</span>';
          $link_options['html'] = TRUE;
        }
      }

      // Add custom HTML codes around the menu items.
      if ($sfsettings['wrapul'] && strpos($sfsettings['wrapul'], ',') !== FALSE) {
        $wul = explode(',', $sfsettings['wrapul']);
        // In case you just wanted to add something after the element.
        if (drupal_substr($sfsettings['wrapul'], 0) == ',') {
          array_unshift($wul, '');
        }
      }
      else {
        $wul = array();
      }

      // Add custom HTML codes around the hyperlinks.
      if ($settings['wraphl'] && strpos($settings['wraphl'], ',') !== FALSE) {
        $whl = explode(',', $settings['wraphl']);
        // The same as above
        if (drupal_substr($settings['wraphl'], 0) == ',') {
          array_unshift($whl, '');
        }
      }
      else {
        $whl = array();
      }

      // Add custom HTML codes around the hyperlinks text.
      if ($settings['wraphlt'] && strpos($settings['wraphlt'], ',') !== FALSE) {
        $whlt = explode(',', $settings['wraphlt']);
        // The same as above
        if (drupal_substr($settings['wraphlt'], 0) == ',') {
          array_unshift($whlt, '');
        }
        $menu_item['link']['title'] = $whlt[0] . check_plain($menu_item['link']['title']) . $whlt[1];
        $link_options['html'] = TRUE;
      }


      if (!empty($menu_item['link']['has_children']) && !empty($menu_item['below']) && $depth != 0) {
        // Megamenu is still beta, there is a good chance much of this will be changed.
        if (!empty($settings['megamenu_exclude'])) {
          if (is_array($settings['megamenu_exclude'])) {
            $megamenu = (in_array($mlid, $settings['megamenu_exclude'])) ? 0 : $megamenu;
          }
          else {
            $megamenu = ($settings['megamenu_exclude'] == $mlid) ? 0 : $megamenu;
          }
          // Send the result to the sub-menu.
          $sfsettings['megamenu'] = $megamenu;
        }
        if ($megamenu == 1) {
          $megamenu_wrapper = ($menu_item['link']['depth'] == $settings['megamenu_depth']) ? TRUE : FALSE;
          $megamenu_column = ($menu_item['link']['depth'] == $settings['megamenu_depth'] + 1) ? TRUE : FALSE;
          $megamenu_content = ($menu_item['link']['depth'] >= $settings['megamenu_depth'] && $menu_item['link']['depth'] <= $settings['megamenu_levels']) ? TRUE : FALSE;
        }
        // Render the sub-menu.
        $var = array(
          'id' => $id,
          'menu' => $menu_item['below'],
          'depth' => $depth, 'trail' => $trail,
          'sfsettings' => $sfsettings
        );
        $children = theme('superfish_build', $var);
        // Check to see whether it should be displayed.
        $show_children = (($menu_item['link']['depth'] <= $depth || $depth == -1) && $children['content']) ? TRUE : FALSE;
        if ($show_children) {
          // Add item counter classes.
          if ($settings['itemcounter']) {
            $item_class[] = 'sf-total-children-' . $children['total_children'];
            $item_class[] = 'sf-parent-children-' . $children['parent_children'];
            $item_class[] = 'sf-single-children-' . $children['single_children'];
          }
          // More helper classes.
          $item_class[] = ($megamenu_column) ? 'sf-megamenu-column' : '';
          $item_class[] = $link_class[] = 'menuparent';
        }
        $parent_children++;
      }
      else {
        $item_class[] = 'sf-no-children';
        $single_children++;
      }

      $item_class = implode(' ', array_filter($item_class));
      
      if (isset($menu_item['link']['localized_options']['attributes']['class'])) {
        $link_class_current = $menu_item['link']['localized_options']['attributes']['class'];
        $link_class = array_merge($link_class_current, array_filter($link_class));
      }
      $menu_item['link']['localized_options']['attributes']['class'] = $link_class;
 
      $link_options['attributes'] = $menu_item['link']['localized_options']['attributes'];
      
      // Render the menu item.
      $output['content'] .= '<li id="menu-' . $mlid . '-' . $id . '"';
      $output['content'] .= ($item_class) ? ' class="' . trim($item_class) . '">' : '>';
      $output['content'] .= ($megamenu_column) ? '<div class="sf-megamenu-column">' : '';
      $output['content'] .= isset($whl[0]) ? $whl[0] : '';
      $output['content'] .= l($menu_item['link']['title'], $menu_item['link']['link_path'], $link_options);
      $output['content'] .= isset($whl[1]) ? $whl[1] : '';
      $output['content'] .= ($megamenu_wrapper) ? '<ul class="sf-megamenu"><li class="sf-megamenu-wrapper ' . $item_class . '">' : '';
      $output['content'] .= ($show_children) ? (isset($wul[0]) ? $wul[0] : '') : '';
      $output['content'] .= ($show_children) ? (($megamenu_content) ? '<ol>' : '<ul>') : '';
      $output['content'] .= ($show_children) ? $children['content'] : '';
      $output['content'] .= ($show_children) ? (($megamenu_content) ? '</ol>' : '</ul>') : '';
      $output['content'] .= ($show_children) ? (isset($wul[1]) ? $wul[1] : '') : '';
      $output['content'] .= ($megamenu_wrapper) ? '</li></ul>' : '';
      $output['content'] .= ($megamenu_column) ? '</div>' : '';
      $output['content'] .= '</li>';
    }
  }
  $output['total_children'] = $total_children;
  $output['parent_children'] = $parent_children;
  $output['single_children'] = $single_children;
  return $output;
}
