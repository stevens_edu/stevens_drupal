<?php
/**
 * @file
 * Implementation of the stevens.edu sanitization procedure.
 */

/**
 * Class defining a sanitization procedure for Stevens.
 */
class SanitizeStevensProcedure extends SanitizeDefaultProcedure {
  /**
   * Conduct sanitization of the current site.
   */
  public function sanitize() {
    drush_log(dt('Removing unpublished nodes.'));
    $unpub_nodes = array();
    $unpub_q = db_query('SELECT n.nid 
      FROM node n 
      WHERE status=0;'); 
    $unpub_res = $unpub_q->fetchAll();
    foreach ($unpub_res as $row) {
      drush_log(dt('  Deleting @nid.', array('@nid' => $row->nid)));
      $unpub_nodes[] = $row->nid;
    }
    node_delete_multiple($unpub_nodes);

    drush_log(dt('Scrubbing GatherContent API key.'));
    variable_del('gathercontent_api_key');

    drush_log(dt('Scrubbing Twitter API key and secret.'));
    variable_del('twitter_consumer_key');
    variable_del('twitter_consumer_secret');

    drush_log(dt('Scrubbing Acquia secrets.'));
    variable_del('ah_network_key');
    variable_del('acquia_key');
    variable_del('acquia_identifier');
    variable_del('acquia_subscription_data');
    variable_del('acquia_search_key');
    variable_del('acquia_search_host');
    variable_del('acquia_search_derived_key_salt');

    drush_log(dt('Truncating search indices and configuration.'));
    variable_del('apachesolr_default_enviornments');
    variable_del('apachesolr_enviornments');
    variable_del('apachesolr_default_search_page');
    variable_del('apachesolr_search_mlt_blocks');

    $this->trunc('search_dataset');
    $this->trunc('search_index');
    $this->trunc('search_node_links');
    $this->trunc('search_total');
    $this->trunc('apachesolr_environment');
    $this->trunc('apachesolr_environment_variable');
    $this->trunc('apachesolr_index_bundles');
    $this->trunc('apachesolr_index_entities');
    $this->trunc('apachesolr_index_entities_node');
    $this->trunc('apachesolr_search_page');

    drush_log(dt('Truncating Feeds configuration and metadata.'));
    $this->trunc('feeds_source');
    $this->trunc('feeds_log');

    drush_log(dt('Truncating Twitter account configuration.'));
    $this->trunc('twitter_account');

    drush_log(dt('Deleting private file metadata (excepting that managed by Sanitize).'));
    $result = db_delete('file_managed')
      ->condition('uri', 'private://%', 'LIKE')
      ->condition('uri', 'private://sanitize/%', 'NOT LIKE')
      ->execute();

    parent::sanitize();
  }

  /**
   * Prepare the export SQL before being written to a file.
   *
   * @param resource $handle
   *   A file handle for the temporary SQL file to be written.
   */
  public function prepareExport($handle) {
    parent::prepareExport($handle);
  }

  /**
   * Perform additional cleanup following sanitization.
   */
  public function postSanitize() { 
    parent::postSanitize();
  }
}
