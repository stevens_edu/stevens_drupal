<?php
/**
 * @file
 * Details of news content migration from /news.
 */

abstract class StevensNewsMigration extends DrupalNode7Migration {
  /**
   * Mapping of tids from the old to the new site.
   *
   *   Keys are source tids from admin_unit. Values are destination tids.
   *
   *   It should be noted that while this method is usually considered brittle,
   *   content is frozen on the old site, and has already been staged.
   */
  protected $tid_map_other = array(
    '531' => 271,
    '490' => 241,
    '487' => 256,
    '556' => 261,
    '571' => 266,
    '489' => 226,
    '316' => 281,
    '318' => 226,
    '224' => 221,
    '551' => 361,
    '484' => 366,
    '480' => 416,
    '242' => 371,
    '235' => 211,
    '277' => 256,
    '292' => 376,
    '307' => 251,
    '309' => 381,
    '305' => 361,
    '240' => 386,
    '477' => 391,
    '486' => 391,
    '483' => 396,
    '275' => 386,
    '225' => 401,
    '526' => 206,
    '233' => 271,
    '576' => 411,
    '280' => 426,
  );

  protected $tid_map_schools = array(
    '54' => 76,
    '55' => 86,
    '53' => 81,
    '56' => 91,
    '196' => 91,
    '501' => 91,
  );

  protected $tid_map_departments = array(
    '59' => 196,
    '63' => 191,
    '62' => 201,
    '60' => 176,
    '61' => 166,
    '64' => 181,
    '58' => 186,
    '102' => 196,
    '99' => 196,
    '101' => 196,
    '100' => 196,
    '98' => 196,
    '57' => 171,
    '88' => 186,
    '86' => 186,
    '90' => 186,
    '84' => 186,
    '80' => 186,
    '89' => 186,
    '79' => 186,
  );

  protected $tid_map_programs = array(
    '134' => 286,
    '105' => 291,
    '106' => 296,
    '107' => 301,
    '109' => 306,
    '108' => 311,
    '66' => 316,
    '67' => 321,
    '74' => 171,
    '117' => 326,
    '115' => 331,
    '120' => 336,
    '142' => 341,
    '140' => 346,
    '75' => 356,
    '76' => 351,
    '156' => 511,
    '153' => 516,
    '154' => 521,
    '164' => 521,
    '151' => 526,
    '158' => 531,
    '155' => 536,
    '174' => 541,
    '186' => 541,
    '185' => 541,
    '179' => 546,
    '192' => 541,
    '191' => 546,
    '180' => 541,
    '193' => 541,
    '172' => 551,
    '183' => 551,
    '436' => 556,
    '437' => 556,
    '175' => 561,
    '187' => 561,
    '178' => 566,
    '182' => 571,
    '195' => 571,
    '176' => 576,
    '188' => 576,
    '173' => 581,
    '184' => 581,
    '199' => 586,
    '205' => 591,
    '200' => 596,
    '206' => 601,
    '204' => 606,
    '202' => 611,
    '198' => 616,
  );

  protected $tid_map_research = array(
    '494' => 471,
    '516' => 481,
    '319' => 506,
    '133' => 156,
    '111' => 466,
    '113' => 451,
    '112' => 476,
    '72' => 486,
    '71' => 496,
    '323' => 491,
    '147' => 456,
    '516' => 481,
    '209' => 501,
  );

  protected $tid_map_audiences = array(
    '521' => '1',
  );

  protected $tid_map_categories = array(
    '379' => '46',
    '381' => '56',
    '377' => '51',
    '329' => '56',
    '378' => '71',
  );

  protected $tid_news_link = 352;
}

class StevensNewsItemMigration extends StevensNewsMigration {
  /**
   * Overrides DrupalNode7Migration::__construct().
   */
  public function __construct($arguments) {
    parent::__construct($arguments);

    // Add N:1 mappings (date).
    $this->addFieldMapping('field_news_date', 'pubdate');

    // Add 1:N mappings (taxonomy).
    $this->addFieldMapping('pathauto')->defaultValue(1);
    $this->addFieldMapping('path')->defaultValue('');
    $this->addFieldMapping('field_news_programs', 'programs');
    $this->addFieldMapping('field_news_programs:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_news_departments', 'departments');
    $this->addFieldMapping('field_news_departments:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_news_schools', 'schools');
    $this->addFieldMapping('field_news_schools:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_news_tags', 'tags');
    $this->addFieldMapping('field_news_tags:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_news_audiences', 'audiences');
    $this->addFieldMapping('field_news_audiences:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_news__research', 'research');
    $this->addFieldMapping('field_news__research:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_news_category', 'categories');
    $this->addFieldMapping('field_news_category:source_type')->defaultValue('tid');
  }

  /**
   * Overrides DrupalNode7Migration::prepareRow().
   */
  public function prepareRow($row) {
    // Return FALSE if parent implementation says we should.
    // Note that this removes a row from processing.
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Do not process news stories that are In the News.
    // These are News Links, and belong in a different content type.
    $mr = (!empty($row->field_media_relations)) ? $row->field_media_relations : array();
    if (in_array($this->tid_news_link, $mr)) {
      return FALSE;
    }

    // Remove media tags from body.
    $row->body[0] = preg_replace('/\[\[.*?\]\]/', '', $row->body[0]);

    // Merge date (UNIX timestamp) and time (text) fields in a consistent format.
    $str_date = date("Y-m-d", $row->field_date[0]);
    $str_date .= ' ' . $row->field_time;
    $row->pubdate = (strtotime($str_date) != FALSE) ? $str_date : $row->field_date[0];

    // Map old TIDs to new TIDs.
    $tags = array();
    $depts = array();
    $progs = array();
    $schools = array();
    $audiences = array();
    $research = array();
    $categories = array();

    $old_tids = (!empty($row->field_shared_adminunits)) ? $row->field_shared_adminunits : array();
    $old_category_tids = (!empty($row->field_news_events)) ? $row->field_news_events : array();

    foreach ($old_category_tids as $old_category_tid) {
      if (!empty($this->tid_map_categories[$old_category_tid])) {
        $categories[] = $this->tid_map_categories[$old_category_tid];
      }
    }
    foreach ($old_tids as $old_tid) {
      if (!empty($this->tid_map_other[$old_tid])) {
        $tags[] = $this->tid_map_other[$old_tid];
      }
      if (!empty($this->tid_map_schools[$old_tid])) {
        $schools[] = $this->tid_map_schools[$old_tid];
      }
      if (!empty($this->tid_map_departments[$old_tid])) {
        $depts[] = $this->tid_map_departments[$old_tid];
      }
      if (!empty($this->tid_map_programs[$old_tid])) {
        $progs[] = $this->tid_map_programs[$old_tid];
      }
      if (!empty($this->tid_map_audiences[$old_tid])) {
        $audiences[] = $this->tid_map_audiences[$old_tid];
      }
      if (!empty($this->tid_map_research[$old_tid])) {
        $research[] = $this->tid_map_research[$old_tid];
      }
    }
    $row->research = $research;
    $row->audiences = $audiences;
    $row->programs = $progs;
    $row->departments = $depts;
    $row->schools = $schools;
    $row->tags = $tags;
    $row->categories = $categories;

    return TRUE;
  }
}

class StevensNewsLinkMigration extends StevensNewsMigration {
  /**
   * Overrides DrupalNode7Migration::__construct().
   */
  public function __construct($arguments) {
    parent::__construct($arguments);

    // Add 1:1 mappings.
    $this->addFieldMapping('field_news_link_source', 'field_source');
    $this->addFieldMapping('field_news_link_link', 'field_link');

    // Add N:1 mappings (date).
    $this->addFieldMapping('field_news_link_date', 'pubdate');
  }

  /**
   * Overrides DrupalNode7Migration::prepareRow().
   */
  public function prepareRow($row) {
    // Return FALSE if parent implementation says we should.
    // Note that this removes a row from processing.
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Do not process news stories that are not In the News.
    // These are News Links, and belong in a different content type.
    $mr = (!empty($row->field_media_relations)) ? $row->field_media_relations : array();
    if (!in_array($this->tid_news_link, $mr)) {
      return FALSE;
    }

    // Merge date (UNIX timestamp) and time (text) fields in a consistent format.
    $str_date = date("Y-m-d", $row->field_date[0]);
    $str_date .= ' ' . $row->field_time;
    $row->pubdate = (strtotime($str_date) != FALSE) ? $str_date : $row->field_date[0];

    return TRUE;
  }
}

class StevensNewsEventMigration extends StevensNewsMigration {
  /**
   * Overrides DrupalNode7Migration::__construct().
   */
  public function __construct($arguments) {
    parent::__construct($arguments);

    // Add 1:1 mappings.
    $this->addFieldMapping('field_event_link', 'field_link');
    $this->addFieldMapping('field_event_subtitle', 'field_subtitle');

    // Add N:1 mappings (date).
    $this->addFieldMapping('field_event_start_time', 'field_date');
    $this->addFieldMapping('field_event_end_time', 'field_date:value2');

    // Add 1:N mappings (taxonomy).
    $this->addFieldMapping('field_news_category', 'categories');
    $this->addFieldMapping('field_news_category:source_type')->defaultValue('tid');

    // Add file mappings.
    $this->addFieldMapping('field_event_image', 'field_image')
         ->sourceMigration('StevensNewsFile');
    $this->addFieldMapping('field_event_image:file_class')
         ->defaultValue('MigrateFileFid');
    $this->addFieldMapping('field_event_image:preserve_files')
         ->defaultValue(TRUE);
    $this->addFieldMapping('field_event_image:language')
         ->defaultValue(LANGUAGE_NONE);    
  }

  /**
   * Overrides DrupalNode7Migration::prepareRow().
   */
  public function prepareRow($row) {
    // Return FALSE if parent implementation says we should.
    // Note that this removes a row from processing.
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    if ($row->field_date[0] < 1456790400) {
      return FALSE;
    }

    $old_category_tids = (!empty($row->field_news_events)) ? $row->field_news_events : array();

    foreach ($old_category_tids as $old_category_tid) {
      if (!empty($this->tid_map_categories[$old_category_tid])) {
        $categories[] = $this->tid_map_categories[$old_category_tid];
      }
    }

    $row->categories = $categories;

    return TRUE;
  }
}
