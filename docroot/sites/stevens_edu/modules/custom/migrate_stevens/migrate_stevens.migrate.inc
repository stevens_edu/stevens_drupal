<?php
/**
 * @file
 * Declares our migrations.
 */
    

/**
 * Implements hook_migrate_api().
 */
function migrate_stevens_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'stevens' => array(
        'title' => t('Stevens Migrations'),
      ),
    ),
    'migrations' => array(),
  );

  // News Migrations

  $common_arguments = array(
    'source_connection' => 'news',
    'source_version' => 7,
    'group_name' => 'stevens',
  );

  // File migration for /news.
  $file_arguments = $common_arguments + array(
    'machine_name' => 'StevensNewsFile',
    'description' => t('Import files from News site.'),
    'default_uid' => 1,
    'source_dir' => 'https://www.stevens.edu/news/sites/news/files/',
    'destination_dir' => 'public://legacy_news',
  );
  Migration::registerMigration('DrupalFile7Migration', 
    $file_arguments['machine_name'], $file_arguments);

  // Node migration for news items.
  $news_node_arguments = $common_arguments + array(
    'default_uid' => 1,
    'machine_name' => 'StevensNewsItem',
    'description' => t('Import News nodes to News Items'),
    'source_type' => 'news', 
    'destination_type' => 'news_item',
  );

  // Mapping of term references and other field happens in derived class.
  Migration::registerMigration('StevensNewsItemMigration', 
    $news_node_arguments['machine_name'], $news_node_arguments);

  // Node migration for news items.
  $news_node_arguments = $common_arguments + array(
    'default_uid' => 1,
    'machine_name' => 'StevensNewsLink',
    'description' => t('Import News nodes to News Links'),
    'source_type' => 'news', 
    'destination_type' => 'news_link',
  );

  // Mapping of term references and other field happens in derived class.
  Migration::registerMigration('StevensNewsLinkMigration', 
    $news_node_arguments['machine_name'], $news_node_arguments);

  // Node migration for news events.
  $news_node_arguments = $common_arguments + array(
    'default_uid' => 1,
    'machine_name' => 'StevensNewsEvent',
    'description' => t('Import Event nodes from News to Events'),
    'source_type' => 'event', 
    'destination_type' => 'event',
    'dependencies' => array('StevensNewsFile'),
  );

  // Mapping of term references and other field happens in derived class.
  Migration::registerMigration('StevensNewsEventMigration', 
    $news_node_arguments['machine_name'], $news_node_arguments);

  return $api;
}

