<?php

/**
 * SAML 2.0 remote IdP metadata for SimpleSAMLphp.
 *
 * Remember to remove the IdPs you don't use from this file.
 *
 * See: https://simplesamlphp.org/docs/stable/simplesamlphp-reference-idp-remote
 */

/*
 * Stevens Okta IdP
 */
$metadata['http://www.okta.com/exk4a34o9Oi5b3R7W696'] = array (
  'entityid' => 'http://www.okta.com/exk4a34o9Oi5b3R7W696',
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' =>
  array (
    0 =>
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://login.stevens.edu/app/siot_stevensedu_1/exk4a34o9Oi5b3R7W696/sso/saml',
    ),
    1 =>
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://login.stevens.edu/app/siot_stevensedu_1/exk4a34o9Oi5b3R7W696/sso/saml',
    ),
  ),
  'keys' =>
  array (
    0 =>
    array (
      'encryption' => false,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => '
MIIDmDCCAoCgAwIBAgIGAXqq6XFYMA0GCSqGSIb3DQEBCwUAMIGMMQswCQYDVQQGEwJVUzETMBEG
A1UECAwKQ2FsaWZvcm5pYTEWMBQGA1UEBwwNU2FuIEZyYW5jaXNjbzENMAsGA1UECgwET2t0YTEU
MBIGA1UECwwLU1NPUHJvdmlkZXIxDTALBgNVBAMMBHNpb3QxHDAaBgkqhkiG9w0BCQEWDWluZm9A
b2t0YS5jb20wHhcNMjEwNzE1MTYwMzIzWhcNMzEwNzE1MTYwNDIzWjCBjDELMAkGA1UEBhMCVVMx
EzARBgNVBAgMCkNhbGlmb3JuaWExFjAUBgNVBAcMDVNhbiBGcmFuY2lzY28xDTALBgNVBAoMBE9r
dGExFDASBgNVBAsMC1NTT1Byb3ZpZGVyMQ0wCwYDVQQDDARzaW90MRwwGgYJKoZIhvcNAQkBFg1p
bmZvQG9rdGEuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoZbCO3qiFqGoP1oL
OOonOODJhs/6jMPsdeFlu+VbUUCIUiTR6UYeMZqDyBmefoGI+siEoO2NZJZTlV7VYOG4CVtZkXSJ
R5zSAHC8iSWYeW91oCZ3goakzAkqRsyUb1UTw0WgY2WE+8bu/5dp6Zm8uqb7Tt2zrxsWOCtcqXNe
uitHVwcCvUm7RUg1s7b+5pstyOij1sufV6Iz+JJWFsQPckSX6ooLEN31dxHzT5zBOqDUi6mAIVKZ
pmrNiH6It6lF1L4MFsrro3JcjKWW+LVwZe8t6MBWSAOfQ4D5iNhMtvfmwL1z2DJ3HvNit/F7dcQo
Zz77PeHl31mZLYjfR+0fSwIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQAqTu4+dJxv56OSZl1IoBNs
aY1qWaqC+YH6K675n1ukHB/PEHIymWGq4XtGggv3OqC5PQhZCjWq2ZgGA05owgqydOH651svOlED
16j0OjiD37rIX3L/Z+bueOYzTkGqqL4i8xAQ9fglVA+2YnojKmodzEceuIhp59RaqfoZwHSKqmVg
4MCNXt7vlcPS8odiwD9vhosjGHoHBQkENR+eOaPBbF3KjMInLYFibUSjBjEI3+12WptzZ/HI3G3n
9uLbL+vBWG2Omrky3c7+XXpEQwIyv1KSCli/0pWhWGlrpK4TRvufsbI1P+8oz2KYXz7YceosD65c
Uw0wOngUzgcz/M2j
          ',
    ),
  ),
);
