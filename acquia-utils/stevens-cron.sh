#!/usr/bin/env bash
export PATH="$PATH:/usr/local/bin"
export PHPRC="/var/www/site-php/${1}/php.ini"
LOG=/mnt/tmp/${1}/cron-$(date +%F).log

if [ -n "${2}" ]; then
URI=${2}
else
URI=${1}.prod.acquia-sites.com
fi
SITEID=${3}

echo -e "*\n**\n***\nCron Started: $(date)\n***\n**\n*" >> ${LOG}
drush --root=/var/www/html/${1}/docroot/${SITEID} --uri=${URI} -vd cron >> ${LOG} 2>&1
echo -e "*\n**\n***\nCron Completed: $(date)\n***\n**\n*\n" >> ${LOG}