<?php

/**
 * @file
 * Custom drush aliases file for all sites.
 *
 * This file defines individual site aliases as well as site lists.
 * Site list aliases can be used to execute a command on all sites on the list.
 *
 * Sites must be defined in the $sites array.
 */

// Sites.
$sites = array(
  'stevens_edu',
  'library',
  'bot',
  'ann',
  'swebb',
);

// Environments.
$envs = array(
  'dev',
  'dev2',
  'dev3',
  'dev4',
  'test',
  'sandbox',
  'prod',
  'ra',
);

// Name of this docroot.
$docroot = 'stevens';

// Build the $aliases array.
foreach ($envs as $env) {
  // Derive subdomain suffix from site environment.
  switch ($env) {
    case 'prod':
      $suffix = '';
      $ssh_suffix = '';
      break;
    case 'test':
      $suffix = 'stage';
      $ssh_suffix = 'stg';
      break;
    default:
      $suffix = $env;
      $ssh_suffix = $env;
  }

  foreach ($sites as $site) {
    // Subdomain starts with the site name, except for stevens_edu.
    $prefix = ($site == 'stevens_edu' ? '' : $site);
    // Delimit with a hyphen when necessary.
    $delim = ($prefix && $suffix ? '-' : '');
    // Build the complete subdomain.
    $sub = $prefix . $delim . $suffix;
    // Use www in lieu of nothing at all.
    if (!$sub) $sub = 'www';

    // Library has an unusual directory.
    $site_dir = ($site == 'library' ? 'library_v2' : $site);

    // Create a regular alias.
    $aliases["$site.$env"] = array(
      'root' => "/var/www/html/$docroot.$env/docroot",
      'ac-site' => $docroot,
      'ac-env' => $env,
      'ac-realm' => 'prod',
      'uri' => "https://$sub.stevens.edu",
      'site' => $docroot,
      'env' => $env,
      'path-aliases' => array (
        '%drush-script' => 'drush8',
        '%site' => "sites/$site_dir/",
      ),
    );

    // Create a livedev alias with regular alias as parent.
    $aliases["$site.$env.livedev"] = array(
      'parent' => "@$site.$env",
      'root' => "/mnt/gfs/$docroot.$env/livedev/docroot",
    );

    // Create an ssh-enabled alias with regular alias as parent. 
    $aliases["$site.$env.ssh"] = array(
      'parent' => "@$site.$env",
      'remote-host' => "$docroot$ssh_suffix.ssh.prod.acquia-sites.com",
      'remote-user' => "$docroot.$env",
    );

    // Append to environment-based, site-based, and combination site lists (group aliases).
    $aliases["all_$env"]['site-list'][] = "@$site.$env";      // To drush all sites, one environment.
    $aliases["all_$site"]['site-list'][] = "@$site.$env.ssh"; // To drush one site, all environments.
    $aliases['all']['site-list'][] = "@$site.$env.ssh";       // To drush all sites, all environments.
  }
}
